/***********************************************************
	Module Name:	inits.c  

	Author:			Bob Halliday, March, 2007   
					bohalliday@aol.com    

	Description:
		Power up initializations for the 2378
		are executed from this module
					  
	Subroutines:	Initializations()	

	Revision History:


*************************************************************/

#define  BODY_INITS

#include <stdio.h>
#include <string.h>
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"  

#include "inits.h"
#include "isr.h"
#include "uart.h"

#include "psi2000.h"
#include "admin.h"
#include "sbus.h"
#include "rtc.h"
#include "database.h"
#include "factory.h"
#include "sio.h"
#include "utils.h"
#include "mprocess.h"
#include "carts.h"
#include "lapage.h"
#include "diags.h"


#ifdef SPECTRALINK
#include "spectralink.h"
#endif


/***********************************************************
	Subroutine:	Initializations()

	Description:
		The first thing that main() does is call this
		routine, which, in turn, calls all of the 
		"Constructors" (Initialization Routines) for
		each module.
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
void Initializations ()
{
	// Clear all of RAM to zeroes
//	Psi2000_ClearRam ();
 	iPauseDebugTimer=0;

	// Initialize the discrete IO
	InitsIoportsInitialize ();

	// Initialize Timer0 and Timer1
	TimersInitialize ();

	// Initialize the 7 interrupts in the system.
	IsrInitialize ();

// --------------  Interrupts are now active ------------------------------ //

	// Copy in the Flash variables 
	DBInit();
	
	// Initialize the rs-232 channels
	UartInitialize ();
	DetermineResetCause();

   	// Activate the Watchdog Timer
#ifdef WATCHDOG
	StartWatchdogTimer ();                           
#endif

   	LAPageInit();                        // Init Local Area Pageing      
 	PetWatchdog();    
  	AdminInit( UART_0 );                 // Init Admin procedures        
   	SBInit();                            // Init Super Bus             
   	RTCInit();                           // Init Real Time Clock   
	PetWatchdog();    
   	MessageProcessInit();                // Init Message Processing      
   	CartsInitialize ();                  // Init Carts Registration
   	DiagsInitialize ();                  // Init selftest diagnostics
   	DiagsEEpromTest ();                  // Diagnostic self-test on the eeprom
 	PetWatchdog();    
  	DiagsRomTest ();                     // Test the ROM memory
   	DiagsLookForErrors ();               // If self test error, this routine will never return
#ifdef SPECTRALINK
	SpectralinkInitialize ();
#endif
   	ACTIVE_LED = 0;                      // Turn Led On
	FlashRamData.cSmartCareDoesFallDetector=FALSE;
 	PetWatchdog();    

	// v5.05 - Tell the factory test that we are a Vector
	FactorySetVector ();
#if 0
	SioSetMessage(UART_2, (uint8*)"Hello\r", strlen("Hello\r"));
#endif   	
	PetWatchdog();    
	CounterOf5MsecTicks=0;
	lSecondsCounter=0;
	iDebugMessageTimer=0;
	cReportingTimer=0;
	iStatusTimer=0;
	iSmartCareIsDownTimer=0;
	iReceiverBeingTested=-1;
	sRFIDBeingTracked[0]=0;
	iBufferErrors=0;
	bDumpRawData=FALSE;
	iUnregisteredSignalsReceivered=0;
	iReceiverBeingTestedResponded=FALSE;
	iTestedGood=0;
	iTestedBad=0;
	iReceiversReset=0;
	bSuperBusSnifferEnabled = FALSE;	 

	//Print the menu on start up	
	//X=0=off, 1=print alarm messages only, 2=print alarm and supervisories
  	SendDebugMessageX ("", FALSE, TRUE);	
	Uart[UART_1].MessageCount=1;
	Uart[UART_1].ReceiveFifo[0]='?';
	ProcessUart1Messages ();
  



//			FlashRamData.cDebugMessageMode=0; 
//			FlashModified = TRUE;




/*************
	Now raise the NIC reset pin
*****************/
	// Write a ONE to P0.24 to take the NIC out of reset
	FIO0SET =   INITS_0_NIC_RESET;
	
 	
/****************************************************
	bh - v4.04
	At power up, the rs-485 bus gets "jiggled" and receiver #1 sometimes doesn't see
	its initial poll.  This is fatal to a system which is trying to determine which
	protocol a receiver is operating under.
	Therefore, to accomodate noise on the bus, we
	 1) Wait here for 400ms.
	 2) Poll the Receiver #1 twice in a row at power up.
*******************************************************/

	// use the poll timer to delay 400ms:
	PetWatchdog ();
	while (PollTimer < 80)
		{
		RTCInit ();
        }
	PetWatchdog ();
    // The 400ms delay has been completed.                  
	PollTimer = 0;

	// Now let's specially poll Receiver #1 as the first of two in a row.
	ucSBReceiverNumber = 1;
	SBusSendPoll ();
				
	SendFacilityCodeToCawms();
/****************************************************
*******************************************************/

	// Continue to support Factory Mode accurately
	if (InFactoryMode)
		{
		Factory2 = TRUE;
		} 
	
//-------------------------------------------------------------   	
 	//PetWatchdog();    
  	
}


/***********************************************************
	Subroutine:	StartWatchdogTimer()

	Description:
		This routine initializes and activates the Watchdog Timer.
		When this routine is called, interrupts are not active.

		This routine must be called before interrupts are activated.
		        
	Inputs:	
		
	Outputs:
		WDMOD
		WDTC
		WDCLKSEL
		WDFEED 
		
	Locals:
		
*************************************************************/
void StartWatchdogTimer ()
{
	// Enable the Watchdog Timer 
	WDMOD |= INITS_WATCHDOG_CONFIG;			//0x03

	//  Set the time period	- watchdog times out in 262.14ms:
	//    (1/4,000,000) * (4 * 262,140) = 262.14ms
	//    (1/4,000,000) * (4 * 4,194,303) = 4.194 sec
	WDTC = INITS_WATCHDOG_TIME_PERIOD_4SEC;		//0x3FFFF

	// The internal RC oscillator (4 MHz) is selected by default to clock the Watchdog
	WDCLKSEL = 0; 

	//	Activate the Watchdog Timer:
	//WDFEED = INITS_WD_1;
	//WDFEED = INITS_WD_2;
}




/***********************************************************
	Subroutine:	InitsIoportsInitializations()

	Description:
		This routine sets up all of the IO ports to either inputs
		or outputs.
		        
	Inputs:	
		
	Outputs: 
		Set 1 for output ports
		Set 0 for input  ports
		
	Locals:
		
*************************************************************/
void InitsIoportsInitialize ()
{
	// Power on all peripherals - retain power to the EMC - External Memory Controller
	PCONP = INITS_POWER_ON_PERIPHERALS;			 
	EMC_CTRL = 0;

	/* Port 0:
	   Bit	Direction	Function
	   31	Input
	   30	Input
	   29	Input		
	   28	Output		rs-485-2 RX Enable	
	   27	Input			
	   26	Output		rs-485 RX Enable		
	   25	Output		rs-485 TX Enable		
	   24	Output		NIC Reset		
	   23	Input		
	   22	Input		
	   21	Input		
	   20	Input		
	   19	Input		
	   18	Input		
	   17	Input		
	   16	Input		UART 1 RXD
	   15	Output		UART 1 TXD
	   14	Input		
	   13	Output		rs-485-2 TX Enable
	   12	Input		
	   11	Input		UART 2 RXD
	   10	Output		UART 2 TXD
	   9	Input		
	   8	Output		Status LED		
	   7	Output		Heartbeat LED		
	   6	Input		
	   5	Input		
	   4	Output		rs-485 xmit/rcv control - EVAL BOARD ONLY!		
	   3	Input		UART 0 RXD
	   2	Output		UART 0 TXD
	   1	Input		UART 3 RXD
	   0	Output		UART 3 TXD
	*/

	FIO0MASK = 0;
	// Enable RxD0 and TxD0, RXD1 and TXD1, RXD2 and TXD2, RXD3 and TXD3
	PINSEL0 = INITS_ENABLE_PINSEL0;
	PINSEL1 = INITS_ENABLE_PINSEL1;
	// Neither pull-ups nor pull-downs are attached
	PINMODE0 = INITS_NO_PULLUPS;
	PINMODE1 = INITS_NO_PULLUPS;

	// start out with all outputs cleared to zero
	// This drives the NIC RESET pin low on P0.24
	FIO0CLR =   INITS_CLEAR_P0_PINS;
	// but the rs-485 xmitter must go high to be activated
	// and the STATUS LED should be DE-activated
	FIO0SET =   INITS_SET_P0_PINS;
	// set the direction register
	FIO0DIR =   INITS_0_OUTPUT_PORTS;						

	/* Port 1:
	   Bit	Direction	Function
	   31	Output		Test Output
	   30	Output		Test Output
	   29	Output		Test Output
	   28	Output		Test Output
	   27	Output		Test Output
	   26	Output		Test Output
	   25	Output		Test Output
	   24	Output		Test Output
	   23	Input		
	   22	Input		
	   21	Input		
	   20	Input		
	   19	Input		
	   18	Input		
	   17	Input		
	   16	Input		
	   15	Input		
	   14	Input		
	   13	Input		
	   12	Input		
	   11	Input		
	   10	Input		
	   9	Input		
	   8	Input		
	   7	Input		
	   6	Input		
	   5	Input		
	   4	Input		
	   3	Input			
	   2	Input		
	   1	Input		
	   0	Input		
	*/

	FIO1MASK = 0;
	// All pins on port 1 are GPIOs.
	PINSEL2 = INITS_ENABLE_PINSEL2;
	// 
	PINSEL3 = INITS_ENABLE_PINSEL3;
	// Neither pull-ups nor pull-downs are attached
	PINMODE2 = INITS_NO_PULLUPS;
	PINMODE3 = INITS_NO_PULLUPS;

	// start out with all outputs set active
	FIO1SET = INITS_1_OUTPUT_PORTS;
	// 
	FIO1DIR = INITS_1_OUTPUT_PORTS;						
  
	/* Port 2:
	   Bit	Direction	Function
	   31   Input	
	   30   Input	
	   29   Input	
	   28   Input	
	   27   Input	
	   26   Input	
	   25   Input	
	   24   Input	
	   23   Input 
	   22	Input		
	   21	Input		
	   20	Input		
	   19	Input		
	   18	Input		
	   17	Input		
	   16	Input		
	   15	Input		
	   14	Input		
	   13	Input		
	   12	Output		Signal to Microchip PIC:  Initiate Firmware Download		
	   11	Input		Pushbutton - test input		
	   10	Input		input for the bootblock		
	   9	Input		
	   8	Input		
	   7	Output		Test Output
	   6	Output		Test Output
	   5	Output		Test Output
	   4	Output		Test Output
	   3	Output		Test Output
	   2	Output		Test Output
	   1	Output		Test Output
	   0	Output		Test Output
	*/

	FIO2MASK = 0;
	// All pins on port 2 are GPIOs.
	PINSEL4 = INITS_ENABLE_PINSEL4;
	// 
	PINSEL5 = INITS_ENABLE_PINSEL5;
	// Disable the ETM interface --- enable the LED outputs 
	PINSEL10 = 0;
	// Neither pull-ups nor pull-downs are attached
	PINMODE4 = INITS_NO_PULLUPS;
	PINMODE5 = INITS_NO_PULLUPS;

	// start out with all outputs active
	FIO2SET = INITS_2_OUTPUT_PORTS;
	// the bottom 8 pins are test outputs
	FIO2DIR = INITS_2_OUTPUT_PORTS;						
  
	/* Port 3:
	   Bit	Direction	Function
	   8 - 31			not available
	   7	I/O			EMC Data 7
	   6	I/O			EMC Data 6
	   5	I/O			EMC Data 5
	   4	I/O			EMC Data 4
	   3	I/O			EMC Data 3
	   2	I/O			EMC Data 2
	   1	I/O			EMC Data 1
	   0	I/O			EMC Data 0
	*/

	FIO3MASK = 0;
	// Configure Port 3 as GPIOs
	PINSEL6 = INITS_ENABLE_PINSEL6;
	PINSEL7 = INITS_ENABLE_PINSEL7;
	// Neither pull-ups nor pull-downs are attached
	PINMODE6 = INITS_NO_PULLUPS;
	PINMODE7 = INITS_NO_PULLUPS;

	FIO3CLR = INITS_CLEAR_P3_PINS;

	// GPIO 0-7 can be inputs or outputs - start out with them configured as inputs
	FIO3DIR = INITS_3_OUTPUT_PORTS;						
  
	/* Port 4:
	   Bit	Direction	Function
	   31   Output		EMC Chip Enable (~CE)
	   30	Output		EMC Address 18
	   29	Output		EMC Address 17
	   28	Output		EMC Address 16
	   27   Input	
	   26   Input	
	   25   Output		EMC Write Enable (~WE)	
	   24   Output		EMC Output Enable (~OE)	
	   23   Input 
	   22	Input		
	   21	Input		
	   20	Input		
	   19	Input		
	   18	Input		
	   17	Input		
	   16	Input		
	   15	Output		EMC Address 15
	   14	Output		EMC Address 14
	   13	Output		EMC Address 13
	   12	Output		EMC Address 12
	   11	Output		EMC Address 11
	   10	Output		EMC Address 10
	   9	Output		EMC Address 9
	   8	Output		EMC Address 8
	   7	Output		EMC Address 7
	   6	Output		EMC Address 6
	   5	Output		EMC Address 5
	   4	Output		EMC Address 4
	   3	Output		EMC Address 3
	   2	Output		EMC Address 2
	   1	Output		EMC Address 1
	   0	Output		EMC Address 0
	*/


	FIO4MASK = 0;
	// Configure Port 4 as GPIOs
	PINSEL8 = INITS_ENABLE_PINSEL8;
	PINSEL9 = INITS_ENABLE_PINSEL9;
	// Activate the pullups for this port.
	PINMODE8 = INITS_PULLUPS_ACTIVE;
	PINMODE9 = INITS_PULLUPS_ACTIVE;
 
	// start out with all outputs cleared to zero, except for ~OE, ~WE and ~CE,
	// which must always be left high.
	FIO4CLR = INITS_CLEAR_P4_PINS;
	FIO4SET = INITS_SET_P4_PINS;
	// GPIOs 0-15, 24-25, and 28-31 are outputs				     
	FIO4DIR = INITS_4_OUTPUT_PORTS;
}

uint8 cRSIR;
void DetermineResetCause(void)
{
   	cRSIR = RSIR;
	RSIR=0x0F;	//Clear the flags

	SendDebugMessageX("",FALSE,TRUE);
	//sprintf (sString,"RSIR = %02X",cRSIR);
	//SendDebugMessageX(sString,FALSE,TRUE);

  	if ((cRSIR&0x01)>0)
		SendDebugMessageX("CAR starting from a Power-On Reset",TRUE,TRUE) ;
	else if ((cRSIR&0x02)>0)
		SendDebugMessageX("CAR starting from an External Reset",TRUE,TRUE) ;
	else if ((cRSIR&0x04)>0)
		SendDebugMessageX("CAR starting from an Watchdog Timer Reset",TRUE,TRUE) ;
	else if ((cRSIR&0x08)>0)
		SendDebugMessageX("CAR starting from an Brown-Out Reset",TRUE,TRUE) ;
	else {SendDebugMessageX("CAR starting from a Program Crash",TRUE,TRUE); iPauseDebugTimer=5;}
}



