/********************************************************
	File	:	wiegand.h
	Title	:	wiegand Drivers Header File
	Author	:	Bob Halliday -- bohalliday@aol.com
	Ported	:	September, 2007

	Description:
		Header file for the WIEGAND driver source file.
		
************************************************************/

#ifndef HEADER_WIEGAND
#define HEADER_WIEGAND

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_WIEGAND
#define SCOPE
#else
#define SCOPE	extern
#endif

/*******************************
*   Section 1:  Equate Definitions: 
*********************************/
// Wiegand status inputs and outputs
// Port 0:
#define WIEGAND_MAG_LOCK_RELAY2_MASK 				0x00100000
#define WIEGAND_BUZZER_CONTROL_MASK					0x00200000
#define WIEGAND_MAG_LOCK_RELAY1_MASK				0x00400000
#define WIEGAND_LED_CONTROL_MASK					0x00080000
#define WIEGAND_DATA_ZERO_MASK						0x00040000
#define WIEGAND_DATA_ONE_MASK						0x00020000
#define WIEGAND_TTL_OUTPUTS		(WIEGAND_DATA_ONE_MASK | WIEGAND_DATA_ZERO_MASK | WIEGAND_LED_CONTROL_MASK)							

// Port 1:
#define WIEGAND_DOOR_STATUS_MASK					0x00100000
#define WIEGAND_LOCK_STATUS_MASK					0x00200000
#define WIEGAND_PAUSE_RESET_MASK					0x00400000
// Port 2:  Rocker Switch inputs - 0-7

#define WIEGAND_NUMBER_OF_SENSORS					15
#define WIEGAND_BASE_SIZE							16
#define WIEGAND_SENSOR_SIZE							16
#define WIEGAND_SERIAL_NUMBER_SIZE					3
#define WIEGAND_END_OF_PHONE_STRING					0x3F
#define WIEGAND_TOP_BIT								0x80000000
#define WIEGAND_BOTTOM_BIT							0x00000001
				 
#define WIEGAND_WAIT_BETWEEN_TRANSACTIONS			(uint32)(10/TIME_BASE)
#define WIEGAND_POLL_TIME							(uint32)(40/TIME_BASE)
#define WIEGAND_WAIT_FOR_RESUME_TIME				(uint32)(15/TIME_BASE)
#define WIEGAND_WAIT_FOR_BUS_TIME					(uint32)(60000/TIME_BASE)
#define WIEGAND_READBACK_DELAY						(uint32)(4000/TIME_BASE)


// Wiegand state machine states:
enum
	{
	WIEGAND_DISABLED,
	WIEGAND_START_WAIT,
	WIEGAND_READ_EEPROM,
	WIEGAND_FINISHED_READING,
	WIEGAND_TERMINATE_CONNECTION
	};


// WiegandInterrupt state machine states:
enum
	{
	WIEGAND_INT_DISABLED,
	WIEGAND_INT_SENDING_DATA,
	WIEGAND_INT_RECEIVING_DATA
	};


// indeces for base operational data
enum
	{
	BASE_OP_SN0,
	BASE_OP_SN1,
	BASE_OP_SN2,
	BASE_OP_DEVICE_TYPE,
	BASE_OP_WIEGAND_CRC,
	BASE_OP_LOW_LIMIT0,
	BASE_OP_LOW_LIMIT1,
	BASE_OP_LOW_LIMIT2,
	BASE_OP_HIGH_LIMIT0,
	BASE_OP_HIGH_LIMIT1,
	BASE_OP_HIGH_LIMIT2,
	BASE_OP_LIMIT_CRC
	};


//---------------------------------------------------------------------




// Variables used in the Wiegand module
typedef struct
{
	uint8	State;									 
	uint8	NumberOfSensorsBackup;								
	uint8	CallbackIntervalDays;
	uint8	Index;
	uint8	ReadbackFlag;
	uint8	SelfTestPeriod;
} WIEGAND_DATA;





/*******************************
	Section 2: RAM Definitions
********************************/

// Local Variables:
#ifdef BODY_WIEGAND

 			 
	
#endif



// Global Variables

SCOPE		WIEGAND_DATA			Wiegand;



/*************************************
*  Section 3:  Function prototypes 
*************************************/
void 	WiegandInitialize (void);
void 	WiegandExecutive (void);
void 	WiegandInterrupt (void);
void 	WiegandAddBit (uint8);

void 	WiegandUpdateSystemVariables (void);
void 	WiegandDeconstructPhoneNumber (uint32);
void 	WiegandReadAllSensorSerialNumbers (uint32);
void 	WiegandTriggerEEpromReadback (void);
void 	WiegandConvertCallbackInterval (uint32 );
void 	WiegandCheckBaseOperationalData (uint32 );
void 	WiegandSetBaseStatus (void);
void    WiegandConvertBcdToAsciiPhoneNumber (uint8 * );
uint8 	WiegandInspectData (uint8 *);



/*******************************
*   Section 4:  Macros 
*********************************/









/*******************************
*   Section 5:  ROM Tables 
*********************************/

/* Data to be transmitted */
#ifdef BODY_WIEGAND



#endif


#undef SCOPE


#endif

/* ======== END OF FILE ======= */
