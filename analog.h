/********************************************************
	File	:	analog.h
	Title	:	analog Drivers Header File
	Author	:	Bob Halliday
	Ported	:	August 2005

	Description:
		Header file for the ANALOG driver source file.
		
************************************************************/

#ifndef HEADER_ANALOG
#define HEADER_ANALOG

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_ANALOG
#define SCOPE
#else
#define SCOPE	extern
#endif

/*******************************
*   Section 1:  Equate Definitions: 
*********************************/
#define ANALOG_NUM_CHANNELS						4				
#define ANALOG_NUM_READINGS						8				
#define ANALOG_READ_CYCLE_TIME					2			// should be 10
#define ANALOG_SET_CONTROL						0x00210147;
#define ANALOG_POWER_DOWN						0x00000147;
#define ANALOG_10_BITS							0x000003FF;

#define ANALOG_DEFAULT_TEMP_LOW					50
#define ANALOG_DEFAULT_TEMP_HIGH				90

#define ANALOG_STATUS_ERRORS					0x78

// ANALOGNT -- ANALOG Interrupt Register
#define ANALOG_INTERRUPT_BIT					0x01
#define ANALOG_CLOCK_INIT						0x30

#define ANALOG_NUMBER_OF_SENSORS				15
#define ANALOG_BASE_SIZE						16
#define ANALOG_SENSOR_SIZE						16
#define ANALOG_SERIAL_NUMBER_SIZE				3
#define ANALOG_END_OF_PHONE_STRING				0x3F
#define ANALOG_AC_PRESENT						40
				 
#define ANALOG_WAIT_FOR_BUS_TIME				(uint32)(60000/TIME_BASE)
#define ANALOG_READBACK_DELAY					(uint32)(20/TIME_BASE)
#define ANALOG_NO_AC_FLASH_TIME					(uint32)(750/TIME_BASE)

#define TEMP_MIN_AD_READING         			32					// was 65 in ST code
#define TEMP_TABLE_SIZE             			174
#define ANALOG_OK_BATTERY_LEVEL					200
#define ANALOG_RESEND_BATTERY_TIME				180					// should be 180
#define ANALOG_AC_REPORT1						8
#define ANALOG_AC_REPORT2						6
#define ANALOG_AC_REPORT_TIME_CHANGE			3
#define ANALOG_TEMP_REPORTING_TIME				60
#define ANALOG_TEMP_HIGH						110
#define ANALOG_TEMP_LOW							40


// Analog state machine states:
enum
	{
	ANALOG_DISABLED,
	ANALOG_START_WAIT,
	ANALOG_READ_CHANNELS,
	ANALOG_FINISHED_READING,
	ANALOG_TERMINATE_CONNECTION
	};

// Battery monitoring state machine states:
enum
	{
	ANALOG_BATTERY_OK,
	ANALOG_BATTERY_BAD,
	ANALOG_BATTERY_WHATEVER
	};


// analog input field indeces
enum
	{
	ANALOG_AC,
	ANALOG_BATTERY,
	ANALOG_TEMPERATURE,
	ANALOG_RSSI
	};



//---------------------------------------------------------------------




// Variables used in the Analog module
typedef struct
{
	uint8	State;									 
	uint8	Count;								
	uint8	Mask;
	uint8	Index;
	uint8	Temperature;
	uint8	OverTempFlag;
	uint8	BatteryState;									 
	uint8	AcReportCycle;									 
	uint8	AcReportTime;									 
	uint16	Results [ANALOG_NUM_CHANNELS];
	uint16	Readings [ANALOG_NUM_CHANNELS];
} ANALOG_DATA;





/*******************************
	Section 2: RAM Definitions
********************************/

// Local Variables:
#ifdef BODY_ANALOG

 			 
	
#endif



// Global Variables
SCOPE		ANALOG_DATA			Analog;



/*************************************
*  Section 3:  Function prototypes 
*************************************/
void 	AnalogInitialize (void);
void 	AnalogExecutive (void);
void 	AnalogReactivateSystem (void);
uint8 	AnalogTakeReadings (void);
void 	AnalogPrintValues (uint8);
void 	AnalogCheckAc (void);
void 	AnalogCheckAcLeds (void);
void 	AnalogConvertToFahrenheit (void);
void 	AnalogCheckBattery (void);



/*******************************
*   Section 4:  Macros 
*********************************/





/*******************************
*   Section 5:  ROM Tables 
*********************************/

/* Data to be transmitted */
#ifdef BODY_ANALOG


const uint8 ToFahrenheit [ TEMP_TABLE_SIZE ] =
{
0,  	 0,   0,  0,    0,   0,   1,   2,   3,   4,
6,     7,   8,  9,   10,  11,  12,  13,  14,  15,
16,   17,  18,  18,  19,  20,  21,  22,  23,  24,
25,   26,  26,  27,  28,  29,  30,  31,  31,  32,
33,   34,  35,  36,  36,  37,  38,  39,  40,  40,
41,   42,  43,  43,  44,  45,  46,  47,  47,  48,
49,   50,  50,  51,  52,  53,  53,  54,  55,  56,
56,   57,  58,  59,  59,  60,  61,  62,  62,  63,
64,   65,  65,  66,  67,  68,  68,  69,  70,  71,
71,   72,  73,  73,  74,  75,  76,  76,  77,  78,
79,   80,  80,  81,  82,  83,  83,  84,  85,  86,
86,   87,  88,  89,  89,  90,  91,  92,  93,  93,
94,   95,  96,  97,  97,  98,  99, 100, 101, 102,
102, 103, 104, 105, 106, 107, 108, 108, 109, 110,
111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
121, 122, 123, 124, 125, 126, 127, 128, 129, 130,
132, 133, 134, 135, 136, 138, 139, 140, 142, 144,
145, 147, 149, 150
};




#else


#endif


#undef SCOPE


#endif

/* ======== END OF FILE ======= */
