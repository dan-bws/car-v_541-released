/***********************************************************
	Module Name:	uart.c    

	Author:			Bob Halliday, June, 2007   
					bohalliday@aol.com    

	Description:	
	This file is holds the uart transmit and receive drivers for
	the 2 uart ports, UART0 and UART1.
					  
	Subroutines:	UartInitialize()	
					UartDisableTransmitterInterrupt ()
					UartEnableTransmitterInterrupt ()
					Uart0Interrupt ()
					Uart1Interrupt ()
					Uart2Interrupt ()
					Uart3Interrupt ()

	Revision History:


*************************************************************/
#define BODY_UART 

#include <stdio.h>
#include <string.h>
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"  
#include "ctype.h"  

#include "admin.h"
#include "database.h"
#include "inits.h"
#include "isr.h"
#include "sio.h"              /* Serial Defines             */
#include "uart.h"
#include "utils.h"
#include "sbus.h"
#include "carts.h"
#include "factory.h"



char cLastCharSentUart1 = 0;
int iCom1TxCharWaiting;

/***********************************************************
	Subroutine:	UartInitialize()

	Description:
		This routine will initialize the 3 Uarts to run at 
		
		UART 0 runs at 19,200 or 115,200 baud, connected to the PC and SmartCare.
		UART 1 runs at 115,200 baud.  It is a spare rs-485 channel but used for debug messages
		UART 2 runs at 9600 baud, connected to WaveWare or Spectralink
		UART 3 runs at 19,200 baud, connected to the Superbus RS-485 channel.
		
		Receiver Interrupts are enabled.

		09/27/07 --- System Speed reduced from 72 to 48 Mhz
     
	Inputs:		

	Outputs:
		UxFCR
		UxLCR
		UxDLL
		UxDLM
		UxIER

	Locals:	
			
*************************************************************/
void UartInitialize ()
{
	// enable fifos and interrupt after 1 char received or sent
	U0FCR = UART_ENABLE_FIFOS;
	U1FCR = UART_ENABLE_FIFOS;
	U2FCR = UART_ENABLE_FIFOS;
	U3FCR = UART_ENABLE_FIFOS;

 	// UART0: Set DLAB and 7 data bits, MARK parity, 2 stop bits 
	U0LCR = UART_CONTROL_0 | UART_ENABLE_DLAB;
	U1LCR = UART_CONTROL_2 | UART_ENABLE_DLAB;
	// UART2: Set DLAB and word length set to 8 bits, 1 stop bit, No parity 
	U2LCR = UART_CONTROL_2 | UART_ENABLE_DLAB;
	// UART3: Set DLAB and word length set to 8 bits, 1 stop bit, Even parity 
	U3LCR = UART_CONTROL_3 | UART_ENABLE_DLAB;

	// 09/27/07 -- pclk has been lowered from 72 to 48 Mhz
	// Set Baud Rate for Uart0 - 19200 baud  (pclk is 48 MHz)
	if (FlashRamData.ucHighSpeedMode)
		U0DLL = UART_115200_BAUD;			// Set to High Speed Mode - 115,200 baud
	else
		U0DLL = UART_DEFAULT_BAUD;			// Set to Low Speed Mode - 19,200 baud
	U0DLM = 0;

	// Set Baud Rate for Uart1 
	U1DLL = UART_115200_BAUD;	    //debug port   
	U1DLM = 0;

	// Set Baud Rate for Uart2 - 19200 baud   (pclk is 24 MHz)
	U2DLL = UART_19200_BAUD;
	U2DLM = 0;

	// Set Baud Rate for Uart3 - 19200 baud	 (pclk is 48 MHz)
	U3DLL = UART_DEFAULT_BAUD;
	U3DLM = 0;

	// Clear DLAB control bit 
	U0LCR = UART_CONTROL_0;
	U1LCR &= ~UART_ENABLE_DLAB;
	U2LCR = UART_CONTROL_2;
	U3LCR = UART_CONTROL_3;

	// enable receive interrupts on all Uarts
	U0IER = UART_RECEIVE_INT;
	U1IER = UART_RECEIVE_INT;
	U2IER = UART_RECEIVE_INT;
	U3IER = UART_RECEIVE_INT;

	Uart[1].MessageCount = 0;
	Uart[1].ReceiveIn =0;
	Uart[1].ReceiveOut =0;
	Uart[1].TransmitIn =0;
	Uart[1].TransmitOut =0;
 	Com0_xmit_out=Com0_xmit_in=0;
	Com1_xmit_out=Com1_xmit_in=0;
 	Uart[UART_0].TransmitActive = FALSE;
	Uart[UART_1].TransmitActive = FALSE;
	iCom1TxCharWaiting=0;
}



//RAV 11-17-2104
extern char sString[];
extern long	lCheckSumErrors,lPacketsSent,lBadResponses,lGoodResponses,lAlertsProcessed,iCartTimeouts,lSecondsCounter,iSmartCareTimeouts;

void ProcessUart1Messages (void)
{
	unsigned int m,s;
	long h;

//	if (EECheckForWriteOperation ())return; // EEprom Write Operation in progress
// 	if (iPauseDebugTimer>0) return;	//If doing a big database download then skip sending debug

	if (Uart[1].MessageCount==0) return;
	iDebugMessageTimer=0;
	iReceiverBeingTested=-1;		//Always end testing when a character is receiver
 	iReceiverBeingTestedResponded=FALSE;
	bSuperBusSnifferEnabled = FALSE;	 
	
	Uart[1].ReceiveFifo[0] = toupper ((int) Uart[1].ReceiveFifo[0]);

  	if ((Uart[1].ReceiveFifo[0]=='?') || (Uart[1].ReceiveFifo[0]==0x0D))
	{
  		SendDebugMessageX ("", FALSE, TRUE);
  		SendDebugMessageX (szVersion, FALSE, FALSE);
		if (FlashRamData.cDebugMessageMode==1) SendDebugMessage (", Debugger ON (print SmartCare Messages only)");
			else if (FlashRamData.cDebugMessageMode==2) SendDebugMessage (", Debugger ON (print alarm signals only)");
				else if (FlashRamData.cDebugMessageMode==3) SendDebugMessage (", Debugger ON (print alarm and supervise RF signals)");
				else if (FlashRamData.cDebugMessageMode==4) SendDebugMessage (", Debugger ON (print alarm and supervise RF signals with raw)");
					else  {SendDebugMessage (", RF Debugger OFF"); FlashRamData.cDebugMessageMode=0;}

		h = (long)lSecondsCounter/3600;
		m = (int)(lSecondsCounter -(h*3600))/60;
		s = (int)(lSecondsCounter - (h*3600) - (m*60));
		sprintf (sString,"   CAR has been running for %02li:%02i:%02i - ",h,m,s);
  		SendDebugMessageX (sString, FALSE, FALSE);
   		if ((cRSIR&0x01)>0)
			SendDebugMessageX("Power-On Reset",FALSE,TRUE) ;
		else if ((cRSIR&0x02)>0)
			SendDebugMessageX("External Reset",FALSE,TRUE) ;
		else if ((cRSIR&0x04)>0)
		{
			SendDebugMessageX("Watchdog Timer Reset",FALSE,TRUE) ;
			FlashRamData.cDebugMessageMode=0; //always stop debufgger if we crashed
			FlashModified = TRUE;
			}
		else if ((cRSIR&0x08)>0)
			SendDebugMessageX("Brown-Out Reset",FALSE,TRUE) ;
		else SendDebugMessageX("Program Crash Reset",FALSE,TRUE) ;

		//0=off, 1=print alarm messages only, 2=print alarm and supervisories
		SendDebugMessageX ("   Type X to disable RF debugging", FALSE, TRUE);
		SendDebugMessageX ("        X1 to print SmartCare Messages only", FALSE, TRUE);
 		//while (Uart [UART_1].TransmitActive) { Uart1Transmitter ();}
		SendDebugMessageX ("        X2 to print alarm RF signals only", FALSE, TRUE);
		SendDebugMessageX ("        X3 to print alarm and supervise RF signals", FALSE, TRUE);
		SendDebugMessageX ("        X4 to print alarm and supervise RF signals with raw data", FALSE, TRUE);
 		while (Uart [UART_1].TransmitActive) { Uart1Transmitter ();}
		SendDebugMessageX ("   Type S to print Superbus stats", FALSE, TRUE);
		SendDebugMessageX ("   Type R to print a receiver list", FALSE, TRUE);
 		SendDebugMessageX ("   Type C* to clear all stats", FALSE, TRUE);
 		SendDebugMessageX ("   Type Z* to enable Superbus Sniffer", FALSE, TRUE);
 		SendDebugMessageX ("   Type F=xxx to set facility code where xxx is code", FALSE, FALSE);
 		sprintf (sString," = %i", FlashRamData.cFacilityCode );
		SendDebugMessageX (sString, FALSE, TRUE);
 		while (Uart [UART_1].TransmitActive) {Uart1Transmitter ();}
		SendDebugMessageX ("   Type Txxx to test a receiver where xxx is receiver number", FALSE, TRUE);
		SendDebugMessageX ("   Type Bxxxxx to track an RFID where xxxxx (5 ls chars) is RFID number in HEX", FALSE, TRUE);
		SendDebugMessageX ("   Type Bxxxxx* to track an RFID with raw data where xxxxx (5 ls chars) is RFID number in HEX", FALSE, FALSE);
		if (sRFIDBeingTracked[0]==0) sprintf (sString, " - Off");
			else sprintf (sString, " - tracking %s",sRFIDBeingTracked);
  	SendDebugMessageX (sString, FALSE, TRUE);
	
		SendDebugMessageX ("   Type A=xxx to set max RSSI for false alarm filter (00 to disable filter)", FALSE, FALSE);
		sprintf (sString," = %i", FlashRamData.cRssiLevelforFalsePacket);
		SendDebugMessageX (sString, FALSE, TRUE);

 		SendDebugMessageX ("   Type K* to reboot the CAR", FALSE, TRUE);
  	SendDebugMessageX ("", FALSE, TRUE);
	}

	if (Uart[1].ReceiveFifo[0]==(unsigned char)'X')
	{
		bDumpRawData=FALSE;
		FlashRamData.cDebugMessageMode = Uart[1].ReceiveFifo[1]-0x30;
		if (FlashRamData.cDebugMessageMode==1) SendDebugMessage ("Debugger ON - print SmartCare messages only");
			else if (FlashRamData.cDebugMessageMode==2) SendDebugMessage ("Debugger ON - print alarm RF data only");
				else if (FlashRamData.cDebugMessageMode==3) SendDebugMessage ("Debugger ON - print alarm and supervise RF data");
					else if (FlashRamData.cDebugMessageMode==4) SendDebugMessage ("Debugger ON - print alarm and supervise RF data with Raw");
						else {SendDebugMessageX ("Debugger Off", FALSE, TRUE); FlashRamData.cDebugMessageMode=0;}
		FlashModified = TRUE;
		sRFIDBeingTracked[0]=0;	//Disable RFID tracking
	}


	if ((Uart[1].ReceiveFifo[0]=='F') && (Uart[1].ReceiveFifo[1]=='='))
	 {
		sString[0]=Uart[1].ReceiveFifo[2];
		sString[1]=Uart[1].ReceiveFifo[3];
		sString[2]=Uart[1].ReceiveFifo[4];
		sString[3]=0;
		if (sString[2]==0x0D) sString[2]=0;
		if (sString[1]==0x0D) sString[1]=0;
		m = AsciiToInt((uint8 *)sString);
		FlashRamData.cFacilityCode = (unsigned char)m;
		SendFacilityCodeToCawms ();
		sprintf (sString, "Setting facility code to %u", m);
		SendDebugMessageX (sString, TRUE, TRUE);
		FlashModified = TRUE;
 }

 	if ((Uart[1].ReceiveFifo[0]=='A') && (Uart[1].ReceiveFifo[1]=='='))
	 {
		sString[0]=Uart[1].ReceiveFifo[2];
		sString[1]=Uart[1].ReceiveFifo[3];
		sString[2]=Uart[1].ReceiveFifo[4];
		sString[3]=0;
		if (sString[2]==0x0D) sString[2]=0;
		if (sString[1]==0x0D) sString[1]=0;
		m = AsciiToInt((uint8 *)sString);
		FlashRamData.cRssiLevelforFalsePacket = (unsigned char)m;
		sprintf (sString, "Setting false alarm RSSI filter to %u", m);
		SendDebugMessageX (sString, TRUE, TRUE);
		FlashModified = TRUE;
 }
 
 
	if ((Uart[1].ReceiveFifo[0]=='Z') && (Uart[1].ReceiveFifo[1]=='*'))
	{
		bSuperBusSnifferEnabled = TRUE;
		sRFIDBeingTracked[0]=0;	//Disable RFID tracking
	}	 

	if ((Uart[1].ReceiveFifo[0]=='K') && (Uart[1].ReceiveFifo[1]=='*'))
	{
		SendDebugMessageX ("Rebooting, please wait...", TRUE, TRUE);
 		while (Uart [UART_1].TransmitActive) { Uart1Transmitter ();}
		while (TRUE);	//reboot!
	}	 

 	if (Uart[1].ReceiveFifo[0]=='R')
	{
		SendReceiverStatsList();   
	}

	if (Uart[1].ReceiveFifo[0]=='S')
	{
		SendDebugStats();
		SendCawmDebugStats();
		SendDatabaseStats ();
		SendReceiverStats();
//		SendPollingStats();
 		SendDebugMessageX ("", FALSE, TRUE);
	}

 	if (Uart[1].ReceiveFifo[0]=='P')
	{
		if (Uart[1].ReceiveFifo[1]=='*')
			memset (CartsPollingList, 0, POLLING_SIZE);
		SendPollingStats();
 		SendDebugMessageX ("", FALSE, TRUE);
	}

  	if (Uart[1].ReceiveFifo[0]=='T')
		{
		sString[0]=Uart[1].ReceiveFifo[1];
		sString[1]=Uart[1].ReceiveFifo[2];
		sString[2]=Uart[1].ReceiveFifo[3];
		sString[3]=0;
		if (sString[2]==0x0D) sString[2]=0;
		if (sString[1]==0x0D) sString[1]=0;
   	  	iReceiverBeingTested = AsciiToInt((uint8 *)sString);
		if ((iReceiverBeingTested>0) && (iReceiverBeingTested<255))
			{
			sRFIDBeingTracked[0]=0;	//shut off RFID tracking if testing a receiver
			bDumpRawData=FALSE;
			iReceiverBeingTestedResponded=FALSE;
   			iTestedBad=0;
			iTestedGood=0;
			sprintf (sString, "Starting test on receiver #%u", iReceiverBeingTested);
 			SendDebugMessageX (sString, TRUE, TRUE);
			CartsAddToFastPollingList(iReceiverBeingTested);
			}
		else iReceiverBeingTested=-1;
		}

  	if (Uart[1].ReceiveFifo[0]=='B')	 //Track an alert device
		{
		h=0;
		bDumpRawData=FALSE;
		while ((Uart[1].ReceiveFifo[1]!=0x0D) && (h<6)) //count digits entered in command
		{
		 if( Uart[1].ReceiveFifo[1]!=0x0D) h++;
		}
		sRFIDBeingTracked[0]='0'; //always zero? Why
		sRFIDBeingTracked[1]=toupper(Uart[1].ReceiveFifo[1]);
		sRFIDBeingTracked[2]=toupper(Uart[1].ReceiveFifo[2]);
		sRFIDBeingTracked[3]=toupper(Uart[1].ReceiveFifo[3]);
		sRFIDBeingTracked[4]=toupper(Uart[1].ReceiveFifo[4]);
		sRFIDBeingTracked[5]=toupper(Uart[1].ReceiveFifo[5]);
		sRFIDBeingTracked[6]=0;
		if (Uart[1].ReceiveFifo[1]==0x0d)sRFIDBeingTracked[0]=0;//turn off tracking

		if (Uart[1].ReceiveFifo[6]=='*') bDumpRawData=TRUE; 

		iReceiverBeingTestedResponded=FALSE;
  		FlashRamData.cDebugMessageMode=0;
		if (sRFIDBeingTracked[0]>0)
			sprintf (sString, "Starting tracking alert device #%s", sRFIDBeingTracked);
				else strcpy (sString, "Tracking Off");
		SendDebugMessageX (sString, TRUE, TRUE);
		}



	if ((Uart[1].ReceiveFifo[0]=='C') && (Uart[1].ReceiveFifo[1]=='*'))
	{
		lCheckSumErrors=0;	
		lPacketsSent=0;		
		lBadResponses=0;	  
		lGoodResponses=0;
		lAlertsProcessed=0;
		lRFPacketsReceived=0;
		lSupersProcessed=0;
		iCartTimeouts=0;
		iSmartCareTimeouts=0;
		iStatusTimer=5;	
		iSmartCareIsDownTimer=600;
		iCpacMessagesSent=0;
		iCpacMessagesReceived=0;
		bDumpRawData=FALSE;
		iBufferErrors=0;
		iReceiversReset=0;
		iFlaseAlarms=0;
		iUnregisteredSignalsReceivered=0;
		sRFIDBeingTracked[0]=0;
		memset (cReceiverType,0,sizeof(cReceiverType));
		memset (iReceiverTimeouts,0,sizeof (iReceiverTimeouts));
  	SendDebugMessageX ("Ok", TRUE, TRUE);
 }
  
	//Empty receive buffer
	Uart[1].MessageCount=0;
 	Uart[1].ReceiveIn =0;
	Uart[1].ReceiveOut =0;
	}


extern unsigned char SavedAlarmsCount;

void SendDebugStats (void)
{
	if (EECheckForWriteOperation ())return; // EEprom Write Operation in progress
	if (iPauseDebugTimer>0) return;	//If doing a big database download then skip sending debug
	iStatusTimer=600;
	sprintf (sString, "Superbus: Sent=%li ",lPacketsSent);
	SendDebugMessageX (sString, TRUE, FALSE);
	sprintf (sString, "Rec=%li ",lGoodResponses);
	SendDebugMessageX (sString, FALSE, FALSE);
	sprintf (sString, "(%0.2f%%)  ",(float)((float)lGoodResponses/(float)lPacketsSent)*100.0);
	SendDebugMessageX (sString, FALSE, FALSE);
	sprintf (sString, "WR TOs=%li  ",iCartTimeouts);
	SendDebugMessageX (sString, FALSE, FALSE);
	sprintf (sString, "Cksum Err=%li  ",lCheckSumErrors);
	SendDebugMessageX (sString, FALSE, FALSE);
	sprintf (sString, "Rejected=%li  ",lBadResponses);
	SendDebugMessageX (sString, FALSE, FALSE);
	sprintf (sString, "SC TOs=%li  ",iSmartCareTimeouts);
	
	PetWatchdog ();
 	while (Uart [UART_1].TransmitActive) {Uart1Transmitter ();}
	SendDebugMessageX (sString, FALSE, FALSE);
	sprintf (sString, "Buf Err=%u  ",iBufferErrors);
	SendDebugMessageX (sString, FALSE, TRUE);

	while (Uart [UART_1].TransmitActive) {Uart1Transmitter ();}

 
	sprintf (sString, "RF Signals: Total=%li  ",lRFPacketsReceived);   
	SendDebugMessageX (sString, TRUE, FALSE);
	sprintf (sString, "Alerts=%li  ",lAlertsProcessed);
	SendDebugMessageX (sString, FALSE, FALSE);
	sprintf (sString, "Sups=%li  ",lSupersProcessed);
	SendDebugMessageX (sString, FALSE, FALSE);
	sprintf (sString, "Unreg Sigs=%u  ",iUnregisteredSignalsReceivered);
	SendDebugMessageX (sString, FALSE, FALSE);
  sprintf (sString, "Alarms Pending=%u  ",SavedAlarmsCount);
	SendDebugMessageX (sString, FALSE, FALSE);  
	sprintf (sString, "False Alarms=%li ",iFlaseAlarms);
	SendDebugMessageX (sString, FALSE, FALSE);
	if (gucstandalone_mode)
	   	SendDebugMessageX ("  SmartCare Down!", FALSE, FALSE);
 	SendDebugMessageX ("", FALSE, TRUE);
}



void SendCawmDebugStats (void)
{
	if (EECheckForWriteOperation ())return; // EEprom Write Operation in progress
	if (iPauseDebugTimer>0) return;	//If doing a big database download then skip sending debug
	sprintf (sString, "CAWM Status:  Messages Sent=%li  ",iCpacMessagesSent);
	SendDebugMessageX (sString, TRUE, FALSE);
	sprintf (sString, "Received=%li ",iCpacMessagesReceived);
	SendDebugMessageX (sString, FALSE, FALSE);

 	SendDebugMessageX ("", FALSE, TRUE);
}





 extern uint16 HighestCarPoint;
 void SendDatabaseStats (void)
{
	if (EECheckForWriteOperation ())return; // EEprom Write Operation in progress
	if (iPauseDebugTimer>0) return;	//If doing a big database download then skip sending debug
	sprintf (sString, "Database Status:  Highest Point=%u  ",HighestCarPoint);
	SendDebugMessageX (sString, TRUE, FALSE);
	sprintf (sString, "Highest RFID=%li ",HighestRfid);
	SendDebugMessageX (sString, FALSE, FALSE);

	if (FlashRamData.ucHighSpeedMode)
		SendDebugMessageX ("Host Port=115K", FALSE, FALSE);
			else SendDebugMessageX ("Host Port=19.2K", FALSE, FALSE);
												
	sprintf (sString, "   Supervisory Time Left=%u min ",SupervisorBigTimer);
	SendDebugMessageX (sString, FALSE, FALSE);

 	SendDebugMessageX ("", FALSE, TRUE);
}




 void SendPollingStats (void)
  {
  	int i;
	SendDebugMessageX ("Polling List=", TRUE, FALSE);
	for (i=0; i<POLLING_SIZE; i++)
	{
	  	sprintf (sString, "%02X",CartsPollingList [i]);
		SendDebugMessageX (sString, FALSE, FALSE);
	  }											
 	SendDebugMessageX ("", FALSE, TRUE);


	SendDebugMessageX ("Protocol List=", TRUE, FALSE);
	for (i=0; i<POLLING_SIZE; i++)
	{
	  	sprintf (sString, "%02X",CartsProtocolList [i]);
		SendDebugMessageX (sString, FALSE, FALSE);
	  }											
 	SendDebugMessageX ("", FALSE, TRUE);
  	while (Uart [UART_1].TransmitActive) {Uart1Transmitter ();}


	SendDebugMessageX ("Reprogram List=", TRUE, FALSE);
	for (i=0; i<POLLING_SIZE; i++)
	{
	  	sprintf (sString, "%02X",CartsReprogramList [i]);
		SendDebugMessageX (sString, FALSE, FALSE);
	  }											
 	SendDebugMessageX ("", FALSE, TRUE);


	SendDebugMessageX ("Missing List=", TRUE, FALSE);
	for (i=0; i<POLLING_SIZE; i++)
	{
	  	sprintf (sString, "%02X",CartsMissingList [i]);
		SendDebugMessageX (sString, FALSE, FALSE);
	  }											
 	SendDebugMessageX ("", FALSE, TRUE);
  }


 


void SendReceiverStats (void)
{
	unsigned char i, iTotal;
	if (EECheckForWriteOperation ())return; // EEprom Write Operation in progress
	if (iPauseDebugTimer>0) return;	//If doing a big database download then skip sending debug
	iStatusTimer=600;
 
 	SendDebugMessageX ("Receivers On Line: ", TRUE, FALSE);
	iTotal=0;
	for (i=0; i<SB_MAX_NEW_DEVICES; i++)
	if ((CartsGetDeviceData(i) && (cReceiverType[i]>0x00)) && (cReceiverType[i]<0xFE))
	{
		sprintf (sString,"%i,", i);
 		SendDebugMessageX (sString, FALSE, FALSE);
		iTotal++;
		}
	while (Uart [UART_1].TransmitActive) { Uart1Transmitter ();}
	sprintf (sString, " (%u responding!)", iTotal);
	SendDebugMessageX (sString, FALSE, FALSE);

	sprintf (sString,"  Receivers Resets=%i", iReceiversReset);
	SendDebugMessageX (sString, FALSE, TRUE);

}


  

 void SendReceiverStatsList (void)
 {
 	unsigned char i, k, ucData;

	if (EECheckForWriteOperation ())return; // EEprom Write Operation in progress
	if (iPauseDebugTimer>0) return;	//If doing a big database download then skip sending debug
	SendDebugMessageX ("Receivers On Line", TRUE, TRUE);

	k=0;
	for (i=0; i<SB_MAX_NEW_DEVICES; i++)
	if (CartsGetDeviceData(i)) 
	{
		sprintf (sString,"   %-3i-", i);

	  	ucData = SBGetDeviceData( i );
 		if( (ucData == SB_DEVICE_INACTIVE ) || (cReceiverType[i]==0xFE))
			{
			strcat (sString, "Offline");
		 	}
		else
		 	{												 //			  "Offline"
			if (cReceiverType[i]==0x01) strcat 					(sString, "CAWM   ");
				else  if (cReceiverType[i]==0x02)strcat 		(sString, "Dual   ");
					else  if (cReceiverType[i]==0x00)strcat 	(sString, "Unknown");
						else  if (cReceiverType[i]==0xFF)strcat (sString, "No Resp");
							else strcat 						(sString, "319 MHz");
			}
		SendDebugMessageX (sString, FALSE, FALSE);
		sprintf (sString, "-%5u timeouts", iReceiverTimeouts[i]);
		k++;
		if (k==3) {SendDebugMessageX (sString, FALSE, TRUE); k=0;}
			else 
				{ 
				SendDebugMessageX (sString, FALSE, FALSE);
				SendDebugMessageX ("    ", FALSE, FALSE);
				}
 		while (Uart [UART_1].TransmitActive) { Uart1Transmitter ();}
		}											
	SendCR_DebugPort ();    
  }


/***********************************************************
	Subroutine:	UartEnableTransmitterInterrupt()

	Description:
		This routine will turn on the Transmitter Interrupt for UARTx
        
	Inputs:
		UartChannel - 0, 1 

	Outputs:
		U0IER
		U1IER 
		U2IER 
		U3IER 

	Locals:	
			
*************************************************************/
void UartEnableTransmitterInterrupt (uint8 UartChannel)
{
	switch( UartChannel )
   		{
		case UART_0 :
			break;
		case UART_1 :
			// Turn off the receiver
			FIO0SET = INITS_RS485_2_RX_ENABLE;
			FIO3SET = PORT3_RS485_2_RX_ENABLE;
			// enable the rs485 transmitter
			FIO0SET = INITS_RS485_2_TX_ENABLE;
			FIO3SET = PORT3_RS485_2_TX_ENABLE;
			break;
		case UART_2 :
//			U2IER |= UART_TRANSMIT_INT;		// v5.17
			break;
		case UART_3 :
			// Turn off the receiver
			FIO0SET = INITS_RS485_RX_ENABLE;
			WEAK_NOP_8();
			// Final product setting - enable the rs485 transmitter
			FIO0SET = INITS_RS485_TX_ENABLE;
			WEAK_NOP_8();
			break;
		}
}



				

				
				


				
/***********************************************************
	Subroutine:	Uart0Interrupt()

	Description:
	This is the UART0 Transmit or Receive Interrupt.
	This routine will either transmit or receive one byte of data to/from SmartCare

	Inputs:
		U0IIR
		U0RBR
		U0LSR
		Uart0ArrayTransmit []
		Uart0TransmitIndex
		Uart0ReceiveIn
		
	Outputs:
		Uart0Receive []
		Uart0ReceiveIn
		Uart0TransmitIndex
		U0THR

	Locals:
		RcvChar
		
*************************************************************/
void Uart0Interrupt ( )
{
	// If more than one character is available, empty out the hardware buffer
	while (U0LSR & UART_STATUS_RCV)
	if (U0LSR & UART_STATUS_RCV)
		{
		Uart0ReceiveCharacter ();
		}
}


/*************************************************************/
void Uart0ReceiveCharacter ( )
{
	uint8 RcvChar;
	uint8 index;

   	// Data is coming from the Host --- increment the counter to show the Host is present
   	ReceiveHostCharCount++;
	// This is a Receiver Interrupt - get receive character
	RcvChar	= U0RBR;
	// ACK characters can be thrown away
	if (RcvChar != ASCII_ACK)
		{
//		if ( !(U0LSR & UART_STATUS_ERROR))  // 11/01/07 -- ignore UART errors
//			{
			// No errors found during receive
			index = Uart [UART_0].ReceiveIn++;
			// Store the received character in the circular queue
			Uart [UART_0].ReceiveFifo [index] = RcvChar;
			// Watch for wrap on Receive in pointer
			Uart [UART_0].ReceiveIn &= UART_FIFO_SIZE_MASK;
//			}
		}
}
				
/***********************************************************
	Subroutine:	Uart1Interrupt()

	Description:
	This is the UART1 Transmit or Receive Interrupt.
	This routine will either transmit or receive one byte of data.

	Inputs:
		U1IIR
		U1RBR
		U1LSR
		Uart1ArrayTransmit []
		Uart1TransmitIndex
		Uart1ReceiveIn
		
	Outputs:
		Uart1Receive []
		Uart1ReceiveIn
		Uart1TransmitIndex
		U1THR

	Locals:
		RcvChar
		
*************************************************************/
void Uart1Interrupt ( )	  //debug port   
{
	uint8 RcvChar;

	// Is this a transmit or receive interrupt?
	if ( U1IIR & UART_INT_STATUS_RCV )
		{
		// This is a Receiver Interrupt - get receive character
		RcvChar	= U1RBR;
		if ( !(U1LSR & UART_STATUS_ERROR))
			{
			// No errors found during receive
			Uart [UART_1].ReceiveFifo [Uart [UART_1].ReceiveIn++] = RcvChar;
			if (RcvChar==0x0D) Uart[1].MessageCount++;
			// Watch for wrap on Receive in pointer
			Uart [UART_1].ReceiveIn &= UART_FIFO_SIZE_MASK;
			}
		}
	else
		{
		// This is a transmit interrupt - transmit next character
		U1THR = cLastCharSentUart1 = Com1_Queue [Com1_xmit_out++];
		iCom1TxCharWaiting--;
		// Watch for wrap on transmit out pointer
		//Com1_xmit_out &= COM_1_QUEUE_MASK;
  		if (Com1_xmit_out>=COM_1_QUEUE_SIZE) Com1_xmit_out=0;

		// Check for end of Transmit Data:
		if (Com1_xmit_out == Com1_xmit_in )
			{
			// all data has been transmitted - terminate interrupts
			U1IER &= ~UART_TRANSMIT_INT;
			Uart[UART_1].TransmitActive = FALSE;
			iCom1TxCharWaiting=0;
			}
		}
}
		
				
/***********************************************************
	Subroutine:	Uart2Interrupt()

	Description:
	This is the UART2 Transmit or Receive Interrupt.
	UART2 is the Backup Paging Channel -- either Waveware or Spectralink.
	This routine will either transmit or receive one byte of data.

	Inputs:
		U2IIR
		U2RBR
		U2LSR
		Uart2ArrayTransmit []
		Uart2TransmitIndex
		Uart2ReceiveIn
		
	Outputs:
		Uart2Receive []
		Uart2ReceiveIn
		Uart2TransmitIndex
		U2THR

	Locals:
		RcvChar
		
*************************************************************/
void Uart2Interrupt ( )
{
	uint8 RcvChar;
	uint8 index;

	// Is this a transmit or receive interrupt?
	if ( U2IIR & UART_INT_STATUS_RCV )
		{
		// This is a Receiver Interrupt - get receive character
		RcvChar	= U2RBR;
		if ( !(U2LSR & UART_STATUS_ERROR))
			{
			// No errors found during receive
			index = Uart [UART_2].ReceiveIn++;
			Uart [UART_2].ReceiveFifo [index] = RcvChar;
			// Watch for wrap on Receive in pointer
			Uart [UART_2].ReceiveIn &= UART_FIFO_SIZE_MASK;
			}
		}
}
		
	
	
	
	
	
	
	
	
	
				
/***********************************************************
	Subroutine:	Uart3Interrupt()

	Description:
	This is the UART3 Transmit or Receive Interrupt for the Superbus rs-485 channel.
	This routine will either transmit or receive one byte of data.

	Inputs:
		U3IIR
		U3RBR
		U3LSR
		Uart3ArrayTransmit []
		Uart3TransmitIndex
		Uart3ReceiveIn
		
	Outputs:
		Uart3Receive []
		Uart3ReceiveIn
		Uart3TransmitIndex
		U3THR

	Locals:
		RcvChar
		
*************************************************************/
void Uart3Interrupt ( )
{
	uint8 RcvChar;
	uint8 index;

	// Is this a transmit or receive interrupt?
	if ( U3IIR & UART_INT_STATUS_RCV )
		{
		// This is a Receiver Interrupt - get receive character(s)
		RcvChar	= U3RBR;
		// Clear the Uart errors by reading the LSR register
		index = (U3LSR & UART_STATUS_ERROR);
		// restart the resync timer
		Rs485ResyncTimer = 0;

		// No errors found during receive
		index = Uart [UART_3].ReceiveIn++;
		Uart [UART_3].ReceiveFifo [index] = RcvChar;
		// Watch for wrap on Receive in pointer
		Uart [UART_3].ReceiveIn &= UART_FIFO_SIZE_MASK;
		}


}
		






/*************************************************************/
				
/***********************************************************
	Subroutine:	Uart0Transmitter()

	Description:
	This is the UART0 Transmit character routine
	This routine will transmit one byte of data to SmartCare
	This routine is called from the foreground.

	Inputs:
		U0LSR
		Com0_Queue []
		Com0_xmit_out
		Com0_xmit_in
		
	Outputs:
		Com0_Queue []
		Com0_xmit_out
		U0THR

	Locals:

*************************************************************/
void Uart0Transmitter ( )
{
// Is it time to transmit another rs-232 character?
if ( Uart[UART_0].TransmitActive )
	{
	// Is it time to transmit another rs-232 character?
	if (( U0LSR & UART_TRANSMITTER_EMPTY ) && ( U0LSR & UART_STATUS_TEMT ))
		{
		// The transmitter is active and empty - transmit next character
		U0THR = Com0_Queue [Com0_xmit_out++];
		// Watch for wrap on transmit out pointer
		Com0_xmit_out &= COM_0_QUEUE_MASK;

		// Check for end of Transmit Data:
		if (Com0_xmit_out == Com0_xmit_in )
			{
			// all data has been transmitted - terminate transmission mode
			Uart[UART_0].TransmitActive = FALSE;
			}
		}
	}
}


 /*************************************************************/
void Uart1Transmitter ( )
{

	// Is it time to transmit another rs-485 character?
		PetWatchdog ();
		if (( U1LSR & UART_TRANSMITTER_EMPTY ) && ( Uart[UART_1].TransmitActive ))
		{
		// YES - transmit next rs-485 character
		U1THR = cLastCharSentUart1 = Com1_Queue [Com1_xmit_out++];
		iCom1TxCharWaiting--;
		// Watch for wrap on transmit out pointer
		//Com1_xmit_out &= COM_1_QUEUE_MASK;
		if (Com1_xmit_out>=COM_1_QUEUE_SIZE) Com1_xmit_out=0;

		// Check for end of Transmit Data:
		if (Com1_xmit_out == Com1_xmit_in )
			{
			// all data has been transmitted - terminate transmission mode
			Uart[UART_1].TransmitActive = FALSE;

			// Wait for character to be sent before turning off transmitter
			//PetWatchdog ();
			while (( !(U1LSR & UART_TRANSMITTER_EMPTY))	|| ( !(U1LSR & UART_STATUS_TEMT)))
				{
				//Uart0Transmitter (); //process superbus commands?
				}

			// all data has been transmitted - terminate transmission mode
			// Turn off the transmitter - Final Product Control
			FIO0CLR = INITS_RS485_2_TX_ENABLE;
			FIO3CLR = PORT3_RS485_2_TX_ENABLE;

			// Enable the Receiver
			FIO0CLR = INITS_RS485_2_RX_ENABLE;
			FIO3CLR = PORT3_RS485_2_RX_ENABLE;
			}
		}
}


/***********************************************************
	Subroutine:	Uart3Transmitter()

	Description:
	This is the UART3 Transmit routine for the Superbus rs-485 channel.
	This routine will transmit one byte of data.
	This routine is called from the foreground.

	Inputs:
		U3LSR
		Uart [3]. []
		
	Outputs:
		Uart [3]. []
		U3THR
		FIO0CLR

	Locals:
		index
		
*************************************************************/
void Uart3Transmitter ( )
{
	uint8 index;

	// Is it time to transmit another rs-485 character?
			PetWatchdog ();
	if ( Uart[UART_3].TransmitActive )
	{
	// Is it time to transmit another rs-485 character?
	if ( U3LSR & UART_TRANSMITTER_EMPTY )
//	 && ( U3LSR & UART_STATUS_TEMT ))				// v5.1 patch
		{
		// YES - transmit next rs-485 character
		index = Uart [UART_3].TransmitOut++;
		U3THR = Uart [UART_3].TransmitFifo [index];
		// Watch for wrap on transmit out pointer
		Uart [UART_3].TransmitOut &= UART_FIFO_SIZE_MASK;

		// Check for end of Transmit Data:
		if (Uart [UART_3].TransmitOut == Uart [UART_3].TransmitIn )
			{
			// all data has been transmitted - terminate transmission mode
			Uart[UART_3].TransmitActive = FALSE;

			// Wait for character to be sent before turning off transmitter
			//PetWatchdog();
			while ((U3LSR & UART_REALLY_TRANSMITTER_EMPTY) != UART_REALLY_TRANSMITTER_EMPTY)
				{
				WEAK_NOP_8();
				}

			// Turn off the transmitter - Final Product Control
			WEAK_NOP_8();
			FIO0CLR = INITS_RS485_TX_ENABLE;

			// Enable the Receiver
			WEAK_NOP_8();
			FIO0CLR = INITS_RS485_RX_ENABLE;
			}
		}
	}
}
		


/***********************************************************
	Subroutine:	Uart2Transmitter()

	Description:
	This is the UART2 Transmit routine for Backup Paging or Spectralink.
	This routine will transmit one byte of data.
	This routine is called from the foreground.

	Inputs:
		U2LSR
		Uart [2]. []
		
	Outputs:
		Uart [2]. []
		U2THR
		FIO0CLR

	Locals:
		index
		
*************************************************************/
void Uart2Transmitter ( )
{
	uint8 index;

	// Is it time to transmit another rs-485 character?
if ( Uart[UART_2].TransmitActive )
	{
	// Is it time to transmit another rs-485 character?
	if ( U2LSR & UART_TRANSMITTER_EMPTY )
		{
		// YES - transmit next rs-485 character
		index = Uart [UART_2].TransmitOut++;
		U2THR = Uart [UART_2].TransmitFifo [index];
		// Watch for wrap on transmit out pointer
		Uart [UART_2].TransmitOut &= UART_FIFO_SIZE_MASK;

		// Check for end of Transmit Data:
		if (Uart [UART_2].TransmitOut == Uart [UART_2].TransmitIn )
			{
			// all data has been transmitted - terminate transmission mode
			Uart[UART_2].TransmitActive = FALSE;
			}
		}
	}
}
		

