/***********************************************************
	Module Name:	spectralink.c  

	Author:			Bob Halliday, October, 2006   
					bohalliday@aol.com    (781) 863-8245

	Description:	
	This file is responsible for monitoring the state of 256 spectralink
					  
	Subroutines:	SpectralinkInitialize ()	
					SpectralinkExecutive ()
					SpectralinkReceiveExecutive ()

	Revision History:

*************************************************************/
#define BODY_SPECTRALINK 

#include "system.h"			
#include "main.h"
#include "lpc23xx.h"
#include "psi2000.h"
#include "admin.h"
#include "rtc.h"
#include "database.h"
#include "sio.h" 
#include "mprocess.h"
#include "timers.h"
#include "utils.h"
#include "tec.h"
#include "sbus.h"
#include "lapage.h"
#include "uart.h"

#ifdef SPECTRALINK
#include "spectralink.h"
#endif

#include <string.h>

extern const uint8 szCRLF[];		


#ifdef SPECTRALINK

/***********************************************************
	Subroutine:	  SpectralinkInitialize()

	Description:
		This routine will set up the Spectralink Registration Array
		
        
	Inputs:		

	Outputs:

	Locals:	
			
*************************************************************/
void SpectralinkInitialize ()
{
	// Say that spectralink is active, not backup paging.
//	SL_SpectralinkActive = 1;

}






/***********************************************************
	Subroutine:	SpectralinkExecutive()

	Description:
		The top level routine for Spectralink support logic
        
	Inputs:		

	Outputs:

	Locals:	
			
*************************************************************/
void SpectralinkExecutive ()
{
	// Watch for data
	SpectralinkReceiveExecutive ();

	// Handle the sending of alarms
	SpectralinkAlarmExecutive ();

	// Handle the printing out of all port numbers
	SpectralinkPortPrintOutExecutive ();

}



/***********************************************************
	Subroutine:	SpectralinkReceiveExecutive()

	Description:
		This routine monitors for receive data over the Spectralink port,
		and executes receive packets.
        
	Inputs:		

	Outputs:

	Locals:	
			
*************************************************************/
void SpectralinkReceiveExecutive ()
{
//	uint8 response;
//	const uint8 szReceive[] = "RCV:  ";

	if ( SioByteCount( UART_2 ) )
   		{
        // Get next Receive Byte
		SL_Receive [SL_ReceiveIndex++] = SioGetByte( UART_2 );

		// decode command
		if (SL_ReceiveIndex == SL_PORT_NUMBER0)
			{
			// The first 4 bytes of the message have been received
			if (!SpectralinkValidateReceiveCommand ())
				{
				// The command received is invalid - 
				// throw out the first byte and move the other 3 up
				SL_Receive [SL_LENGTH1] = SL_Receive [SL_LENGTH2];
				SL_Receive [SL_LENGTH2] = SL_Receive [SL_OPCODE1];
				SL_Receive [SL_OPCODE1] = SL_Receive [SL_OPCODE2];
				SL_ReceiveIndex--;
				}
			return;
			}
		else if (SL_ReceiveIndex > SL_PORT_NUMBER0)
			{
			// At least 4 bytes have already been received
			if (SL_ReceiveIndex >= (SL_Count+4))
				{
				// A full packet has been received - 
				if (SpectralinkValidateChecksum (SL_Count + 2))
					{
					// The checksum is valid - execute the command
	           		(SpectralinkCommandExecute [SL_CommandIndex] )(SL_Receive, SL_Count);

					// Restart the OAI Heartbeat timer
					OAI_HeartbeatTimer = 0;
					}

				// Restart the Receiver Index
				SL_ReceiveIndex = 0;
				}
			}
		}
}


/***********************************************************
	Subroutine:	SpectralinkAlarmExecutive()

	Description:
		This routine will transmit alarms over spectralink as they are received.
        
	Inputs:		

	Outputs:

	Locals:	
			
*************************************************************/
void SpectralinkAlarmExecutive ()
{
	// Which spectralink alarm state are we in?
	switch (SL_AlarmState)
		{
		// The PC is active.  No need to support Spectralink
		case OAI_IDLE:
			if (gucstandalone_mode)
				{
				// The PC host has disappeared
				SpectralinkSetup ();
				SL_AlarmState++;
				}
			break;

		// The PC just went off line.  Make sure to register with the OAI first.
		case OAI_LOGON:
			SpectralinkXmitLogin ();
			SL_AlarmState = SpectralinkSendCommandPacket (OAI_LOGIN_COMMAND,
														  OAI_LOGIN_LENGTH);
			break;

		// Get the protocol version number of the Spectralink system.
		case OAI_GET_VERSION:
			SL_AlarmState = SpectralinkSendCommandPacket (OAI_CHECK_PROTOCOL_VERSION_COMMAND,
														  OAI_VERSION_LENGTH);
			SL_RegistrationBase = 0;
			OAI_WaitForAckTimer = 0;
			break;

		// Get the protocol version number of the Spectralink system.
		case OAI_GET_VERSION_RESPONSE:
			if (OAI_WaitForAckTimer > OAI_WAIT_FOR_ACK_TIME)
				{
				// No response is forthcoming from the system - go over to backup paging
				SL_SpectralinkActive = 0;
				SL_AlarmState = OAI_SPECTRALINK_DISABLED;
				}
			break;

		// This block will ask for the state of registration of the next 64 handsets
		case OAI_REGISTER1:
			SL_SpectralinkActive = 1;
			SpectralinkXmitRegistration ();
			SL_AlarmState = SpectralinkSendCommandPacket (OAI_REGISTRATION_REQUEST_COMMAND,
														  OAI_REGISTRATION_LENGTH);
			SL_RegistrationResponseFlag = 0;
			OAI_WaitForAckTimer = 0;
			break;

		// Wait for the handset registration response
		case OAI_REGISTER2:
			if (SL_RegistrationResponseFlag)
				{
				// We have received an ACK from the Handset
				SL_AlarmState = OAI_REGISTER1;
				// Check for Registration Base overflow.
				SL_RegistrationBase += SL_REGISTRATION_BLOCK_SIZE;
				if (SL_RegistrationBase >= SL_REGISTRATION_LIMIT_SIZE)
					{
					SL_AlarmState = OAI_WATCH_FOR_ALARM;
					}
				}
			else if (OAI_WaitForAckTimer > OAI_WAIT_FOR_ACK_TIME)
				{
				// No response is forthcoming from the system - restart the Logon procedure
				SL_AlarmState = OAI_IDLE;
				}
			break;


		// Watch for an alarm to arrive
		case OAI_WATCH_FOR_ALARM:
			// Send a Spectralink Heartbeat once every 10 seconds
			SpectralinkHeartBeat ();
			// Hang up all phones after 5 minutes of being off hook
			SpectralinkHangUp ();
			// Watch for a Spectralink Heartbeat 
			SpectralinkWatchForHeartBeat ();

			if (!gucstandalone_mode)
				{
				// The PC host has reappeared
				SL_AlarmState = OAI_IDLE;

				// Disable Receive data from the Spectralink system:
				EnablePagingReceiver = 0;
				}
			else if (SL_AlarmHead != SL_Head)
				{
				// A new alarm is available to send
				SL_AlarmState = SpectralinkGetPortNumber ();
				}
			break;

		// issue a Priority Ring Command
		case OAI_PRIORITY_RING:
			if (!SpectralinkWaitForBuffer ())
				break;
			SpectralinkXmitPriorityRing ();
			// Put the Port Number into the Transmit Packet
			SpectralinkConvertPortNumber (SL_PortNumber);
			SL_HandsetRespondedFlag = OAI_WaitForAckTimer = 0;
			SL_AlarmState = SpectralinkSendCommandPacket (OAI_PRIORITY_RING_COMMAND,
														  OAI_PRIORITY_RING_LENGTH);
			break;


		// Wait for an ACKnowledge response from the Handset (Port Number).
		case OAI_PRIORITY_RING_ACKNOWLEDGE:
			if (SL_HandsetRespondedFlag)
				{
				// We have received an ACK from the Handset
				SL_AlarmState++;
				}
			else if (OAI_WaitForAckTimer > OAI_WAIT_FOR_ACK_TIME)
				{
					SL_AlarmState++;
				}
			break;


		// Issue a Set Display Command
		case OAI_SET_DISPLAY:
			if (!SpectralinkWaitForBuffer ())
				break;
			SpectralinkXmitDisplayAlarm (SL_AlarmHead);
			// Save the pointer to where we are in the calling list
			SpectralinkWalkPointer [SL_GetPortIndex-1] = SL_AlarmHead;
			// Put the Port Number into the Transmit Packet
			SpectralinkConvertPortNumber (SL_PortNumber);
			OAI_HangupTimer = 0;
			SL_DisplayUpdatedFlag = OAI_WaitForAckTimer = 0;
			SL_AlarmState = SpectralinkSendCommandPacket (OAI_SET_DISPLAY_COMMAND,
														  OAI_SET_DISPLAY_LENGTH);
			break;


		// Wait for an ACKnowledge response from the Handset (Port Number).
		case OAI_SET_DISPLAY_ACKNOWLEDGE:
			if (SL_DisplayUpdatedFlag)
				{
				// We have received an ACK from the Handset
				SL_AlarmState++;
				SL_DisplayUpdatedFlag = 0;


				// Set Flag to say that phone is off hook
				SL_OffHook [SL_GetPortIndex-1] = TRUE;
				}
			else if (OAI_WaitForAckTimer > OAI_WAIT_FOR_ACK_TIME)
				{
				// No ACK is forthcoming from this handset - give up and go to the next handset
				SL_AlarmState++;
				}
			break;


		case OAI_NEXT_PHONE:
			// Wait until comm buffer has been emptied before contacting the next phone
			if (SpectralinkWaitForBuffer ())
				{
				SL_AlarmState = OAI_WATCH_FOR_ALARM;
				}
			break;

		// Spectralink did not respond to a protocol request.  The interface is disabled.
		case OAI_SPECTRALINK_DISABLED:
			if (!gucstandalone_mode)
				{
				// The PC host has reappeared
				SL_AlarmState = OAI_IDLE;
				}

      	default:
			break;
		}
}

		

/***********************************************************
	Subroutine:	SpectralinkSaveAlarm (  )

	Description:
		This subroutine will save the passed alarm to
		the Spectralink queue and update the Head Pointer
		and Tail Pointer.

	Inputs:
		Alarm_Pointer

	Outputs:
		SpectralinkQueue [] []
		SL_Head
		SL_Tail
		SL_Display 

	Locals:
			
*************************************************************/
void SpectralinkSaveAlarm (uint8 *Alarm_Pointer)
{
	// First store the alarm in the queue
	memcpy ( SpectralinkQueue [SL_Head],
			  (uint8 *) Alarm_Pointer, SPECTRALINK_RECORD_SIZE );

	// update the Head Pointer
	++SL_Head;
	SL_Head &= SPECTRALINK_WALK_MASK;

	// Update the Display Pointer
	if (!SpectralinkInSession)
		SL_Display = SL_Head;

	// Make sure the head hasn't caught up with the tail
	if (SL_Head == SL_Tail)
		{
		// Update the Tail Pointer to keep it behind the Head Pointer
		++SL_Tail;
		SL_Tail &= SPECTRALINK_WALK_MASK;
		}
}



/***********************************************************
	Subroutine:	SpectralinkSendCommandPacket (  )

	Description:
		This subroutine will transmit a command packet to a phone

	Inputs:	
		command
		packet length
		port number

	Outputs:
		SL_Transmit [] 

	Locals:
			
*************************************************************/
uint8 SpectralinkSendCommandPacket (uint8 command, 
											uint8 length)
{
//	const uint8 szSend[] = "SEND: ";
	// Set the msg length bytes
    SL_Transmit [SL_LENGTH1] = BinHexAscii (length >> 4);	
    SL_Transmit [SL_LENGTH2] = BinHexAscii (length);

	// Set the Command Code
	SL_Transmit [SL_OPCODE2] = command;

	// Now calculate and store the checksum, along with the CR-LF
	SpectralinkCalculateAndStoreChecksum (length+SL_OPCODE1);

	// Transmit the message
	SioSetMessage ( UART_2, SL_Transmit, length+6 );


			// bh - debug
//			SioSetMessage( UART_0,(uint8 *) szSend,strlen ((const char *)szSend));
//			SioSetMessage( UART_0,(uint8 *) SL_Transmit, length+6);
			// bh - end debug



	return (SL_AlarmState+1);
}

	


/***********************************************************
	Subroutine:	SpectralinkConvertPortNumber (  )

	Description:
		This subroutine will convert the Port Number to Ascii
		and store it in the Spectralink Transmit Buffer

	Inputs:	
		SL_PortNumber

	Outputs:
		SL_Transmit []

	Locals:
			
*************************************************************/
void SpectralinkConvertPortNumber (uint16 port_number )
{
	// Convert the Port Number to Ascii and store it in the Transmit Buffer
    SL_Transmit [SL_PORT_NUMBER0] = BinHexAscii (port_number >> 12);	
    SL_Transmit [SL_PORT_NUMBER1] = BinHexAscii (port_number >> 8);
    SL_Transmit [SL_PORT_NUMBER2] = BinHexAscii (port_number >> 4);	
    SL_Transmit [SL_PORT_NUMBER3] = BinHexAscii (port_number);
}

    

/***********************************************************
	Subroutine:	SpectralinkCalculateAndStoreChecksum (  )

	Description:
		This subroutine will calculate and store the checksum for
		a Spectralink Transmit Message. It will also store the CR-LF
		at the end of the message

	Inputs:	
		length

	Outputs:
		SL_Transmit []

	Locals:
		i, checksum
			
*************************************************************/
void SpectralinkCalculateAndStoreChecksum (uint8 length)
{
	uint8 i;
	 uint8 checksum;

	i = length;

	// perform the checksum calculation
	checksum = SpectralinkCalculateChecksum (SL_Transmit, length);

	// Convert the checksum to ascii and store it in the Transmit Buffer
    SL_Transmit [i++] = BinHexAscii (checksum >> 4);	
    SL_Transmit [i++] = BinHexAscii (checksum);
    
    // Store the CR, LF characters	
    SL_Transmit [i++] = CR;	
    SL_Transmit [i]   = LF;
}

/***********************************************************
	Subroutine:	SpectralinkValidateReceiveCommand (  )

	Description:
		This subroutine will validate the Spectralink Received Command

	Inputs:
		SL_Receive []

	Outputs:
		SL_Count
		Sl_Command 

	Returns:
		TRUE - Received Command is Valid
		FALSE - Command is Invalid		
	
*************************************************************/
uint8 SpectralinkValidateReceiveCommand ()
{
	 uint8 i;

	// get the binary count
    SL_Count = HexAsciiBin (SL_Receive);
	// v5.16: Validate the binary count
	if ( SL_Count >= SPECTRALINK_RECEIVE_SIZE)
		return FALSE;

	// get the binary command
	SL_Command = HexAsciiBin (&SL_Receive [SL_OPCODE1]);

	for ( i = 0;  i < OAI_NUMBER_OF_COMMANDS;  i++ )
		{
		if (SL_Command == SpectralinkCommands [i] )
			{
			// A valid command has been found
			SL_CommandIndex = i;
			return TRUE;
			}
		}
	return FALSE;
}


/***********************************************************
	Subroutine:	SpectralinkValidateChecksum (  )

	Description:
		This subroutine will validate the received checksum

	Inputs:
		SL_Receive []
		count

	Outputs:

	Returns:
		TRUE - Received Checksum is Valid
		FALSE - Checksum is Invalid		
	
*************************************************************/
uint8 SpectralinkValidateChecksum (uint8 length)
{
	 uint8 received_checksum;

	// get the checksum
	received_checksum = HexAsciiBin (&SL_Receive [length]);

	return (received_checksum == SpectralinkCalculateChecksum (SL_Receive, length)); 
}



/***********************************************************
	Subroutine:	SpectralinkCalculateChecksum (  )

	Description:
		This subroutine will validate the received checksum

	Inputs:
		SL_Receive []
		SL_ReceiveIndex

	Outputs:
		SL_Count
		Sl_Command 

	Returns:
		checksum
	
*************************************************************/
uint8 SpectralinkCalculateChecksum (uint8 *buffer, uint8 end)
{
	 uint8 i, checksum=0;

	for (i = 0;  i < end;  i++ )
		{
		checksum += buffer [i];
		}

	return checksum;
}


/***********************************************************
	Subroutine:	OAI_Heartbeat (  )

	Description:
		Response #81
		This subroutine processes an OAI Heartbeat.
		The Heartbeat Timer is reset to zero.

	Inputs:	
		buffer - start of receive buffer
		count  - number of bytes in receive packet

	Outputs:
		SL_HeartBeat

	Locals:
			
*************************************************************/
uint8 OAI_Heartbeat (uint8 *buffer, uint8 count)
{
	SL_HeartBeat = 0;
	return 0;
}


/***********************************************************
	Subroutine:	OAI_IllFormedMessage (  )

	Description:
		Response #82
		This subroutine will process an Ill-Formed Message reply.
		If the Ill Formed message is in response to a Registration Request,
		this routine will terminate the registration request process.

	Inputs:	
		buffer - start of receive buffer
		count  - number of bytes in receive packet

	Outputs:
		SL_AlarmState

	Locals:
			
*************************************************************/
uint8 OAI_IllFormedMessage (uint8 *buffer, uint8 count)
{
	 uint8 command2;

	// get the byte following the command byte
    command2 = HexAsciiBin ( &buffer [SL_PORT_NUMBER0] );

	if ((command2 == OAI_FIELD_OUT_OF_RANGE)
	 && (buffer [SL_DISPLAY_OFFSET1] == OAI_REGISTRATION_REQUEST_COMMAND))
		{
		// The registration process is complete - terminate it.
		SL_AlarmState = OAI_WATCH_FOR_ALARM;
		}

	return 0;
}






/***********************************************************
	Subroutine:	OAI_Event (  )

	Description:
		Response #83
		This subroutine will trigger a new Login sequence

	Inputs:	
		buffer - start of receive buffer
		count  - number of bytes in receive packet

	Outputs:
		SpectralinkRegistrationList []

	Locals:
			
*************************************************************/
uint8 OAI_Event (uint8 *buffer, uint8 count)
{
	 uint8 command2;

	// get the byte following the command byte
    command2 = HexAsciiBin ( &buffer [SL_PORT_NUMBER0] );

	if (command2 == OAI_GATEWAY_RESTART)
		{
		// We have received an OAI Gateway Restart command
		SpectralinkXmitLogin ();
		SpectralinkSendCommandPacket (OAI_LOGIN_COMMAND,
									  OAI_LOGIN_LENGTH);
		}

	return 0;
}

/***********************************************************
	Subroutine:	OAI_WirelessTelephoneEvent (  )

	Description:
		Response #84
		This subroutine will execute a wireless telephone event:
			Up Key - display the next alarm
			Down Key - display the previous alarm 

	Inputs:	
		buffer - start of receive buffer
		count  - number of bytes in receive packet

	Outputs:

	Locals:
			
*************************************************************/
uint8 OAI_WirelessTelephoneEvent (uint8 *buffer, uint8 count)
{
	uint16 port_number;
	uint8  action, index;
	uint8 just_display;

	// Check to see if UP or DOWN Key has been pressed
	// get the Action Code
    action = HexAsciiBin (&SL_Receive [SL_DISPLAY_OFFSET0]);

	if (action == OAI_HANDSET_RESPONDED)
		{
		// We have received a "Handset Responded" Acknowledgement
		SL_HandsetRespondedFlag = 1;
		return TRUE;
		}
	if (action == OAI_DISPLAY_UPDATE_RECEIVED)
		{
		// We have received a "Display Update Received" Acknowledgement
		SL_DisplayUpdatedFlag = 1;
		return TRUE;
		}

	// Get the handset port number
	port_number = SpectralinkGetOaiPortNumber ();
	SpectralinkConvertPortNumber (port_number);

	just_display = 0;
	SL_LeftArrowActive = TRUE;
	SL_RightArrowActive = TRUE;
	if (action > OAI_START_OAI_SESSION)
		{
		// A Start OAI session key has been pressed
		just_display = TRUE;
		SL_RightArrowActive = FALSE;
		}
	else
		{
		// Check to see if we have received an UP or DOWN command
		if (action==OAI_STAR_KEY)
			// Go Backward through the list of alarms
			SL_Up = FALSE;
		else if (action==OAI_POUND_KEY)
			// Go forward through the list of alarms
			SL_Up = TRUE;
		else
			return TRUE;
		}

	// Get the correct index for the port number
	index = SpectralinkGetIndexFromPortNumber (port_number);
	// A NULL index means the Port Number is not registered
	if (index == 0xFF)
		return TRUE;

	// Increment or Decrement the Walk Pointer
	action = SpectralinkWalkPointer [index];
	if (!just_display)
		{
		if (SL_Up)
			// Increment the Walk Pointer
			++action;
		else
			// Decrement the index
			--action;
		}

	// Handle overflow or underflow
	action &= SPECTRALINK_WALK_MASK;
	if (!(SpectralinkQueue [action][0]|SpectralinkQueue [action][1]|SpectralinkQueue [action][2]))
		{
		// Record is blank - cancel this action
		if (SL_Up)
			SL_RightArrowActive = FALSE;			// turn off right arrow
		else
			SL_LeftArrowActive = FALSE;				// turn off left arrow
		// Now update the Left and Right arrow keys
		SpectralinkXmitIcons (SL_LeftArrowActive, SL_RightArrowActive);
		SpectralinkSendCommandPacket (OAI_SET_ICON_COMMAND, OAI_SET_ICON_LENGTH);
		return TRUE;
		}
	// Record is nonblank - go ahead and transmit this alarm
	SpectralinkWalkPointer [index] = action;

	// Transmit the corresponding alarm
	SpectralinkXmitDisplayAlarm (action);
	// Put the Port Number into the Transmit Packet
	SpectralinkSendCommandPacket (OAI_SET_DISPLAY_COMMAND,
								  OAI_SET_DISPLAY_LENGTH);

	// Now update the Left and Right arrow keys
	SpectralinkXmitIcons (SL_LeftArrowActive, SL_RightArrowActive);
	SpectralinkSendCommandPacket (OAI_SET_ICON_COMMAND, OAI_SET_ICON_LENGTH);
	return TRUE;
}






/***********************************************************
	Subroutine:	OAI_DecodeRegistrationResponse (  )

	Description:
		Response #85
		This subroutine will execute a wireless telephone event:
			Up Key - display the next alarm
			Down Key - display the previous alarm 

	Inputs:	
		buffer - start of receive buffer
		count  - number of bytes in receive packet
		SL_RegistrationBase

	Outputs:

	Locals:
			
*************************************************************/
uint8 OAI_DecodeRegistrationResponse (uint8 *buffer, uint8 count)
{
	uint16 port_number;
	uint8 * ptr;
	uint8 i;
	uint8 active;

	// get the packet count and validate it
	if (count <= (SL_REGISTRATION_BLOCK_SIZE*2))
		{
		// Yikes - bad error - the received count is too small
		SL_AlarmState = OAI_IDLE;
		return 1;
		}

	// get the first port number and validate it
    port_number =  (uint16) HexAsciiBin (&buffer [SL_PORT_NUMBER0]);
    port_number <<= 8;
    port_number |= (uint16) HexAsciiBin (&buffer [SL_PORT_NUMBER2]);

	if (port_number != SL_RegistrationBase)
		{
		// Yikes - bad error - the received first port number does not match the requested value.
		SL_AlarmState = OAI_IDLE;
		return 1;
		}

	// set pointer to start of return data
	ptr = (uint8 *) &buffer [SL_ACK_CODE];
	for ( i = 0;  i < (SL_REGISTRATION_BLOCK_SIZE*2);  i+=2, port_number++ )
		{
		active = HexAsciiBin ((uint8 *) &ptr [i]);
		if ( active != SL_HANDSET_INACTIVE )
			{
			// This handset is active - mark it as so.
			SpectralinkHandsets [SL_HandsetsIndex++] = port_number;
			if (SL_HandsetsIndex >= SL_NUMBER_OF_HANDSETS)
				{
				// Yikes - we've filled up our array of Handsets! - terminate the registration process!
				SL_AlarmState = OAI_WATCH_FOR_ALARM;
				return 1;
				}
			}
		}

	// Set flag to say that the Registration Response has been properly received
	SL_RegistrationResponseFlag = 1;
	return 0;
}



/***********************************************************
	Subroutine:	OAI_GetVersionNumber (  )

	Description:
		Response #86
		This subroutine will read the Protocol Version Number.
		The protocol version is either 
			1. 	v1.2
			2.	v2.0
			3.	future?

	Inputs:	
		buffer - start of receive buffer
		count  - number of bytes in receive packet

	Outputs:
		SL_AlarmState

	Locals:
		version
			
*************************************************************/
uint8 OAI_GetVersionNumber (uint8 *buffer, uint8 count)
{
	uint8 version;

	// get the first digit of the version number
    version = HexAsciiBin (&buffer [SL_PORT_NUMBER0]);

	// Now it's okay to move on to the phone registration process
	SL_AlarmState = OAI_REGISTER1;
 	return version;
}


/***********************************************************
	Subroutine:	SpectralinkGetOaiPortNumber (  )

	Description:
		This subroutine converts an ASCII Port Number to a binary 16-bit Port Number

	Inputs:	
		SL_Receive

	Outputs:
		port_number

	return:
		port_number
			
*************************************************************/
uint16 SpectralinkGetOaiPortNumber ()
{
	uint16 port_number;

	// get the binary count
    port_number = (uint16) HexAsciiBin (&SL_Receive [SL_PORT_NUMBER0]) << 8;
	// get the binary command
	return ( port_number + (uint16) HexAsciiBin (&SL_Receive [SL_PORT_NUMBER2]));
}



/***********************************************************
	Subroutine:	SpectralinkGetPortNumber (  )

	Description:
		This subroutine is called once a minute.
		It will check to see if it is time to write the Registration List to EEprom.

	Inputs:	
		SL_GetPortIndex

	Outputs:
		SL_GetPortIndex
		SL_PortNumber
		SL_AlarmHead

	return:
		SL_AlarmState
			
*************************************************************/
uint8 SpectralinkGetPortNumber ()
{
		PetWatchdog ();
	while ( SL_GetPortIndex	< SL_NUMBER_OF_HANDSETS )
		{
		if (SpectralinkHandsets [SL_GetPortIndex] != SL_END_OF_FILE)
			{
			// we found the next active handset
			SL_PortNumber = SpectralinkHandsets [SL_GetPortIndex++];
			return SL_AlarmState+1;
			}
		SL_GetPortIndex++;
		}

	// No more active handsets found
	SL_GetPortIndex = 0;
	// Bump the head pointer up by one position
	++SL_AlarmHead;
	SL_AlarmHead &= SPECTRALINK_WALK_MASK;
	return SL_AlarmState;
}




/***********************************************************
	Subroutine:	SpectralinkXmitLogin (  )

	Description:
		This subroutine will fill in the Login information for the Login Packet

	Inputs:	

	Outputs:
		SpectralinkRegistrationList []

	Locals:
		i
			
*************************************************************/
void SpectralinkXmitLogin ()
{
	// Now copy the Logon message into the buffer
	memcpy ( &SL_Transmit [SL_PORT_NUMBER0],
			  (uint8 *) Philips_Logon, sizeof (Philips_Logon) );

}


/***********************************************************
	Subroutine:	SpectralinkXmitRegistration (  )

	Description:
		This subroutine will fill in the Registration information for the Registration Packet

	Inputs:
		SL_RegistrationBase	

	Outputs:
		SL_Transmit []
		SL_RegistrationBase	

	Locals:
		i
			
*************************************************************/
void SpectralinkXmitRegistration ()
{
	uint16 top;

	// Set the starting Port Number - 4 ASCII Digits
	SL_Transmit [SL_PORT_NUMBER0] = BinHexAscii (SL_RegistrationBase >> 12);
	SL_Transmit [SL_PORT_NUMBER1] = BinHexAscii (SL_RegistrationBase >> 8);
	SL_Transmit [SL_PORT_NUMBER2] = BinHexAscii (SL_RegistrationBase >> 4);
	SL_Transmit [SL_PORT_NUMBER3] = BinHexAscii (SL_RegistrationBase);

	// Set the ending Port Number - 4 ASCII Digits
	top = (SL_RegistrationBase + SL_REGISTRATION_BLOCK_SIZE) - 1;
	SL_Transmit [SL_ACK_CODE] 		 = BinHexAscii (top >> 12);
	SL_Transmit [SL_RETURN_DISPLAY]  = BinHexAscii (top >> 8);
	SL_Transmit [SL_DISPLAY_OFFSET0] = BinHexAscii (top >> 4);
	SL_Transmit [SL_DISPLAY_OFFSET1] = BinHexAscii (top);

}



/***********************************************************
	Subroutine:	SpectralinkXmitPriorityRing (  )

	Description:
		This subroutine will set up the Priority Ring Transmit Packet

	Inputs:	

	Outputs:
		SpectralinkRegistrationList []

	Locals:
		i
			
*************************************************************/
void SpectralinkXmitPriorityRing ()
{
	// Set the Ring Type
	SL_Transmit [SL_ACK_CODE] = '0';
	SL_Transmit [SL_RETURN_DISPLAY] = OAI_RING_TYPE_LOW;

	// Set the Number of Ring Cycles
	SL_Transmit [SL_DISPLAY_OFFSET0] = OAI_RING_CYCLES_HIGH;
	SL_Transmit [SL_DISPLAY_OFFSET1] = OAI_RING_CYCLES_LOW;

	// Set the Number of Ring Bursts
	SL_Transmit [SL_DATA] = OAI_RING_BURSTS;

	// Set the Active Voice Call Alert
	SL_Transmit [SL_DATA+1] = OAI_ALERT_TONE;

	// Set the Ring Burst ON Duration
	SL_Transmit [SL_DATA+2] = OAI_RING_ON_DURATION_HIGH;
	SL_Transmit [SL_DATA+3] = OAI_RING_ON_DURATION_LOW;

	// Set the Ring Burst OFF Duration
	SL_Transmit [SL_DATA+4] = OAI_RING_OFF_DURATION_HIGH;
	SL_Transmit [SL_DATA+5] = OAI_RING_OFF_DURATION_LOW;

	// Set the Ring Burst ON Duration
	SL_Transmit [SL_DATA+6] = OAI_RING_ON_DURATION_HIGH;
	SL_Transmit [SL_DATA+7] = OAI_RING_ON_DURATION_LOW;

	// Set the Ring Burst OFF Duration
	SL_Transmit [SL_DATA+8] = OAI_RING_OFF_DURATION_HIGH;
	SL_Transmit [SL_DATA+9] = OAI_RING_OFF_DURATION_LOW;


	// Set the Ring Burst ON Duration
	SL_Transmit [SL_DATA+10] = OAI_RING_ON_DURATION_HIGH;
	SL_Transmit [SL_DATA+11] = OAI_RING_ON_DURATION_LOW;

	// Set the Ring Burst OFF Duration
	SL_Transmit [SL_DATA+12] = OAI_RING_OFF_DURATION_HIGH;
	SL_Transmit [SL_DATA+13] = OAI_RING_OFF_DURATION_LOW;


}


/***********************************************************
	Subroutine:	SpectralinkXmitDisplayAlarm (  )

	Description:
		This subroutine will transmit a Display Alarm command to a phone

	Inputs:	
		alarm_index

	Outputs:
		SL_Transmit [] 

	Locals:
			
*************************************************************/
void SpectralinkXmitDisplayAlarm (uint8 alarm_index)
{
	// Set the Acknowledgement code
	SL_Transmit [SL_ACK_CODE] = OAI_ACTIVE_ACK;

	// Set the Retain Display code
	SL_Transmit [SL_RETURN_DISPLAY] = OAI_RETAIN_DISPLAY;

	// Set the Display Offset bytes
	SL_Transmit [SL_DISPLAY_OFFSET0] = '0';
	SL_Transmit [SL_DISPLAY_OFFSET1] = '0';

	// Now copy the message into the buffer
	memcpy ( &SL_Transmit [SL_DATA],
			  (uint8 *) SpectralinkQueue [alarm_index], SPECTRALINK_RECORD_SIZE );

}


/***********************************************************
	Subroutine:	SpectralinkXmitIcons (  )

	Description:
		This subroutine will transmit a Display Icon command to a phone

	Inputs:	
		LeftArrow
		RightArrow

	Outputs:
		SL_Transmit [] 

	Locals:
			
*************************************************************/
void SpectralinkXmitIcons (uint8 LeftArrow, uint8 RightArrow )
{
	// Set the Acknowledgement code
	SL_Transmit [SL_ACK_CODE] = OAI_NO_ACK;

	// Set the RETURN Display code
	SL_Transmit [SL_RETURN_DISPLAY] = OAI_CLEAR_DISPLAY;

	// Set the Left Arrow Icon
	SL_Transmit [SL_DISPLAY_OFFSET0] = OAI_ICON_LEFT_ARROW_HIGH;
	SL_Transmit [SL_DISPLAY_OFFSET1] = OAI_ICON_LEFT_ARROW_LOW;
	SL_Transmit [SL_DATA] = OAI_ICON_OFF;
	if (LeftArrow)
		SL_Transmit [SL_DATA] = OAI_ICON_SOLID_ON;

	// Set the Right Arrow Icon
	SL_Transmit [SL_DATA+1] = OAI_ICON_RIGHT_ARROW_HIGH;
	SL_Transmit [SL_DATA+2] = OAI_ICON_RIGHT_ARROW_LOW;
	SL_Transmit [SL_DATA+3] = OAI_ICON_OFF;
	if (RightArrow)
		SL_Transmit [SL_DATA+3] = OAI_ICON_SOLID_ON;
}



/***********************************************************
	Subroutine:	SpectralinkXmitSelectConnections (  )

	Description:
		This subroutine will set up the Select Connections Transmit Packet

	Inputs:	

	Outputs:
		SL_Transmit []

	Locals:
*************************************************************/
void SpectralinkXmitSelectConnections ()
{
	// Set to Display Message from App Program
	SL_Transmit [SL_ACK_CODE] = OAI_APP_PROGRAM;

	// Set connect audio to silence
	SL_Transmit [SL_RETURN_DISPLAY] = OAI_APP_PROGRAM;

	// Set to send wireless telephone keys to App Program
	SL_Transmit [SL_DISPLAY_OFFSET0] = OAI_APP_PROGRAM;

}


/***********************************************************
	Subroutine:	SpectralinkXmitEndCall (  )

	Description:
		This subroutine will set up the End Call Transmit Packet

	Inputs:	

	Outputs:
		SL_Transmit []

	Locals:
			
*************************************************************/
void SpectralinkXmitEndCall ()
{
	// Set the End OAI Session Type = 0x00:  Return handset to Standby
	SL_Transmit [SL_ACK_CODE] = '1';

}


				


/***********************************************************
	Subroutine:	SpectralinkSetRegistrationData()

	Description:
		64 bytes of Spectralink Registration data will be transmitted to the host
		This routine will convert the 32 Binary bytes to 64 Ascii bytes 

	Inputs:
		pMessage

	Outputs:
		Registration []

	Locals:
			
*************************************************************/



/***********************************************************
	Subroutine:	SpectralinkInitiateDiagnosticPoll()

	Description:
		This routine will start Diagnostic Polling Mode,
		which involves the one-time polling of each of 249 possible CARTs.

	Inputs:
		SpectralinkRegistrationList []

	Outputs:
		SpectralinkRegistrationList []
		SpectralinkCopyRegistrationList []

	Locals:
		i	
*************************************************************/



/***********************************************************
	Subroutine:	 SpectralinkGetIndexFromPortNumber()

	Description:
		This routine will search the list of Port Numbers for a match.

	Inputs:
		SpectralinkHandsets []
		port_number

	Outputs:
		index

	Returns:
		index of found Port number
		index=FF, if port number not found
			
*************************************************************/
uint8 SpectralinkGetIndexFromPortNumber (uint16 port_number)
{
	 uint8 i;

	for ( i = 0;  i < SL_NUMBER_OF_HANDSETS;  i++ )
		{
		if (port_number == SpectralinkHandsets [i])
			{
			// The matching Port Number has been found!
			return i;
			}
		}

	// No matching Port Number found
	return 0xFF;
}	


/***********************************************************
	Subroutine:	SpectralinkAddPortNumber()

	Description:
		This routine will store a new port number value at record x-1
		
	Inputs:
		record
		port_number

	Outputs:
		SpectralinkHandsets []

	Locals:
			
*************************************************************/
void SpectralinkAddPortNumber (uint8 record, uint16 port_number)
{
	SpectralinkHandsets [record-1] = port_number;
}

/***********************************************************
	Subroutine:	SpectralinkAddPortNumber()

	Description:
		This routine will store a new port number value at record x-1
		
	Inputs:
		record
		port_number

	Outputs:
		SpectralinkHandsets []

	Locals:
			
*************************************************************/
void SpectralinkDeleteAllPortNumbers ()
{
	 uint8 i;

	for ( i = 0;  i < SL_NUMBER_OF_HANDSETS;  i++ )
		{
		SpectralinkHandsets [i] = SL_END_OF_FILE;
		}
}


/***********************************************************
	Subroutine:	SpectralinkDeletePortNumber()

	Description:
		This routine will delete a port number at record x-1
		
	Inputs:
		record
		port_number

	Outputs:
		SpectralinkHandsets []

	Locals:
			
*************************************************************/
void SpectralinkDeletePortNumber (uint8 record)
{
	SpectralinkHandsets [record-1] = SL_END_OF_FILE;
}


/***********************************************************
	Subroutine:	SpectralinkHangUp ()

	Description:
		This routine will hang up all phones after 5 minutes of inactivity
		
	Inputs:
		SL_HangUpState
		OAI_HangupTimer
		SL_OffHook [ ]

	Outputs:
		SL_HangUpState
		SL_HangUpActive
		OAI_HangupTimer 
		SL_OffHook [ ]

	Locals:
			
*************************************************************/
void SpectralinkHangUp ()
{
	if (!SL_HangUpActive)
		return;				// No need to hang up phones.

	switch (SL_HangUpState)
		{
		case HANGUP_IDLE:
			if (OAI_HangupTimer > SL_HANG_UP_TIME)
				{
				// It's time to transmit a Heartbeat to the Spectralink system.
				OAI_HangupTimer = 0;
				SL_HangUpIndex = 0;
				SL_HangUpState++;
				}
			break;

/********************
		case HANGUP_SEND_SET_DISPLAY:
			if (SL_OffHook [SL_HangUpIndex])
				{
				SpectralinkXmitDisplayAlarm (SL_AlarmHead);
				// Put the Port Number into the Transmit Packet
				SpectralinkConvertPortNumber (SpectralinkHandsets [SL_HangUpIndex]);
				OAI_WaitForAckTimer = 0;
				SpectralinkSendCommandPacket (OAI_SET_DISPLAY_COMMAND,
										  OAI_SET_DISPLAY_LENGTH);
				SL_HangUpState++;
				}
			else
				SL_HangUpState = HANGUP_END_CALL;
			break;

		case HANGUP_WAIT1:
			if (OAI_WaitForAckTimer >= ONE_SECOND)
				{
				SL_HangUpState++;
				}
			break;
*********************************/

		case HANGUP_END_CALL:
			if (SL_OffHook [SL_HangUpIndex])
				{
				// This phone has been off hook for too long - put it back on hook
				SL_OffHook [SL_HangUpIndex] = FALSE;
				SpectralinkXmitEndCall ();
				// Get the handset port number
				SpectralinkConvertPortNumber (SpectralinkHandsets [SL_HangUpIndex]);
				SpectralinkSendCommandPacket (OAI_END_CALL_COMMAND,
									  	  	  OAI_END_CALL_LENGTH);
				}
			// update index and state variables
			SL_HangUpIndex++;
			SL_HangUpState++;
			break;

		case HANGUP_WAIT:
			if (!SpectralinkWaitForBuffer ())
				break;

			if (( SL_HangUpIndex < SL_NUMBER_OF_HANDSETS )
			 &&	(SpectralinkHandsets [SL_HangUpIndex] != SL_END_OF_FILE))
				{
				// There are still more handsets that need to be hung up.
				SL_HangUpState--;
				}
			else
				{
				// end of hang up commands to active handsets
				SL_HangUpActive = 0;
				SL_HangUpState = HANGUP_IDLE;
				}
			break;
		}
}




/***********************************************************
	Subroutine:	SpectralinkReadPortNumber()

	Description:
		This routine will read a port number value at record x-1
		
	Inputs:
		record
		port_number

	Outputs:
		SpectralinkHandsets []

	Locals:
			
*************************************************************/
uint16 SpectralinkReadPortNumber (uint8 record)
{
	return (SpectralinkHandsets [record-1]);
}


/***********************************************************
	Subroutine:	SpectralinkHeartBeat()

	Description:
		This routine will output a HeartBeat packet once every 'n' seconds
		
	Inputs:
		SL_Seconds

	Outputs:
		SL_Seconds 

	Locals:
			
*************************************************************/
void SpectralinkHeartBeat ()
{
	if (SL_Seconds > SL_HEARTBEAT_TIME)
		{
		// It's time to transmit a Heartbeat to the Spectralink system.
		SL_Seconds = 0;
		SpectralinkSendCommandPacket (OAI_HEARTBEAT_COMMAND,
									  OAI_HEARTBEAT_LENGTH);
		}
}


/***********************************************************
	Subroutine:	SpectralinkWatchForHeartBeat()

	Description:
		This routine will watch for a HeartBeat packet from the Spectralink System.
		If none is received in 40 seconds, the CAR will re-issue a Logon Command.
		
	Inputs:
		SL_HeartBeat

	Outputs:
		SL_HeartBeat 

	Locals:
			
*************************************************************/
void SpectralinkWatchForHeartBeat ()
{
	if (SL_HeartBeat > SL_WATCH_FOR_HEARTBEAT_TIME)
		{
		// No Heartbeat has been received from Spectralink in 40 seconds.
		SL_HeartBeat = 0;
		SpectralinkXmitLogin ();
		SpectralinkSendCommandPacket (OAI_LOGIN_COMMAND,
									  OAI_LOGIN_LENGTH);
		}
}



/***********************************************************
	Subroutine:	SpectralinkSetup()

	Description:
		At the start of spectralink backup support, this routine will initialize
		the Spectralink system variables.

	Inputs:
		SL_Head

	Outputs:
		SL_Transmit []
		SL_AlarmHead
		SpectralinkWalkPointer []
		SL_GetPortIndex

	Locals:
			
*************************************************************/
void SpectralinkSetup (void)
{
	 uint8 i;

	// The first byte of the ASCII command is always zero.
	SL_Transmit [SL_OPCODE1] = '0';

	// initialize the display pointers for each of 200 phones:
	SL_AlarmHead = SL_Head;
	for ( i = 0;  i < SL_NUMBER_OF_HANDSETS;  i++ )
		{
		SpectralinkWalkPointer [i] = SL_Head;
		}

	// restart index into list of port entries
	SL_GetPortIndex = 0;

	// Enabled Receive data from the Spectralink system:
	EnablePagingReceiver = 1;

	// Start out with no handsets registered
	SpectralinkDeleteAllPortNumbers ();

	// Initialize index to Handsets Array
	SL_HandsetsIndex = 0;

	// Initialize Heartbeat timer
	SL_HeartBeat = 0;

}


/***********************************************************
	Subroutine:	SpectralinkActivatePrintOut()

	Description:
		This routine will activate the state machine that prints out the phone ports
        
	Inputs:		

	Outputs:

	Locals:	
			
*************************************************************/
void SpectralinkActivatePrintOut ()
{
	SL_PrintIndex = 0;
	SL_PrintOutState = SL_PRINT_ACTIVE;
}

/***********************************************************
	Subroutine:	SpectralinkWaitForBuffer()

	returns:  TRUE - transmit buffer empty
			  FALSE- transmitter still active

*************************************************************/
uint8 SpectralinkWaitForBuffer ()
{
	return (!Uart [UART_2].TransmitActive);
}



/***********************************************************
	Subroutine:	SpectralinkAlarmExecutive()

	Description:
		This routine will print out the list of all active phone ports
        
	Inputs:		

	Outputs:

	Locals:	
			
*************************************************************/
void SpectralinkPortPrintOutExecutive ()
{
	// Which spectralink alarm state are we in?
		PetWatchdog ();
	switch (SL_PrintOutState)
		{
		// No printing activity
		case SL_PRINT_IDLE:
			break;

		// Find the next port number to print
		case SL_PRINT_ACTIVE:
			while (SL_PrintIndex < SL_NUMBER_OF_HANDSETS)
				{
				//PetWatchdog ();
				// If the next slot has an active handset, print it out.
				if (SpectralinkHandsets [SL_PrintIndex] != SL_END_OF_FILE)
					{
					// Convert the slot number from binary to ASCII
					UIntToAscii( szReceiverNumber, SL_PrintIndex+1 );
					strcpy ((char *) szReceiverNumber, (const char *)  &szReceiverNumber [2]);
					// Add a comma
					szReceiverNumber [3] = ',';
					szReceiverNumber [4] = 0;

					// Convert the port number from binary to ASCII
					UIntToAscii( acTemp, SpectralinkHandsets [SL_PrintIndex++] );
					strcat ((char *) szReceiverNumber, (const char *) acTemp);
					// Transmit the slot and Ascii port number to the application
    				AdminMessage ((char *) szReceiverNumber);

					// wait for printing to finish before sending the next active handset
					SL_PrintOutState++;
					return;
					}
				 SL_PrintIndex++;
				 }
			// Entire list of handsets has been searched and printed
			SL_PrintOutState = SL_PRINT_IDLE;
			break;

		// Wait for printing to be completed
		case SL_PRINT_WAIT:
			if (!Uart [UART_0].TransmitActive)
				{
				// printing of previous message is finished.
				SL_PrintOutState--;
				}
			break;
		}
}


#endif


/* ======== END OF FILE ======== */

