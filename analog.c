/***********************************************************
	Module Name:	Analog.c

	Author:			Bob Halliday, July, 2006

	Description:	
	This Module provides the Analog drivers for the system:
	Once every 2 seconds, the Analog system is turned on, and the 
	4 analog input channels are activated using BURST Mode.
	8 samples are read over 160ms, then averaged to produce a result.

	Subroutines:		
					AnalogInitialize ()

	Revision History:


*************************************************************/
#define BODY_ANALOG

#include <stdio.h>
#include <string.h>
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"

#include "analog.h"
#include "config.h"
#include "i2c.h"
#include "isr.h"
#include "inits.h"
#include "modem.h"
#include "rf.h"
#include "uart.h"
#include "utils.h"



/***********************************************************
	Subroutine:	AnalogInitialize()

	Description:
    	Initialize the Analog drivers at power up.
		Set the analog 10-second timer to expire immediately

	Inputs:		

	Outputs:

	Locals:		
*************************************************************/
void AnalogInitialize ( )
{
	// Set the 10-second timer to expire at once
	Timer.Analog = ANALOG_READ_CYCLE_TIME;

	// Activate the analog state machine
	Analog.State = ANALOG_START_WAIT;

	// initialize RSSI results
	Analog.Results [ANALOG_RSSI] = 0x280;
}



/***********************************************************
	Subroutine:	AnalogExecutive()

	Description:
    	This high level routine controls the executive functions of the Analog System
		If a host PC ever writes new data to our EEprom, this function will read
		all the new data out into Cache RAM.

	Inputs:		
		 Analog.State

	Outputs:
		 Analog.State

	Locals:
			
*************************************************************/
void AnalogExecutive ( )
{
	// This module disabled initially at power up
	if (Modem.State == MODEM_SYSTEM_POWER_UP)
		return;

	// service red LED
	AnalogCheckAcLeds ();

	// Which state are we in?
	switch (Analog.State)
		{
		case ANALOG_DISABLED:
			break;
		
		// Start of transaction - look for a transaction request
		case ANALOG_START_WAIT:
			// Check for end of delay
			if (Timer.Analog >= ANALOG_READ_CYCLE_TIME)
 			 	{
 			 	// It's time to reactivate the analog subsystem
				Timer.Analog = 0;
				AnalogReactivateSystem ();
				Analog.State++;
				}
			break;

		case ANALOG_READ_CHANNELS:
			if ( TimersTimerIsExpired ( ANALOG_TIMER, ANALOG_READBACK_DELAY ))
		 		{
				// It's time to read all 4 analog channels again
				TimersRestartTimer (ANALOG_TIMER);
				Analog.State++;
				}
			break;

		case ANALOG_FINISHED_READING:
			Analog.State = AnalogTakeReadings ();
			break;
		}
}



/***********************************************************
	Subroutine:	AnalogReactivateSystem()

	Description:
    	This routine will reactivate the Analog subsystem:
			1. Run the clock at 3.6864 Mhz
			2. Run in Burst Mode
			3. Read and average 8 samples, each 10ms apart

	Inputs:		
		 Analog.State

	Outputs:
		 Analog.State

	Locals:
			
*************************************************************/
void AnalogReactivateSystem ( )
{
	uint32 i;

	// restart the time between samples
	TimersRestartTimer (ANALOG_TIMER);

	// Set the Analog Control Register
	ADCR = ANALOG_SET_CONTROL;

	// clear the collection registers
	for ( i=0; i < (ANALOG_NUM_CHANNELS-1); i++ )
		{
		Analog.Readings [i] = 0;
		}

	// clear the number of samples counter
	Analog.Count = 0;
}



/***********************************************************
	Subroutine:	AnalogTakeReadings()

	Description:
    	This routine will read the analog results into a set of collection registers.
		4 channels are read:
		 AD0 - DC voltage
		 AD1 - Battery voltage
		 AD2 - RSSI
		 AD3 - Temperature

	Inputs:
		Analog.Readings		
		Analog.Count

	Outputs:
		Analog.Readings
		Analog.Count

	returns:
		Analog.State
				

	Locals:		
*************************************************************/
uint8 AnalogTakeReadings ( )
{
	uint32 AnalogInput;
	uint32 i;

	// Read AC voltage on AD0
	AnalogInput = (ADDR0 >> 6) & ANALOG_10_BITS;
	Analog.Readings [ANALOG_AC] += AnalogInput;

	// Read Battery voltage on AD1
	AnalogInput = (ADDR1 >> 6) & ANALOG_10_BITS;
	Analog.Readings [ANALOG_BATTERY] += AnalogInput;

	// Read RSSI on AD2 - do this only in the 5ms Timer interrupt
//	AnalogInput = (ADDR2 >> 6) & ANALOG_10_BITS;
//	Analog.Readings [ANALOG_RSSI] += AnalogInput;

	// Read Temperature on AD6
	AnalogInput = (ADDR6 >> 6) & ANALOG_10_BITS;
	Analog.Readings [ANALOG_TEMPERATURE] += AnalogInput;

	if ( ++Analog.Count >= ANALOG_NUM_READINGS )
		{
		// All samples have been collected - save them
		for ( i=0;  i < (ANALOG_NUM_CHANNELS-1);  i++ )
			{
			// Store the average of 8 readings in the results
			Analog.Results [i] = Analog.Readings [i] >> 3;
			}

		// turn off the analog subsystem
//		ADCR = ANALOG_POWER_DOWN;
		// Convert temperature reading to Fahrenheit
		AnalogConvertToFahrenheit ();
		// Make sure to check for proper battery power
		AnalogCheckBattery ();
		// Make sure to check for AC present
		AnalogCheckAc ();

		// debug line:  print the analog results out COM0
		AnalogPrintValues (UART_0);
		return ANALOG_START_WAIT;
		}

	return ANALOG_READ_CHANNELS;

}



/***********************************************************
	Subroutine:	AnalogPrintValues()

	Description:
		Print the 4 analog results
        
	Inputs:
		UartTransmitState

	Outputs:
		UartTransmitState
		UartArrayTransmit []

	Locals:	
			
*************************************************************/
void AnalogPrintValues (uint8 UartChannel)
{
	uint32 i, j;

	// clear the collection registers
	j = 0;
	for ( i=0; i < ANALOG_NUM_CHANNELS; i++ )
		{
		Uart [UartChannel].TransmitArray [j++] = BinHexAscii (Analog.Results [i] >> 8);
		Uart [UartChannel].TransmitArray [j++] = BinHexAscii (Analog.Results [i] >> 4);
		Uart [UartChannel].TransmitArray [j++] = BinHexAscii (Analog.Results [i]);
		Uart [UartChannel].TransmitArray [j++] = BLANK;
		}
	Uart [UartChannel].TransmitArray [j] = NULL;
//	UartTransmitPacket (Uart [UartChannel].TransmitArray, UART_2, TRUE, (4 * ANALOG_NUM_CHANNELS));
}



/***********************************************************
	Subroutine:	AnalogCheckAc()

	Description:
		If AC power is missing, flash the RED Led once every 3/4 of a second
        
	Inputs:
		UartTransmitState

	Outputs:
		UartTransmitState
		UartArrayTransmit []

	Locals:	
			
*************************************************************/
void AnalogCheckAc ()
{
	if (Analog.Results [ANALOG_AC] < ANALOG_AC_PRESENT)
		{
		// AC is not present - send an alarm message once every 8 or 6 hours
		if (Timer.AcPowerHours >= Analog.AcReportTime)
			{
			// 6 or 8 hours have passed - report AC failure to the CRB
   			Config.BaseStatus |= STS_BIT_AC_POWER_LOST;
			// restart the hours timer
			Timer.AcPowerHours = 0;

			// check for changing our calling cycle:
			if (Analog.AcReportTime != ANALOG_AC_REPORT2)
				{
				// We're still calling once every 8 hours
				if (++Analog.AcReportCycle >= ANALOG_AC_REPORT_TIME_CHANGE)
					{
					// Change the AC error reporting time to once every 6 hours
					Analog.AcReportTime = ANALOG_AC_REPORT2;
					}
				}
			}

		}
	else
		{
		// Turn off timers when AC input is okay
		Timer.AcPowerHours = 0;
		Analog.AcReportCycle = 0;
		Analog.AcReportTime = ANALOG_AC_REPORT1;
		}

}


/***********************************************************
	Subroutine:	AnalogCheckAcLeds()

	Description:
		If AC power is missing, flash the RED Led once every 3/4 of a second
        
	Inputs:
		UartTransmitState

	Outputs:
		UartTransmitState
		UartArrayTransmit []

	Locals:	
			
*************************************************************/
void AnalogCheckAcLeds ()
{
	// Don't flash LEDs during a phone call
	if (Modem.StopNoAcFlashing)
		return;

	if (Analog.Results [ANALOG_AC] < ANALOG_AC_PRESENT)
		{
		// AC is not present - Flash the RED led
		if ( TimersTimerIsExpired ( ANALOG_NO_AC_TIMER, ANALOG_NO_AC_FLASH_TIME ))
			{
			TimersRestartTimer (ANALOG_NO_AC_TIMER);
			if (IOPIN & INITS_RED_LED_CONTROL)
				IOCLR = INITS_RED_LED_CONTROL;
			else
				IOSET = INITS_RED_LED_CONTROL;
			}
		}
	else
		{
		// Turn off Red LED when AC input is okay
		IOSET = INITS_RED_LED_CONTROL;
		}

}


/***********************************************************
	Subroutine:	AnalogCheckBattery()

	Description:
		If Battery power is too low, this routine will trigger a Base State call to the CRB,
		once every 24 hours
        
	Inputs:
		Analog.BatteryState
		Timer.BatteryMinutes

	Outputs:
		Analog.BatteryState
		Config.BaseStatus
		Timer.BatteryMinutes

	Locals:	
			
*************************************************************/
void AnalogCheckBattery ()
{
	// Which state are we in?
	switch (Analog.BatteryState)
		{
		case ANALOG_BATTERY_OK:
			if (Analog.Results [ANALOG_BATTERY] < ANALOG_OK_BATTERY_LEVEL )
				{
				// The battery has gotten too low
				Analog.BatteryState++;
				// Set the Battery Error Flag which will trigger a call to the Base unit
   				Config.BaseStatus |= STS_BIT_BATTERY_LIMIT;
				// Initialize the 24 hour timer
				Timer.BatteryMinutes = 0;
				}
			break;
		
		// Start of transaction - look for a transaction request
		case ANALOG_BATTERY_BAD:
			if (Analog.Results [ANALOG_BATTERY] >= ANALOG_OK_BATTERY_LEVEL )
				{
				// The battery has been restored to a healthy state
				Analog.BatteryState--;
				}
			else if (Timer.BatteryMinutes >= ANALOG_RESEND_BATTERY_TIME)
				{
				// Set the Battery Error Flag which will trigger a call to the Base unit
   				Config.BaseStatus |= STS_BIT_BATTERY_LIMIT;
				// Re-Initialize the 24 hour timer
				Timer.BatteryMinutes = 0;
				}
			break;

		default:
			Analog.BatteryState = ANALOG_BATTERY_OK;
		}
}




/***********************************************************
	Subroutine:	AnalogConvertToFahrenheit()

	Description:
		This routine will convert the 10-bit analog temperature reading to
		an 8-bit fahrenheit value.
		It will also watch for over- and under-temp conditions.
        
	Inputs:
		Analog.Results [ANALOG_TEMPERATURE]

	Outputs:
		Analog.Temperature

	Locals:
		temperature	
			
*************************************************************/
void AnalogConvertToFahrenheit ()
{
	uint8 temperature;
	uint16 scratchpad;

	scratchpad = Analog.Results [ANALOG_TEMPERATURE] >> 2;
	temperature = (uint8) scratchpad;
	if (temperature > TEMP_MIN_AD_READING)
	   	temperature -= TEMP_MIN_AD_READING;  		// adjust zero point of temp
	else
	   	temperature = 0;
	if (temperature >= TEMP_TABLE_SIZE)
	   	temperature = TEMP_TABLE_SIZE-1;
	   	        
	Analog.Temperature = ToFahrenheit [temperature];

	// check for overtemp:
	if (Analog.OverTempFlag)
		{
		// We are already in an overtemp condition - check the one-hour timer
		if (Timer.TemperatureMinutes >= ANALOG_TEMP_REPORTING_TIME)
			{
			// One hour has expired
			if ((temperature > Config.BaseOperationalData [BASE_OP_HIGH_LIMIT0]) 
			 || (temperature < Config.BaseOperationalData [BASE_OP_LOW_LIMIT0]))
				{
				// Re-report an overtemp condition
				Timer.TemperatureMinutes = 0;
				// Set the Overtemp Error Flag which will trigger a call to the Base unit
   				Config.BaseStatus2 |= STS_BIT_OVERTEMP;
				}
			else
				{
				// turn off temperature error flag
				Analog.OverTempFlag = FALSE;
				}
			}
		}
	else
		{
		// check for over or under temperature
		if ((temperature > Config.BaseOperationalData [BASE_OP_HIGH_LIMIT0]) 
		 || (temperature < Config.BaseOperationalData [BASE_OP_LOW_LIMIT0]))
			{
			// Report an overtemp condition
			Timer.TemperatureMinutes = 0;
			Analog.OverTempFlag = TRUE;
			// Set the Overtemp Error Flag which will trigger a call to the Base unit
   			Config.BaseStatus2 |= STS_BIT_OVERTEMP;
			}
		}
}
 


/* ======== END OF FILE ======== */
