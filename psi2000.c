/*************************************************
*
*
*       PSI2000.C
*
*****************************************************/
#include "system.h"
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"
#include "inits.h"
#include "psi2000.h"
#include "admin.h"
#include "sbus.h"
#include "rtc.h"
#include "database.h"
#include "factory.h"
#include "sio.h"
#include "utils.h"
#include "mprocess.h"
#include "carts.h"
#include "lapage.h"
#include "sio.h"
#include "uart.h"
#include "diags.h"

#include <stdio.h>
#include <string.h>


#ifdef SPECTRALINK
#include "spectralink.h"
#endif

				    

uint8 HostCheckInFinished;

const uint8 szCheckInMessage[22] = "0000000000E1A\r\n";
const uint8 szPagerCheckInMessage[7] = "ATI\r";
const uint8 szVersionMessage[40] = "lrbin541 01/03/2018";  //
char szVersion[] = "CAR Debugger V5.41 (build 18) ";
const uint8 szFallDetectedEnabled[] = " FD Enabled";
const uint8 psiCRLF[] = "\r\n";
const uint8 temp_date_time[] = "109/14/09 11:11:11";               

/*************************************************************************/

void HeartBeat()
{
	// v5.03 - 03/11/08 - Check to see if we are in Factory Mode
	if (CpacFactoryMode)
		return;

//------------------------------------------------------
	// bh - v4.06 - send a heartbeat message to the Pager once every 5 seconds
	if ( PagerHeartBeatTimer >= PAGER_HEARTBEAT_TIME )
		{
		// 5 second timeout for pager heartbeat
		PagerHeartBeatTimer = 0;
		if (gucstandalone_mode)
			{
#ifdef SPECTRALINK
			if (!SL_SpectralinkActive)
#endif
        		SioSetMessage( UART_2, (uint8 *) szPagerCheckInMessage, strlen ((const char *) szPagerCheckInMessage ) );
			}
		}
//------------------------------------------------------------

// v5.12 - restore this legacy heartbeat
//------v5.13 - remove this legacy heartbeat
//	if ( StateOfHealthTimer < 24000 )
//    	return;

//    StateOfHealthTimer = 0;
	// Don't xmit during while Logged On, or during a points download
//    if (gucAdminMode || gucAdminTrapFlag)
//		return;
//    SioSetMessage( UART_0, (uint8 *) szCheckInMessage, strlen( (const char *)szCheckInMessage ) );
//------------------------------------------------------------
}





/*************************************************************************/

void HostCheckIn()
{
	const uint8 szHT[] = "\x09";    
    unsigned char  aucData[ (RTC_DATA_LENGTH * 2) + 4 ];     
    const uint8 szP3192K[] = "P3=19.2K ";         
    const uint8 szP5192K[] = "P5=19.2K";         
    const uint8 szCRLF2[] = "\r\n\r\n";

        // We only need to do Host Check In once at power up.
	if (HostCheckInFinished)
    	{
        return;
        }

    HostCheckInFinished = TRUE;

	RTCGetData( aucData );
    SioSetMessage( UART_0, (uint8 *) psiCRLF, strlen ((const char *) psiCRLF ) );   
    // bh - 02/20/06 -don't print a NULL return string
//    if (aucData [0])
//    	SioSetMessage( UART_0, aucData, strlen ((const char *) aucData ) - 1  );
	// temporary code   
    SioSetMessage( UART_0, (uint8 *) temp_date_time, strlen ((const char *) temp_date_time ) );
	// end of temporary code

    SioSetMessage( UART_0, (uint8 *) szHT, strlen ((const char *) szHT )       );   
    SioSetMessage( UART_0, (uint8 *) szHT, strlen ((const char *) szHT )   );   

    SioSetMessage( UART_0, (uint8 *) szCheckInMessage, strlen ((const char *) szCheckInMessage ) );
    // bh - 10/25/07
	// debug - test - v5.12
#ifdef WATCHDOG
	if (WDMOD & INITS_WATCHDOG_RESET)
		{
		// We suffered a Watchdog Reset
    	SioSetMessage( UART_0, (uint8 *) szVersionMessage, strlen ((const char *) szVersionMessage ) );
		// Clear the watchdog reset flag
		WDMOD = INITS_WATCHDOG_CONFIG;
		}
	else
		{
		// This was not a watchdog reset
	    SioSetMessage( UART_0, (uint8 *) szVersionMessage, strlen ((const char *) szVersionMessage ) );
		}
#else
	    SioSetMessage( UART_0, (uint8 *) szVersionMessage, strlen ((const char *) szVersionMessage ) );
#endif

    SioSetMessage( UART_0, (uint8 *) szHT, strlen ((const char *) szHT )       );   
    SioSetMessage( UART_0, (uint8 *) szHT, strlen ((const char *) szHT )   );   

	// v4.03 - rs-485 port always runs at 19.2K baud
    SioSetMessage( UART_0, (uint8 *) szP3192K, strlen ((const char *) szP3192K ) );

//	v4.04 - Restore the display of the Port 5 baud rate.
    SioSetMessage( UART_0, (uint8 *) szP5192K, strlen ((const char *) szP5192K ) );
    SioSetMessage( UART_0, (uint8 *) szCRLF2, strlen ((const char *)szCRLF2));
}





/*************************************************************************/
void PrintVersionNumber ()
{
     // bh - 10/25/05
	SioSetMessage( UART_0, (uint8 *) psiCRLF, strlen ((const char *) psiCRLF ) );   
    SioSetMessage( UART_0, (uint8 *) szVersionMessage, strlen ((const char *) szVersionMessage ) );
	if (FlashRamData.cSmartCareDoesFallDetector)
	    SioSetMessage( UART_0, (uint8 *) szFallDetectedEnabled, strlen ((const char *) szFallDetectedEnabled ) );
    SioSetMessage( UART_0, (uint8 *) psiCRLF, strlen ((const char *) psiCRLF ) );  
}


/**********************************************************************
	Routine:		ActiveLedInit ()
	
	Description:
		1. Toggle the Heartbeat LED.
		2. If either the superbus or SmartCare comm channel is dead, toggle the ERROR LED.  



   The LED on the back panel will signal:
		OFF 		- no errors
		Solid ON 	- System is in the BookBlock
		Flashing	- no communication on the RS-485 bus or on the SmartCare Bus


*************************************************************************/
void ActiveLedInit()
{
	static uint8 ActiveLed;

	if (LedTimer < LedTimeOut )
    	return;

    // restart led timer on timeout
    LedTimer = 0;

    // Toggle the flashing led
    ActiveLed ^= TRUE;

	//If we lose the superbus then clear all receiver types
	if (!SuperbusMonitorTimer) memset (cReceiverType,0,255);


	// v5.03 - 12/12/07
	// With no comm errors, the ERROR led on the back panel should remain off.
	FIO0CLR = INITS_STATUS_LED;
    if (ActiveLed)
    	{
        // Set Heartbeat LED to OFF 
		FIO0CLR = INITS_HEARTBEAT_LED;
		// If the rs-485 channel is dead, 
		//  or the SmartCare channel is dead, flash the status led
		if (!SuperbusMonitorTimer || gucstandalone_mode)
			FIO0CLR = INITS_STATUS_LED;
        }
	else
    	{
        // Set Heartbeat LED to ON 
		FIO0SET = INITS_HEARTBEAT_LED;
		// If the rs-485 channel is dead, flash the status led
		if (!SuperbusMonitorTimer || gucstandalone_mode)
			{
			FIO0SET = INITS_STATUS_LED;
			if (gucstandalone_mode)
			   iSmartCareTimeouts++;
					else iCartTimeouts++;	//rv 12-21-4 this is where a receiver did not respond 
			}
        }


    // set to ON time
    LedTimeOut = 20;
    if (ActiveLed)
    	{
        // Set to OFF time
        LedTimeOut = 180;
        }
}



/*****************************************************

        Subroutine:     Psi2000_ClearRam ()

        Description:   Clear all of RAM to zeroes at power up.
                                   This routine is called from init.a51
                                   If it is called from this source file, then it's too late,
                                   and initialized variables will get destroyed.


*******************************************************/
extern uint32 Stack_Mem;
void Psi2000_ClearRam ()
{
	// The linker sets RamPtr into the R6-R7 register pair
     uint32 * RamPtr;

    // Now we must clear all of external RAM to zeroes
    for ( RamPtr = (uint32 *) START_OF_RAM;  RamPtr < (uint32 *) &Stack_Mem;  RamPtr++ )
    	{
        *RamPtr = 0;
        }
}



