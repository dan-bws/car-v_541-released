/***********************************************************
	Module Name:	Config.c

	Author:			Bob Halliday, July, 2006

	Description:	
	This Module provides the Config communications drivers for the system:
	Read or Write 1-32 Bytes
	The 2104 is the Master of the Config Bus

	Subroutines:		
					ConfigInitialize ()

	Revision History:


*************************************************************/
#define BODY_CONFIG

#include <stdio.h>
#include <string.h>
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"

#include "analog.h"
#include "config.h"
#include "i2c.h"
#include "isr.h"
#include "inits.h"
#include "modem.h"
#include "rf.h"
#include "sensors.h"
#include "uart.h"
#include "utils.h"



/***********************************************************
	Subroutine:	ConfigInitialize()

	Description:
    	Initialize the Config drivers at power up

	Inputs:		

	Outputs:

	Locals:		
*************************************************************/
void ConfigInitialize ( )
{
	// set default number of sensors
	Config.NumberOfSensors = SENSORS_NUMBER_OF_SENSORS;

	// Read in EEprom variables to cache RAM
	ConfigUpdateSystemVariables ();

	// Set the Power Up Status which will trigger a call to the Base unit
   	Config.BaseStatus |= STS_BIT_POWERUP;

}



/***********************************************************
	Subroutine:	ConfigExecutive()

	Description:
    	This high level routine controls the executive functions of the Config System
		If a host PC ever writes new data to our EEprom, this function will read
		all the new data out into Cache RAM.

	Inputs:		
		 Config.State

	Outputs:
		 Config.State

	Locals:
			
*************************************************************/
void ConfigExecutive ( )
{
	// Which state are we in?
	switch (Config.State)
		{
		case CONFIG_DISABLED:
			break;
		
		// Start of transaction - look for a transaction request
		case CONFIG_START_WAIT:
			// Check for end of delay
			if ( TimersTimerIsExpired ( CONFIG_READBACK_TIMER, CONFIG_READBACK_DELAY ))
 			 	{
 			 	// Wait to start Readback has expired -- Read key EEprom variables into RAM 
				ConfigUpdateSystemVariables ();
				Config.State--;
				}
			break;

		case CONFIG_READ_EEPROM:
			break;

		case CONFIG_FINISHED_READING:
			break;
		}
}



/***********************************************************
	Subroutine:	ConfigTriggerEEpromReadback()

	Description:
    	This routine will trigger a comprehensive EEprom readback 4 seconds after this call.

	Inputs:		
		 Config.State

	Outputs:
		 Config.State

	Locals:
			
*************************************************************/
void ConfigTriggerEEpromReadback ( )
{
	// restart the config Readback Timer
	TimersRestartTimer (CONFIG_READBACK_TIMER);

	// Start waiting for 4 seconds
	Config.State = CONFIG_START_WAIT;
}



/***********************************************************
	Subroutine:	ConfigUpdateSystemVariables()

	Description:
    	This routine will read system variables out of EEprom
		and cache them in RAM

	Inputs:		

	Outputs:

	Locals:		
*************************************************************/
void ConfigUpdateSystemVariables ( )
{
	// Read the number of sensors
	I2cSetTransaction (1, I2C_RESUME_ADDRESS_LOW+I2C_READ, I2C_NUMBER_OF_SENSORS, 
   						(uint8 *)&Config.NumberOfSensors, (uint32 *) 0);

	// Read the Callback interval
	I2cSetTransaction (1, I2C_RESUME_ADDRESS_LOW+I2C_READ, I2C_CALLBACK_INTERVAL, 
   						(uint8 *)&Config.CallbackIntervalDays, (uint32 *) ConfigConvertCallbackInterval);

	// Read the Base Operational Data
	I2cSetTransaction (CONFIG_BASE_SIZE, I2C_RESUME_ADDRESS_LOW+I2C_READ, I2C_BASE_OPERATIONAL_DATA, 
   						(uint8 *)&Config.BaseOperationalData, (uint32 *) ConfigCheckBaseOperationalData);

	// Read the phone number (16 bytes), followed by the backup phone number (16 bytes)
	I2cSetTransaction (MODEM_PHONE_NUMBER_SIZE, I2C_RESUME_ADDRESS_LOW+I2C_READ, I2C_PHONE_NUMBER, 
   						(uint8 *)&Modem.BcdPhoneNumber, (uint32 *) ConfigDeconstructPhoneNumber);

	// Read the Serial Number of the first sensor
	Config.Index = 0;
	I2cSetTransaction (CONFIG_SENSOR_SIZE, I2C_RESUME_ADDRESS_LOW+I2C_READ, I2C_SENSOR1_OPERATIONAL_DATA, 
   						(uint8 *)&Config.SerialNumber, (uint32 *) 0);

	// Read all the Sensor Data from EEprom
	I2cSetTransaction (SENSORS_DATA_SIZE, I2C_RESUME_ADDRESS_LOW+I2C_READ, 
					    I2C_BASE_EVENT_QUEUE + ((Config.Index+1) << 4), 
   					    (uint8 *)&SensorsMatrix [Config.Index], (uint32 *) ConfigReadAllSensorSerialNumbers);

}


/***********************************************************
	Subroutine:	ConfigConvertCallbackInterval()

	Description:
    	The Callback Interval was just read from EEprom.
		It must be range checked - it should be in the range of 1 - 7 days.
		Then it is converted to hours.

	Inputs:		
		Config.CallbackIntervalDays

	Outputs:
		Config.CallbackIntervalMinutes

	Locals:		
*************************************************************/
void ConfigConvertCallbackInterval (uint32 NakCount )
{
	uint32 temp;

	// If the EEprom is dead, use 1 day as the default value
	if (NakCount)
		{
		// Default value equals one day
		Config.CallbackIntervalDays = 1;
		Config.NumberOfSensors = 0;
		}

	// Range Check the Number of Sensors
	if ( Config.NumberOfSensors > SENSORS_NUMBER_OF_SENSORS )
		{
		// Default Number of Sensors equals 0 
		Config.NumberOfSensors = 0;
		Config.NumberOfSensorsBackup = 0;
		// Write the number of sensors
		I2cSetTransaction (2, I2C_RESUME_ADDRESS_LOW, I2C_NUMBER_OF_SENSORS, 
   							(uint8 *)&Config.NumberOfSensors, (uint32 *) 0);
		}

	// Range check the CallBack Interval
	if ((Config.CallbackIntervalDays > CONFIG_MAX_CALLBACK_INTERVAL)
	 || (Config.CallbackIntervalDays == 0))
		{
		// Illegal upper value found
		Config.CallbackIntervalDays = 1;
		// Write the Callback Interval to EEprom
		I2cSetTransaction (1, I2C_RESUME_ADDRESS_LOW, I2C_CALLBACK_INTERVAL, 
   							(uint8 *)&Config.CallbackIntervalDays, (uint32 *) 0);
		}

	// Convert Days to Minutes
	temp = (uint32) Config.CallbackIntervalDays;
	Config.CallbackIntervalMinutes = temp * (24*60);
}






/***********************************************************
	Subroutine:	ConfigCheckBaseOperationalData()

	Description:
    	The Base Operational Data was just read from EEprom.
		The temperature out of range values must be checked 

	Inputs:		
		Config.BaseOperationalData [ ]

	Outputs:
		Config.BaseOperationalData [ ]

	Locals:		
*************************************************************/
void ConfigCheckBaseOperationalData (uint32 NakCount )
{
	// If the EEprom is dead, go to default Base Operational Data
	if (NakCount)
		{
		// Default values 
		Config.BaseOperationalData [BASE_OP_LOW_LIMIT0] = ANALOG_TEMP_LOW;
		Config.BaseOperationalData [BASE_OP_HIGH_LIMIT0]= ANALOG_TEMP_HIGH;
		return;
		}

	// validate the temperature rails
	if ((Config.BaseOperationalData [BASE_OP_LOW_LIMIT0] < ANALOG_TEMP_LOW)
	 || (Config.BaseOperationalData [BASE_OP_LOW_LIMIT0] > ANALOG_TEMP_HIGH))
		{
		// if out of range, set temperature low limit to default value.
		Config.BaseOperationalData [BASE_OP_LOW_LIMIT0] = ANALOG_TEMP_LOW;
		}
	if ((Config.BaseOperationalData [BASE_OP_HIGH_LIMIT0] < ANALOG_TEMP_LOW)
	 || (Config.BaseOperationalData [BASE_OP_HIGH_LIMIT0] > ANALOG_TEMP_HIGH))
		{
		// if out of range, set temperature high limit to default value.
		Config.BaseOperationalData [BASE_OP_LOW_LIMIT0] = ANALOG_TEMP_HIGH;
		}

	// inspect the crc bytes for errors
	ConfigInspectData (Config.BaseOperationalData);
}





/***********************************************************
	Subroutine:	ConfigDeconstructPhoneNumber()

	Description:
    	The phone number has just been read out of EEprom.
		It is in BCD format - 2 digits per byte.
		It must be deconstructed into ASCII format, one digit per byte in Ascii.
		A <CR>, followed by a NULL, must be stored at the end of the phone string.

	Inputs:		
		Modem.PhoneNumber - the BCD version is found at the halfway point in the array.

	Outputs:
		Modem.PhoneNumber

	Locals:		
*************************************************************/
void ConfigDeconstructPhoneNumber (uint32 NakCount )
{
	// No processing allowed if NakCount is nonzero
	if (NakCount)
		return;

	ConfigConvertBcdToAsciiPhoneNumber (Modem.BcdPhoneNumber);
}



/***********************************************************
	Subroutine:	ConfigConvertBcdToAsciiPhoneNumber()

	Description:
    	The phone number has just been read out of EEprom.
		It is in BCD format - 2 digits per byte.
		It must be deconstructed into ASCII format, one digit per byte in Ascii.
		A <CR>, followed by a NULL, must be stored at the end of the phone string.

	Inputs:		
		FromBcd - the BCD version of the phone number

	Outputs:
		Modem.PhoneNumber -- The ascii version of the phone number

	Locals:		
*************************************************************/
void ConfigConvertBcdToAsciiPhoneNumber (uint8 * FromBcd )
{
	uint32 i,j;

	// set index to start of BCD phone number
	j = 0;

	// convert 15 BCD bytes to 30 ASCII bytes
	for ( i = 0;  i < (MODEM_PHONE_NUMBER_SIZE-2); i++ )
		{
		Modem.PhoneNumber [i] = (FromBcd [j] >> 4) | ASCII;
		if (Modem.PhoneNumber [i] == CONFIG_END_OF_PHONE_STRING)
			break;
		i++;
		Modem.PhoneNumber [i] =  (FromBcd [j++]   & 0x0F) | ASCII;
		if (Modem.PhoneNumber [i] == CONFIG_END_OF_PHONE_STRING)
			break;
		}

	// put a <CR>, followed by a NULL at the end of the string
	Modem.PhoneNumber [i++]  =  CR;
	Modem.PhoneNumber [i]  =  NULL;
}


/***********************************************************
	Subroutine:	ConfigReadAllSensorSerialNumbers()

	Description:
    	All 15 Sensor Serial Numbers are cached in RAM for easy access.
		This routine transfers each serial number out of EEprom and into RAM, one at a time.
		The receive data is 3 chars.  These get merged into a 32-bit word.

	Inputs:		
		Config.Index
		Config.SerialNumber []

	Outputs:
		Config.Index
		Config.SensorSerialNumber []

	Locals:		
*************************************************************/
void ConfigReadAllSensorSerialNumbers (uint32 NakCount )
{
	uint16 address;
	uint32 serial_number;

	// No processing allowed if NakCount is nonzero or there are no sensors registered
	if (NakCount || !Config.NumberOfSensors)
		return;

	// inspect the crc bytes for errors
	ConfigInspectData (Config.SerialNumber);

	// get the msb serial number
	serial_number = (uint32) Config.SerialNumber [0];
	serial_number <<= 8;
	// get the middle serial number
	serial_number |= (uint32) Config.SerialNumber [1];
	serial_number <<= 8;
	// get the lsb serial number
	serial_number |= (uint32) Config.SerialNumber [2];
	// Okay, now save the 24 bit value in the array
	Config.SensorSerialNumber [Config.Index++] = serial_number;

	// When 15 sensors have been processed, we are done
	if (Config.Index >= Config.NumberOfSensors)
		return;

	// Calculate the next EEprom address to read
	address = I2C_SENSOR1_OPERATIONAL_DATA + (Config.Index * CFG_EEPROM_OPER_RECORDSIZE);

	// Read the next sensor serial number
	I2cSetTransaction (CONFIG_SENSOR_SIZE, I2C_RESUME_ADDRESS_LOW+I2C_READ, address, 
   						(uint8 *)&Config.SerialNumber, (uint32 *) 0);

	// Read the next Sensor Data Record from EEprom
	I2cSetTransaction (SENSORS_DATA_SIZE, I2C_RESUME_ADDRESS_LOW+I2C_READ, 
					    I2C_BASE_EVENT_QUEUE + ((Config.Index+1) << 4), 
   						(uint8 *)&SensorsMatrix [Config.Index], (uint32 *) ConfigReadAllSensorSerialNumbers);

}



/***********************************************************
	Subroutine:	ConfigSetBaseStatus()

	Description:
    	The Self Test Button was just pressed.
		This routine will construct the Base Status byte.

	Inputs:		
		Analog.Results [ ]
		Analog.OverTempFlag

	Outputs:
		Config.BaseStatus

	Locals:		
*************************************************************/
void ConfigSetBaseStatus ()
{
	// Set the self-test status condition
	Config.BaseStatus |= STS_BIT_SELFTEST;
	// Check the AC present condition
	if (Analog.Results [ANALOG_AC] < ANALOG_AC_PRESENT)
		{
		// AC is not present - send an alarm message 
		Config.BaseStatus |= STS_BIT_AC_POWER_LOST;
		}
	// Check the conditon of the battery
	if (Analog.Results [ANALOG_BATTERY] < ANALOG_OK_BATTERY_LEVEL )
		{
		// Set the Battery Error Flag 
		Config.BaseStatus |= STS_BIT_BATTERY_LIMIT;
		}
	// Check the temperature status
	if (Analog.OverTempFlag)
		{
 		// Set the Overtemp Error Flag 
 		Config.BaseStatus2 |= STS_BIT_OVERTEMP;
 		}
}




/***********************************************************
	Subroutine:	ConfigInspectData()

	Description:
    	The Self Test Button was just pressed.
		This routine will construct the Base Status byte.

	Inputs:		
		Analog.Results [ ]
		Analog.OverTempFlag

	Outputs:
		Config.BaseStatus

	Returns:
		TRUE - CRC tests passed
		FALSE- CRC tests failed		
*************************************************************/
uint8 ConfigInspectData (uint8 * Buffer)
{
	uint8 i, j;

   	// check Device CRC
   	i = Data_Calc_CRC (CONFIG_DEVICE_SIZE, Buffer);
   	// Check limit CRC                    
   	j = Data_Calc_CRC (CONFIG_LIMIT_SIZE, Buffer + CONFIG_DEVICE_SIZE);  
   	if ( j || i )
   		{
		Config.BaseStatus |= STS_BIT_MEM;
      	return FALSE;
   	}
   	return TRUE;
}


/* ======== END OF FILE ======== */
