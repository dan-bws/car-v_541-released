/***********************************************************
	Module Name:	download.c

	Author:			Bob Halliday, July, 2005
					bohalliday@aol.com    (781) 863-8245

	Description:
	Firmware Download operations are supported by this file

	
	Subroutines:	DownloadInitialization()
					Download5MsInterrupt ()
                    DownloadRestartTimer ()
                    DownloadTimerIsExpired ()

	Revision History:


*************************************************************/
#define BODY_DOWNLOAD


#include <string.h>

#include "main.h"
#include "timers.h"  

#include "download.h"
#include "icp.h"
#include "labview.h"



/***********************************************************
	Subroutine:	DownloadInitialize()

	Description:

	Inputs:

	Outputs:
		
	Locals:
	
*************************************************************/
void DownloadInitialize (void)
{

}


/***********************************************************
	Subroutine:	DownloadProcessIntelHex()

	Description:
	This routine will convert an Intel Hex String to binary
	and store it at the appropriate offset in flash memory.

	Inputs:
		DownloadArray []
		LabviewDataLength
		
	Outputs:
		returns:
			TRUE - data processed successfully
			FALSE- data error

	Locals:
*************************************************************/
uint8 DownloadProcessIntelHex ( uint8 * DownloadArray )
{
	uint8 i, j, Packed;
	uint8 HexCount, Type;
	uint16 Address;

	// First byte of string must be a colon
	if (DownloadArray [ICP_CONTROL_BYTE] != DOWNLOAD_COLON)
		{
		// colon is missing - we have a failure
		return FALSE;
		}

	// get the binary count
	HexCount = DownloadPack (DownloadArray [ICP_ADDRESS_LOW], DownloadArray [ICP_ADDRESS_HIGH]);
	// get the high address
	Address = (uint16) (DownloadPack (DownloadArray [ICP_NUMBER_OF_BYTES], DownloadArray [ICP_START_OF_DATA])) << 8;
	// get the low address
	Address |= (uint16) DownloadPack (DownloadArray [ICP_START_OF_DATA+1], DownloadArray [ICP_START_OF_DATA+2]);

	// Addresses for Configuration Data or EEprom must be halved:
	if ( Address >= DOWNLOAD_CONFIG_ADDRESS )
		{
		// Divide the address in half - i.e., 0x400E = 0x2007
		Address >>= 1;
		}

	// get the binary type
	Type = DownloadPack (DownloadArray [ICP_START_OF_DATA+3], DownloadArray [ICP_START_OF_DATA+4]);

	// Convert ascii string to binary string
	for ( i=0, j=ICP_START_OF_DATA+5;
		  i < HexCount;
		  i++,  j+=2 )
		{
		Packed = DownloadPack (DownloadArray [j], DownloadArray [j+1]);
		if (i & 0x01)
			DownloadArray [i-1] = Packed;	// save the LSB		
		else
			DownloadArray [i+1] = Packed;	// save the MSB
		}

	// Now write the binary string to Flash memory
	if ( !Type && HexCount )
		{
		return (DownloadWriteBinaryToFlash (DownloadArray, Address, HexCount));
		}

	return TRUE;
}


/*******************************************************************************
	Function:	DownloadPack()

	Description:
	This subroutine will convert two ASCII characters to one binary value

	Inputs:
		CharHigh
		CharLow

	Outputs:
		return character

	Locals:
********************************************************************************/
uint8 DownloadPack (uint8 CharHigh, uint8 CharLow)
{
	uint8 ReturnChar;

	// Convert Ascii letters to binary values
	if (CharHigh >= ASCII_A)
		CharHigh -= ASCII_CONVERT;
	if (CharLow >= ASCII_A)
		CharLow -= ASCII_CONVERT;

	ReturnChar =  (CharHigh & DOWNLOAD_MASK_HIGH) << 4;
	ReturnChar |= (CharLow & DOWNLOAD_MASK_HIGH);
	return ReturnChar;
}




/*******************************************************************************
	Function:	DownloadWriteBinaryToFlash()

	Description:
	This subroutine will write an array of binary values
	to FLASH memory.
	The received address value should be offset by 0x5000

	Inputs:
		Buffer - new data to be written to Flash
		Address
		Count

	Outputs:
		TRUE  - Write to Flash was successful
		FALSE - Write to Flash was unsuccessful

	Locals:
********************************************************************************/
uint8 DownloadWriteBinaryToFlash ( uint8 * Buffer, uint16 Address, uint8 Count)
{
	uint8 rom * FlashAddress;
	uint8 i, j;
	uint8 Status;

	// Start by copying 32 words, or 64 bytes, out of Flash
	FlashAddress = (uint8 rom *) DOWNLOAD_START_OF_FLASH_IMAGE + (Address & DOWNLOAD_PAGE_MASK);
	for ( i = 0;  i < DOWNLOAD_BLOCK_SIZE;  i++ )
		{
		DownloadImage [i] = *FlashAddress++;
		}

	// Now copy the new data into the download image
	j = Address & DOWNLOAD_MEMORY_MASK;
	for ( i = 0;  i < Count; i++ )
		{
		DownloadImage [j++] = Buffer [i];
		}

	for ( DownloadRetries = 0;  
		  DownloadRetries < DOWNLOAD_NUMBER_OF_RETRIES;
		  DownloadRetries++ )
		{
		// Write one page of memory
		FlashAddress = (uint8 rom *) DOWNLOAD_START_OF_FLASH_IMAGE + (Address & DOWNLOAD_PAGE_MASK);
		DownloadEraseBlock ( FlashAddress );

#ifdef F8722
		// For the 8722, write one 64-byte block of data
		DownloadWriteBlock ( FlashAddress, DownloadImage );
#else
		// For the 6520, write 8 blocks of 8 bytes each
		for ( i = 0;  i < DOWNLOAD_PAGE_SIZE;  i++ )
			{
			// Multiply j by 8
			j = i << 3;
			DownloadWriteBlock ( FlashAddress, &DownloadImage [j] );
			FlashAddress += DOWNLOAD_PAGE_SIZE;
			}
		FlashAddress = (uint8 rom *) DOWNLOAD_START_OF_FLASH_IMAGE + (Address & DOWNLOAD_PAGE_MASK);
#endif

		// Now we need to verify the results of the writes
		Status = DownloadVerifyBlock ( FlashAddress, DownloadImage, DOWNLOAD_BLOCK_SIZE ); 

		if (Status)
			{
			// on verify success, break out of retries loop
			break;
			}
		}

	// copy the first page of data to the return data buffer
	memcpy ((void *) LabviewResponseData,
			(const void *) DownloadImage, 
			(size_t) DOWNLOAD_BLOCK_SIZE);
	LabviewResponseDataCount = DOWNLOAD_BLOCK_SIZE;

	return Status;
}


/*******************************************************************************
	Function:	DownloadEraseBlock()

	Description:
	This subroutine will erase a block of 64 bytes

	Inputs:
		FlashAddress

	Outputs:
		TRUE  - Write to Flash was successful
		FALSE - Write to Flash was unsuccessful

	Locals:
********************************************************************************/
uint8 DownloadEraseBlock ( uint8 rom * EraseAddress )
{
	// Set the table pointer address
	TBLPTR  = (volatile near unsigned short long) EraseAddress;

	// Point to Flash program memory
	EECON1 = DOWNLOAD_ENABLE_ERASE;

	// Disable Interrupts
  	INTCONbits.GIE = FALSE;
  	
  	// do quirky writes to eecon2
  	EECON2 = 0x55;          
  	EECON2 = 0xAA;	  kkk
  	
  	// execute erase
  	EECON1bits.WR = TRUE;

	// execute a NOP
	Nop ();
  	
  	// re-enable interrupts          
  	INTCONbits.GIE = TRUE;
	  	
  	// disable write to memory
  	EECON1bits.WREN = FALSE;

	return TRUE;
}



/*******************************************************************************
	Function:	DownloadWriteBlock()

	Description:
	This subroutine will write a block of 64 bytes to Flash

	Inputs:
		FlashAddress

	Outputs:
		TRUE  - Write to Flash was successful
		FALSE - Write to Flash was unsuccessful

	Locals:
********************************************************************************/
uint8 DownloadWriteBlock ( uint8 rom * WriteAddress, uint8 * Image )
{
	uint8 i;

	// Set the table pointer address
	TBLPTR  = (volatile near unsigned short long) WriteAddress;

	// write either 8 or 64 bytes to the staging area
	for ( i = 0;  i < DOWNLOAD_PAGE_SIZE;  i++ )
		{
		TABLAT = Image [i];
  		_asm
		TBLWTPOSTINC
    	_endasm 
		}

	// restore the pointer to the block we need to be in
	TBLPTR  = (volatile near unsigned short long) WriteAddress;

	// Point to Flash program memory
	EECON1 = DOWNLOAD_ENABLE_FLASH;

	// Disable Interrupts
  	INTCONbits.GIE = FALSE;
  	
  	// do quirky writes to eecon2
  	EECON2 = 0x55;          
  	EECON2 = 0xAA;
  	
  	// execute a write of one block - 8 bytes in the 6520
  	EECON1bits.WR = TRUE;

	// execute a NOP
	Nop ();
  	
  	// re-enable interrupts          
  	INTCONbits.GIE = TRUE;
	  	
  	// disable write to memory
  	EECON1bits.WREN = FALSE;

	return TRUE;
}


/*******************************************************************************
	Function:	DownloadVerifyBlock()

	Description:
	This subroutine will verify a block of n bytes in Flash

	Inputs:
		FlashAddress
		Image
		Count
		DownloadRetries

	Outputs:
		TRUE  - Verify was successful
		FALSE - Verify was unsuccessful

	Locals:
		i
********************************************************************************/
uint8 DownloadVerifyBlock ( uint8 rom * FlashAddress, uint8 * Image, uint8 Count )
{
	uint8 i, Status;
	uint8 Flashbyte, Imagebyte;

	Status = TRUE;
	for ( i = 0;  i < Count;  i++, Image++ )
		{
		Flashbyte = *FlashAddress++;
		Imagebyte = *Image;
		if ( Flashbyte != Imagebyte )
			{
			Status = FALSE;
			break;
			}
		// copy the flash back to the image for return to the host
		if (DownloadRetries == (DOWNLOAD_NUMBER_OF_RETRIES-1))
			*Image = Flashbyte;
		}

	// verification has been completed
	return Status;

}











/* ======== END OF FILE ======== */
