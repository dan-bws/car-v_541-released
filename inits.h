/********************************************************
	File	:	inits.h
	Title	:	inits.h
	Author	:	Bob Halliday

	Description:
		Header file for inits.c
		
************************************************************/

#ifndef HEADER_INITS
#define HEADER_INITS

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_INITS
#define SCOPE
#else
#define SCOPE	extern
#endif

/*******************************
*   Section 1:  Equate Definitions: 
*********************************/
#define INITS_START_OF_RAM					0x40000000
#define INITS_END_OF_RAM					0x40005000

//#define INITS_TRISA_INIT					0x10
#define INITS_TRISA_INIT					0x00
#define INITS_TRISB_INIT					0x00
#define INITS_TRISC_INIT					0x80
#define INITS_TRISD_INIT					0x00
#define INITS_TRISE_INIT					0x70
#define INITS_TRISG_INIT					0x04
#define INITS_POWER_ON_PERIPHERALS			0xFFFF0FFE

#define INITS_ENABLE_PINSEL0				0x4050005A
#define INITS_ENABLE_PINSEL1				0x00000001
//#define INITS_0_OUTPUT_PORTS				0x17001190
#define INITS_0_OUTPUT_PORTS				0x16702190
#define INITS_1_OUTPUT_PORTS				0xFF000000
#define INITS_2_OUTPUT_PORTS				0x000010FF
#define INITS_2_DOWNLOAD					0x00001000
//#define INITS_3_OUTPUT_PORTS				0x00000000
#define INITS_3_OUTPUT_PORTS				0x07800000
#define INITS_4_OUTPUT_PORTS				0xF300FFFF
#define INITS_CLEAR_P0_PINS					0xFFFFFFFF
#define INITS_SET_P0_PINS					0x00000010
#define INITS_STATUS_LED					0x00000100
#define INITS_CLEAR_P4_PINS					0x7CFFFFFF
#define INITS_SET_P4_PINS					0x83000000
#define INITS_CLEAR_P3_PINS					0x000000FF
#define INITS_HEARTBEAT_LED					0x00000080
#define INITS_STATUS_LED					0x00000100
#define INITS_RS485_TX_ENABLE				0x02000000
#define INITS_RS485_RX_ENABLE				0x04000000
#define INITS_RS485_ENABLE					0x00000010

#define INITS_0_NIC_RESET					0x01000000
#define INITS_ENABLE_PINSEL2				0x00000000
#define INITS_ENABLE_PINSEL3				0x00000000
#define INITS_ENABLE_PINSEL4				0x00000000
#define INITS_ENABLE_PINSEL5				0x00000000
#define INITS_ENABLE_PINSEL6				0x00000000
#define INITS_ENABLE_PINSEL7				0x00000000
#define INITS_ENABLE_PINSEL8				0x00000000
#define INITS_ENABLE_PINSEL9				0x00000000

#define INITS_NO_PULLUPS					0xAAAAAAAA
#define INITS_PULLUPS_ACTIVE				0x00000000
#define INITS_PULLDOWNS_ACTIVE				0xFFFFFFFF

#define INITS_WATCHDOG_RESET				0x04
#define INITS_WATCHDOG_CONFIG				0x03
#define INITS_WATCHDOG_TIME_PERIOD_25MS		0x3FFFF				// v5.10 - return to original 1/4 second
#define INITS_WATCHDOG_TIME_PERIOD_4SEC		0x3FFFFF			// 
#define INITS_SELECT_CLOCK					0x03
#define INITS_WATCHDOG_CONFIG				0x03
#define INITS_WD_1							0xAA
#define INITS_WD_2							0x55



//  Tester Support:
#define INITS_BUZZER_CONTROL				0x00100000		// Port 0.20
//#define INITS_BUZZER_CONTROL				0x00200000		// Port 0.21
#define INITS_MAGLOCK_CONTROL				0x00400000		// Port 0.22


#define INITS_RS485_2_TX_ENABLE				0x00002000
#define INITS_RS485_2_RX_ENABLE				0x10000000
// Port 3 Uart 1 Control Lines:
#define PORT3_RS485_2_TX_ENABLE				0x02000000
#define PORT3_RS485_2_RX_ENABLE				0x04000000

// Port 3 Uart 3 Control Lines:
#define PORT3_RS485_1_TX_ENABLE				0x00800000
#define PORT3_RS485_1_RX_ENABLE				0x01000000



/*******************************
	Section 2: RAM Definitions
********************************/

// Local Variables:
#ifdef BODY_INITS


	
#endif



// Global Variables
extern uint8 cRSIR;


/*******************************
*  Section 3:  Function prototypes 
*********************************/
void Initializations (void);
void InitsRamInitialize (void);
void InitsIoportsInitialize (void);
void StartWatchdogTimer (void);
void DetermineResetCause(void);



/*******************************
*   Section 4:  Macros 
*********************************/






/*******************************
*   Section 5:  ROM Tables 
*********************************/


#ifdef BODY_INITS


#else


#endif


#undef SCOPE


#endif

/* ======== END OF FILE ======= */
