/***************************************************************************
	File	:	timers.h
	Title	:	Timer Management Header File
	Author	:	Bob Halliday
					bohalliday@aol.com  
	Created	:	August, 2005

	Description:
		Timers Definitions for the 2104 timers.c source file

	Contents:

	Revision History:


***************************************************************************/
#ifndef HEADER_TIMERS
#define HEADER_TIMERS

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_TIMERS
#define SCOPE
#else
#define SCOPE	extern
#endif



typedef unsigned char		    uint8;  /*  8 bits */
typedef unsigned short 		    uint16; /* 16 bits */
typedef unsigned long int	    uint32; /* 32 bits */

typedef signed char			    int8;   /*  8 bits */
typedef signed short 		    int16;  /* 16 bits */
typedef signed long int		    int32;  /* 32 bits */

typedef volatile uint8			vuint8;  /*  8 bits */
typedef volatile uint16			vuint16; /* 16 bits */
typedef volatile uint32			vuint32; /* 32 bits */


/*******************************
*   Section 1:  Equate Definitions:
*********************************/

// The Time Base is defined as 5ms for the Vector Project
#define TIME_BASE								5	        				// 5 ms
//#define ONE_TENTH_OF_A_SECOND					(uint8)(100/TIME_BASE)
#define TIMERS_POWER_ACTIVE						(uint8)(250/TIME_BASE)
#define TIMERS_LED1_OFF							(uint8)(50/TIME_BASE)
#define TIMERS_LED2_OFF							(uint8)(50/TIME_BASE)
#define TIMERS_LED3_OFF							(uint8)(200/TIME_BASE)

#define ONE_FULL_SECOND							(uint8)(1000/TIME_BASE)
#define ONE_FULL_MINUTE							60
#define ONE_FULL_HOUR							60
#define ONE_FULL_DAY							24
#define ONE_FULL_WEEK							7
#define ONE_FULL_YEAR							52

#define ONE_TENTH_SECOND			20			 /* one tenth second duration count */
#define ONE_SECOND					200 		 /* one second duration count 		*/
#define ONE_MINUTE					(200*60)	 //


#define MILLISECONDS_5      					1
#define MILLISECONDS_10     					(MILLISECONDS_5*  2)
#define MILLISECONDS_20     					(MILLISECONDS_5*  4)
#define MILLISECONDS_30     					(MILLISECONDS_5*  6)
#define MILLISECONDS_40     					(MILLISECONDS_5*  8)
#define MILLISECONDS_50     					(MILLISECONDS_5* 10)
#define MILLISECONDS_60     					(MILLISECONDS_5* 12)
#define MILLISECONDS_70     					(MILLISECONDS_5* 14)
#define MILLISECONDS_80     					(MILLISECONDS_5* 16)
#define MILLISECONDS_90     					(MILLISECONDS_5* 18)
#define MILLISECONDS_100    					(MILLISECONDS_5* 20)
#define MILLISECONDS_200    					(MILLISECONDS_5* 40)
#define MILLISECONDS_300    					(MILLISECONDS_5* 60)
#define MILLISECONDS_400    					(MILLISECONDS_5* 80)
#define MILLISECONDS_500    					(MILLISECONDS_5*100)

#define SECONDS_1           					(MILLISECONDS_5*200)

#define TIMER0_RESET_INT			0x03
#define TIMER1_STOP_ON_MR0			0x06
#define TIMER0_MR0_IR				0x01
#define TIMER_PERIPHERAL_CLOCKING	0x02

// running at 18mHz  - one tick every 55ns
// running at 12mHz  - one tick every 83ns		-- 09/26/07
// running at 6 mHz  - one tick every 166ns		-- 12/11/07
#define TIMERS_1_MS			(uint32) 20000
#define TIMERS_2_MS			(uint32) 40000
#define TIMERS_3_MS			(uint32) 60000
#define TIMERS_4_MS			(uint32) 80000
#define TIMERS_5_MS			(uint32) 60241			// 48 Mhz
//#define TIMERS_5_MS			(uint32) 30120		// 24 Mhz

//#define TIMERS_12_5_MS 		(uint32) 227273		// the 12.5ms time at 18 Mhz
#define TIMERS_12_5_MS 		(uint32) 150602			// the 12.5ms time at 12 Mhz - 09/26/07

#define TIMERS_TOGGLE_TEST			0xC0000000


// Software Timers definitions:
enum Timers
	{
	UART0_RESYNC_TIMER,	
	UART1_RESYNC_TIMER,	
	ONE_SECOND_TIMER,
	KEYS_DEBOUNCE_TIMER,
	SWITCHES_DEBOUNCE_TIMER,

	NUM_SOFTWARE_TIMERS
	};



// Variables used in the Timers module
typedef struct
{
	uint32	Seconds;									 
	uint32	Minutes;									 
	uint32	Hours;									 
	uint32	CountMinutes;									 
	uint32	CountHours;									 
	uint32	CountDays;									 
} TIMERS_DATA;


/*******************************
*   Section 2:  RAM Variables:
*********************************/

// Private Variables:
#ifdef BODY_TIMERS




#endif


__align(8)


// Public Variables:
SCOPE	    uint32			TimersArray [NUM_SOFTWARE_TIMERS];

SCOPE		TIMERS_DATA		Timer;
SCOPE	    uint8			InterruptWatchdog;
SCOPE	    uint16			SavedAlarmsTimer;
SCOPE volatile uint8 		PollTimer;
SCOPE	    uint8			OneSecondTimer;
SCOPE	    uint8			SuperbusMonitorTimer;
SCOPE	    uint16			StateOfHealthTimer;
SCOPE	    uint16			PagerHeartBeatTimer;
SCOPE	    uint8			LedTimer;
SCOPE	    uint16			SupervisorTimer;
SCOPE	    uint16			SupervisorBigTimer;
SCOPE	    uint8			CapCodeTimer;
SCOPE	    uint8			Rs485ResyncTimer;
SCOPE	    uint16			SB_SupervisorWaitTimer;
SCOPE	    uint8			FlashEraseTimer;
SCOPE     uint16    iMiscTimer;
SCOPE		long			lSecondsCounter;
SCOPE		int				iStatusTimer;
SCOPE		int				iPauseDebugTimer;
SCOPE		int				iSmartCareIsDownTimer;
SCOPE		int				iDebugMessageTimer;
SCOPE		uint8			cReportingTimer;
SCOPE	    char			cGeneric_5msec_Timer;
SCOPE	    uint16			CounterOf5MsecTicks;


/*******************************
*   Section 3:  Function prototypes
*********************************/
void  TimersInitialize (void);
void  Timers5MsInterrupt (void);
void  TimersRestartTimer (uint32);
uint8 TimersTimerIsExpired (uint32, uint32);

void  TimersInitShortTimer (uint32);
uint8 TimersShortTimerExpired (void);
void  TimersExecutive (void);
void  Timers_msInterrupt (void);
void SetLedBankPort1(unsigned char cLeds);
void SetLedBankPort2(unsigned char cLeds);
void PetWatchdog (void);


/*******************************
*   Section 4:  Macros
*********************************/



/********************************
*   Section 5:  ROM Tables
*********************************/

#ifdef BODY_TIMERS


#endif



#undef SCOPE
#endif

/* ======== END OF FILE ======== */
