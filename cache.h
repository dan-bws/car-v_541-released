/********************************************************
	File	:	cache.h
	Title	:	cache.h
	Author	:	Bob Halliday
				bohalliday@aol.com   (781) 863-8245
	Copyright:	(C) 2005 Exergen Corporation, Watertown, MA

	Description:
		Header file holding the definitions for cache.c
		
************************************************************/

#ifndef HEADER_CACHE
#define HEADER_CACHE

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_CACHE
#define SCOPE
#else
#define SCOPE	extern
#endif

/*******************************
/*  Section 1:  Equate Definitions: 
*********************************/
#define CACHE_VERSION_SIZE			6

// Parms Flags
#define CACHE_DUT_FLAG				0x01
#define CACHE_SIZE					255
#define CACHE_CONSTANTS_SIZE		4
#define CACHE_NUM_CONSTANTS			4
#define CACHE_ADDRESS_OFFSET		8
#define CACHE_NULL_ADDRESS			0xFFFF


// indeces into a record in Cache Array
enum
	{
	CACHE_COUNT,					// 0x00
	CACHE_ADDR_LOW,					// 0x01
	CACHE_ADDR_HI,					// 0x02
	CACHE_DATA,						// 0x03
	CACHE_04						// 0x04
	};



/*******************************
	Section 2: RAM Definitions
********************************/

// Local Variables:
#ifdef BODY_CACHE


	
#endif



// Global Variables

#ifdef BODY_CACHE
#pragma udata cache_data=0x600
#endif

SCOPE	uint8		CacheArray [CACHE_SIZE];


#ifdef BODY_CACHE
#pragma udata cache2_data=0x0A0
#endif

SCOPE	uint8		CacheCount;
SCOPE	uint8		CacheIndex;
SCOPE	uint8		CacheByteCounter;
SCOPE	uint16		CacheAddress;
SCOPE	uint8		CacheConfigFlag;

SCOPE	DOUBLE_BYTE_TYPE 		CacheAddr;


/*******************************
*  Section 3:  Function prototypes 
*********************************/
uint8 CacheCommandProgramVerify ( uint8 * );
uint8 CacheCommandConstants (uint8 * );
void  CacheClear (void);
uint8 CacheInsert (uint8 * );
void  CacheUpdateCacheOperators (void);
void  CacheRestart (void);
void  CacheUpdateCounterAndAddress (void);
void  CacheStartFromConfigOrZero (uint8);
uint8 CacheIsSpaceAvailable (uint8);



/*******************************
/*  Section 4:  Macros 
*********************************/






/*******************************
/*  Section 5:  ROM Tables 
*********************************/


#ifdef BODY_CACHE




#else


#endif


#undef SCOPE


#endif

/* ======== END OF FILE ======= */
