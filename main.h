/********************************************************
	File	:	main.h
	Title	:	main.h
	Author	:	Bob Halliday
				bohalliday@aol.com  

	Description:
		Header file holding the definitions for main.c
		
************************************************************/

#ifndef HEADER_MAIN
#define HEADER_MAIN

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_MAIN
#define SCOPE
#else
#define SCOPE	extern
#endif

/*******************************
  Section 1:  Equate Definitions: 
*********************************/
/***********************************************************************/
/*
 * The basic data types
 */

typedef unsigned char		    uint8;  /*  8 bits */
typedef unsigned short 		    uint16; /* 16 bits */
typedef unsigned long int	    uint32; /* 32 bits */

typedef signed char			    int8;   /*  8 bits */
typedef signed short 		    int16;  /* 16 bits */
typedef signed long int		    int32;  /* 32 bits */

typedef volatile uint8			vuint8;  /*  8 bits */
typedef volatile uint16			vuint16; /* 16 bits */
typedef volatile uint32			vuint32; /* 32 bits */

typedef void    				(*FUNCTION_LIST) (void);			

extern char szVersion[];


typedef union
	{
    struct
  		{
    	unsigned b0:1;
    	unsigned b1:1;
    	unsigned b2:1;
    	unsigned b3:1;
    	unsigned b4:1;
    	unsigned b5:1;
    	unsigned b6:1;
    	unsigned b7:1;
  		} byte_bits;
	uint8 	byte;
	} 
	FLAGS;



typedef union
	{
    uint16  OneShort;
    uint8   TwoBytes[2];
	} 
	DOUBLE_BYTE_TYPE;



#define BIT0 (unsigned char)0x01
#define BIT1 (unsigned char)0x02
#define BIT2 (unsigned char)0x04
#define BIT3 (unsigned char)0x08
#define BIT4 (unsigned char)0x10
#define BIT5 (unsigned char)0x20
#define BIT6 (unsigned char)0x40
#define BIT7 (unsigned char)0x80



#define	TRUE						1
#define FALSE						0
#define MAIN_PUSH_BUTTON_MASK		0x4000
#define LEARN_MODE_TIME_EXPIRED		30


//  Learn Mode States
enum
	{
	LEARN_MODE_OFF,
	LEARN_MODE_ACTIVE,
	LEARN_MODE_EXTRA
	};



/*******************************
	Section 2: RAM Definitions
********************************/

// Local Variables:
#ifdef BODY_MAIN


	
#endif



// Global Variables
SCOPE	uint8		MainLearnModeTimer;
SCOPE	uint8		MainLearnModeState;

SCOPE	uint8		WDT_RESET;
SCOPE	uint8		ACTIVE_LED;
SCOPE	uint8		EEPROMSEL0;

#define MAX_STRING_LENGTH 200
SCOPE	char sString[MAX_STRING_LENGTH];

/*******************************
*  Section 3:  Function prototypes 
*********************************/
void  MainStartLearnMode (void);
void  MainWatchLearnMode (void);
void  MainEndLearnMode (void);




/*******************************
*   Section 4:  Macros 
*********************************/






/*******************************
*  Section 5:  ROM Tables 
*********************************/
#ifdef BODY_MAIN

const uint8 MainVersionNumber [ ] =
	{
	"v01.00"
	};



#else
extern const uint8 MainVersionNumber [ ];


#endif


#undef SCOPE


#endif

/* ======== END OF FILE ======= */
