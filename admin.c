/***********************************************************
	Module Name:	admin.c

	Author:			Bob Halliday, June, 2007
					bohalliday@aol.com

	Description:
	This file is holds the rs-232 decode logic for the SmartCare interface

	Subroutines:	AdminInit()
					AdminProcessCommand ()
					AdminCommandCheck ()
					SetTransmitterNumber ()
					 (45 others...)

	Revision History:


*************************************************************/

/************************************************************************/
/*                                                                      */
/*              Administrative Procedures                               */
/*                                                                      */
/************************************************************************/

#include "system.h"
#include "lpc23xx.h"
#include "main.h"
#include "sio.h"
#include "psi2000.h"
#include "timers.h"
#include "utils.h"
#include "tec.h"
#include "sbus.h"
#include "rtc.h"
#include "inits.h"
#include "database.h"
#include "carts.h"
#include "cpac.h"
#include "factory.h"
#include "lapage.h"
#include "addendum.h"
#include "mprocess.h"
#include "uart.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define ALLOCATE_ADMIN
#include "admin.h"

#ifdef SPECTRALINK
#include "spectralink.h"
#endif

extern uint8 	FlashState;
extern char sString[];


const uint8 gszOperationCancledMessage[] = "Process Cancelled";
const uint8 szOK[] = "\nOK";
const uint8 szInvalidCommand[] = "\n\rInvalid Command";
const uint8 szInvalidData[] = "\n\rInvalid Data";
const uint8 ctOK[] = "001";
const uint8 ctInvalidCommand[] = "002";
const uint8 ctInvalidData[] = "003";
const uint8 szCR[] = "\r";
const uint8 szHT[] = "\x09";
const uint8 szCRLF[] = "\r\n";
const uint8 szCOMMA[] = "\x2C";
const uint8 szEnterSupervisoryTimeMessage[] = "Enter Supervisory Time in Minutes(2-65536)";
const uint8 szSupervisoryTimeSetMessage[] = "Supervisory Time Saved";
const uint8 szStopDisplayPointData[]	= "Display Mode Stopped";
char  szNullMessage[] = "";

uint16  unPagerNumber;
uint16  unPagerData;
uint16  unSupervisoryTime;
void 	(*func)(void);

//RAV
//If we get the new smartCare version command from SC then we know that it supports fall detector
//If we don't get it then we cannot send the new alarm code for falls. We must send the
//fall as a regular alarm message (using the 'A', not 'F' alarm code).
//char cSmartCareDoesFallDetector=FALSE;

extern const FUNCTION_LIST3 FactoryCommandExecute [];


/*
 *  The Following is a table that is created by snooping the admin command messages, specifically
 *  the AdminSend5ByteCpacCommand calls. The broadcast list
 */

int	QwamBroadcastList [255];  // Initialised to have no members
uint8 	NotifyMsg[35] ;


/************************************************************************/
/*                                                                      */
/* AdminInit() Initilizes Administrative procedures.                    */
/*                                                                      */
/************************************************************************/
void AdminInit ( unsigned char ucPort )
{
    ucAdminCommandPort = ucPort;

    // bh - v4.03 - read new Locating Time parameter from non-volatile EEprom
    LocatingTime = FlashRamData.ucLocatorTime;
    if (( LocatingTime < ADMIN_LOCATE_TIME_LOW ) || ( LocatingTime > ADMIN_LOCATE_TIME_HIGH ))
    {
        // Invalid Locating Time - set the default value
        LocatingTime = ADMIN_LOCATE_TIME_DEFAULT;
    }

    // bh - v4.07 - read new rssi averaging flag
    RssiAveragingFlag = FlashRamData.eeRssiAveraging;


    // bh - v4.04 - read PIR Filtering time
    PirFilteringTime = FlashRamData.ucPirFilterTime;
    if ((PirFilteringTime == PIR_EXPIRED)
            || (PirFilteringTime == 0))
    {
        // 0xFF is a reserved value - if received, set it to the nominal filtering time
        PirFilteringTime = PIR_NOMINAL_DELAY;
    }

    /* Init the QwamBroadcastList table */
    memset (QwamBroadcastList, 0, sizeof(QwamBroadcastList));

}



/************************************************************************/
/*                                                                      */
/*  InformDownStreamQwams()                                             */
/*    This function send A broadcast message to the down stream CPACS   */
/*    each one of the recepients a message to indicate the link         */
/*    to the Smartcare is down or up.                                   */
/************************************************************************/

void InformDownStreamQwams( int status)
{
    // create a message to send out as a broadcast message
    NotifyMsg[0]  = 0xFF ;	// The destination address is the broadcast address
    NotifyMsg[1]  = 3;      // The byte count of the message including the
    NotifyMsg[2]  = (status == 1?SB_CPAC_SMARTCARE_DOWN: SB_CPAC_SMARTCARE_UP); // Function code
    CpacAddMessageToQueue (NotifyMsg);
}


void SendTimeToCawms (unsigned char *sData)
{
    // create a message to send out as a broadcast message
    NotifyMsg[0]  = 0xFF;	// The destination address is the broadcast address
    NotifyMsg[1]  = 19;      // The byte count of the message including the
    NotifyMsg[2]  = SB_SET_CLOCK; // Function code
    memcpy (NotifyMsg+3, sData, 15);
    CpacAddMessageToQueue (NotifyMsg);
//	SendDebugMessage ("Message: Setting clocks on CAWMs");

}

void SendFacilityCodeToCawms (void)
{
    // create a message to send out as a broadcast message
    NotifyMsg[0]  = 0xFF;	// The destination address is the broadcast address
    NotifyMsg[1]  = 4;      // The byte count of the message including the
    NotifyMsg[2]  = SB_SET_FACILITY_CODE; // Function code
    NotifyMsg[3]  =	FlashRamData.cFacilityCode;
    CpacAddMessageToQueue (NotifyMsg);
//	SendDebugMessage ("Message: Setting clocks on CAWMs");

}


/************************************************************************/
/*                                                                      */
/* AdminProcessCommand() Processes the command stream in pCmdBuf.       */
/*                                                                      */
/************************************************************************/
uint16 AdminProcessCommand( unsigned char  *pMessage, unsigned char IsHost)
{
    static unsigned char  ucState = LOGON;
    static uint16	 unCurrentCommand = NONE;
    const uint8 szPSI2000_LOGON[] = "PSI2000 LOGON OK.";
    const uint8 szPSI2000_LOGOFF[] = "PSI2000 LOGOFF OK.";


    if( (ucState == GET_CMD) || (ucState == LOGON))
    {
        unCurrentCommand = AsciiToInt( pMessage );
    }

    // Watch for commands that fall within the tester range.
    if ((unCurrentCommand >= CMD_ENABLE_FACTORY_TEST_MODE)
            && (unCurrentCommand <= CMD_READ_DRY_CONTACT_INPUTS))
    {
        // Go to the correct Factory Decode Logic
        (FactoryCommandExecute [unCurrentCommand-CMD_ENABLE_FACTORY_TEST_MODE] )();
        return 0;
    }



    /* Process Commands  */
    switch( ucState )
    {
    case LOGON :
    {
        if ( unCurrentCommand == ADMIN_MASTER_PASSWORD )
        {
            if (AdminCaretechHost)
                AdminMessage ( (char *) ctOK );
            else
                AdminMessage( (char *) szPSI2000_LOGON );
            gucAdminMode = TRUE;
            ucState = GET_CMD;
            // bh - 10/24/06
            return 0;
        }
        // 44444 is Caretech Host Identifier
        /*******
        		    else if ( unCurrentCommand == ADMIN_CARETECH_HOST )
        		      	{
        		 		AdminMessage ( (char *) ctOK );
        				// We thought we were an ERS unit, but now and forever, we are SmartCare
        				AdminCaretechHost = TRUE;
        				FlashRamData.SmartCareActive = TRUE;
        				// Set flag to trigger flash write
        				FlashModified = TRUE;
        			    return 0;
        		      	}
        *****************/
//		    return 0;
        /*************************************
        89C450 noise on the rs-232 channels causes false characters to throw the
        state machine back into the LOGON state.  To work around this, allow
        commands to be executed even if we are stuck in the LOGON state waiting
        for another LOGON command to be received.
        *************************************************/
        // bh - 10/24/06
        else
            break;
    }

    case GET_CMD :
    case PROCESS_CMD :
    {
        break;
    }

    default:
    {
        unCurrentCommand = NONE;
        ucState = GET_CMD;
        return 0;
    }
    }


    /* Process Individual Commands   */
    if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
    {
        sprintf (sString, "From SmartCare: Received command #%u", unCurrentCommand);
        SendDebugMessageX (sString, TRUE, FALSE);
    }
    switch( unCurrentCommand )
    {
    case NONE :
    {
        AdminMessage( (char *) "READY..." );
        break;
    }

    case LOGOFF :
    {
        if (AdminCaretechHost)
            AdminMessage ( (char *) ctOK );
        else
            AdminMessage( (char *) szPSI2000_LOGOFF);
        gucAdminMode = FALSE;
        ucState = LOGON;
        break;
    }

    case ADMIN_MASTER_PASSWORD :
    {
        // bh - 11/02/05 - Even if we are already logged on, behave as if we are logging on again
        if (AdminCaretechHost)
            AdminMessage ( (char *) ctOK );
        else
            AdminMessage( (char *) szPSI2000_LOGON );
        break;
    }


    // bh - 10/20/05
    case 0x00 :	//
    case 65515 :	// 0xFFEB
    {
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - This does squat", FALSE, FALSE);
        break;
    }

    case SET_TRANSMITTER_NUMBER :	//
    {
        ucState = SetTransmitterNumber( pMessage, FALSE );
        break;
    }
    case SET_MODE_7 :	//  [7] - v4.03a - 01/03/2007
    {
        ucState = Set_Mode_7 ();
        AdminMessage( (char *) ctOK );
        break;
    }
    case SET_MODE_8 :	//  [8]
    {
        ucState = Set_Mode_8 (pMessage);
        break;
    }

    case DISPLAY_SB_POINT_DATA :	//  [9]
    {
        ucState = SetDisplayPointData( pMessage );
        break;
    }

    case RESET_SB_POINT_TABLE :	//  [10]
    {
        ucState = ResetSBPoints( pMessage );
        break;
    }
    case DISPLAY_SB_POINT_DB :	//  [11]
    {
        ucState = DisplayPointDB( pMessage, FALSE );
        break;
    }
    case SET_SB_POINT_DB :	//  [12]
    {
        ucState = SetPointDB( pMessage );
        break;
    }

    case IN_FACTORY_MODE :	//  [111]
    {
        InFactoryMode = TRUE;
        AdminMessage ( (char *) szOK );
        break;
    }
    case OUT_FACTORY_MODE :	//  [112]
    {
        InFactoryMode = Factory2 = FALSE;
        AdminMessage ( (char *) szOK );
        break;
    }

    case GET_RTC_DATA :	//  [202]
    {
        ucState = DisplayRTCData( pMessage );
        break;
    }
    case SET_RTC_DATA :	//  [201]
    {
        ucState = SetRTCData( pMessage );
        break;
    }
    case GET_SB_NUMBER_OF_DEVICES :	//  [302]
    {
        ucState = GetNumberOfDevices( pMessage );
        break;
    }
    case SET_SB_DEVICE_DATA :	//  [303]
    {
        ucState = SetDeviceData( pMessage );
        break;
    }
    case GET_SB_DEVICE_DATA :	//  [304]
    {
        ucState = GetDeviceData( pMessage );
        break;
    }
    case RESET_SB_DEVICE_DATA :	//  [310]
    {
        ucState = ResetDevicesData( pMessage );
        break;
    }
    case SET_SB_SUPERVISORY_TIME :	//  [403]
    {
        ucState = SetSupervisoryTime( pMessage );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Set Supervision Time", FALSE,FALSE);
        break;
    }
    case GET_SB_SUPERVISORY_TIME :	//  [404]
    {
        ucState = GetSupervisoryTime( pMessage );
        break;
    }


    /***********************************************************************
    	Cpac Commands
    ***********************************************************************/
    case ADMIN_CPAC_REQUEST_OPTIONS:				// 500
        ucState = AdminRequestOptions (pMessage);
        sprintf (sString, " - Reading options at CAWM #%i", HexAsciiBin (&pMessage [RCVR_INDEX]));
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (sString, FALSE,FALSE);
        break;
    case ADMIN_CPAC_LOCK:							// 501
        ucState = AdminLock (pMessage);
        sprintf (sString, " - Locking Door at CAWM #%i", HexAsciiBin (&pMessage [RCVR_INDEX]));
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (sString, FALSE,FALSE);
        break;
    case ADMIN_CPAC_UNLOCK:							// 502
        ucState = AdminUnlock (pMessage);
        sprintf (sString, " - Unlocking Door at CAWM #%i", HexAsciiBin (&pMessage [RCVR_INDEX]));
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (sString, FALSE,FALSE);
        break;
    case ADMIN_CPAC_CLEAR_ALERT:					// 503
        ucState = AdminClearAlert (pMessage);
        sprintf (sString, " - Clear alert at CAWM #%i", HexAsciiBin (&pMessage [RCVR_INDEX]));
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (sString, FALSE,FALSE);
        break;
    case ADMIN_CPAC_STORE_TITLE:					// 504
        ucState = AdminStoreTitle (pMessage);
        sprintf (sString, " - Write Title to CAWM #%i", HexAsciiBin (&pMessage [RCVR_INDEX]));
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (sString, FALSE,FALSE);
        break;
    case ADMIN_CPAC_READ_TITLE:						// 505
        ucState = AdminReadTitle (pMessage);
        sprintf (sString, " - Read Title CAWM #%i", HexAsciiBin (&pMessage [RCVR_INDEX]));
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (sString, FALSE,FALSE);
        break;
    case ADMIN_CPAC_SET_OPTIONS:					// 506
        ucState = AdminSetCpacOptions (pMessage);
        sprintf (sString, " - Setting options at CAWM #%i", HexAsciiBin (&pMessage [RCVR_INDEX]));
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
				{
            SendDebugMessageX (sString, FALSE,FALSE);
            SendDebugMessageX (" [", FALSE,FALSE);
						SendDebugMessageX ((char *)pMessage, FALSE,FALSE); 
						SendDebugMessageX ("] ", FALSE,FALSE);
				}
        break;
    case ADMIN_CPAC_ENTER_TEST_MODE:				// 507
        ucState = AdminEnterWalkTestMode (pMessage);
        break;
    case ADMIN_CPAC_EXIT_TEST_MODE:					// 508
        ucState = AdminExitWalkTestMode (pMessage);
        break;
    case ADMIN_CPAC_SET_SERIAL_NUMBER:				// 509
        ucState = AdminSendSerialNumber (pMessage);
        sprintf (sString, " - Read S/N at CAWM #%i", HexAsciiBin (&pMessage [RCVR_INDEX]));
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (sString, FALSE,FALSE);
        break;
    case ADMIN_CPAC_READ_SERIAL_NUMBER:				// 510
        ucState = AdminReadSerialNumber (pMessage);
        sprintf (sString, " - Read S/N at CAWM #%i", HexAsciiBin (&pMessage [RCVR_INDEX]));
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (sString, FALSE,FALSE);
        break;
    // for debug only - deleting a title will render that record unusable in the future:
    case ADMIN_CPAC_DELETE_TITLE:					// 590
        ucState = AdminDeleteTitle (pMessage);
        break;

    /***********************************************************************
    	End of Cpac Commands
    ***********************************************************************/


    case SET_PAGER_DATA :	//  [601]
    {
        ucState = SetPagerData( pMessage );
        break;
    }
    case GET_PAGER_DATA :	//  [602]
    {
        ucState = GetPagerData( pMessage );
        break;
    }
    case RESET_PAGERS_DATA :	//  [610]
    {
        ucState = ResetPagersData( pMessage );
        break;
    }


    case P3_9600 :	//  [701]
    case P3_192K :	//  [702]
    case P5_1200 :	//  [703]
    case P5_9600 :	//  [704]
        AdminMessage ( (char *) ctInvalidCommand );
        break;


    case P5_192K :	//  [705]
        ucState = P5_192K_Baud ( );
        break;

#ifdef RSSI_FLOOR
    case ENABLE_RSSI_AVERAGING :	//  [708] - v407
        FlashRamData.eeRssiAveraging = 1;
        RssiAveragingFlag = 1;
        // fall through to the next case to set the Location Flag as well
#endif

    case SET_LOCATION_FLAG :	//  [709]
    {
        ucState = SetLocationFlag( pMessage );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Set Locating On", FALSE,FALSE);
        break;
    }
    case RESET_LOCATION_FLAG :	//  [710]
    {
        ucState = ResetLocationFlag( pMessage );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Set Locating Off", FALSE,FALSE);
        break;
    }
    case READ_LOCATION_FLAG :	//  [710]
    {
        ucState = ReadLocationFlag( pMessage );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Read Locating flag", FALSE,FALSE);
        break;
    }

    case DISPLAY_COMMANDS :			//  [800]
    {
        //PetWatchdog ();
        ucState = PROCESS_CMD;
        while (ucState == PROCESS_CMD)
        {
            ucState = DisplayCommands ( pMessage );
        }
        break;
    }

    case ZERO_EEPROM_DATA :			//  [888]
    {
        ucState = ZeroEEPROMData( pMessage );
        break;						//
    }
    case POLL_COMMAND:
    {
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - SmartCare asks for our version number", FALSE, FALSE);
        PrintVersionNumber ();
        break;
    }

    case ADMIN_SMARTCARE_VERSION:	//if we get this command then Smartcare is a version
        //that supports Fall Detector so we can send fall alarms to it
        FlashRamData.cSmartCareDoesFallDetector = TRUE;
        // Set flag to trigger flash write
        FlashModified = TRUE;
        AdminMessage ( (char *) ctOK );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - SmartCare is reporting its version number", FALSE,FALSE);
        break;

    // v3.86 - support for Caretech commands
    // 44444 is Caretech Host Identifier
    case ADMIN_CARETECH_HOST:
    {
        AdminCaretechHost = TRUE;
        FlashRamData.SmartCareActive = TRUE;
        //FlashRamData.cSmartCareDoesFallDetector = FALSE;
        // Set flag to trigger flash write
        FlashModified = TRUE;
        AdminMessage ( (char *) ctOK );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - SmartCare says Hello!", FALSE, FALSE);
        break;
    }
    // 50000 is CARTs Registration command
    case ADMIN_REGISTER_CARTS:
    {
        if ( strlen ((const char *) pMessage) < (CARTS_REG_ASCII_LENGTH + 6))
        {
            // The message size is too short!
            if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
                SendDebugMessageX (" - ERROR: Received bad WR List from SmartCare", FALSE, FALSE);
            AdminMessage( (char *) ctInvalidData );
        }
        else
        {
            CartsGetRegistrationData (&pMessage [AFTER_COMMA_MARK]);
            if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
                SendDebugMessageX (" - Received WR List from SmartCare", FALSE, FALSE);
            AdminMessage( (char *) ctOK );
        }
        break;
    }

    // v3.89 - more new Caretech commands
    // 50001 is Reset the Points Database - This corresponds to Command #10
    case ADMIN_RESET_POINTS:
    {
        Flash_InitiateEraseOperation (TRUE);
        AdminMessage( (char *) ctOK );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Clearing database", FALSE, FALSE);
        break;
    }
    // 50002 is set transmitter data - This corresponds to Command #12
    case ADMIN_ENTER_XMITTER_DATA:	 //50002,00640,C0FB7D00120130,b440
    {
        iPauseDebugTimer=3;
        AdminSetPointData (pMessage);
        break;
    }
    // 50003 is Set Realtime Clock - This corresponds to Command #201
    case ADMIN_SET_REALTIME_CLOCK:
    {
        AdminSetDateTime (pMessage);
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Setting Clock", FALSE, FALSE);
        break;
    }
    // 50004 is Set Supervisory Time - This corresponds to Command #403
    case ADMIN_SET_SUPERVISORY_TIME:
    {
        AdminSetSupervisoryTime (pMessage);
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Setting Supervise Time", FALSE, FALSE);
        break;
    }
    // 50005 is Set Pager Data - This corresponds to Command #601
    case ADMIN_SET_PAGER_DATA:
    {
        AdminSetPagerData (pMessage);
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Setting Pagers", FALSE, FALSE);
        break;
    }
    // Read Registration Data
    case ADMIN_READ_REGISTRATION:
    {
        CartsSetRegistrationData (0);
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Reading CARTS", FALSE, FALSE);
        break;
    }
    // Set Transmitter Data - This corresponds to Command #2
    case ADMIN_SET_XMITTER_DATA:
    {
        ucState = SetTransmitterNumber (pMessage, TRUE);
        unCurrentCommand = SET_TRANSMITTER_NUMBER;
        break;
    }
    // Read Point Number - This corresponds to Command #11
    case ADMIN_READ_POINT_NUMBER:
    {
        //DisplayPointDB ( &pMessage [AFTER_COMMA_MARK], TRUE );
				SeRSSIFilterFromSmartCare (&pMessage [AFTER_COMMA_MARK]); //Jan 3, 2018 repurpose this SmartCare button
        break;
    }
    // Read Real Time Clock  - This corresponds to Command #202
    case ADMIN_READ_REALTIME_CLOCK:
    {
        RTCGetData (szReceiverNumber);
        AdminMessage( (char *)szReceiverNumber );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Reading Clock", FALSE, FALSE);
        break;
    }
    // Read Supervisory Time - This corresponds to Command #404
    case ADMIN_GET_SUPERVISORY_TIME:
    {
        UIntToAscii( szReceiverNumber, SBGetSupervisoryTime () );
        AdminMessage( (char *)szReceiverNumber );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Reading Supervise Time", FALSE, FALSE);
        break;
    }
    // Read Pager Data - This corresponds to Command #602
    case ADMIN_GET_PAGER_DATA:
    {
        unPagerData = LAGetPagerData( AsciiToInt( &pMessage [AFTER_COMMA_MARK] ) );
        UIntToAscii( szReceiverNumber, unPagerData );
        AdminMessage ((char *)szReceiverNumber);
        break;
    }
    // Run Diagnostic Poll -- 51006
    case ADMIN_RUN_DIAGNOSTIC_POLL:
    {
        CartsInitiateDiagnosticPoll ();
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Starting diag poll", FALSE,FALSE);
        break;
    }
    // 50010 -- Initiate firmware download
    case ADMIN_INITIATE_DOWNLOAD:
    {
        AdminInitiateFirmwareDownload ();
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Starting firmware download", FALSE, FALSE);
        break;
    }


    // 60002 -- Write one Point to Addendum Points Database
    case ADMIN_ADDENDUM_WRITE_ONE_POINT:
    {
        AddendumSetPointData (pMessage);
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
				{
            SendDebugMessageX (" - Write 1 data point - ", FALSE,FALSE);
					SendDebugMessageX ((char *)pMessage, FALSE,FALSE);
				}
        break;
    }
    // 61002 -- Read the number of points in the Addendum Database
    case ADMIN_ADDENDUM_READ_NUM_POINTS:
    {
        UIntToAscii( szReceiverNumber, AddendumDatabase.RecordCount );
        AdminMessage( (char *)szReceiverNumber );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Read number of data point", FALSE,FALSE);
        break;
    }
    // 60003 -- Erase one point from the Points Database
    case ADMIN_ADDENDUM_ERASE_ONE_POINT:
    {
        AddendumEraseOnePoint (&pMessage [AFTER_COMMA_MARK]);
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - Erase 1 data point", FALSE,FALSE);
        break;
    }



#ifdef SPECTRALINK
    // Spectralink:  Read a  Port Number
    case ADMIN_READ_PORT_NUMBER:
    {
        AdminReadPortNumber (pMessage);
        break;
    }
    // Spectralink:  Print out all Port Numbers
    case ADMIN_READ_ALL_PORTS:
    {
        SpectralinkActivatePrintOut ();
        AdminMessage( (char *)szCRLF );
        break;
    }
#endif

    // Switch to High Speed Mode
    case ADMIN_SWITCH_TO_HIGH_SPEED:
    {
        AdminSwitchToHighSpeedMode ();
        break;
    }
    // Set Locator Time Interval
    case ADMIN_SET_LOCATOR_TIME:
    {
        AdminSetLocatorTime (pMessage);
        break;
    }
    // Set Locator Time Interval
    case ADMIN_SET_PIR_FILTER_TIME:
    {
        AdminSetPirFilterTime (pMessage);
        //AdminSetRFID_FilterTime (pMessage);
        break;
    }
    // Read Locator Time Interval
    case ADMIN_READ_LOCATOR_TIME:
    {
        AdminReadLocatorTime ();
        break;
    }
    // Set Locator Time Interval
    case ADMIN_READ_PIR_FILTER_TIME:
    {
        AdminReadPirFilterTime ();
				//AdminReadRFIDFilterTime();
        break;
    }


    default:
    {
        // bh - Invalid Command was received!
        if (AdminCaretechHost)
            // bh - 4.06 restored
        {
            AdminMessage ( (char *) ctInvalidCommand );
        }
        else
            AdminMessage ( (char *) szInvalidCommand );
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            SendDebugMessageX (" - ERROR: Invalid command from SmartCare", FALSE,TRUE);
        return 0;
    }
    }
    if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
        SendDebugMessageX ("", FALSE, TRUE);

    //transmit queue is small. Need to empty it before proceeeding with more data
    while (Uart [UART_1].TransmitActive)
    {
        Uart1Transmitter ();
    }


    if( ucState == GET_CMD )
    {
        unCurrentCommand = NONE;
    }
    return 0;
}

/************************************************************************/
/*                                                                      */
/* AdminCommandCheck()  Test for complete command reception then        */
/* dispatches to proper procedure.                                      */
/*                                                                      */
/************************************************************************/
void AdminCommandCheck( void )
{
    unsigned char  ucByte;

    if ( SioByteCount( ucAdminCommandPort ) )
    {
        /* Get next Receive Byte                   */
        ucByte = SioGetByte( ucAdminCommandPort );
        ucByte = toupper (ucByte);

        // v 3.89 ---- Don't echo anything to Caretech Host
        if (gucAdminMode && (!AdminCaretechHost) )
        {
            SioSetMessage( ucAdminCommandPort, &ucByte, 1 );
        }

        // bh --- 12/09/05 --- ESC should terminate a data entry sequence.
        if( ucByte == SB_KEYPAD_BYPASS )
        {
            if (ucAdminCommandBufferByteCount)
            {
                aucAdminCommandBuffer[ 0 ] = SB_KEYPAD_BYPASS;
                ucAdminCommandBufferByteCount = 1;
                ucByte = '\r';
            }
        }
        /* Test for command terminator cr*/
        if( ucByte == '\r' )
        {
            /* Insert '0' Terminator         */
            aucAdminCommandBuffer[ ucAdminCommandBufferByteCount ] = 0;
            /*  Dispatch to command handler              */
            // bh - 11/07/05 - Any commands from the Host will kill Mode 9 or Mode 7.
            if (Mode_9_Flag || Mode_7_Flag )
            {
                AdminMessage( (char *) szStopDisplayPointData );
            }
            Mode_9_Flag = 0;
            Mode_8_Flag = 0;
            Mode_7_Flag = 0;
            AdminProcessCommand( aucAdminCommandBuffer, TRUE );
            ucAdminCommandBufferByteCount = 0; /* Set command buffer empty    */
        }

        else if( ucAdminCommandBufferByteCount >= ADMIN_BUFFER_SIZE )
        {
            ucAdminCommandBufferByteCount = 0; /* Buffer overflow reset       */
        }

        else
        {
            /* Else Store Byte in Buffer   */
            aucAdminCommandBuffer[ucAdminCommandBufferByteCount++] = ucByte;
        }
    }
}



/************************************************************************/
/*		command 2 */
unsigned char SetTransmitterNumber( unsigned char * pMessage, unsigned char IsCareTech )
{
    const uint8	szTripPointMessage[ 20 ] = "Trip Transmitter ";
    const uint8   szPointNumberSetMessage[] = "Transmitter Data Sent";
    uint8 	szAdminTripPointMessage[ 82 ] = "";
    uint8   szSignalStrength[10] = "";
    static unsigned char  ucState = GET_CMD;
    unsigned long ulID;
    unsigned char  acTemp[51];
    unsigned char ucType;
    signed char cIndexID;
    static unsigned char SaveCareTech;

    // v3.89 --- Watch for new Caretech command
    if (IsCareTech)
    {
        // This is a Caretech Command - take the short cut
        DisableEnq = 1;
        gucAdminTrapFlag = 1;

        // v5.08 - enable timeout timer for Learn Mode
        MainStartLearnMode ();

        ucState = GET_XMIT_NUMBER;
        SaveCareTech = 1;
        SavedAlarmsState = SAVED_ALARMS_IDLE;
        return PROCESS_CMD;
    }

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        gucAdminTrapFlag = 0;

        // v5.08 - turn off timeout timer for Learn Mode
        MainEndLearnMode ();

        // v. RC4
        unregistered_copy = 0;
        MprocessDumpSavedAlarms ();
        DisableEnq = 0;
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0, (uint8 *) szCRLF, strlen ((const char *)szCRLF)) ;
    }
    case GET_POINT_NUMBER:
    {
        if( pMessage[0] == 0 )
            return PROCESS_CMD;		/* return if no number input */

        StringCat( szAdminTripPointMessage, (uint8 *)szTripPointMessage, 60 );
        AdminMessage( (char *) szAdminTripPointMessage );

        DisableEnq = 1;
        gucAdminTrapFlag = 1;

        // v5.08 - enable timeout timer for Learn Mode
        MainStartLearnMode ();

        // stop the saved alarms state machine if it is running
        SavedAlarmsState = SAVED_ALARMS_IDLE;
        ucState = GET_XMIT_NUMBER;
        return PROCESS_CMD;
    }

    case GET_XMIT_NUMBER:
    {
        /* Get Device Type */
        ucType = SBGetDeviceType( pMessage );
        ucIndex = 0;

        /* Output Device Type */
        acTemp[ ucIndex++ ] = BinHexAscii( ucType >> 4 );
        acTemp[ ucIndex++ ] = BinHexAscii( ucType );

        UIntToAscii( szReceiverNumber, pMessage[0] ); //@012405 &b
        // bh - 01/18/06 - send three digits if there are 100 or more active carts
        if( pMessage[0] > 99 )
        {
            acTemp[ ucIndex++ ] = szReceiverNumber [2];
        }
        else
        {
            acTemp[ ucIndex++ ] = ' ';
        }

        acTemp[ ucIndex++ ] = szReceiverNumber[3];	//@022801
        acTemp[ ucIndex++ ] = szReceiverNumber[4];	//@022801

        /* Output Device ID in normal form  */
        ulID = SBGetDeviceID( pMessage );				//&b
        acTemp[ ucIndex++ ] = ' ';

        for( cIndexID = 7; cIndexID >= 0; cIndexID-- )
        {
            acTemp[ ucIndex++ ] = BinHexAscii( (unsigned char)(ulID >> (cIndexID * 4) ) );
        }

        acTemp[ ucIndex++ ] = BinHexAscii( ( pMessage[ 8 ]) >> 4 );	//&b 013105 window sensor pos
        acTemp[ ucIndex++ ] = BinHexAscii( pMessage[ 8 ] );

        /* Output AGC and RSSI values */
        acTemp[ ucIndex++ ] = ' ';
        acTemp[ ucIndex++ ] = BinHexAscii( pMessage[ 8 ] & 0x03 ); //@022801
        acTemp[ ucIndex++ ] = BinHexAscii( pMessage[ 8 ] & 0x00 ); //@051903
        acTemp[ ucIndex++ ] = BinHexAscii( ( pMessage[ 9 ]) >> 4 );
        acTemp[ ucIndex++ ] = BinHexAscii( pMessage[ 9 ] );

        acTemp[ ucIndex++ ] = '=';
        acTemp[ ucIndex ] = 0;
        unSignalStrength = (uint16) pMessage[9];
        UIntToAscii( szSignalStrength, unSignalStrength );
        StringCat( acTemp, szSignalStrength, 10 );

        AdminMessage( (char *) acTemp );
        // v3.89 ---  exit if this is a Caretech command
        if (SaveCareTech)
        {
            SaveCareTech = 0;
            gucAdminTrapFlag = 0;

            // v5.08 - turn off timeout timer for Learn Mode
            MainEndLearnMode ();

            goto Caretech_Exit;
        }
        ucState = SET_XMIT_NUMBER;
        return PROCESS_CMD;
    }
    case SET_XMIT_NUMBER:
    {
        if( pMessage[0] != 0 )
            return PROCESS_CMD;		/* wait for enter/command key */

        AdminMessage( (char *)szPointNumberSetMessage );

Caretech_Exit:
        default:
            // check for any saved alarms to dump at the end of command #2
            MprocessDumpSavedAlarms ();
            gucAdminTrapFlag = 0;

            // v5.08 - turn off timeout timer for Learn Mode
            MainEndLearnMode ();

            // v. RC4
            unregistered_copy = 0;
            DisableEnq = 0;
            ucState = GET_CMD;
//         return GET_CMD;
        }
    }

    return GET_CMD;
}



/************************************************************************/
/*		command 7 */

unsigned char Set_Mode_7 ( )
{

    Mode_7_Flag = 1;
    return GET_CMD;
}


/************************************************************************/
/*		command 8 */

unsigned char Set_Mode_8 ( unsigned char * pMessage )
{
    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        Mode_8_Flag = 0;
        return GET_CMD;
    }

    Mode_8_Flag = 1;
    return GET_CMD;
}




/************************************************************************/
/*		command 9 */

unsigned char SetDisplayPointData( unsigned char * pMessage )
{
    const uint8 szStartDisplayPointData[] = "Start Display Mode [Any Key = Stop].";
    static unsigned char  ucState = GET_CMD;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) szStopDisplayPointData );
        Mode_9_Flag = 0;
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;		//@021204

        AdminMessage( (char *) szStartDisplayPointData );
        default:
            ucState = GET_CMD;
            Mode_9_Flag = 1;
//         return GET_CMD;
        }
    }
    return GET_CMD;
}



/*************************************************************************/
void Admin_RunModeNine( unsigned char * pMessage )
{
    uint8   szSignalStrength[10] = "";
    unsigned long ulID;
    unsigned char  acTemp[61];
    unsigned char ucDeviceType;
    signed char cIndexID;

    ucDeviceType = SBGetDeviceType( pMessage );

    // bh - v4.04 - output date and time first
    RTCGetData( acTemp );
    acTemp [17] = 0;
    cr_flag = 1;
    AdminMessage ( (char *) acTemp );
    cr_flag = 0;

    /* Output Device Type  - 2 digits */
    /* Output Device ID in normal form  */
    ucIndex = 0;
    acTemp[ ucIndex++ ] = ' ';
    acTemp[ ucIndex++ ] = BinHexAscii( ucDeviceType >> 4 );
    acTemp[ ucIndex++ ] = BinHexAscii( ucDeviceType );

    // next - output receiver number
    acTemp[ ucIndex++ ] = ' ';
    ulID = SBGetDeviceID( pMessage );
    acTemp[ ucIndex++ ] = ' ';
    acTemp[ ucIndex++ ] = BinHexAscii( ( pMessage[ 0 ]) >> 4 );
    acTemp[ ucIndex++ ] = BinHexAscii ( pMessage[ 0 ]);
    acTemp[ ucIndex++ ] = ' ';
    acTemp[ ucIndex++ ] = ' ';

    // next output the RFID
    for( cIndexID = 7; cIndexID >= 0; cIndexID-- )
    {
        acTemp[ ucIndex++ ] = BinHexAscii( (unsigned char)(ulID >> (cIndexID * 4) ) );
    }

    // now output the 16 bits of Truth Tables
    acTemp[ ucIndex++ ] = ' ';
    acTemp[ ucIndex++ ] = BinHexAscii ( ( pMessage[ 7 ]) >> 4 );
    acTemp[ ucIndex++ ] = BinHexAscii ( pMessage[ 7 ]);
    acTemp[ ucIndex++ ] = BinHexAscii ( ( pMessage[ 8 ]) >> 4 );
    acTemp[ ucIndex++ ] = BinHexAscii ( pMessage[ 8 ] );

    // Finally, output the RSSI value in both decimal and hexadecimal
    acTemp[ ucIndex++ ] = ' ';
    acTemp[ ucIndex++ ] =	BinHexAscii( ( pMessage[ 9 ]) >> 4 );
    acTemp[ ucIndex++ ] =	BinHexAscii( pMessage[ 9 ] );
    acTemp[ ucIndex++ ] = '=';
    acTemp[ ucIndex ] = 0;
    unSignalStrength = pMessage[9];
    UIntToAscii( szSignalStrength, unSignalStrength );
    StringCat( acTemp, szSignalStrength, 10 );
    AdminMessage( (char *) acTemp );
}






/************************************************************************/
/*		command 10 */
unsigned char ResetSBPoints( unsigned char * pMessage )
{
    const uint8 szResetMessage[] = "Reset All Transmitter Data?(ESC=NO)";
    const uint8 szDatabaseReset[] = "All Transmitter Data Reset.";
    static unsigned char  ucState = GET_CMD;


    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;
        AdminMessage( (char *) szResetMessage );
        ucState = GET_CONFIRM_RESET_SB_POINTS;
        return PROCESS_CMD;
    }
    case GET_CONFIRM_RESET_SB_POINTS:
    {
        Flash_InitiateEraseOperation (TRUE);
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;
        AdminMessage( (char *) szDatabaseReset );
        ucState = GET_CMD;
        return GET_CMD;
    }
    default:
    {
        ucState = GET_CMD;
//         return GET_CMD;
    }
    }

    return GET_CMD;
}
/************************************************************************/
/*		command 11 */
unsigned char DisplayPointDB( unsigned char * pMessage, unsigned char IsCareTech )
{

    const uint8 szEnterPointMessage[] = "#";
    const uint8 Dashes [] = "--------";
    static unsigned char  ucState = GET_CMD;

    uint16 unPointNumber;
    unsigned char ucTempIndex;
    unsigned char acTemp[ 20 ] = "";
    unsigned char* pPointMessageData;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    // v3.89 --- Allow alternate command from Caretech
    if (IsCareTech)
    {
        goto Run2_CareTech;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        AdminMessage( (char *) szEnterPointMessage );
        ucState = GET_POINT_NUMBER;
        return PROCESS_CMD;
    }
    case GET_POINT_NUMBER:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;
Run2_CareTech:
        unPointNumber = AsciiToInt( pMessage );
        pPointMessageData = SBGetPointData (unPointNumber,TRUE);


        /* Dump Transmitter data */
        ucTempIndex = 0;
        for( ucIndex = 0; ucIndex < 7; ucIndex++ )
        {
            acTemp[ ucTempIndex++ ] =
                BinHexAscii( (pPointMessageData[ ucIndex ] >> 4) );
            acTemp[ ucTempIndex++ ] =
                BinHexAscii( pPointMessageData[ ucIndex ] );
        }

        // v5.00 --- insert a comma here
        acTemp[ ucTempIndex++ ] = ',';
        acTemp[ ucTempIndex   ] = 0;

        if (pPointMessageData [ ucIndex ] < 0x7F)
            StringCat( acTemp, &pPointMessageData [ ucIndex ], POINT_MESSAGE_SIZE1 );
        else
            StringCat( acTemp, (uint8 *) Dashes, POINT_MESSAGE_SIZE1 );
        AdminMessage ((char *) acTemp );

        default:
            ucState = GET_CMD;
        }
    }

    return GET_CMD;
}




//We are repurposing the Display Point button in the Maintenance screen of SMartCare to read or set the
//RSSI fitler level for alert devices to stop false alarms on weak signals.
//If user does a DisplayPoint 0 then turn off filter
//If user does a DisplayPoint 1 then read RSSI fitler value

void SeRSSIFilterFromSmartCare (unsigned char * pMessage)
{
	int iSetting;
       iSetting = AsciiToInt( pMessage );

	if (iSetting == 999)
	{
		AdminReadRFIDFilterTime();
		return;
	}
 
	AdminSetRFID_FilterTime ((unsigned char) iSetting);
}





/************************************************************************/
/*		command 12 */
unsigned char SetPointDB( unsigned char * pMessage )
{
    unsigned char result;
    const uint8 szEnterPointData[] = "D";
    const uint8 szPointDBSet[] = "OK";
    static unsigned char  ucState = GET_CMD;


    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        DisableEnq = 0;
        // check for any saved alarms to dump at the end of command #12
        SavedAlarmsState = SAVED_ALARMS_START_TIMER;
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        // bh - 11/08/05 --  init unregistered copy
        unregistered_copy = 0;

        SioSetMessage( UART_0,(uint8 *) szCRLF, strlen ((const char *)szCRLF)) ;
        AdminMessage( (char *) szEnterPointData );

        DisableEnq = 1;
        Command_12_active = 1;
        gucAdminTrapFlag = 1;
        // stop the saved alarms state machine if it is running
        SavedAlarmsState = SAVED_ALARMS_IDLE;
        ucState = GET_POINT_DATA_MESSAGE;
        return PROCESS_CMD;
    }
    case GET_POINT_DATA_MESSAGE:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;		//@021204

        result = SBSetPointData( pMessage );
        if ( result )
        {
            // bh - Invalid Data was received!
            AdminMessage(  (char *) szInvalidData );
        }
        else
        {
            // Data entered was okay
            AdminMessage( (char *) szPointDBSet );
        }
        default:
            DisableEnq = 0;
            // check for any saved alarms to dump at the end of command #12
            SavedAlarmsState = SAVED_ALARMS_START_TIMER;
            ucState = GET_CMD;
//        return GET_CMD;
        }
    }

    return GET_CMD;
}
/************************************************************************/
/************************************************************************/

/************************************************************************/
/*		command 201 */
unsigned char SetRTCData( unsigned char * pMessage )
{
    const uint8 szEnterTimeDate[] = "Enter Date & Time(mm/dd/yy hh:mm:ss)";
    const uint8 szTimeDateSet[] = "Date & Time Accepted";
    static unsigned char  ucState = GET_CMD;


    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        AdminMessage( (char *) szEnterTimeDate );
        ucState = GET_DATE_TIME;
        return PROCESS_CMD;
    }
    case GET_DATE_TIME:
    {
        if (AdminCheckDateTime (pMessage) )
        {
            // bh - Invalid Data was received!
            AdminMessage( (char *) szInvalidData );
            ucState = GET_CMD;
            return GET_CMD;
        }
        // Set the new data and time
        RTCSetData( pMessage );

        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        AdminMessage( (char *) szTimeDateSet   );
        ucState = GET_CMD;
        return GET_CMD;
    }
    }
    return GET_CMD;
}
/************************************************************************/
/*		command 202 */
unsigned char DisplayRTCData( unsigned char * pMessage )
{
    unsigned char  aucData[ (RTC_DATA_LENGTH * 2) + 4 ];

    RTCGetData( aucData );
    SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;
    AdminMessage( (char *) aucData );

    return GET_CMD;
}
/************************************************************************/

/************************************************************************/
/*		command 302 */
unsigned char GetNumberOfDevices( unsigned char * pMessage )
{
    uint8 	szNumberOfDevicesMessage[ 10 ] = "";

    static unsigned char  ucState = GET_CMD;
    static uint16  unNumberOfDevices;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        unNumberOfDevices = SBGetNumberOfDevices();
        UIntToAscii( szNumberOfDevicesMessage, unNumberOfDevices );
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;
        AdminMessage(  (char *) szNumberOfDevicesMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }
    default:
    {
        ucState = GET_CMD;
//         return GET_CMD;
    }
    }

    return GET_CMD;
}

/************************************************************************/
/*		command 303 */
unsigned char SetDeviceData( unsigned char * pMessage )
{
    const uint8 szEnterDeviceNumberMessage[] = "Enter Receiver # ";
    const uint8 szEnterDeviceDataMessage[] = "Enter Receiver Data(0-1) ";
    const uint8 szDeviceDataSetMessage[] = "Receiver Data Saved";

    static unsigned char  ucState = GET_CMD;
    static uint16  unDeviceNumber;
    uint16 unDeviceData;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        AdminMessage( (char *) szEnterDeviceNumberMessage );
        ucState = GET_DEVICE_NUMBER;
        return PROCESS_CMD;
    }
    case GET_DEVICE_NUMBER:
    {
        if( pMessage[0] == 0 )
            return PROCESS_CMD;		/* return if no number input */

        unDeviceNumber = AsciiToInt( pMessage );

        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;		//@021204

        AdminMessage( (char *) szEnterDeviceDataMessage );


        ucState = GET_DEVICE_DATA;
        return PROCESS_CMD;
    }
    case GET_DEVICE_DATA:
    {
        if( pMessage[0] == 0 )
            return PROCESS_CMD;		/* return if no number input */


        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;		//@021204

        unDeviceData = AsciiToInt( pMessage );
        SBSetDeviceData( unDeviceNumber, unDeviceData );

        AdminMessage( (char *) szDeviceDataSetMessage );

        ucState = GET_CMD;
        return GET_CMD;
    }
    default:
    {
        ucState = GET_CMD;
//         return GET_CMD;
    }
    }

    return GET_CMD;
}

/************************************************************************/
/*		command 304 */
unsigned char GetDeviceData( unsigned char * pMessage )
{
    const uint8 szEnterDeviceNumberMessage[] = "Enter Receiver # ";
    uint8 	szDeviceData[17] = "";

    static unsigned char  ucState = GET_CMD;
    static uint16  unDeviceNumber;
    uint16 unDeviceData;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        AdminMessage( (char *) szEnterDeviceNumberMessage );
        ucState = GET_DEVICE_NUMBER;
        return PROCESS_CMD;
    }
    case GET_DEVICE_NUMBER:
    {
        if( pMessage[0] == 0 )
            return PROCESS_CMD;		/* return if no number input */

        unDeviceNumber = AsciiToInt( pMessage );
        unDeviceData = SBGetDeviceData( unDeviceNumber );
        UIntToAscii( szDeviceData, unDeviceData );

        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        AdminMessage( (char *) szDeviceData );

        ucState = GET_CMD;
        return GET_CMD;
    }
    default:
    {
        ucState = GET_CMD;
//         return GET_CMD;
    }
    }

    return GET_CMD;
}
/************************************************************************/
/*		command 310 */
unsigned char ResetDevicesData( unsigned char * pMessage )
{
    const uint8 szResetDevicesMessage[] = "Reset Receivers?(ESC=NO)";
    const uint8 szDevicesDataResetMessage[] = "Receivers Reset.";

    static unsigned char  ucState = GET_CMD;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;		//@021204

        AdminMessage( (char *) szResetDevicesMessage );

        ucState = WAIT_RESET_DEVICES;
        return PROCESS_CMD;
    }
    case WAIT_RESET_DEVICES:
    {

        SBResetDevicesData();

        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;		//@021204

        AdminMessage( (char *) szDevicesDataResetMessage );

        ucState = GET_CMD;
        return GET_CMD;
    }
    default:
    {
        ucState = GET_CMD;
//         return GET_CMD;
    }
    }

    return GET_CMD;
}
/************************************************************************/
/*		command 403 */
unsigned char SetSupervisoryTime( unsigned char * pMessage )
{
    static unsigned char  ucState = GET_CMD;
    unsigned char i;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        AdminMessage( (char *) szEnterSupervisoryTimeMessage );

        ucState = GET_INT_VALUE;
        return PROCESS_CMD;
    }
    case GET_INT_VALUE:
    {
        for ( i=0; pMessage[i];  i++)
        {
            if ((pMessage[i] < '0') || (pMessage[i] > '9'))
            {
                // bh - Invalid Data was received!
                AdminMessage( (char *) szInvalidData );
                ucState = GET_CMD;
                return GET_CMD;
            }
        }

        unSupervisoryTime = AsciiToInt( pMessage );
        if( unSupervisoryTime < 2 )
        {
            // bh - Invalid Data was received!
            AdminMessage( (char *) szInvalidData );
            ucState = GET_CMD;
            return GET_CMD;
        }

        SBSetSupervisoryTime( unSupervisoryTime );
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;
        AdminMessage( (char *) szSupervisoryTimeSetMessage );
	        //sprintf (sString, "Supervisory time set to %u minutes", unSupervisoryTime);
  				//SendDebugMessageX (sString, TRUE, TRUE);

        default:
            ucState = GET_CMD;
//            return GET_CMD;
        }
    }

    return GET_CMD;
}

/************************************************************************/
/*		command 404 */
unsigned char GetSupervisoryTime( unsigned char * pMessage )
{
    uint8 	szSupervisoryTimeMessage[ 10 ] = "";
    static unsigned char  ucState = GET_CMD;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        unSupervisoryTime = SBGetSupervisoryTime();
        UIntToAscii( szSupervisoryTimeMessage, unSupervisoryTime );

        AdminMessage( (char *) szSupervisoryTimeMessage );
               
	        //sprintf (sString, "Supervisory time set to %u minutes", unSupervisoryTime);
  				//SendDebugMessageX (sString, TRUE, TRUE);
 
        ucState = GET_CMD;
        return GET_CMD;
    }
    default:
    {
        ucState = GET_CMD;
//         return GET_CMD;
    }
    }

    return GET_CMD;
}


/*****************************************************
     500 commands for CPAC support
*****************************************************/

// v5.00 - CPAC support
/***************************************************************************
	Subroutine:		AdminSend5ByteCpacCommand

	Description:
		The host has sent a CPAC command to Vector
		The command format is 50x,xx
		This routine will send the same command to the CPAC.

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
void AdminSend5ByteCpacCommand (uint8 * pMessage, uint8 SbusCommand, uint8 inlength, uint8 outlength, uint8 iTries)
{
    int index;
    char c;
    uint8 * ptr;

    // Error check the received string
    if (( pMessage [COMMA1_INDEX] != COMMA)
            || ( strlen ((const char *) pMessage) != inlength))
    {
        AdminMessage( (char *) ctInvalidData );
        return;
    }

    /*  Code to build the QWAM broadcast table  */

    index = HexAsciiBin (&pMessage[RCVR_INDEX]);
    if((index >= 0) && (index < 256) )  // NOTE need to find out what is the max number here.
    {
        if(!QwamBroadcastList[index])
            QwamBroadcastList[index] = 1; // sets the device ID ( which is the index) as a QWAM
        // i.e a device we nee to send a message to when
        // the Smartcare interface is down.

    }

    // Set the CPAC Receiver Address
    AdminCpacData [TK_ADDRESS] = HexAsciiBin (&pMessage [RCVR_INDEX]);

    // Check to make sure this CPAC is currently on line
    if (!CartsCheckActive (AdminCpacData [TK_ADDRESS]))
    {
        AdminMessage( (char *) ctInvalidData );
        return;
    }

    //RV 1-14-2105 - We dont check that the CAWM messages arrive and are successful at the CAWM.
    //So lets snd it a few times with a short delay in between

    for (c=0; c<=iTries-1; c++)
    {
        // Set the CPAC count - will later get incremented to inlength for the checksum
        AdminCpacData [TK_COUNT] = outlength-1;
        // Set the CPAC command
        AdminCpacData [TK_RESPONSE_CODE] = SbusCommand;
        // Set the Capability Code
        AdminCpacData [TK_CAPABILITY_CODE] = CPAC_CAPABILITY_CODE;
        // Set the minutes value - only valid for the 501 commmand
        // Will get overwritten by the checksum for other commands
        AdminCpacData [TK_STATUS_BYTE] = HexAsciiBin (&pMessage [DATA_INDEX]);
        // Add this message to the transmit queue
        CpacAddMessageToQueue (AdminCpacData);

        if (c<iTries)
        {
            //Transmit it
            ptr = CpacSearchForMessageInQueue (CpacOutQueue, AdminCpacData [TK_ADDRESS]);
            if (ptr)
            {
                // It's time to send a CPAC command to a CPAC
                SB_PollTime = SB_CPAC_POLL_TIME;
                CpacTransmitCommand (ptr);
                CpacDeleteMessageFromQueue (CpacMarkTheSpot);
            }

            //Delay 25 msec
            cGeneric_5msec_Timer = 5;
            PetWatchdog ();
            while (cGeneric_5msec_Timer>0)
            {
                WEAK_NOP_9();
            }
        }
    }

    // Respond with "Okay"
    AdminMessage( (char *) ctOK );
}



/***************************************************************************
	Subroutine:		AdminRequestOptions -- 500

	Description:
		The host has sent a CPAC Request Options Command
		The command format is 500,xx
		This routine will send the same command to the CPAC.

	Inputs:


		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminRequestOptions (uint8 * pMessage)
{
    AdminSend5ByteCpacCommand ( pMessage, SB_CPAC_REQUEST_OPTIONS, 6, 5, 1);
    return GET_CMD;
}

/***************************************************************************
	Subroutine:		AdminLock -- 501

	Description:
		The host has sent a CPAC Lock Mag Lock Command
		The command format is 501,xx
		This routine will send the same command to the CPAC.

	Inputs:


		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminLock (uint8 * pMessage)
{
    AdminSend5ByteCpacCommand ( pMessage, SB_CPAC_LOCK, 9, 6, 4);
    return GET_CMD;
}
/***************************************************************************
	Subroutine:		AdminUnlock -- 502

	Description:
		The host has sent a CPAC Unlock Mag Lock Command
		The command format is 502,xx
		This routine will send the same command to the CPAC.

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminUnlock (uint8 * pMessage)
{
    AdminSend5ByteCpacCommand ( pMessage, SB_CPAC_UNLOCK, 6, 5, 4);

    return GET_CMD;
}
/***************************************************************************
	Subroutine:		AdminClearAlert -- 503

	Description:
		The host has sent a CPAC Clear Alert Command
		The command format is 503,xx
		This routine will send the same command to the CPAC.

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminClearAlert (uint8 * pMessage)
{
    AdminSend5ByteCpacCommand ( pMessage, SB_CPAC_CLEAR_ALERT, 6, 5, 1);
    return GET_CMD;
}
/***************************************************************************
	Subroutine:		AdminStoreTitle -- 504

	Description:
		The host has sent a CPAC Store Title Command
		The command format is 504,xx,0123456789abcde
		   where the last field is variable in length from 1 to 15 characters
		This routine will store the title and corresponding receiver number in the database

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminStoreTitle (uint8 * pMessage)
{
    uint8 length;

    // Error check the received string
    length = ( strlen ((const char *) pMessage));
    if (( pMessage [COMMA1_INDEX] != COMMA)
            || ( pMessage [COMMA2_INDEX] != COMMA)
            || ( length < MIN_STORE_TITLE_LENGTH)
            || ( length > MAX_STORE_TITLE_LENGTH))
    {
        AdminMessage( (char *) ctInvalidData );
        return GET_CMD;
    }

    // Save the new title into the Database
    CpacSaveNewTitle (pMessage);
    // Respond with "Okay"
    AdminMessage( (char *) ctOK );

    return GET_CMD;
}
/***************************************************************************
	Subroutine:		AdminReadTitle -- 505

	Description:
		The host has sent a Read CPAC Title Command
		The command format is 505,xx
		This routine will return the title to SmartCare

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminReadTitle (uint8 * pMessage)
{
    uint8 Receiver;
    uint8 *ptr;
    uint8 title [CPAC_TITLE_RECORD_SIZE];

    // Set the CPAC Receiver Address
    Receiver = HexAsciiBin (&pMessage [RCVR_INDEX]);
    // Search for the title
    ptr = CpacReadTitle (Receiver);
    if (ptr)
    {
        // The receiver number was found in the Database
        memcpy (title, ptr, CPAC_TITLE_SIZE);		// this step is necessary to append a NULL terminator to the string
        AdminMessage( (char *) title );
    }
    else
    {
        // Error - no match on Receiver
        AdminMessage( (char *) ctInvalidData );
    }

    return GET_CMD;
}


/***************************************************************************
	Subroutine:		AdminDeleteTitle -- 590

	Description:
		The host has sent a Delete CPAC Title Command
		The command format is 590,xx
		This routine will delete the title from the title database

NOTE!!!!!
	This routine is for debug purposes only.
	ONCE A TITLE IS DELETED, THE RECORD IN THAT SLOT BECOMES UNUSABLE!!!!

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminDeleteTitle (uint8 * pMessage)
{
    uint8 Receiver;
    uint8 *ptr;

    // Set the CPAC Receiver Address
    Receiver = HexAsciiBin (&pMessage [RCVR_INDEX]);
    // Search for the title
    ptr = CpacReadTitle (Receiver);
    if (ptr)
    {
        // The receiver number was found in the Database - back up to the receiver field
        --ptr;
        // Mark this record as deleted.  This record is now unusable in the future.
        *ptr = CPAC_TITLE_RECORD_DELETED;
        // Respond with "Okay"
        AdminMessage( (char *) ctOK );
    }
    else
    {
        // Error - no match on Receiver
        AdminMessage( (char *) ctInvalidData );
    }

    return GET_CMD;
}

/***************************************************************************
	Subroutine:		AdminSetCpacOptions -- 506

	Description:
		The host has sent an Enter Walk Test Mode Command
		The command format is 506,xx,1122334455667788
		This routine will send the same command to the CPAC.

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminSetCpacOptions (uint8 * pMessage)
{
    uint32 i;

    // Error check the received string
    if (( pMessage [COMMA1_INDEX] != COMMA)
            || ( pMessage [COMMA2_INDEX] != COMMA)
            || ( strlen ((const char *) pMessage) != 23))
    {
        AdminMessage( (char *) ctInvalidData );
        return GET_CMD;
    }

    // Set the CPAC Receiver Address
    AdminCpacData [TK_ADDRESS] = HexAsciiBin (&pMessage [RCVR_INDEX]);

    // Check to make sure this CPAC is currently on line
    if (!CartsCheckActive (AdminCpacData [TK_ADDRESS]))
    {
        AdminMessage( (char *) ctInvalidData );
        return GET_CMD;
    }

    // Set the CPAC count
    AdminCpacData [TK_COUNT] = 12;
    // Set the CPAC command
    AdminCpacData [TK_RESPONSE_CODE] = SB_CPAC_SET_OPTIONS;
    // Set the Capability Code
    AdminCpacData [TK_CAPABILITY_CODE] = CPAC_CAPABILITY_CODE;
    // Set 8 bytes of data
    for ( i = 0;  i < 8;   i++ )
    {
        AdminCpacData [TK_STATUS_BYTE+i] = HexAsciiBin (&pMessage [DATA_INDEX + (i*2)]);
    }
    // Add this message to the transmit queue
    CpacAddMessageToQueue (AdminCpacData);
    // Respond with "Okay"
    AdminMessage( (char *) ctOK );
    return GET_CMD;
}
/***************************************************************************
	Subroutine:		AdminEnterWalkTestMode -- 507

	Description:
		The host has sent an Enter Walk Test Mode Command
		The command format is 507,xx,yy
		This routine will send the same command to the CPAC.

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminEnterWalkTestMode (uint8 * pMessage)
{
    // Error check the received string
    if (( pMessage [COMMA1_INDEX] != COMMA)
            || ( pMessage [COMMA2_INDEX] != COMMA)
            || ( strlen ((const char *) pMessage) != 9))
    {
        AdminMessage( (char *) ctInvalidData );
        return GET_CMD;
    }

    // Set the CPAC Receiver Address
    AdminCpacData [TK_ADDRESS] = HexAsciiBin (&pMessage [RCVR_INDEX]);

    // Check to make sure this CPAC is currently on line
    if (!CartsCheckActive (AdminCpacData [TK_ADDRESS]))
    {
        AdminMessage( (char *) ctInvalidData );
        return GET_CMD;
    }

    // Set the CPAC count
    AdminCpacData [TK_COUNT] = 5;
    // Set the CPAC command
    AdminCpacData [TK_RESPONSE_CODE] = SB_CPAC_ENTER_TEST_MODE;
    // Set the Capability Code
    AdminCpacData [TK_CAPABILITY_CODE] = CPAC_CAPABILITY_CODE;
    // Set one byte of data
    AdminCpacData [TK_STATUS_BYTE] = HexAsciiBin (&pMessage [DATA_INDEX]);
    // Add this message to the transmit queue
    CpacAddMessageToQueue (AdminCpacData);
    // Respond with "Okay"
    AdminMessage( (char *) ctOK );
    return GET_CMD;
}
/***************************************************************************
	Subroutine:		AdminExitWalkTestMode -- 508

	Description:
		The host has sent an Exit Walk Test Mode Command
		The command format is 508,xx
		This routine will send the same command to the CPAC.

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminExitWalkTestMode (uint8 * pMessage)
{
    AdminSend5ByteCpacCommand ( pMessage, SB_CPAC_EXIT_TEST_MODE, 6, 5, 1);
    return GET_CMD;
}



/***************************************************************************
	Subroutine:		AdminSendSerialNumber

	Description:
		The host has sent a CPAC Set Serial Number Command to Vector
		The command format is 509,xx,yyyyyy
		This routine will send the same command to the CPAC.

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminSendSerialNumber (uint8 * pMessage)
{
    // Error check the received string
    if (( pMessage [COMMA1_INDEX] != COMMA)
            || ( strlen ((const char *) pMessage) != SB_CPAC_WRITE_MSG_SIZE))
    {
        AdminMessage( (char *) ctInvalidData );
        return GET_CMD;
    }

    // Set the CPAC Receiver Address
    AdminCpacData [TK_ADDRESS] = HexAsciiBin (&pMessage [RCVR_INDEX]);

    // Check to make sure this CPAC is currently on line
    if (!CartsCheckActive (AdminCpacData [TK_ADDRESS]))
    {
        AdminMessage( (char *) ctInvalidData );
        return GET_CMD;
    }

    // Set the CPAC count - will later get incremented to inlength for the checksum
    AdminCpacData [TK_COUNT] = SB_CPAC_WRITE_SIZE-1;
    // Set the CPAC command
    AdminCpacData [TK_RESPONSE_CODE] = SB_CPAC_WRITE_SERIAL_NUMBER;
    // Set the Capability Code
    AdminCpacData [TK_CAPABILITY_CODE] = CPAC_CAPABILITY_CODE;
    // Set the SERIAL NUMBER value
    AdminCpacData [TK_STATUS_BYTE] = HexAsciiBin (&pMessage [DATA_INDEX]);
    AdminCpacData [TK_NOISE_FLOOR_RIGHT] = HexAsciiBin (&pMessage [DATA_INDEX+2]);
    AdminCpacData [TK_NOISE_FLOOR_LEFT] = HexAsciiBin (&pMessage [DATA_INDEX+4]);
    // Add this message to the transmit queue
    CpacAddMessageToQueue (AdminCpacData);
    // Respond with "Okay"
    AdminMessage( (char *) ctOK );
    return GET_CMD;
}

/***************************************************************************
	Subroutine:		AdminReadSerialNumber -- 510

	Description:
		The host has sent a Read Serial Number Command
		The command format is 510,xx
		This routine will send the same command to the CPAC.

	Inputs:
		pMessage [ ]

	Outputs:
		AdminCpacData [ ]

*****************************************************************************/
uint8 AdminReadSerialNumber (uint8 * pMessage)
{
    AdminSend5ByteCpacCommand ( pMessage, SB_CPAC_READ_SERIAL_NUMBER, 6, 5, 1);
    return GET_CMD;
}




//-----------------------------------------------------------------------
// end of CPAC commands
//-----------------------------------------------------------------------



/************************************************************************/
/*		command 601 */
unsigned char SetPagerData( unsigned char * pMessage )
{
    const uint8 szEnterPagerNumberMessage[] = "Enter Pager #(1-999)";
    const uint8 szEnterPagerDataMessage[] = "Enter Pager Data(1-255)";
    const uint8 szPagerDataSetMessage[] = "Pager Data Save";

    static unsigned char  ucState = GET_CMD;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;		//@021204

        AdminMessage( (char *) szEnterPagerNumberMessage );
        ucState = GET_PAGER_NUMBER;
        return PROCESS_CMD;
    }
    case GET_PAGER_NUMBER:
    {
        if( pMessage[0] == 0 )
            return PROCESS_CMD;		/* return if no number input */

        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;		//@021204

        unPagerNumber = AsciiToInt( pMessage );
        if (( unPagerNumber == 0 ) || ( unPagerNumber > 999 ))
        {
            // bh - Invalid Data was received!
            AdminMessage( (char *) szInvalidData );
            ucState = GET_CMD;
            return GET_CMD;
        }

        AdminMessage( (char *) szEnterPagerDataMessage );


        ucState = GET_INT_VALUE;
        return PROCESS_CMD;
    }
    case GET_INT_VALUE:
    {
        if( pMessage[0] == 0 )
            return PROCESS_CMD;		/* return if no number input */

        unPagerData = AsciiToInt( pMessage );

        LASetPagerData( unPagerNumber, unPagerData );
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;
        AdminMessage( (char *) szPagerDataSetMessage );

        ucState = GET_CMD;
        return GET_CMD;
    }
    default:
    {
        ucState = GET_CMD;
//         return GET_CMD;
    }
    }

    return GET_CMD;
}

/************************************************************************/
/*		command 602 */
unsigned char GetPagerData( unsigned char * pMessage )
{
    const uint8 szEnterPagerNumberMessage[] = "Enter Pager #";
    uint8 	szPagerData[17] = "";

    static unsigned char  ucState = GET_CMD;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;		//@021204

        AdminMessage( (char *) szEnterPagerNumberMessage );
        ucState = GET_PAGER_NUMBER;
        return PROCESS_CMD;
    }
    case GET_PAGER_NUMBER:
    {
        if( pMessage[0] == 0 )
            return PROCESS_CMD;		/* return if no number input */

        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;		//@021204

        unPagerNumber = AsciiToInt( pMessage );

        unPagerData = LAGetPagerData( unPagerNumber );

        UIntToAscii( szPagerData, unPagerData );

        AdminMessage( (char *) szPagerData );

        ucState = GET_CMD;
        return GET_CMD;
    }
    default:
    {
        ucState = GET_CMD;
//         return GET_CMD;
    }
    }

    return GET_CMD;
}

/************************************************************************/
/*		command 610 */
unsigned char ResetPagersData( unsigned char * pMessage )
{
    const uint8 szResetPagersMessage[] = "Reset All Pagers?";
    const uint8 szPagersDataResetMessage[] = "All Pagers Reset.";

    static unsigned char  ucState = GET_CMD;

    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
//      gucAdminTrapFlag = 0;
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        AdminMessage( (char *) szResetPagersMessage );

        ucState = WAIT_RESET_PAGERS;
        return PROCESS_CMD;
    }
    case WAIT_RESET_PAGERS:
    {

        LAResetPagersData();

        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        AdminMessage( (char *) szPagersDataResetMessage );

        ucState = GET_CMD;
        return GET_CMD;
    }
    default:
    {
//         gucAdminTrapFlag = 0;
        ucState = GET_CMD;
//         return GET_CMD;
    }
    }

    return GET_CMD;
}





/************************************************************************/
/*		command 705 */
unsigned char P5_192K_Baud (  )
{
    // Write 19.2k Mode to non-volatile EEprom
    FlashRamData.ucHighSpeedMode = 0;
    FlashRamData.ucP5_192K_Flag = 1;

    // Set flag to trigger flash write
    FlashModified = TRUE;

    // Data entered was okay
    AdminMessage( (char *) ctOK );
    // Wait here for "001" data to be sent
    PetWatchdog ();
    while (Uart [UART_0].TransmitActive)
    {
        Uart0Transmitter ();
    }
    // Delay to allow for the last character of OK to reach the PC
    for (Scratch = 0;  Scratch < 100;  Scratch++)
    {
        PetWatchdog ();
        SBPoll ();
    }

    // Now reprogram UART 0 to run at 19.2K
    // UART0: Set DLAB and 7 data bits, MARK parity, 2 stop bits
    U0LCR = UART_CONTROL_0 | UART_ENABLE_DLAB;
    U0DLL = UART_DEFAULT_BAUD;
    U0DLM = 0;
    // Clear DLAB control bit
    U0LCR = UART_CONTROL_0;
    return GET_CMD;
}



/************************************************************************/
/*		command 709 */
unsigned char SetLocationFlag( unsigned char * pMessage )
{
    const uint8 szLocationFlag[] = "\n\rLocation Flag is ON\n\r";

    FlashRamData.ucLocationFlag = 1;

    // Set flag to trigger flash write
    FlashModified = TRUE;

    if (AdminCaretechHost)
        AdminMessage ( (char *) ctOK );
    else
        SioSetMessage( UART_0,(uint8 *) szLocationFlag,strlen ((const char *)szLocationFlag));
    return GET_CMD;
}

/************************************************************************/
/*		command 710 */
unsigned char ResetLocationFlag( unsigned char * pMessage )
{
    const uint8 szLocationFlag[] = "\n\rLocation Flag is OFF\n\r";

    FlashRamData.ucLocationFlag = 0;
    FlashRamData.eeRssiAveraging = 0;

    // Set flag to trigger flash write
    FlashModified = TRUE;
    RssiAveragingFlag = 0;

    if (AdminCaretechHost)
        AdminMessage ( (char *) ctOK );
    else
        SioSetMessage( UART_0,(uint8 *) szLocationFlag,strlen ((const char *)szLocationFlag));
    return GET_CMD;
}
/************************************************************************/
/*		command 711 - Read Locator Mode  */
unsigned char ReadLocationFlag( unsigned char * pMessage )
{
    if (FlashRamData.ucLocationFlag)
    {
        if (FlashRamData.eeRssiAveraging)
        {
            AdminMessage ( (char *) "708" );
        }
        else
        {
            AdminMessage ( (char *) "709" );
        }
    }
    else
        AdminMessage ( (char *) "710" );
    return GET_CMD;
}




/************************************************************************/
/*		command 800 */
unsigned char DisplayCommands ( unsigned char * pMessage )
{
    const uint8 szDispComM[] = "Commands List\n\r";
    const uint8 szDispComM1[] = "1 Ready..\n\r";
//const uint8 szDispComM2[] = "2 Set Transmitter Number\n\r";
    const uint8 szDispComM5[] = "9 Display Transmitter Incoming Data\n\r";
    const uint8 szDispComM6[] = "10 Reset All Transmitters\n\r";
    const uint8 szDispComM7[] = "11 Display Transmitter Data from Database\n\r";
    const uint8 szDispComM8[] = "12 Enter Transmitter Data to Database\n\r";
    const uint8 szDispComM14[] = "201 Set Real Time Clock Data\n\r";
    const uint8 szDispComM15[] = "202 Get Real Time Clock Data\n\r";
    const uint8 szDispComM17[] = "302 Get Number of Bus Receivers\n\r";
    const uint8 szDispComM18[] = "303 Set Bus Receiver Data\n\r";
    const uint8 szDispComM19[] = "304 Get Bus Receiver Data\n\r";
    const uint8 szDispComM20[] = "310 Reset Bus Receiver Data\n\r";

    const uint8 szDispComM21[] = "403 Set Supervisory Time\n\r";
    const uint8 szDispComM22[] = "404 Get Supervisory Time\n\r";
    const uint8 szDispComM29[] = "601 Set Pager Data\n\r";
    const uint8 szDispComM30[] = "602 Get Pager Data\n\r";
    const uint8 szDispComM31[] = "610 Reset all Pager Data\n\r";
    const uint8 szDispComM36[] = "705 Set PC Port=19.2K Baud\n\r";
    const uint8 szDispComM40[] = "709 Set Location ON\n\r";
    const uint8 szDispComM41[] = "710 Set Location OFF\n\r";
    const uint8 szDispComM52[] = "800 Display all Commands\n\r";
//const uint8 szDispComM55[] = "888 Reset All FLASH Data\n\r";
    const uint8 szDispComM56[] = "33333 Get Vector Firmware Version\n\r";
    const uint8 szDispComM57[] = "50030 Set PC Port=115.2K Baud\n\r";

    SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM,strlen ((const char *)szDispComM)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM1,strlen ((const char *)szDispComM1)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM5,strlen ((const char *)szDispComM5)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM6,strlen ((const char *)szDispComM6)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM7,strlen ((const char *)szDispComM7)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM8,strlen ((const char *)szDispComM8)) ;

    SioSetMessage( UART_0,(uint8 *) szDispComM14,strlen ((const char *)szDispComM14)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM15,strlen ((const char *)szDispComM15)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM17,strlen ((const char *)szDispComM17)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM18,strlen ((const char *)szDispComM18)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM19,strlen ((const char *)szDispComM19)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM20,strlen ((const char *)szDispComM20)) ;

    SioSetMessage( UART_0,(uint8 *) szCR,strlen ((const char *) szCR )) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM21,strlen ((const char *)szDispComM21)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM22,strlen ((const char *)szDispComM22)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM29,strlen ((const char *)szDispComM29)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM30,strlen ((const char *)szDispComM30)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM31,strlen ((const char *)szDispComM31)) ;

    SioSetMessage( UART_0,(uint8 *) szDispComM36,strlen ((const char *)szDispComM36)) ;
    SioSetMessage( UART_0,(uint8 *) szDispComM40,strlen ((const char *)szDispComM40)) ;	//@120601
    SioSetMessage( UART_0,(uint8 *) szDispComM41,strlen ((const char *)szDispComM41)) ;	//@120601
    SioSetMessage( UART_0,(uint8 *) szDispComM52,strlen ((const char *)szDispComM52)) ;	//@120601
    SioSetMessage( UART_0,(uint8 *) szDispComM56,strlen ((const char *)szDispComM56)) ;	//@091503
    SioSetMessage( UART_0,(uint8 *) szDispComM57,strlen ((const char *)szDispComM57)) ;	//@091503

    return GET_CMD;
}
/************************************************************************/

/************************************************************************/
/************************************************************************/
/************************************************************************/

/*		command 888 */
unsigned char ZeroEEPROMData( unsigned char * pMessage )
{
    const uint8  szZeroMessage[] = "Zero EEPROM1?  Takes 10 seconds....(ESC=NO)";
    static unsigned char  ucState = GET_CMD;


    if( *pMessage == SB_KEYPAD_BYPASS )
    {
        AdminMessage( (char *) gszOperationCancledMessage );
        ucState = GET_CMD;
        return GET_CMD;
    }

    switch( ucState )
    {
    case GET_CMD:
    {
        SioSetMessage( UART_0,(uint8 *) szCRLF,strlen ((const char *)szCRLF)) ;

        AdminMessage( (char *) szZeroMessage );
        ucState = GET_CONFIRM_ZERO_EEPROM;
        return PROCESS_CMD;
    }
    case GET_CONFIRM_ZERO_EEPROM:
    {
        Flash_InitiateEraseOperation (FALSE);
        ucState = GET_CMD;
        return GET_CMD;
    }
    default:
    {
        ucState = GET_CMD;
//         return GET_CMD;
    }
    }

    return GET_CMD;
}


/************************************************************************/
void AdminMessage( char *pszAdminMessage )
{
    SioSetMessage( ucAdminCommandPort, (uint8 *) pszAdminMessage, strlen ((const char *) pszAdminMessage	) );
    if (cr_flag != 1)
    {
        SioSetMessage( ucAdminCommandPort, (uint8 *) szCRLF, strlen ((const char *) szCRLF )  );
    }
}






//*****************************************************************************************
void SendDebugMessage( char *pszAdminMessage )
{
    SendDebugMessageX (pszAdminMessage, TRUE, TRUE);
}




void SendDebugMessageX( char *pszAdminMessage, char bSendTimeStamp, char bSendCR )
{
    unsigned char  aucData[35];
    if (EECheckForWriteOperation ())return; // EEprom Write Operation in progress
    if (iPauseDebugTimer>0) return;	//If doing a big database download then skip sending debug
    if (strlen ((const char *) pszAdminMessage) > MAX_STRING_LENGTH) return;

    //This is to throttle the transmiit data which can cause a crash
    if (iCom1TxCharWaiting < 0)
    {
        iCom1TxCharWaiting=0;
        Com1_xmit_in=0;
        Com1_xmit_out = 0;
    }
    if (iCom1TxCharWaiting > COM_1_QUEUE_SIZE)
    {
        iCom1TxCharWaiting=0;
        Com1_xmit_in=0;
        Com1_xmit_out = 0;
    }
    if (iCom1TxCharWaiting > COM_1_QUEUE_SIZE-20)
    {
        if (iCom1TxCharWaiting < COM_1_QUEUE_SIZE-4)
            SioSetMessage( UART_1, (uint8 *) "?\r", 2);
        iBufferErrors++;
        return;
    }

    if (bSendTimeStamp)
    {
        RTCGetData( aucData);
        aucData[17]='-';
        aucData[18]='>';
        aucData[19]=0;
        SioSetMessage( UART_1, (uint8 *) aucData, strlen ((const char *) aucData));
    }

    if (strlen ((const char *) pszAdminMessage) >0) SioSetMessage( UART_1, (uint8 *) pszAdminMessage, strlen ((const char *) pszAdminMessage	) );

    if (bSendCR)
    {
        SioSetMessage( UART_1, (uint8 *) szCR, strlen ((const char *) szCR )  );
    }
}


//*****************************************************************************************

void SendCR_DebugPort(void)
{
    SioSetMessage( UART_1, (uint8 *) szCR, strlen ((const char *) szCR )  );
}






char* GetAlarmType (unsigned char ucType)
{
    switch (ucType)
    {
    case POINT_ALARM:
        return "PHB Alarm";
    case POINT_FALL_DETECTED:
        return "Fall Alarm";
    case POINT_ACKNOWLEGE:
        return "Acknowledge";
    case POINT_TAMPER:
        return "Tamper";
    case POINT_RESTORE:
        return "Restore";
    case POINT_LOW_BATTERY:
        return "Low Batt";
    case POINT_CPAC:
        return "CAWM Alarm";
    case POINT_SUPERVISORY:
        return "Supervise Alarm";
    case POINT_PHB_7000:
        return "PHB+ Alarm";
    case POINT_AAHB_7000:
        return "AAHB+ Alarm";
    case POINT_MHB_7000:
        return "MHB Alarm";
    default:
        return "Unknown";
    }
}





char* GetDeviceType (unsigned char ucType)
{
    switch (ucType)
    {
    case SB_WALLMOUNT_TOUCHPAD:
        return "Wall Touchpad";
    case SB_SMOKE_DETECTOR:
        return "Smoke Det";
    case SB_PENDANT:
        return "PHB";
    case SB_PASSIVE_INFARED_RECEIVER:
        return "PIR";
    case SB_CPAC_TYPE:
        return "CPAC";
    case SB_GLASS_BREAK_SENSOR:
        return "Glass Break";
    case SB_DOOR_WINDOW_SENSOR:
        return "UTX";
    case SB_SINGLE_BUTTON_PANIC:
        return "Panic";
    case SB_HANDHELD_TOUCHPAD:
        return "Hand Touchpad";
    case SB_7000_PHB:
        return "7000 PHB";
    case SB_7000_AAHB:
        return "7000 AAHB";
    case SB_7000_MHB:
        return "7000 MHB";
    default:
        return "Unknown";
    }
}






/************************************************************************/


/************************************************************************/
/*		command 50002 - Set Point Data */
void AdminSetPointData ( unsigned char * pMessage )
{
    unsigned char result;

    // Make sure to turn off Points Checking if it was on.
    ActivateDuplicatePointsCheckFlag = 0;
    HighestCarPoint = 0;

    // v5.00 --- Wait here if an ERASE operation is still in progress
    PetWatchdog ();
    while (FlashState == FLASH_WAIT_FOR_ERASE)
    {
        FlashExecutive ();
        // Service foreground UART tranmsmitters
        Uart0Transmitter ();
        Uart3Transmitter ();
    }
    PetWatchdog ();

    // Write the new Points Record to EEprom
    result = SBSetPointData( &pMessage [AFTER_COMMA_MARK] );  //==6
    if ( result )
    {
        // bh - Invalid Data was received!
        AdminMessage(  (char *) ctInvalidData );
    }
    else
    {
        // Data entered was okay
        AdminMessage( (char *) ctOK );
        // bh - v4.02 - set flag to say we are in download mode
        Command_12_active = 1;
        gucAdminTrapFlag = 1;
        SavedAlarmsState = SAVED_ALARMS_START_TIMER;
    }
}


/************************************************************************/
/*		command 50003 - Set Date and Time */
void AdminSetDateTime ( unsigned char * pMessage )
{
    if (AdminCheckDateTime (&pMessage [AFTER_COMMA_MARK]) )
    {
        // bh - Invalid Data was received!
        AdminMessage( (char *) ctInvalidData );
    }
    else
    {
        // Set the new date and time
        RTCSetData( &pMessage [AFTER_COMMA_MARK] );
        //Send this time to all CAWMs
        SendTimeToCawms(&pMessage [AFTER_COMMA_MARK]);
        // Data entered was okay
        AdminMessage( (char *) ctOK );

        SendFacilityCodeToCawms ();
    }
}




/************************************************************************/
/*		command 50004 - Set Supervisory Time */
void AdminSetSupervisoryTime ( unsigned char * pMessage )
{
    unsigned char i;

    for ( i=AFTER_COMMA_MARK; pMessage[i];  i++)
    {
        if ((pMessage[i] < '0') || (pMessage[i] > '9'))
        {
            // bh - Invalid Data was received!
            AdminMessage( (char *) ctInvalidData );
            return;
        }
    }

    unSupervisoryTime = AsciiToInt( &pMessage [AFTER_COMMA_MARK] );
    if( unSupervisoryTime < 2 )
    {
        // bh - Invalid Data was received!
        AdminMessage( (char *) ctInvalidData );
    }
    else
    {
        SBSetSupervisoryTime( unSupervisoryTime );
        // Data entered was okay
        AdminMessage( (char *) ctOK );
    }
}





/************************************************************************/
/*		command 50005 - Set Pager Data */
void AdminSetPagerData ( unsigned char * pMessage )
{
    unsigned char i;
    unsigned char comma_index;

    // locate second comma in data string
    for ( i = AFTER_COMMA_MARK; pMessage[i] != ',';  i++ )
    {
        if ( i > (SECOND_COMMA+2))
            break;
    }
    // replace the second comma with a NULL terminator
    comma_index = i;
    pMessage [comma_index++] = 0;
    // get the page number
    unPagerNumber = AsciiToInt( &pMessage [AFTER_COMMA_MARK] );
    if (( unPagerNumber == 0 ) || ( unPagerNumber > 999 ))
    {
        // bh - Invalid Data was received!
        AdminMessage( (char *) ctInvalidData );
    }
    else
    {
        // Get the page data
        unPagerData = AsciiToInt( &pMessage [comma_index] );
        LASetPagerData( unPagerNumber, unPagerData );
        // Data entered was okay
        AdminMessage( (char *) ctOK );
    }
}


/************************************************************************/
/*		command 51002 - Read Point Data */


/************************************************************************/
/*		AdminCheckDateTime ()

	return 1 - Date/Time error
		   0 - Date/Time is valid
************************************************/
unsigned char AdminCheckDateTime ( unsigned char * pMessage )
{
    unsigned char i;

    for (i = 0; pMessage[i]; i++)
    {
        if ((pMessage[i] < '0') || (pMessage[i] > '9'))
        {
            if ((pMessage[i] != ':') && (pMessage[i] != '/') && (pMessage[i] != ' '))
            {
                // bh - Invalid Data was received!
                return 1;
            }
        }
    }
    return 0;
}


/************************************************************************
  		command 50010 - Initiate Firmware Download

		We have received a 50010 command:
		1) Respond with "001" - ACK
		2) Wait until response has been sent
		3) Clobber the checksum in Flash at location 0x00000014
		4) Hang and wait for the Watchdog to reset us, sending the code into the bootblock

************************************************************************/
void AdminInitiateFirmwareDownload ( )
{
//	IAP			iap_entry;
//	uint32		temp;
//	void		(*bootloader_entry)(void);
//	uint32 *	ptr1;
//	uint32 *	ptr2;

    // Respond with "001" (okay) to the firmware download command
    AdminMessage( (char *) ctOK );
    // Wait here for "001" data to be sent
    PetWatchdog ();
    while (Uart [UART_0].TransmitActive)
    {
        // Transmit 001 to SmartCare
        Uart0Transmitter ();
    }

    // Turn off interrupts
    VICIntEnClr = 0xFFFFFFFF;
    VICIntSelect = 0;

    // Signal the Microchip microcontroller to reset us.
    FIO2CLR = INITS_2_DOWNLOAD;

    /************************************
    //---------------------------------------------------
    	// Connect RXD0 and TXD0 pins to GPIO
    	PINSEL0 = 0xFFFFFFF3;

    	// Select P0.14 as an output and P0.1 as an input
    	temp = IODIR0;
    	temp |= 0x4000;
    	temp &= 0xFFFFFFFD;
    	IODIR0 = temp;

    	// Make p2.10 an output and lower it!
    	FIO2DIR = 0x000014FF;
    	FIO2CLR = 0x00000400;

    	// Clear P0.14
    	IOCLR0 = 0x4000;

    	// Disconnect the PLL
    	PLLCON = 0;
    	PLLFEED = 0xAA;
    	PLLFEED = 0x55;

    	// Set APBDIV to the reset value
    	APBDIV = 0x00;

    	// Restore the reset state of Timer1
    	T1PR = 0x00;
    	T1MCR = 0x00;
    	T1CCR = 0x00;

    	// Map bootloader vectors
    	MEMMAP = 0x00;

    	// Now jump to the ISP
    //	IAP_Command [0] = IAP_REINVOKE_ISP;
    //	iap_entry (IAP_Command, IAP_Results);
    //	temp = IAP_Results [0];


    	// Point to bootloader entry point
    	bootloader_entry = (void (*)(void)) (0x00);
    	// Jump to the bootloader
    	bootloader_entry ();

    //------------------------------------------------------
    ********************************************/



    // Now just sit here and wait for the Microchip PIC to reset us:
    while (1)
    {
        Scratch++;

        // service the watchdog timer:
        WDFEED = INITS_WD_1;
        WDFEED = INITS_WD_2;
    }

}



/************************************************************************/
/*		command 50030 - Switch to High Speed Mode */
void AdminSwitchToHighSpeedMode ( )
{
    // Write High Speed Mode to non-volatile EEprom
    FlashRamData.ucHighSpeedMode = HIGH_SPEED_MODE;
    FlashRamData.ucP5_192K_Flag = 1;

    // Set flag to trigger flash write
    FlashModified = TRUE;
    // Data entered was okay
    AdminMessage( (char *) ctOK );
    // Wait here for "001" data to be sent
    PetWatchdog ();
    while (Uart [UART_0].TransmitActive)
    {
        // transmit 001 to SmartCare
        Uart0Transmitter ();
    }
    // Wait for the last character of OK to reach the PC
    PetWatchdog ();
    while ( !(U0LSR & UART_STATUS_TEMT))
    {
        // Poll the next slave
        SBPoll ();
    }
    // Now reprogram UART 0 to run at 115.2K
    // UART0: Set DLAB and 7 data bits, MARK parity, 2 stop bits
    U0LCR = UART_CONTROL_0 | UART_ENABLE_DLAB;
    U0DLL = UART_115200_BAUD;
    U0DLM = 0;
    // Clear DLAB control bit
    U0LCR = UART_CONTROL_0;
}




/************************************************************************/
/*		command 50040 - Set Locator Time */
void AdminSetLocatorTime ( unsigned char * pMessage )
{
    LocatingTime = AsciiToInt( &pMessage [AFTER_COMMA_MARK] );
    if (( LocatingTime < ADMIN_LOCATE_TIME_LOW ) || ( LocatingTime > ADMIN_LOCATE_TIME_HIGH ))
    {
        // bh - Invalid Data was received!
        AdminMessage( (char *) ctInvalidData );
    }
    else
    {
        FlashRamData.ucLocatorTime = LocatingTime;

        // Set flag to trigger flash write
        FlashModified = TRUE;
        // Data entered was okay
        AdminMessage( (char *) ctOK );
    }
}

/************************************************************************/
/*		command 50050 - Set PIR Filter Time */
void AdminSetPirFilterTime ( unsigned char * pMessage )
{
    PirFilteringTime = AsciiToInt( &pMessage [AFTER_COMMA_MARK] );

    // 0xFF or zero get converted to Nominal Filter Time
    if ((PirFilteringTime == PIR_EXPIRED)
            || (PirFilteringTime == 0))
    {
        // zero and 255 are illegal values - if received, set it to the nominal filtering time
        PirFilteringTime = PIR_NOMINAL_DELAY;
    }
    FlashRamData.ucPirFilterTime = PirFilteringTime;

    // Set flag to trigger flash write
    FlashModified = TRUE;
    // Data entered was okay
    AdminMessage( (char *) ctOK );
}



 
void AdminSetRFID_FilterTime ( unsigned char cSetting )
{
	
		if (cSetting<70) cSetting = 0;
		if (cSetting>105) cSetting = 105;
    FlashRamData.cRssiLevelforFalsePacket = cSetting;
	
	 sprintf(sString, " - Setting RFID Filter level to %u", FlashRamData.cRssiLevelforFalsePacket);
   SendDebugMessageX(sString, FALSE, TRUE);

    // Set flag to trigger flash write
    FlashModified = TRUE;
    // Data entered was okay
    AdminMessage( (char *) ctOK );
}



/************************************************************************/
/*		command 51040 - Read Locator Time */
void AdminReadLocatorTime ( )
{
    // Convert the Locator Time to Ascii
    UIntToAscii ( szReceiverNumber, FlashRamData.ucLocatorTime  );
    AdminMessage ( (char *) szReceiverNumber );
}

/************************************************************************/
/*		command 51050 - Read PIR Filter Time */
void AdminReadPirFilterTime ( )
{
    // Convert the PIR Filter Time to Ascii
    UIntToAscii ( szReceiverNumber, FlashRamData.ucPirFilterTime  );
    AdminMessage ( (char *) szReceiverNumber );
}
void AdminReadRFIDFilterTime ( )
{
    // Convert the PIR Filter Time to Ascii
    UIntToAscii ( szReceiverNumber, FlashRamData.cRssiLevelforFalsePacket  );
    AdminMessage ( (char *) szReceiverNumber );
		sprintf (sString," - Reading RSSI Filter = %u",FlashRamData.cRssiLevelforFalsePacket);
    SendDebugMessageX(sString, FALSE, TRUE);

}






/************************************************************************
	Subroutine:		AdminSnifferOutput ()

	Description:
		If Mode 8 is active, this routine will dump the rs-485 message
		to UART_3.

	Inputs:
		buffer

	Outputs:
		acTemp
	Locals:
		x, y, count
***************************************************************************/
void AdminSnifferOutput (uint8 * buffer, uint8 direction)
{
    uint8  x, y, count;

    // Is our internal RS-485 sniffer active?
    if (( Mode_8_Flag ) || (bSuperBusSnifferEnabled))
    {
        // The sniffer is active -- dump the rs-485 data to UART_0
        x = 0;
        count = buffer [TK_COUNT];
        if (count > ADMIN_SNIFFER_MAX) count = ADMIN_SNIFFER_MAX;
        if (direction == SB_SNIFF_TRANSMIT)
        {
            acTemp[ x++ ] = ' ';
            acTemp[ x++ ] = ' ';
            acTemp[ x++ ] = ' ';
        }

        // Print the direction letter
        acTemp[ x++ ] = direction;
        for ( y = 0;  y < count;  y++ )
        {
            acTemp[ x++ ] = BinHexAscii( buffer [y] >> 4 );
            acTemp[ x++ ] = BinHexAscii( buffer [y] );
            acTemp[ x++ ] = ' ';
        }

        // Set a NULL terminator on the end of the message
        acTemp[ x++ ] =  0;
        if (Mode_8_Flag) AdminMessage( (char *) acTemp );
        if (bSuperBusSnifferEnabled) SendDebugMessageX ((char *)acTemp, TRUE, TRUE);
    }
}



#ifdef SPECTRALINK
/************************************************************************
	Subroutine:
		AdminAddPortNumber
		command 50020 - Add new Spectralink Port Number

	Inputs:
		pMessage

	Outputs:
		record
		port_number

	Locals:

*************************************************************************/
void AdminAddPortNumber ( unsigned char * pMessage )
{
    unsigned char i;
    unsigned char record;
    uint16 port_number;

    // locate second comma in data string
    for ( i = AFTER_COMMA_MARK; pMessage[i] != ',';  i++ )
    {
        if ( i > (SECOND_COMMA+2))
            break;
    }
    // replace the second comma with a NULL terminator
    pMessage [i++] = 0;
    // Get the port number
    port_number = AsciiToInt( &pMessage [i] );
    // get the record number
    record = AsciiToInt( &pMessage [AFTER_COMMA_MARK] );
    if (( record == 0 ) || ( record > SL_NUMBER_OF_HANDSETS ))
    {
        // bh - Invalid Data was received!
        AdminMessage( (char *) ctInvalidData );
    }
    else
    {
        SpectralinkAddPortNumber ( record, port_number );
        // Data entered was okay
        AdminMessage( (char *) ctOK );
    }
}


/************************************************************************
	Subroutine:
		AdminDeletePortNumber
		command 50022 - Delete Spectralink Port Number

	Inputs:
		pMessage

	Outputs:
		record
		port_number

	Locals:

*************************************************************************/
void AdminDeletePortNumber ( unsigned char * pMessage )
{
    unsigned char record;

    record = AsciiToInt( &pMessage [AFTER_COMMA_MARK] );
    if ( SpectralinkReadPortNumber (record) == 0 )
    {
        // bh - Invalid Data was received - the record was already zero!
        AdminMessage( (char *) ctInvalidData );
    }
    else
    {
        // Delete the n'th Spectralink Port Number
        SpectralinkDeletePortNumber ( record );
        // Data entered was okay
        AdminMessage( (char *) ctOK );
    }
}



/************************************************************************
	Subroutine:
		AdminReadPortNumber
		command 51020 - Read Spectralink Port Number

	Inputs:
		pMessage

	Outputs:
		record
		port_number

	Locals:

*************************************************************************/
void AdminReadPortNumber ( unsigned char * pMessage )
{
    unsigned char record;
    uint16 port_number;

    // convert the ascii record to binary
    record = AsciiToInt( &pMessage [AFTER_COMMA_MARK] );
    if (( record == 0 ) || ( record > SL_NUMBER_OF_HANDSETS ))
    {
        // bh - Invalid Data was received!
        AdminMessage ((char *) ctInvalidData);
    }
    // Get the port number
    port_number = SpectralinkReadPortNumber (record);
    // Convert the port number from binary to ASCII
    UIntToAscii( szReceiverNumber, port_number );
    // Transmit the Ascii port number to the application
    AdminMessage ((char *) szReceiverNumber);
}





void SendLogMessageToHost (char *sMessage)
{
    SioSetMessage( UART_0, (uint8 *)sMessage, strlen(sMessage) );
}





void PAbt_HandlerX (void) __irq
{
    printf ("PA Exception Error!");
}

void DAbt_HandlerX (void) __irq
{
    printf ("DA Exception Error!");
}

#endif






