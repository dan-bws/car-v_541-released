/********************************************************
	File	:	addendum.h
	Title	:	addendum.h
	Author	:	Bob Halliday
					bohalliday@aol.com    (781) 863-8245
	Copyright:	(C) 2007 Philips/Lifeline Corporation, Framingham, MA

	Description:
		Header file holding the definitions for addendum.c
		
************************************************************/
#ifndef HEADER_ADDENDUM
#define HEADER_ADDENDUM

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_ADDENDUM
#define SCOPE
#else
#define SCOPE	extern
#endif

// Local Variables:
#ifdef BODY_ADDENDUM


	
#endif



/*******************************
*   Section 1:  Equate Definitions: 
*********************************/

#define ADDENDUM_START_FLASH_WRITE			0x02
#define ADDENDUM_RECORD_SIZE				17
#define ADDENDUM_NUM_RECORDS				30
#define ADDENDUM_DATABASE_SIZE				(ADDENDUM_NUM_RECORDS * ADDENDUM_RECORD_SIZE)

#include "main.h"

typedef struct
{
	uint8	RecordCount;									 
	uint8	data [ADDENDUM_NUM_RECORDS][ADDENDUM_RECORD_SIZE];									 
} ADDENDUM;

/*******************************
	Section 2: RAM Definitions
********************************/




// Global Variables

extern		ADDENDUM		AddendumDatabase;
extern		uint8 *			AddendumPointer;





/*******************************
*  Section 3:  Function prototypes 
*********************************/
void  	AddendumInitialize (void);
void  	AddendumSetPointData (uint8 * );
uint8 	AddendumSetSinglePoint (uint8 * );
uint8 * AddendumGetEmptyRecord (void);
uint8 * AddendumSearchForRfid (uint32 );
uint8 * AddendumSearchForPoint (uint32 point);
uint8 * AddendumConfigureFlashWrite (void);
void 	AddendumEraseRecord (uint8 * );
void 	AddendumEraseOnePoint (uint8 *);
void 	AddendumReadDatabase (void);
void 	AddendumClearDatabase (void);






/*******************************
*   Section 4:  Macros 
*********************************/






/*******************************
*   Section 5:  ROM Tables 
*********************************/
#ifdef BODY_ADDENDUM

extern const uint8 ctInvalidData[];
extern const uint8 ctOK[];



#else


#endif


#undef SCOPE


#endif

/* ======== END OF FILE ======= */
