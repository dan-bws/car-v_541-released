/********************************************************
	File	:	config.h
	Title	:	config Drivers Header File
	Author	:	Bob Halliday
	Ported	:	August 2005

	Description:
		Header file for the CONFIG driver source file.
		
************************************************************/

#ifndef HEADER_CONFIG
#define HEADER_CONFIG

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_CONFIG
#define SCOPE
#else
#define SCOPE	extern
#endif

/*******************************
*   Section 1:  Equate Definitions: 
*********************************/
#define CONFIG_MAX_CALLBACK_INTERVAL				7


#define CONFIG_WRITE_TRANSACTION				0x00
#define CONFIG_READ_TRANSACTION					0x01
#define CONFIG_READ								0x01
#define CONFIG_READ_OR_WRITE_MASK				0x01
#define CONFIG_CLOCK_100KHZ						0x15
#define CONFIG_WATCH_GPTB_01					0x03
#define CONFIG_WAIT_TO_WATCH_CLK				80				// 80us
#define CONFIG_SHORT_PACKET						2				// 
#define CONFIG_ACK_WAIT							25


#define CONFIG_INIT_MASTER_MODE					0x30

// This structure has 12 bytes
#define CONFIG_STRUCT_SIZE            			12
#define CONFIG_DEVICE_SIZE            			5
#define CONFIG_LIMIT_SIZE             			7
#define CONFIG_ID_TEMP                			0
#define CONFIG_ID_VOLT                			1
#define CONFIG_ID_RSSI                			2



#define CONFIG_NUMBER_OF_SENSORS					15
#define CONFIG_BASE_SIZE							16
#define CONFIG_SENSOR_SIZE							16
#define CONFIG_SERIAL_NUMBER_SIZE					3
#define CONFIG_END_OF_PHONE_STRING					0x3F
				 
#define CONFIG_WAIT_BETWEEN_TRANSACTIONS			(uint32)(10/TIME_BASE)
#define CONFIG_POLL_TIME							(uint32)(40/TIME_BASE)
#define CONFIG_WAIT_FOR_RESUME_TIME					(uint32)(15/TIME_BASE)
#define CONFIG_WAIT_FOR_BUS_TIME					(uint32)(60000/TIME_BASE)
#define CONFIG_READBACK_DELAY						(uint32)(4000/TIME_BASE)


// Config state machine states:
enum
	{
	CONFIG_DISABLED,
	CONFIG_START_WAIT,
	CONFIG_READ_EEPROM,
	CONFIG_FINISHED_READING,
	CONFIG_TERMINATE_CONNECTION
	};


// ConfigInterrupt state machine states:
enum
	{
	CONFIG_INT_DISABLED,
	CONFIG_INT_SENDING_DATA,
	CONFIG_INT_RECEIVING_DATA
	};


// indeces for base operational data
enum
	{
	BASE_OP_SN0,
	BASE_OP_SN1,
	BASE_OP_SN2,
	BASE_OP_DEVICE_TYPE,
	BASE_OP_CONFIG_CRC,
	BASE_OP_LOW_LIMIT0,
	BASE_OP_LOW_LIMIT1,
	BASE_OP_LOW_LIMIT2,
	BASE_OP_HIGH_LIMIT0,
	BASE_OP_HIGH_LIMIT1,
	BASE_OP_HIGH_LIMIT2,
	BASE_OP_LIMIT_CRC
	};


//---------------------------------------------------------------------




// Variables used in the Config module
typedef struct
{
	uint8	State;									 
	uint8	BaseStatus;								
	uint8	BaseStatus2;								
	uint8	NumberOfSensors;								
	uint8	NumberOfSensorsBackup;								
	uint8	CallbackIntervalDays;
	uint8	Index;
	uint8	ReadbackFlag;
	uint8	SelfTestPeriod;
	uint8	SerialNumber [CONFIG_SENSOR_SIZE];
	uint8	BaseOperationalData [CONFIG_BASE_SIZE];
	uint32	CallbackIntervalMinutes;
	uint32	SensorSerialNumber [CONFIG_NUMBER_OF_SENSORS];
} CONFIG_DATA;





/*******************************
	Section 2: RAM Definitions
********************************/

// Local Variables:
#ifdef BODY_CONFIG

 			 
	
#endif



// Global Variables

SCOPE		CONFIG_DATA			Config;



/*************************************
*  Section 3:  Function prototypes 
*************************************/
void 	ConfigInitialize (void);
void 	ConfigExecutive (void);
void 	ConfigUpdateSystemVariables (void);
void 	ConfigDeconstructPhoneNumber (uint32);
void 	ConfigReadAllSensorSerialNumbers (uint32);
void 	ConfigTriggerEEpromReadback (void);
void 	ConfigConvertCallbackInterval (uint32 );
void 	ConfigCheckBaseOperationalData (uint32 );
void 	ConfigSetBaseStatus (void);
void    ConfigConvertBcdToAsciiPhoneNumber (uint8 * );
uint8 	ConfigInspectData (uint8 *);



/*******************************
*   Section 4:  Macros 
*********************************/









/*******************************
*   Section 5:  ROM Tables 
*********************************/

/* Data to be transmitted */
#ifdef BODY_CONFIG

/*****************
	Power up initialization of Config
	E0 = No echo from config
	V0 = No verbose mode.  Status framed by <CR> only
	B1 = Set Bell 103 Mode.
*******************/
const uint8 ConfigBell_103 [ ] =
	{
	"ATE0V0B1\r"
	};

const uint8 ConfigDialOut [ ] =
	{
	"ATDT"
	};

const uint8 ConfigHangUp [ ] =
	{
	"ATH0\r"
	};


#else


#endif


#undef SCOPE


#endif

/* ======== END OF FILE ======= */
