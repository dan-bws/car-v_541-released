/********************************************************
	File	:	isr.h
	Title	:	isr.h
	Author	:	Bob Halliday
					bohalliday@aol.com    (781) 863-8245
	Copyright:	(C) 2007 Philips/Lifeline Corporation, Framingham, MA

	Description:
		Header file holding the definitions for isr.c
		
************************************************************/
#ifndef HEADER_ISR
#define HEADER_ISR

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_ISR
#define SCOPE
#else
#define SCOPE	extern
#endif

/*******************************
*   Section 1:  Equate Definitions: 
*********************************/
#define	ISR_SELECT_FIQ					0x300000D0
#define	ISR_SELECT_INTS					0x300000D0

#define	ISR_TIMER0_INT					0x00000010
#define	ISR_UART0_INT					0x00000040
#define	ISR_UART1_INT					0x00000080
#define	ISR_UART2_INT					0x10000000
#define	ISR_UART3_INT					0x20000000
#define	ISR_I2C_INT						0x00000200
#define	ISR_EINT0_INT					0x00004000
#define	ISR_EINT3_INT					0x00020000
#define	ISR_GPIO_INT					0x00020000

#define	ISR_TIMER0_ENABLE_INTS			0x20
#define ISR_EINT0_EDGE_SENSITIVE		0x01
#define ISR_EINT0_RISING_EDGE			0x01
#define ISR_EINT0_FALLING_EDGE			0x00


/*******************************
	Section 2: RAM Definitions
********************************/

// Local Variables:
#ifdef BODY_ISR


	
#endif


// Global Variables


/*******************************
*  Section 3:  Function prototypes 
*********************************/
void IsrInitialize (void);
//void IsrInterruptHandler (void)	__fiq;






/*******************************
*   Section 4:  Macros 
*********************************/






/*******************************
*   Section 5:  ROM Tables 
*********************************/
#ifdef BODY_ISR


#else


#endif


#undef SCOPE


#endif

/* ======== END OF FILE ======= */
