/***********************************************************
	Module Name:	addendum.c

	Author:			Bob Halliday, December, 2007
					bohalliday@aol.com   (781) 863-8245

	Description:
		This file manages the Addendum Database.
		Each record in the Addendum Database has 17 bytes:

		Byte	Function
		0-1		CAR Point Number
		2-5		RFID
		6		Control Byte
		7-8		SmartCare Point Number
		9		Title ID
		10-16	Name - a literal ASCII string

					
	Subroutines:	AddendumInterruptHandler()
					AddendumInitialize ()	

	Revision History:


*************************************************************/

#define  BODY_ADDENDUM

#include <stdio.h>
#include <string.h>
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"

#include "database.h"
#include "addendum.h"

#include "admin.h"                        
#include "sio.h"                        
#include "psi2000.h"
#include "utils.h"
#include "sbus.h"
#include "rtc.h"
#include "inits.h"
#include "carts.h"
#include "cpac.h"
#include "mprocess.h"
#include "uart.h"



ADDENDUM		AddendumDatabase;
uint8 *			AddendumPointer;

/***********************************************************
	Subroutine:	 AddendumInitialize()

	Description:
		Initializations for the Addendum Points Database are performed here
		
	Inputs:	
		
	Outputs:
		
	Locals:
		
*************************************************************/
void AddendumInitialize ()
{
}





/***********************************************************
	Subroutine:	 AddendumSetPointData()

	Description:
		command 60002 - Set Point Data 
		A Points Record has been received and must be stored in the Addendum Database.
		
	Inputs:	
		
	Outputs:
		
	Locals:
		
*************************************************************/
void AddendumSetPointData ( uint8 * pMessage )
{
	unsigned char result;

	// v5.00 --- Wait here if an ERASE operation is still in progress
		PetWatchdog ();
	while (FlashState == FLASH_WAIT_FOR_ERASE)
		{
		FlashExecutive ();
		// Service foreground UART tranmsmitters
		Uart0Transmitter ();
		Uart3Transmitter ();
		}
		PetWatchdog ();

	// Write the new Points Record to EEprom
    result = AddendumSetSinglePoint( &pMessage [AFTER_COMMA_MARK] );
	if ( result )
		{
		// bh - Invalid Data was received!
         AdminMessage(  (char *) ctInvalidData );
		}
	else
		{			
		// Data entered was okay
        AdminMessage( (char *) ctOK );
		}
}



/***********************************************************
	Subroutine:	 AddendumSetSinglePoint()

	Description:
		command 60002 - Set Point Data 
		A Points Record has been received and must be stored in the Addendum Database.
		
	Inputs:	
		
	Outputs:
		
	Locals:
		
*************************************************************/
uint8 AddendumSetSinglePoint (uint8  *pMessage)
{
	uint8 ucOutputIndex;
	uint8 ucTemp [14];
	uint8 ucMessageIndex;
	uint8 ucInputIndex;
	uint8 *ptr;					

	// Check for database full
	if (AddendumDatabase.RecordCount >= ADDENDUM_NUM_RECORDS)
		return 1;

   	ucInputIndex = 0;
   	ucOutputIndex = 0;         
    /* Get Point Number */
   	while( pMessage[ ucInputIndex ] != ',' )
   		{
      	ucTemp[ucOutputIndex++ ] = pMessage[ ucInputIndex++ ];
	  	if ( ucInputIndex > 8 )
			{
			// bh - ERROR! -- bad string.  We should have found a comma by now
			return 1;
			}
   		}

	// v500 --- Vector support:  Point Number can be 4 or 5 characters long
	if (ucOutputIndex < MIN_POINT_STRING_SIZE)
		return 1;

   	ucTemp[ ucOutputIndex ] = 0;
   	ucInputIndex++;               /* Point past comma */

 	// Convert the Point Number from Ascii to binary  
   	unPointNumber = AsciiToInt (ucTemp);
	// v5.12 - Look for a copy of this point.  If found, signal success and exit!
	ptr = AddendumSearchForPoint (unPointNumber); 
	if (ptr)
		{
		// This point is already in the Addendum Database - return success!
		return 0;
		}

	// Point to the next empty record in the Addendum Database
	AddendumPointer = AddendumGetEmptyRecord ();
	// Save the Point Number for this record
    *AddendumPointer++ = (uint8) (unPointNumber >> 8);
    *AddendumPointer++ = (uint8) unPointNumber;
   
    // Get Point Message Data 
   	ucMessageIndex = 0;
   	// Convert the RFID from Ascii to Binary and store it in the database                                
   	while( pMessage[ ucInputIndex ] != ',' )
   		{  
      	ucTemp[0] = pMessage[ ucInputIndex++ ];
      	ucTemp[1] = pMessage[ ucInputIndex++ ];
      	ucTemp[2] = 0;
      	*AddendumPointer++ = HexAsciiBin( ucTemp );
		ucMessageIndex++;
	  	if ( ucInputIndex > 60 )
			{
			// bh - ERROR! -- bad string.  We should have found a comma by now
			return 1;
			}
   		}

	// v415 --- Point Message Data MUST be 7 characters, no less!!!!
	if (ucMessageIndex != RFID_MESSAGE_SIZE)
		return 1;

   	ucInputIndex++;               /* Point past comma */
	// Store the ASCII Location value into the database
   	while( pMessage[ ucInputIndex ] != 0 )
   		{  
      	*AddendumPointer++ = pMessage [ ucInputIndex++ ];
	  	if ( ucInputIndex > 80 )
			{
			// bh - ERROR! -- bad string.  We should have found a comma by now
			return 1;
			}
   		}

	// The record has been written to RAM - now write it to FLASH
	FlashAddendumControlFlag = ADDENDUM_START_FLASH_WRITE;
	// Update the record counter
	AddendumDatabase.RecordCount++;

	// v5.00 --- Wait here while the Addendum database gets rewritten to flash
	PetWatchdog ();
	while (FlashModified || FlashAddendumControlFlag || FlashEraseFlag || FlashState)
		{
		FlashExecutive ();
		// Service foreground UART tranmsmitters
		Uart0Transmitter ();
		Uart3Transmitter ();
		}
		PetWatchdog ();

	return 0;
}



/***********************************************************
	Subroutine:	 AddendumEraseOnePoint()

	Description:
		Command 60003 -- Erase one point from the Points Database
		A Points Number has been received and we will erase this Point, 
		 either from the main database, or from the addendum database.
		
	Inputs:	
		pMessage		
	Outputs:
		
	Locals:
		
*************************************************************/
void AddendumEraseOnePoint (uint8 *pMessage)
{
	uint32 PointNumber, carepoint;
	uint8 * ptr;


	// v5.00 --- Wait here if a Flash operation is still in progress
	PetWatchdog ();
	while (FlashState > 0)
		{
		FlashExecutive ();
		// Service foreground UART tranmsmitters
		Uart0Transmitter ();
		Uart3Transmitter ();
		}
		PetWatchdog ();

	// Convert the Point Number from Ascii to binary
	PointNumber = (uint32) AsciiToInt (pMessage);

	// Read initial 3 blocks of FLASH into RAM
	Flash_InitializePointsImage (PointNumber);

	// Get the Care Point for the point to be deleted
	carepoint = SBGetDeviceIDz (FlashRamWalkAddress);
	// Is there a valid rfid at this location?
	 if (carepoint != 0xFFFF)
		{
		// This is a found, valid point. Mark it as deleted
		// An "FFFF" in the SmartCare Point Number flags it as deleted.
   		*(FlashRamWalkAddress + 5) = 0xFF;
   		*(FlashRamWalkAddress + 6) = 0xFF;
		// Trigger Flash Write of the first of two blocks to be written to Flash
		FlashControlWord = TRUE;
		// Flash imaging is not yet active, so let's start it up:
		FlashImageActive = TRUE;
		// Set the Flag to trigger the write of the second block
		FlashAddendumWrite = TRUE;
		// Data entered was okay
        AdminMessage( (char *) ctOK );
		}
	// The point to be deleted was already deleted in the main database - try searching the addendum database
	else
		{
		ptr = AddendumSearchForPoint (PointNumber); 
		if (ptr)
			{
			// The point to be deleted was found in the Addendum Database. So delete from there.
			AddendumEraseRecord (ptr);
			// The record has been erased from RAM - now write the new addendum image to FLASH
			FlashAddendumControlFlag = ADDENDUM_START_FLASH_WRITE;
			// Update the record counter
			AddendumDatabase.RecordCount--;
			// Data entered was okay
        	AdminMessage( (char *) ctOK );
			}
		else
			{
			// The record was not found in any database - ERROR!
         	AdminMessage(  (char *) ctInvalidData );
			}
		}

	// v5.00 --- Wait here while the Addendum database gets rewritten to flash
	PetWatchdog ();
	while (FlashControlWord || FlashAddendumControlFlag || FlashAddendumWrite || FlashState)
		{
		FlashExecutive ();
		// Service foreground UART tranmsmitters
//		Uart0Transmitter ();				// Hold off the Host!	
		Uart3Transmitter ();
		}
		PetWatchdog ();
}
	


/***********************************************************
	Subroutine:		AddendumGetEmptyRecord ()

	Description:
		This routine finds the next blank record in the addendum database
		        
	Inputs:
		AddendumDatabase
		
	Outputs:

	Returns:
		A Pointer to the next empty record in the Addendum Database

***************************************************************************/
uint8 * AddendumGetEmptyRecord ( )
{
	uint32 i, rfid;
	
	// Search for the next blank record in the Addendum Database
	for ( i = 0;  i < ADDENDUM_NUM_RECORDS;  i++ )
		{
		rfid = (uint32) SBGetDeviceIDx ((uint8 *) &AddendumDatabase.data [i][2]);
		if ( rfid == 0 )
			{
			// The end of the addendum database has been found
			return (uint8 *) &AddendumDatabase.data [i][0];
			}
		}

	// This path is impossible to reach
	return 0;
}


/***********************************************************
	Subroutine:		AddendumSearchForRfid ()

	Description:
		This routine will search for a matching RFID.
		        
	Inputs:
		AddendumDatabase
		
	Outputs:

	Returns:
		Pointer to a matching record, or
		Pointer = 0 if no match was found.

***************************************************************************/
uint8 * AddendumSearchForRfid (uint32 record)
{
	uint32 i, rfid;
	
	// Search for a matching record in the Addendum Database
	for ( i = 0;  i < ADDENDUM_NUM_RECORDS;  i++ )
		{
		rfid = (uint32) SBGetDeviceIDx ((uint8 *) &AddendumDatabase.data [i][2]);
		if ( rfid == record )
			{
			// A matching RFID has been found
			return (uint8 *) &AddendumDatabase.data [i][0];
			}
		if ( rfid == 0 )
			{
			// The end of the database has been reached.
			return (uint8 *) 0;
			}
		}

	// No match has been found
	return (uint8 *) 0;
}


/***********************************************************
	Subroutine:		AddendumSearchForPoint ()

	Description:
		This routine will search for a matching Point Number.
		        
	Inputs:
		AddendumDatabase
		
	Outputs:

	Returns:
		Pointer to a matching record, or
		Pointer = 0 if no match was found.

***************************************************************************/
uint8 * AddendumSearchForPoint (uint32 point)
{
	uint32 i, x;
	
	// Search for a matching record in the Addendum Database
	for ( i = 0;  i < ADDENDUM_NUM_RECORDS;  i++ )
		{
		x = AddendumDatabase.data [i][0];
		x <<= 8;
		x |= AddendumDatabase.data [i][1];

		if ( x == point )
			{
			// A matching Point Number has been found
			return (uint8 *) &AddendumDatabase.data [i][0];
			}
		if ( x == 0 )
			{
			// The end of the database has been reached.
			return (uint8 *) 0;
			}
		}

	// No match has been found
	return (uint8 *) 0;
}


/***************************************************************************
	Subroutine:		AddendumConfigureFlashWrite

	Description:
		It's time to write a page of The Points Addendum database to Flash.
		This routine figures out which of 2 pages to write to and returns
		a pointer to the active FLASH Block.

	Inputs:
		CpacWriteTitleDatabase

	Outputs:
		ptr - pointer to start of FLASH block

*****************************************************************************/
uint8 * AddendumConfigureFlashWrite ()
{
	uint8 * ToFlash;
	uint8 * FromRam;
	uint8 x;

	// We need to write one of two possible 1/4k blocks, based on the value of FlashAddendumControlFlag
	x = FlashAddendumControlFlag-1;
	ToFlash = (uint8 *) FLASH_ADDENDUM_VARS + (256 * x);
	FromRam =  (uint8 *)&AddendumDatabase + (256 * x); 
	Flash_WriteBlock (FromRam, ToFlash);
	return (ToFlash);
}



/***************************************************************************
	Subroutine:		AddendumEraseRecord

	Description:
		This routine will erase a single record from the Addendum Database

	Inputs:
		CpacWriteTitleDatabase

	Outputs:
		ptr - pointer to start of FLASH record

*****************************************************************************/
void AddendumEraseRecord (uint8 * ptr)
{
	uint32 x;

	// Get the length of the database from here to the end
	x = 512 - (ptr - (uint8 *)&AddendumDatabase);
	// Copy over the deleted record with the records that are behind it.
	memcpy (ptr, (ptr+ADDENDUM_RECORD_SIZE), (x-ADDENDUM_RECORD_SIZE) ); 
}


/***********************************************************
	Subroutine:		AddendumReadDatabase ()

	Description:
		This routine reads the 1/2k Addendum Database from Flash
		        
	Inputs:
		AddendumDatabase
		
	Outputs:

	Returns:

***************************************************************************/
void AddendumReadDatabase ( )
{
	uint32 i;

	// Now read the 'n' blocks of 256 bytes to the Flash
	FlashReadBlockAddress = FLASH_ADDENDUM_VARS;
	FlashRamBlockAddress = (uint8 *) &AddendumDatabase;
	for ( i = 0;  i < 2;  i++ )
		{
		Flash_ReadBlock ( FlashReadBlockAddress, FlashRamBlockAddress );
		// Update the Flash Block Address
		FlashReadBlockAddress += FLASH_BLOCK_SIZE;
		// Update the Flash RAM Block Address
		FlashRamBlockAddress += FLASH_BLOCK_SIZE;
		}
		
	// Handle case where memory was initialized before the addendum database was conceived:
	if (AddendumDatabase.RecordCount >= ADDENDUM_NUM_RECORDS)
		{
		// addendum database is all FF's - clear it out
		AddendumClearDatabase ();
		}	
}




/***********************************************************
	Subroutine:		AddendumClearDatabase ()

	Description:
		This routine clears the addendum database to zeroes
		        
	Inputs:
		AddendumDatabase
		
	Outputs:
		AddendumDatabase
		FlashAddendumControlFlag

	Returns:

***************************************************************************/
void AddendumClearDatabase ( )
{
	memset ((void *) &AddendumDatabase, (int) 0, (sizeof (ADDENDUM)));

	// Set flag to write the cleared database to Flash
	FlashAddendumControlFlag = ADDENDUM_START_FLASH_WRITE;
}

