/***************************************************************************
	File	:	diags.h
	Title	:	diags.c header file
	Author	:	Bob Halliday
					bohalliday@aol.com    (781) 863-8245
	Created	:	March, 2006
	Copyright:	(C) 2006 Lifeline Systems, Framingham MA

	Description:
		diags Definitions for diags.c

	Contents:

	Revision History:

***************************************************************************/
#ifndef HEADER_DIAGS
#define HEADER_DIAGS

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef BODY_DIAGS
#define SCOPE
#else
#define SCOPE	extern 
#endif

/*
 ********************************
 *  Section 1:  Equate Definitions:
 ********************************
 */
		
#define DIAGS_REG_ASCII_LENGTH					64
#define DIAGS_END_OF_ROM						0x7000
#define DIAGS_OFFSET							4
#define DIAGS_INVERT							0xFF

// 
#define DIAGS_FIRST_PAGE						0x0000
#define DIAGS_MIDDLE_PAGE						0x4000
#define DIAGS_LAST_PAGE							0x7FC0
#define DIAGS_STARTING_BIT_MASK					0x02


/*******************************
*  Section 2:  RAM Variables:
*********************************/

// Private Variables:
#ifdef BODY_DIAGS
unsigned char   DiagsFlag;


#endif



// Public Variables:
SCOPE	unsigned char	DiagsArray [SB_PAGE_SIZE];
SCOPE	unsigned char	DiagsChip;
SCOPE	unsigned char	DiagsRegistrationModified;
SCOPE	unsigned char	DiagsSaveChecksum;



/*******************************
*  Section 3:  Function prototypes
*********************************/

void 	DiagsInitialize (void);
void    DiagsRamTest (void);
void    DiagsRomTest (void);
void    DiagsEEpromTest (void);
void    DiagsLookForErrors (void);
void    DiagsTestForError (void);
void 	DiagsTestEEprom (uint16, unsigned char );




/*******************************
*  Section 4:  Macros
*********************************/




/********************************
*  Section 5:  ROM Tables
*********************************/

#ifdef BODY_DIAGS



#endif




#undef SCOPE
#endif

/* ======== END OF FILE ======== */
