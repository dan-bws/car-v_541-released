/***************************************************************************
	File	:	carts.h
	Title	:	carts.c header file
	Author	:	Bob Halliday
					bohalliday@aol.com    (781) 863-8245
	Created	:	January, 2006
	Copyright:	(C) 2006 Lifeline Systems, Framingham MA

	Description:
		carts Definitions for carts.c

	Contents:

	Revision History:

***************************************************************************/
#ifndef HEADER_CARTS
#define HEADER_CARTS

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef BODY_CARTS
#define SCOPE
#else
#define SCOPE	extern 
#endif



typedef unsigned char		    uint8;  /*  8 bits */
typedef unsigned short 		    uint16; /* 16 bits */
typedef unsigned long int	    uint32; /* 32 bits */

typedef signed char			    int8;   /*  8 bits */
typedef signed short 		    int16;  /* 16 bits */
typedef signed long int		    int32;  /* 32 bits */

typedef volatile uint8			vuint8;  /*  8 bits */
typedef volatile uint16			vuint16; /* 16 bits */
typedef volatile uint32			vuint32; /* 32 bits */

/*
 ********************************
 *  Section 1:  Equate Definitions:
 ********************************
 */
		
#define CARTS_REG_ASCII_LENGTH					64
#define CARTS_REG_BINARY_LENGTH					(CARTS_REG_ASCII_LENGTH/2)
#define CARTS_OFFSET							4
#define CARTS_DEFAULT_ID						0xFF

// 
#define CART_FAST								1
#define CART_SLOW								0
#define CARTS_LAST_BYTE							31
#define CARTS_LAST_MASK							0x02
#define CARTS_STARTING_BIT_MASK					0x01
#define CARTS_REPROGRAM_CYCLE					50


// Carts Executive State Machine values:
enum	
	{
	CARTS_POLL_250_SUPERBUS_2000,
	CARTS_POLL_USE_FAST_AND_SLOW_POLLING_LISTS
	};



/*******************************
    Section 2:  RAM Variables:
*********************************/

// Private Variables:
#ifdef BODY_CARTS
	 	uint8		 		CartsFastListTraversed;


#endif



__align(8)


// Public Variables:
SCOPE 	unsigned char	CartsTemp [CARTS_REG_ASCII_LENGTH+2];
SCOPE 	unsigned char	CartsState;

SCOPE	unsigned char	CartsPollingList [POLLING_SIZE];
SCOPE	unsigned char	CartsProtocolList [POLLING_SIZE];
SCOPE	unsigned char	CartsReprogramList [POLLING_SIZE];
SCOPE	unsigned char	CartsMissingList [POLLING_SIZE];
//SCOPE	unsigned char   CartsRegistrationList [ REGISTRATION_SIZE ]; -- moved to Database.c
SCOPE	unsigned char   CartsCopyRegistrationList [ REGISTRATION_SIZE ];
SCOPE	unsigned char	CartsNumberOfFastCarts;
SCOPE	unsigned char	CartsNumberOfSlowCarts;
SCOPE	unsigned char	CartLastPolled;
SCOPE	unsigned char	CartLastAnswered;

SCOPE	unsigned char	CartsPtr;
SCOPE	unsigned char	CartsMask;
SCOPE	unsigned char	BytePtr;
SCOPE	unsigned char	BitMask;
SCOPE	unsigned char	CartsBytePtr [2];
SCOPE	unsigned char	CartsBitMask [2];



/*******************************
    Section 3:  Function prototypes
*********************************/

void 	CartsInitialize (void);
void    CartsGetRegistrationData (unsigned char   *);
void    CartsSetRegistrationData (unsigned char);
void    CartsUpdatePointers (unsigned char);
void 	CartsSetLastAnswered (unsigned char);
void 	CartsRemoveFromFastPollingList (void);
void 	CartsAddToFastPollingList (unsigned char);
void 	CartsSetDeviceData (unsigned char, unsigned char);
void 	CartsCheckOffLine (void);
void 	CartsClearRegistration (void);
void 	CartsWriteRegistration (void);
void 	CartsInitiateDiagnosticPoll (void);
void 	CartsTerminateDiagnosticPoll (void);
void 	CartsSetReprogramFlag (void);
void 	CartsClearReprogramFlag (void);
void 	CartsUpdateCartsCount (void);

unsigned char  CartsExecutive (void);
unsigned char  CartsPollNextCart (void);
unsigned char  CartsConvertToNumber (unsigned char, unsigned char);
unsigned char  CartsGetDeviceData (unsigned char);

uint8 	CartsCheckActive (uint8);
void 	CartsConvertToPointerMask (uint8);


/*******************************
    Section 4:  Macros
*********************************/




/********************************
    Section 5:  ROM Tables
*********************************/

#ifdef BODY_CARTS



#endif




#undef SCOPE
#endif

/* ======== END OF FILE ======== */
