/***************************************************************************
	File	:	system.h
	Title	:	system.c header file
	Author	:	Bob Halliday
					bohalliday@aol.com    (781) 863-8245
	Created	:	January, 2006
	Copyright:	(C) 2006 Lifeline Systems, Framingham MA

	Description:
		system Definitions for system.c

	Contents:

	Revision History:

***************************************************************************/
#ifndef HEADER_SYSTEM
#define HEADER_SYSTEM

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef BODY_SYSTEM
#define SCOPE
#else
#define SCOPE	extern 
#endif

/*
 ********************************
 *  Section 1:  Equate Definitions:
 ********************************
 */
#define CARTS_250

#define DEBUG	

//#define ROM_DIAGS_ENABLED		


/*******************************
*  Section 2:  RAM Variables:
*********************************/

// Private Variables:
#ifdef BODY_SYSTEM


#endif



// Public Variables:
//SCOPE 	unsigned char	System_Primary;




/*******************************
*  Section 3:  Function prototypes
*********************************/



/*******************************
*  Section 4:  Macros
*********************************/




/********************************
*  Section 5:  ROM Tables
*********************************/

#ifdef BODY_SYSTEM



#endif




#undef SCOPE
#endif

/* ======== END OF FILE ======== */
