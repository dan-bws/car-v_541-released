/***********************************************************
	Module Name:	diags.c  

	Author:			Bob Halliday, March, 2006   
					bohalliday@aol.com    (781) 863-8245

	Description:	
	This file is responsible for monitoring the state of the hardware components
					  
	Subroutines:	DiagsGetRegistrationData ()	
					DiagsGetRegistrationData ()
					DiagsGetRegistrationData ()

	Revision History:

*************************************************************/
#define BODY_DIAGS 

#include "system.h"			
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"
#include "admin.h"
#include "rtc.h"
#include "database.h"
#include "carts.h"
#include "sio.h" 
#include "mprocess.h"
#include "psi2000.h"
#include "utils.h"
#include "tec.h"
#include "sbus.h"
#include "lapage.h"
#include "diags.h"

#include <string.h>


/***********************************************************
	Subroutine:	DiagsInitialize()

	Description:
		This routine will set up the Diags Registration Array
		
        
	Inputs:		

	Outputs:

	Locals:	
			
*************************************************************/
void DiagsInitialize ()
{

}





/***********************************************************
	Subroutine:	DiagsRamTest (  )

	Description:
		This subroutine is called at power on to test RAM memory.
		If RAM memory is defective, an error flag is set at 0xF000

	Inputs:

	Outputs: 
		DiagsFlag  

	Locals:
			
*************************************************************/
void DiagsRamTest ()
{
#ifdef ROM_DIAGS_ENABLED
	//	The linker sets RamPtr into the R6-R7 register pair
	unsigned char  * RamPtr;

	// initialize the diags flag
	DiagsFlag = 0;

	// Init the RAM Pointer.
	RamPtr = (unsigned char  *) START_OF_RAM;

	for ( RamPtr = (uint8  *) START_OF_RAM;  
			RamPtr < (uint8  *) END_OF_RAM;  RamPtr++ )
		{
		// Alter to get test value
		ucData = *RamPtr ^ DIAGS_INVERT;

		// Update memory location
		*RamPtr = ucData;

		// See if the update worked
		if (ucData != *RamPtr)
			DiagsFlag++;

		// Alter to get test value
		ucData = *RamPtr ^ DIAGS_INVERT;

		// Update memory location
		*RamPtr = ucData;

		// See if the update worked
		if (ucData != *RamPtr)
			DiagsFlag++;
		}
#endif
}



/***********************************************************
	Subroutine:	DiagsRamTest (  )

	Description:
		This subroutine is called at power on to test EEprom memory.
		If EEprom memory is defective, an error flag is set at 0xF000
		Test the first, middle and last pages of EEprom0 and EEprom1.

	Inputs:

	Outputs: 

	Locals:
			
*************************************************************/
void DiagsEEpromTest ()
{
	DiagsTestEEprom (DIAGS_FIRST_PAGE,  0);
	DiagsTestEEprom (DIAGS_MIDDLE_PAGE, 0);
	DiagsTestEEprom (DIAGS_LAST_PAGE,   0);
	DiagsTestEEprom (DIAGS_FIRST_PAGE,  1);
	DiagsTestEEprom (DIAGS_MIDDLE_PAGE, 1);
	DiagsTestEEprom (DIAGS_LAST_PAGE,   1);

}


/***********************************************************
	Subroutine:	DiagsTestEEprom (  )

	Description:
		This subroutine is called at power on to test EEprom memory.
		If EEprom memory is defective, an error flag is set at 0xF000
		Test the first, middle and last pages of EEprom0 and EEprom1.

	Inputs:
		address of eeprom page
		bank = 0 or 1

	Outputs:
		DiagsFlag  

	Locals:
			
*************************************************************/
void DiagsTestEEprom (uint16 address, unsigned char bank)
{
#ifdef ROM_DIAGS_ENABLED
	unsigned char  * StartOfPage;

	// Wait for previous write operation to be completed
	EEWait ();

	// first copy the data out of memory, inverted
	StartOfPage = (unsigned char  *) address;
	if (bank)
		{
		// Select EEprom Chip #2
		EEPROMSEL0 = 0;
		}		
	for ( ucIndex = 0; ucIndex < SB_PAGE_SIZE; ucIndex++ )
		{
		// copy 64 bytes of EEprom data out of EEprom
		DiagsArray [ucIndex] = *StartOfPage++ ^ DIAGS_INVERT;
		}

	// Now write the data back
	if (bank)
		{
//		EEPROM_Chip2_Write( address, DiagsArray, SB_PAGE_SIZE );
		EESetUpWait ();
		}
	else
		{
//		WriteOnePage ( address, DiagsArray, SB_PAGE_SIZE );
		}
		
	// Wait for previous write operation to be completed
	EEWait ();

	// Now compare the inverted data
	StartOfPage = (unsigned char  *) address;
	if (bank)
		{
		// Select EEprom Chip #2
		EEPROMSEL0 = 0;
		}		
	for ( ucIndex = 0; ucIndex < SB_PAGE_SIZE; ucIndex++ )
		{
		// now inspect the data for correctness
		if (DiagsArray [ucIndex] != *StartOfPage++)
			DiagsFlag++;

		// re-invert the data
		DiagsArray [ucIndex] ^= DIAGS_INVERT;
		}

	// Now write the data back
	if (bank)
		{
//		EEPROM_Chip2_Write( address, DiagsArray, SB_PAGE_SIZE );
		EESetUpWait ();
		}
	else
		{
//		WriteOnePage ( address, DiagsArray, SB_PAGE_SIZE );
		}
		
	// Wait for previous write operation to be completed
	EEWait ();

	// Now compare the re-inverted data
	StartOfPage = (unsigned char  *) address;
	if (bank)
		{
		// Select EEprom Chip #2
		EEPROMSEL0 = 0;
		}		
	for ( ucIndex = 0; ucIndex < SB_PAGE_SIZE; ucIndex++ )
		{
		// now inspect the data for correctness
		if (DiagsArray [ucIndex] != *StartOfPage++)
			DiagsFlag++;
		}

	// Select EEprom Chip #1
	EEPROMSEL0 = 1;
#endif

}



/***********************************************************
	Subroutine:	DiagsRomTest()

	Description:
		This routine will calculate the checksum on the Flash Memory.
		The checksum is an 8-bit XOR of all Flash Memory in the range:
			0x0100 - 0x7000
		Any result other than zero is a failure
        
	Inputs:		

	Outputs:
		DiagsFlag

	Locals:	
			
*************************************************************/
void DiagsRomTest ( )
{
#ifdef ROM_DIAGS_ENABLED
	unsigned char Checksum = 0;
	const uint8 * Pointer;

	// Calculate the Checksum:
	for ( Pointer = (uint8 *) 0;  Pointer < (uint8 *) DIAGS_END_OF_ROM;  Pointer++ )
		{
		Checksum ^= *Pointer;
		}

	// If the checksum is nonzero, flag an error
	if (Checksum)
		{
		DiagsFlag++;
		DiagsSaveChecksum = Checksum;
		}

#endif
		
}




/***********************************************************
	Subroutine:	DiagsLookForErrors (  )

	Description:
		This subroutine will look for a self-test error by inspecting DiagsFlag
		If DiagsFlag is non-zero, the firmware will hang here forever, with
		the front panel LED turned on.

	Inputs:
		DiagsFlag

	Outputs:

	Locals:
			
*************************************************************/
void DiagsLookForErrors ()
{
#ifdef ROM_DIAGS_ENABLED
	if (DiagsFlag)
		{
		// A self test failure has occurred - hang here forever.
   		ACTIVE_LED = 0;   				// Turn Led On 
        ucIndex = 0;
        acTemp[ ucIndex++ ] = 'C';
        acTemp[ ucIndex++ ] = ' ';
        acTemp[ ucIndex++ ] = BinHexAscii( DiagsSaveChecksum >> 4 );  
        acTemp[ ucIndex++ ] = BinHexAscii( DiagsSaveChecksum );  
        acTemp[ ucIndex++ ] =  0;
		AdminMessage( acTemp );
		while (1)
			{
   			WDT_RESET = 1;
   			WDT_RESET = 0;
			}
		}
#endif
}





/* ======== END OF FILE ======== */
