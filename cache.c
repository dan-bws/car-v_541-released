/***********************************************************
	Module Name:	cache.c  

	Author:			Bob Halliday, June, 2005   
					bohalliday@aol.com    (781) 863-8245

	Description:
		The caching logic for the Calibration Constants
		in the 914 unit is stored here.
					  
	Subroutines:	cache()	

	Revision History:

*************************************************************/

#define  BODY_CACHE

#include <string.h>

#include "main.h"
#include "timers.h"

#include "cache.h"

#include "icp.h"
#include "labview.h"




/***********************************************************
	Subroutine:	CacheCommandProgramVerify()

	Description:
		This routine will cache ROM constants in the tester's RAM
				        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
uint8 CacheCommandProgramVerify (uint8 * Buffer)
{
	uint8 count;
	uint16 Address;

	// reorganize the count and address, moving the count from the back to the front
	count = Buffer [2];
	// Get the address and double it
	// Init the start address
	CacheAddr.OneShort = (((uint16) Buffer [1] << 8) 
					+ Buffer [0]);
	// Convert the Word Address to a Byte Address
	CacheAddr.OneShort <<= 1;

	// set address low
	Buffer [ICP_ADDRESS_LOW] = CacheAddr.TwoBytes [0];
	// set address high
	Buffer [ICP_ADDRESS_HIGH] = CacheAddr.TwoBytes [1];

	// put count at the front of the buffer - add 2 bytes for the address bytes
	Buffer [ICP_CONTROL_BYTE] = count + 2;

	// Now insert the Count, Address and Data into the Constants Cache
	return (CacheInsert (Buffer));
}


/***********************************************************
	Subroutine:	CacheCommandConstants()

	Description:
		This routine will cache ROM constants in the tester's RAM
				        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
uint8 CacheCommandConstants (uint8 * Buffer)
{
	uint8 i, j;
	uint8 status;

	// Set the Count = 4 bytes
	IcpCommandString [ICP_CONTROL_BYTE] = CACHE_CONSTANTS_SIZE;

	// Init the start address
	CacheAddr.OneShort = (((uint16) Buffer [ICP_ADDRESS_HIGH] << 8) 
					+ Buffer [ICP_ADDRESS_LOW]);
	// Convert the Word Address to a Byte Address
	CacheAddr.OneShort <<= 1;

	j = ICP_START_OF_DATA;
	for ( i = 0;  i < CACHE_NUM_CONSTANTS;  i++ )
		{
		// set address low
		Buffer [ICP_ADDRESS_LOW] = CacheAddr.TwoBytes [0];
		// set address high
		Buffer [ICP_ADDRESS_HIGH] = CacheAddr.TwoBytes [1];
		// set the lsb calibration constant
		Buffer [ICP_NUMBER_OF_BYTES] = Buffer [j++];
		// set the msb calibration constant
		Buffer [ICP_START_OF_DATA] = ICP_HIGH_CONSTANTS_BYTE;

		// Now insert the Count, Address and Data into the Constants Cache
		status = CacheInsert (Buffer);
		if (!status)
			break;

		// update the address marker:
		CacheAddr.OneShort += CACHE_ADDRESS_OFFSET;
		}

	return status;
}


/***********************************************************
	Subroutine:	CacheClear()

	Description:
		This routine clears the ROM Constants Cache and
		zeroes all associated registers
				        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
void CacheClear ()
{
	uint8 i;

	for ( i = 0;  i < CACHE_SIZE;  i++ )
		{
		CacheArray [i] = 0;
		}

	// Also zero out the cache count
	CacheCount = 0;
	CacheIndex = 0;

	// Set the address operator to NULL
	CacheAddress = CACHE_NULL_ADDRESS;

	// clear out the config flag
	CacheConfigFlag = FALSE;
}


/***********************************************************
	Subroutine:	CacheInsert()

	Description:
		This routine inserts a new entry into the cache array.
		The entry has 3 components:
			1. Count - 1 byte - counts the address and the data bytes
			2. Address - 2 bytes - low, high
			3. Data - variable length
				        
	Inputs:
		NewString - holds the new Count, Address and Data	
		
	Outputs:
		TRUE - Insertion operation was a success
		FALSE- Insertion operation failed 
		
	Locals:
		
*************************************************************/
uint8 CacheInsert (uint8 * NewString)
{
	uint8 Count = NewString [CACHE_COUNT] + 1;
	uint16 NewAddress, Address;
	uint8 i,j, x, y, ch_in, ch_out;

	if ( CacheCount == 0 )
		{
		// This is easy if the cache is starting out empty- just copy the new string into the cache
		memcpy ((void *) CacheArray, 
			(const void *) NewString,
			(size_t) Count);

		CacheCount = Count;
		}

	else if (Count > (CACHE_SIZE - CacheCount))
		{
		// No more room left in the Cache!
		return FALSE;
		}
		
	else
		{
		// Get the address of the new string to be inserted
		NewAddress = (((uint16) NewString [CACHE_ADDR_HI] << 8) 
					+ NewString [CACHE_ADDR_LOW]);
		for (i=0;  CacheArray [i];  i += (CacheArray [i] + 1) )
			{		 
			// Get the current address of the new nth entry in the Array
			Address = (((uint16) CacheArray [i+CACHE_ADDR_HI] << 8) 
					+ CacheArray [i+CACHE_ADDR_LOW]);

			// Error check for matching addresses:
			if ( NewAddress == Address)
				{
				// Error!  --- Addresses should never match!
				return FALSE;
				}

			// Check to see if we should insert the new data here:
			if ( NewAddress < Address)
				{
				// definitely, this is the place to insert the data
				j = i + Count;
				// open up a hole in the array to hold the new data
				for ( x = i; x < j; x++ )
					{
					ch_out = CacheArray [x];
  //				CacheArray [x] = 0;		// just for debug
					for ( y = x+1; y < CACHE_SIZE;  y++ )
						{
						ch_in = CacheArray [y];
						CacheArray [y] = ch_out;
						ch_out = ch_in;
						}
					}
				break;
				}
			}

		// Now copy in the new data into the Cache Array
		memcpy ((void *) &CacheArray [i], 
			(const void *) NewString,
			(size_t) Count);

		// finally - update the cache count
		CacheCount += Count;
		}

	// If this data is in the Config area, set the ConfigFlag
	if (NewAddress >= ICP_CONFIG_ADDRESS)
		{
		CacheConfigFlag = TRUE;
		}

	// Now initialize the operators for the firmware download
	CacheRestart ();

	// Insert Operation was a success
	return TRUE;
}

/***********************************************************
	Subroutine:	CacheStartFromConfigOrZero ()

	Description:
		This routine will restart the pointers to the cache.
		If Config parameters were stored in the cache, this
		routine will start up pointing to the first Config Address.
		Otherwise, it will point to the first record in the cache.
				        
	Inputs:
	   CacheIndex
		
	Outputs:
	   CacheAddress
	   CacheIndex
	   CacheByteCounter
		
	Locals:
		
*************************************************************/
void CacheStartFromConfigOrZero ( uint8 EndAddress )
{
	uint8 i;
	uint16 Address;

	CacheIndex = 0;
	if (CacheConfigFlag)
		{
		// There may be at least one Config Record in the Cache Array - we must find it.
		for (i=0;  CacheArray [i]; i += (CacheArray [i] + 1) )
			{		 
			// Get the current address of the new nth entry in the Array
			Address = (((uint16) CacheArray [i+CACHE_ADDR_HI] << 8) 
					+ CacheArray [i+CACHE_ADDR_LOW]);
			// Check for a config record
			if (Address >= EndAddress)
				{
				// The config record has been found
				CacheIndex = i;
				break;
				}
			}
		}
	// Get the nth record from the Cache.
	CacheUpdateCacheOperators ();
}




/***********************************************************
	Subroutine:	CacheRestart ()

	Description:
		This routine will restart the pointers to the cache
		at the beginning.
				        
	Inputs:
	   CacheIndex
		
	Outputs:
	   CacheAddress
	   CacheIndex
	   CacheByteCounter
		
	Locals:
		
*************************************************************/
void CacheRestart ( )
{
	// Point the Cache Index to the first record in the cache
	CacheIndex = 0;

	// Get the first record from the Cache.
	CacheUpdateCacheOperators ();
}



/***********************************************************
	Subroutine:	  CacheUpdateCounterAndAddress ()

	Description:
		Update the Cache Byte Counter by subtracting 2 from it.
		If the Cache Counter reaches zero, go get the next
		record from the Cache Array.
				        
	Inputs:
	   CacheAddress
	   CacheCounter
		
	Outputs:
	   CacheAddress
	   CacheIndex
	   CacheByteCounter
		
	Locals:
		
*************************************************************/
void CacheUpdateCounterAndAddress ( )
{
	// Update the cache address
	CacheAddress += 2;
	// Update the Cache Counter (it is always an even number)
	CacheByteCounter -= 2;

	if ( CacheByteCounter == 0 )
		{
		// The Counter is zero.  This record has been processed
		// Get the next record from the Cache.
		CacheUpdateCacheOperators ();
		}
}

/***********************************************************
	Subroutine:	CacheUpdateCacheOperators()

	Description:
		This routine will update the pointers and counters
		associated with pulling data out of the Cache Array.
		If we reach the end of the Cache Array, the operators
		are zeroed and the address is set to all FFFFs.
				        
	Inputs:
	   CacheIndex - pointing to start of a new record
		
	Outputs:
	   CacheAddress
	   CacheIndex
	   CacheByteCounter
		
	Locals:
		
*************************************************************/
void CacheUpdateCacheOperators ( )
{
	// Get the new cache counter - subtract out 2 bytes for the address
	CacheByteCounter = CacheArray [CacheIndex];
	CacheAddress = CACHE_NULL_ADDRESS;
	if (CacheByteCounter)
		{
		// subtract out 2 bytes for the address
		CacheByteCounter -= 2;
 		// Get the current address of the new nth entry in the Array
		CacheIndex++;
 		CacheAddress = (((uint16) CacheArray [CacheIndex+1] << 8) 
					+ CacheArray [CacheIndex]);

		// update the Cache Index to point to the start of the data
		CacheIndex += 2;
		}
	else
		{
		// Reset the index to the front of the cache
		CacheIndex = 0;
		}
}


/***********************************************************
	Subroutine:	CacheIsSpaceAvailable()

	Description:
		This routine will check to see that space is available
		for the next entry.
				        
	Inputs:
	   CacheIndex - pointing to start of a new record
		
	Outputs:
	   TRUE - space is available
	   FALSE- no more space available
		
	Locals:
		
*************************************************************/
uint8 CacheIsSpaceAvailable (uint8 NewEntrySize)
{
	return ( NewEntrySize < (CACHE_SIZE - CacheCount));
}








