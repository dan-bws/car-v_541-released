/***************************************************************************/
/*                                                                         */
/*		DATABASE.H										*/
/*		02/28/00 DBGetPointMessage1						*/
/*		JULY 1997					 					*/
/*                                                                         */
/***************************************************************************/

/***************************************************************************/
/*                                                                         */
/*             Defines                                                     */
/*                                                                         */
/***************************************************************************/


#ifdef DATABASE_ALLOCATE
	#define ALLOC_TYPE
#else
	#define ALLOC_TYPE extern
#endif

typedef unsigned char		    uint8;  /*  8 bits */
typedef unsigned short 		    uint16; /* 16 bits */
typedef unsigned long int	    uint32; /* 32 bits */

typedef signed char			    int8;   /*  8 bits */
typedef signed short 		    int16;  /* 16 bits */
typedef signed long int		    int32;  /* 32 bits */


#define ASCII_DASH				0x2D

											 /* Pointer to System DataBase			*/
#define DATABASE_POINTER ((struct asDataBaseStruct  *) 0x0000)

#define DATABASE_POINTERB ((struct asDataBaseStructb  *) 0x0000)

#define POINT_MESSAGE_SIZE1 	 	8		//8=type#,1-7 char &b
#define SB_MESSAGE_BUFFER_SIZE 		220		/* Receiver Message buffer size  */
#define SB_MESSAGE_BUFFER_SIZE_S 	7		/* Receiver Message buffer size  @012705 &b*/
//#define SB_MAX_POINTS			 	4001 		  /* Max Device Points + 33@062201 */

#define LA_MAX_PAGERS			 	33			/* Max local pagers	1 - 32  */
#define LA_PAGER_ARRAY_SIZE		 	121
#define POLLING_SIZE			 	32
#define REGISTRATION_SIZE		 	32		 
#define POINTS_RECORD_SIZE		 	15
#define MIN_POINT_STRING_SIZE		4		 
#define RFID_MESSAGE_SIZE			7		 

#define SB_PAGE_SIZE			 	(uint16) 0x0040
#define SB_PAGE_MASK			 	(uint16) 0x003F

#define FLASH_CS					0x80000000
#define FLASH_OE					0x01000000
#define FLASH_WE					0x02000000
#define EE_ADDRESS1					0x00005555
#define EE_ADDRESS2					0x00002AAA
#define EE_ADDRESS3					0x00005555
#define EEBYTE1						0xAA
#define EEBYTE2						0x55
#define EEBYTE3						0xA0
 
#define FLASH_ERASE_ADDRESS1		0x00005555
#define FLASH_ERASE_ADDRESS2		0x00002AAA
#define FLASH_ERASE_ADDRESS3		0x00005555
#define FLASH_ERASE_ADDRESS4		0x00005555
#define FLASH_ERASE_ADDRESS5		0x00002AAA
#define FLASH_ERASE_ADDRESS6		0x00005555
#define FLASH_ERASE_BYTE1			0xAA
#define FLASH_ERASE_BYTE2			0x55
#define FLASH_ERASE_BYTE3			0x80
#define FLASH_ERASE_BYTE4			0xAA
#define FLASH_ERASE_BYTE5			0x55
#define FLASH_ERASE_BYTE6			0x10
									  
#define FLASH_OUTPUT_PORT			0x000000FF
#define FLASH_PAGE_BOUNDARY			0xFFFFFF00
#define FLASH_MASK_BOUNDARY			0x000000FF
#define EE_MASK_TOP_BITS			0x00070000
#define EE_MASK_ADDRESS_BITS		0x7000FFFF
#define EE_SHIFT_TOP_BITS		 	12
#define FLASH_BLOCK_SIZE			256
#define FLASH_NUMBER_OF_BLOCKS		3
#define FLASH_RAM_BLOCK_SIZE		(FLASH_BLOCK_SIZE * FLASH_NUMBER_OF_BLOCKS)
#define FLASH_ISOLATE_IO6			0xBF
#define FLASH_WAIT_FOR_WRITE_CYCLE  20
#define FLASH_TOTAL_POINTS_BLOCKS 	1758
#define FLASH_WAIT_TIME				6
#define FLASH_WAIT_ERASE_TIME		10			// v5.16
#define FLASH_ERASE_TIME			6			// v5.16

#define FLASH_VARS					(uint8 *) 0x0007FC00	
#define FLASH_ADDENDUM_VARS			(uint8 *) 0x0007FA00	

enum
	{
	FLASH_IDLE,
	FLASH_WRITE_DELAY1,
	FLASH_WRITE_DELAY2,
	FLASH_READ,
	FLASH_WAIT_FOR_ERASE,
	FLASH_WAIT_BETWEEN_BLOCK_WRITES
	};	 


#define WEAK_NOP_3()	weak_nop(); weak_nop(); weak_nop()
#define WEAK_NOP_4()	weak_nop(); weak_nop(); weak_nop(); weak_nop()
#define WEAK_NOP_5()	weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop()
#define WEAK_NOP_6()	weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop()
#define WEAK_NOP_7()	weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop()
#define WEAK_NOP_8()	weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop()
#define WEAK_NOP_9()	weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop(); weak_nop()


// Variables used in the Flash module
typedef struct
{
	unsigned char ucSignatureByte;				
	unsigned char ucSBNumberOfDevices;
	uint16  unSBSupervisoryTime;
    unsigned char ucP3_9600_Flag;	
    unsigned char ucP3_192K_Flag;	
    unsigned char ucP5_9600_Flag;	
    unsigned char ucP5_192K_Flag;	
	unsigned char ucHighSpeedMode;
	unsigned char ucPirFilterTime;
	unsigned char ucLocatorTime;
	unsigned char eeRssiAveraging;
	unsigned char SmartCareActive;
    unsigned char ucLocationFlag;	
	unsigned char aucLAPagerData[ LA_MAX_PAGERS ];
	unsigned char aucLAPagerNewData [ LA_PAGER_ARRAY_SIZE ];
	unsigned char CartsRegistrationList [ REGISTRATION_SIZE ];
	char cSmartCareDoesFallDetector; //RAV
	unsigned char cDebugMessageMode; //0=off, 1=print alarm messages only, 2=print alarm and supervisories
	unsigned char cRssiLevelforFalsePacket; //if between 70 and 128 then if we get one packet only and 
																					//RSSI is lower than this number we reject the packet. If 00 then no RSSI check.
	unsigned char cFacilityCode;
	unsigned char extra [ 49 ];
} FLASH_RAM_DATA;



__align(8)


// Public variables
ALLOC_TYPE		uint8				FlashModified;
ALLOC_TYPE		uint8				FlashAddendumWrite;
ALLOC_TYPE		uint8				FlashAddendumControlFlag;
ALLOC_TYPE		uint8				FlashRecord [POINTS_RECORD_SIZE+2];
ALLOC_TYPE		FLASH_RAM_DATA		FlashRamData;

ALLOC_TYPE		uint8 * 		BlockAddress;

ALLOC_TYPE		uint8			FlashState;
ALLOC_TYPE		uint8			FlashLastReadByte;
ALLOC_TYPE		uint8			FlashRamBlock [FLASH_RAM_BLOCK_SIZE];
ALLOC_TYPE		uint8			FlashImageActive;
ALLOC_TYPE		uint8			FlashStopRead;
ALLOC_TYPE		uint8			FlashEraseFlag;
ALLOC_TYPE		uint8			FlashPartialEraseFlag;
ALLOC_TYPE		uint8			FlashPageCounter;
ALLOC_TYPE		uint32			FlashWalkAddress;
ALLOC_TYPE		uint8 *			FlashWriteBlockAddress;
ALLOC_TYPE		uint8 *			FlashReadBlockAddress;
ALLOC_TYPE		uint8 *			FlashRamBlockAddress;
ALLOC_TYPE		uint8 *			FlashRamWalkAddress;
ALLOC_TYPE		uint16			FlashDelay;
ALLOC_TYPE		uint16			FlashControlWord;
ALLOC_TYPE		uint16			FlashLastPoint;

ALLOC_TYPE		uint8			FlashSaveHighSpeedMode;



/***************************************************************************/
/*                                                                         */
/*             Function Prototypes                                         */
/*                                                                         */
/***************************************************************************/

void DBInit(void);
unsigned char Flash_WriteByte( unsigned char  * pDestination,
					 unsigned char ucByte );
__weak void weak_nop (void);

void Flash_Enable (void);
uint8 Flash_ReadByte ( uint8 * );
void Flash_WriteBlock ( uint8 *, uint8 * );
void Flash_ReadBlock ( uint8 *, uint8 * );
void FlashExecutive (void);
void Flash_InitializePointsImage (uint16 );
void Flash_InitiateFlashImaging (uint16 );
void Flash_TerminateFlashImaging (void );
void Flash_CheckWalkAddress (void);
void Flash_InitiateEraseOperation (uint8);
void Flash_TerminateEraseOperation (void);

void ClearWriteBusy(void);
void EESetUpWait (void);
void EEWait (void);
void Flash_ChipErase (void);
void Flash_Reinitialize (void);
unsigned char EECheckForWriteOperation (void);






#ifdef DATABASE_ALLOCATE	

#endif


/***************************************************************************/
#undef ALLOC_TYPE
