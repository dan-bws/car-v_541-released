
/***********************************************************
	Module Name:	cpac.c    

	Author:			Bob Halliday, September, 2007   
					bohalliday@aol.com    

	Description:	
	This file provides the core subroutines for CPAC support.
					  
	Subroutines:	CpacInitialize()	
					CpacActivateLockingRelay ()
					CpacDeactivateLockingRelay ()
					CpacAddMessageToQueue ()
					CpacSearchForEndOfQueue

	Revision History:


*************************************************************/

#include "system.h"			
#include "lpc23xx.h"
#include "main.h"
#include "psi2000.h"
#include "admin.h"
#include "sio.h"
#include "sbus.h"
#include "database.h"
#include "utils.h"
#include "timers.h"
#include "uart.h"
#include <stdio.h>
#include <string.h>

#define BODY_CPAC
#include "cpac.h"



extern const uint8  WANDER[];
extern unsigned char  aucSBBuffer[];	



/*************************************************************************/



/***************************************************************************
	Subroutine:		CpacGetAlertValue

	Description:
		This routine will return the proper Alert Value for the CPAC Alarm.

	Inputs:
		aucSBBuffer

	Outputs:
		CPAC Alert Type

*****************************************************************************/
uint8 CpacGetAlertValue ()
{
	// Check for a CPAC Reset Condition
	if (aucSBBuffer [OLD_F45] & SB_CPAC_RESET_FLAG)
		return 'R'; 	

	// Check for a CPAC Loiter Alert
	if (aucSBBuffer [OLD_F45] & SB_OLD_LOITER_FLAG)
		return 'O'; 	

	// Check for a CPAC Pause Alert
	if (aucSBBuffer [OLD_F45] & SB_OLD_PAUSE_FLAG)
		return 'Q';

	// Check for a CPAC Door Prop Alert
	if (aucSBBuffer [OLD_F45] & SB_OLD_DOOR_PROP_FLAG)
		return 'P';

	// Check for a CPAC Door Open and Locked Alert
	if (aucSBBuffer [OLD_F45] & SB_OLD_DOOR_LOCK_FLAG)
		return 'D';
		 	
	// It's none of those, so it must be a Wander Alert
	return 'W';
}




/***************************************************************************
	Subroutine:		CpacBackupPaging

	Description:
		This routine is only called when SmartCare has disappeared and a CPAC alarm 
		has been received by the CAR.
		This routine will set up the backup paging message for the CPAC alarm

	Inputs:
		buffer
		aucSBBuffer
		ucReceiverNumberResponding

	Outputs:
		buffer

	returns:
			TRUE - Backup Paging required
			FALSE - Backup Paging not required

*****************************************************************************/
uint8 CpacBackupPaging (uint8 * buffer)
{
			uint8  szDeviceNumber [8];
	const 	uint8  LOITER[]      = " LOITER ";
	const 	uint8  PATIENT[]     = "PATIENT ";
	const 	uint8  DOOR_OPEN[]   = "DOOR OPEN ";
	const 	uint8  DOOR_LOCKED[] = "DOOR LOCKED ";

	// This is a CPAC Alarm
	if (aucSBBuffer [OLD_F45] & (SB_OLD_PAUSE_FLAG | SB_CPAC_RESET_FLAG))
		// This is a CPAC Pause or Reset Alert -- no need for backup paging
		return FALSE;

	// Convert the Receiver Number to Ascii
	UIntToAscii( szDeviceNumber, ucReceiverNumberResponding  );
	szDeviceNumber[5] = ' ';

	if (aucSBBuffer [OLD_F45] & SB_OLD_DOOR_PROP_FLAG)
		{
		// This is a CPAC Door Open Too Long Alert
		memcpy( buffer, (uint8 *) DOOR_OPEN, 10 );
		StringCat ( buffer, (uint8 *) &szDeviceNumber[2], 4 );
		CpacAddTitle (buffer);
		return TRUE;
		}
	if (aucSBBuffer [OLD_F45] & SB_OLD_DOOR_LOCK_FLAG)
		{
		// This is a CPAC Door Open and Locked Alert
		memcpy( buffer, (uint8 *) DOOR_LOCKED, 12 );
		StringCat ( buffer, (uint8 *) &szDeviceNumber[2], 4 );
		CpacAddTitle (buffer);
		return TRUE;
		}

	// This is either a Wander or a Loiter Alert
	// Put the Patient number on the first line
	memcpy ( buffer, (uint8 *) PATIENT, 8 );	
	// Add in the Patient Number
	StringCat( buffer, CpacRfidString, 6 );	

	if (!CpacAddTitle (buffer))
		{
		// There was no title to add to the second line
		if (aucSBBuffer [OLD_F45] & SB_OLD_LOITER_FLAG)
			// This is a CPAC Loiter Alert
			StringCat( buffer, (uint8 *) LOITER, 8 );
		else
			// This is a CPAC Wander Alert
			StringCat( buffer, (uint8 *) WANDER, 8 );
		// Add in the Receiver Number  
		StringCat ( buffer, (uint8 *) &szDeviceNumber[2], 3 );
		}
	return TRUE;
}




/***************************************************************************
	Subroutine:		CpacAddTitle

	Description:
		If a Title is available, this routine will add it to the Backup Paging message.

	Inputs:
		buffer

	Outputs:
		buffer

	Returns:
		TRUE - Title added to message
		FALSE- No Title added to message

*****************************************************************************/
uint8 CpacAddTitle (uint8 * buffer)
{
	uint8 *ptr;

	// Search for the title
	ptr = CpacReadTitle (ucReceiverNumberResponding);
	if (ptr)
		{
		// The receiver number was found in the Database
		StringCat (buffer, ptr, CPAC_TITLE_SIZE);	
        return TRUE;
		}
	else
		{
		// No Title found in Title Database for this receiver
        return FALSE;
		}




}




/***************************************************************************
	Subroutine:		CpacAddMessageToQueue

	Description:
		A new CPAC message must be sent to a given receiver.
		This routine will buffer the message until the given receiver is about to be polled.

	Inputs:
		buffer

	Outputs:
		CpacOutQueue

*****************************************************************************/
void CpacAddMessageToQueue (uint8 * buffer)
{
	uint8 * ptr;

	ptr = CpacSearchForEndOfQueue (CpacOutQueue);
	if (ptr)
		{
		// Store the new message at the end of the queue
		memcpy (ptr, buffer, buffer [1]);
		} 	
}


/***************************************************************************
	Subroutine:		CpacSearchForEndOfQueue

	Description:
		This routine will search for the end of the OUT or IN queue

	Inputs:
		queue

	Outputs:
		pointer to end of queue

*****************************************************************************/
uint8 * CpacSearchForEndOfQueue (uint8 * queue)
{
	uint32 i;
	
	for ( i = 0;  i < CPAC_QUEUE_SIZE; )
		{
		// The first byte of a record is the Receiver Number, which must be non-zero
		if (!queue [i])
			{
			// A zero value has been found - this is the end of the queue
			return (uint8 *) &queue [i];
			}
		else
			{
			// The count is the second byte in the record
			i += (queue [i+1]);
			}
		}

	// no end found 
	return 0;
}



/***************************************************************************
	Subroutine:		CpacSearchForMessageInQueue

	Description:
		This routine will search for a message in the queue which is attached
		to a specified Receiver Number,
		Or sends a broadcast message if one is being sent.

	Inputs:
		queue
		ReceiverAddress

	Outputs:
		CpacMarkTheSpot
		CpacMessageSent

*****************************************************************************/
uint8 * CpacSearchForMessageInQueue (uint8 * queue, uint8 ReceiverAddress)
{
	uint32 i;
	
	CpacMessageSent = FALSE;

	for ( i = 0;  i < CPAC_QUEUE_SIZE;  )
		{
		// The first byte of a record is the Receiver Number, which must be non-zero
		if (queue [i] == ReceiverAddress)
			{
			// A matching receiver address has been found - return the pointer to this location
			CpacMarkTheSpot = (uint8 *) &queue [i];
			CpacMessageSent = TRUE;
			return (uint8 *) &queue [i];
			}
		else if(queue[i] == 0xFF)  // A broadcast message generated
		    {
#if 0
 			SioSetMessage(UART_2, (uint8*)"CapcSearchForMessageInQueue Called with BCAST\n\r",
							(uint16)sizeof("CapcSearchForMessageInQueue Called with BCAST\n\r")); 
#endif
			CpacMarkTheSpot = (uint8 *) &queue [i];
			CpacMessageSent = TRUE;
			return (uint8 *) &queue [i];	
			}
		else if (queue [i] == 0)
			{
			// We have reached the end of the queue
			return (uint8 *) 0;
			}
		else
			{
			// update to the next spot in the queue
			i += (queue [i+1]);
			}
		}

	// no matching Receiver Address found 
	return (uint8 *) 0;
}



/***************************************************************************
	Subroutine:		CpacDeleteMessageFromQueue

	Description:
		This routine will delete a given message from the Cpac queue
		and move all the other messages up

	Inputs:
		queue

	Outputs:
		queue

*****************************************************************************/
void CpacDeleteMessageFromQueue (uint8 * queue )
{
	uint8 * endofqueue;
	uint8 * endofrecord;
	uint32 count;

	// Get a pointer to the end of the queue
	endofqueue = CpacSearchForEndOfQueue (queue);

	// Set the count between the current point and the end of the queue
	count = endofqueue - queue;

	// Set the address for the next record
	endofrecord = queue + queue [1];

	// Move all the other messages up, thereby deleting the current message
	memcpy (queue, endofrecord, count); 
}


/***************************************************************************
	Subroutine:		CpacTransmitCommand

	Description:
		It is time for the CAR to poll the next device.
		But a command was found in the command queue.
		Therefore, instead of polling, this routine will transmit the command to the CPAC

	Inputs:
		queue

	Outputs:
		queue

*****************************************************************************/
extern char sString[];
char bReportedScDown=FALSE;
void CpacTransmitCommand (uint8 * queue)
{
	// Copy the packet into the transmit buffer
	memcpy (CpacData, queue, queue [TK_COUNT]);
	
	// Increment the count to include the checksum byte
	CpacData [TK_COUNT]++; 

	// Calculate the checksum
	SB_SetChecksum (CpacData);


	if( (CpacData[0] == 0xFF) && ((CpacData[2] == SB_CPAC_SMARTCARE_DOWN) ||
									(CpacData[2] == SB_CPAC_SMARTCARE_UP)))
	{
		if ((CpacData[2] == SB_CPAC_SMARTCARE_DOWN) && (!bReportedScDown))
			{
			if (FlashRamData.cDebugMessageMode>0)
				{			
				sprintf (sString, "To CAWM: Sending SmartCare is down"); 
				SendDebugMessage (sString);
				}
			bReportedScDown=TRUE;
			}
		if ((CpacData[2] == SB_CPAC_SMARTCARE_UP) && (bReportedScDown))
			{
			if (FlashRamData.cDebugMessageMode>0)
				{
				sprintf (sString, "To CAWM: Sending SmartCare is up"); 
				SendDebugMessage (sString);
				}
			bReportedScDown=FALSE;
			}
		
	}
   

	// Transmit the command to the CPAC
	SioSetMessage (UART_3, CpacData, CpacData [TK_COUNT] );
	iCpacMessagesSent++;
}
			



/***************************************************************************
	Subroutine:		CpacSaveNewTitle

	Description:
		The CAR has received a new CPAC Title from SmartCare
		This routine will store the title, along with the Receiver Number,
		in the Database.
		It will either overwrite a previously existing title for this Receiver,
		or it will add the Title to the end of the Database.

	Inputs:
		buffer

	Outputs:
		CpacTitle

*****************************************************************************/
void CpacSaveNewTitle (uint8 * buffer )
{
	uint8 Receiver, Count;
	uint32 i;

	// Set the CPAC Receiver Address
	Receiver = HexAsciiBin (&buffer [RCVR_INDEX]);

	// Align the Receiver with the title data, overwriting the comma in the process
	buffer [COMMA2_INDEX] = Receiver;

	// Get the packet count
	Count = strlen ((const char *) buffer [COMMA2_INDEX]);

	// Search for this receiver or end of record
	for (i = 0;  i < CPAC_TITLE_NUM_RECORDS;  i++ )
		{
		if ((CpacTitle [i].Receiver == Receiver)
		 || (CpacTitle [i].Receiver == 0))
			{
				sprintf (sString, "From SC: Saving cpac data"); 
				SendDebugMessage (sString);
	
				// Copy the new title, along with the Receiver Number, into the Title Database
			memcpy (&CpacTitle [i], &buffer [COMMA2_INDEX], Count);
			// Set Flag/Counter to write the database to Flash
			CpacWriteTitleDatabase = CPAC_START_WRITE;
			break;
			}
		}
}
	


/***************************************************************************
	Subroutine:		CpacReadTitle

	Description:
		The CAR has received a Read Title Command from SmartCare
		This routine will search for the title and return a pointer to it.
		If the Title is not found, a Pointer=0 will be returned.

	Inputs:
		Receiver

	Outputs:
		ptr - pointer to start of title

*****************************************************************************/
uint8 * CpacReadTitle (uint8 Receiver )
{
	uint32 i;

	// Search for this receiver or end of record
	for (i = 0;  i < CPAC_TITLE_NUM_RECORDS;  i++ )
		{
		if (CpacTitle [i].Receiver == Receiver)
			{
			// A matching receiver was found.  Return the corresponding title.
			return ( (uint8 *)  &CpacTitle [i].Name);
			}
		if (CpacTitle [i].Receiver == 0)
			{
			return 0;
			}
		}
	return 0;
}
	

	

/***************************************************************************
	Subroutine:		CpacConfigureFlashWrite

	Description:
		It's time to write a page of title database to Flash.
		This routine figures out which of 3 pages to write to and returns
		a pointer to the active FLASH Block.

	Inputs:
		CpacWriteTitleDatabase

	Outputs:
		ptr - pointer to start of FLASH block

*****************************************************************************/
uint8 * CpacConfigureFlashWrite ()
{
	uint8 * ToFlash;
	uint8 * FromRam;
	uint8 x;

	// We need to write one of three possible 1/4k blocks, based on the value of CpacWriteTitleDatabase
	x = CpacWriteTitleDatabase-1;
	ToFlash = (uint8 *) CPAC_TITLE_SPACE + (256 * x);
	FromRam = (uint8 *) CpacTitle + (256 * x); 
	Flash_WriteBlock (FromRam, ToFlash);
	return (ToFlash);
}


/***************************************************************************
	Subroutine:		CpacReadTitleDatabase ()

	Description:
		Power up routine:
		This routine reads out the entire (768 byte) Title Database from Flash into RAM

	Inputs:
		CpacReadTitleDatabase

	Outputs:
		FromFlash
		ToRam

*****************************************************************************/
void CpacReadTitleDatabase ()
{
	uint8 * FromFlash;
	uint8 * ToRam;
	uint8 x;

	// We need to read all three of the 1/4k blocks
	for (x = CPAC_START_WRITE;  x > 0;   x--)
		{
		FromFlash = (uint8 *) CPAC_TITLE_SPACE + (256 * (x-1));
		ToRam = (uint8 *) CpacTitle + (256 * (x-1)); 
		Flash_ReadBlock (FromFlash, ToRam);
		}
}



/***************************************************************************
	Subroutine:		CpacInitializeFlashDatabase ()

	Description:
		Power up routine:
		This routine zeroes out the Title Database

	Inputs:
		CpacWriteTitleDatabase

	Outputs:
		ptr - pointer to start of FLASH block

*****************************************************************************/
void CpacInitializeFlashDatabase ()
{
	uint32 i;

	// Clear out every record in the title database
	for (i = 0;  i < CPAC_TITLE_NUM_RECORDS;  i++)
		{
		memset ((void *) &CpacTitle [i], (int) 0, (size_t) CPAC_TITLE_RECORD_SIZE);
		}

	// Set Flag/Counter to write the database to Flash
	CpacWriteTitleDatabase = CPAC_START_WRITE;
}


/***************************************************************************
	Subroutine:		CpacSetRfidNumber ()

	Description:
		This routine converts a CPAC's 6-digit RFID to ASCII
		and stores the result in the Alarm Transmit Buffer

	Inputs:
		aucSBBuffer

	Outputs:
		buffer

*****************************************************************************/
void CpacSetRfidNumber ( )
{
//	uint16 decimal;

/********
	// If this is a WatchMate, the one-byte TAG ID must be converted from binary to decimal:
	if ((aucSBBuffer [OLD_F123] == 0) && (aucSBBuffer [OLD_RFID_3] == 0))
		{
		// The top 3 digits are padded to zeroes.
	   	CpacRfidString [ 0 ] = '0';
	   	CpacRfidString [ 1 ] = '0';
	   	CpacRfidString [ 2 ] = '0';
		// Convert the bottom 2 hex digits to 3 decimal Ascii digits
		decimal = BinBcd16 (aucSBBuffer [OLD_RFID_2]);
	   	CpacRfidString [ 3 ] = BinHexAscii ((decimal >> 8));
	   	CpacRfidString [ 4 ] = BinHexAscii ( decimal >> 4 );
	   	CpacRfidString [ 5 ] = BinHexAscii ( decimal );
		}
	else
		{
************/
		// Convert the top 2 digits to Ascii
	   	CpacRfidString [ 0 ] = BinHexAscii ( aucSBBuffer [OLD_F123] >> 4 ); //7
	   	CpacRfidString [ 1 ] = BinHexAscii ( aucSBBuffer [OLD_F123] );
		// Convert the middle 2 digits to Ascii
	   	CpacRfidString [ 2 ] = BinHexAscii ( aucSBBuffer [OLD_RFID_3] >> 4 );   //5
	   	CpacRfidString [ 3 ] = BinHexAscii ( aucSBBuffer [OLD_RFID_3] );
		// Convert the bottom 2 digits to Ascii
	   	CpacRfidString [ 4 ] = BinHexAscii ( aucSBBuffer [OLD_RFID_2] >> 4 );  //4
	   	CpacRfidString [ 5 ] = BinHexAscii ( aucSBBuffer [OLD_RFID_2] );
//		}

	// Attach a terminating NULL character
   	CpacRfidString [ 6 ] = 0;
		
		//CpacRfidString [ 0 ] ='F'; //5-11-2017. Somehow the first character is being dropped but it is always an 'F'
}	















