/***********************************************************
	Module Name:	main.c

	Author:			Bob Halliday, March, 2007
					bohalliday@aol.com

	Description:
		The top level System Executive for the
		CAR cost-reduced system drivers

	Subroutines:	main()

	Revision History:


*************************************************************/

#define  BODY_MAIN

#include "lpc23xx.h"
#include "main.h"
#include "timers.h"

#include "database.h"
#include "psi2000.h"
#include "admin.h"
#include "factory.h"
#include "sio.h"
#include "sbus.h"
#include "lapage.h"
#include "mprocess.h"
#include "inits.h"
#include "uart.h"


#ifdef SPECTRALINK
#include "spectralink.h"
#endif


/***********************************************************
	Subroutine:	main()

	Description:
		After Cstartup initializations are completed,
		the code will jump to this routine, which,
		after performing Power-up initializations,
		will run forever in a "Round-Robin" Loop.

	Inputs:

	Outputs:

	Locals:

*************************************************************/
int main (void)
{
    // power up initializations
    Initializations ( );

    // The top level System Executive runs here
    while (TRUE)
    {
        PetWatchdog ();

        // Control the Flash memory operations
        FlashExecutive ( );
        // bh - 9/21/05 -- run Host Uart receiver infinitely
        AdminCommandCheck ();
        // Watch for the end of Command #2 or #12
        MprocessMonitorSavedAlarms ();
        // check the one-second timer
        DecrementRecentDelay ();
        // process flashing led
        ActiveLedInit( );
        PetWatchdog ();

        if (!CpacFactoryMode)
        {
            // Poll the next slave
            SBPoll ();
            // Also run rs485 receiver indefinitely
            SBCheckInput ();
            // Manage Backup Paging buffering
            LAPageManage ();
						#ifdef SPECTRALINK
            // Watch for spectralink backup activities
            SpectralinkExecutive ();			// v5.14 - disable spectralink activity
						#endif
        }

        // Service foreground UART tranmsmitters
        PetWatchdog ();
        Uart0Transmitter ();
        Uart1Transmitter ();
        Uart2Transmitter ();	// v5.15 - 01/16/08
        Uart3Transmitter ();
        PetWatchdog ();

        // Service the new CPAC-Vector Tester
        FactoryExecutive ();
        PetWatchdog ();

        ProcessUart1Messages();  //Service the debugger port
        PetWatchdog ();
    }  // giant while
}



/***********************************************************
	Subroutine:	MainStartLearnMode ()

	Description:
		This routine starts the 30-second timeout for Learn Mode

	Inputs:

	Outputs:
		MainLearnModeTimer
		MainLearnModeState

	Locals:

*************************************************************/
void MainStartLearnMode ()
{
    // Start the 30 second timer
    MainLearnModeTimer = 0;

    // Enable the Learn Mode State Machine
    MainLearnModeState = LEARN_MODE_ACTIVE;
}



/***********************************************************
	Subroutine:	MainWatchLearnMode ()

	Description:
		This routine is only called once a second.
		It increments the one-second Learn Mode Timer.
		If the Timer expires, Learn Mode is terminated!

	Inputs:
		MainLearnModeState

	Outputs:
		MainLearnModeTimer

	Locals:
		msg [ ]

*************************************************************/
void MainWatchLearnMode ()
{
    uint8 msg [2];

    // What's the state of Learn Mode?
    switch (MainLearnModeState)
    {
    // Learn Mode is not active at this point
    case LEARN_MODE_OFF:
        break;

    // Learn Mode is active - update the timeout timer
    case LEARN_MODE_ACTIVE:
        if (++MainLearnModeTimer >= LEARN_MODE_TIME_EXPIRED)
        {
            // The timeout has expired - terminate Learn Mode!
            msg [0] = SB_KEYPAD_BYPASS;
            AdminProcessCommand ( msg, TRUE );
        }
        break;
    }
}

/***********************************************************
	Subroutine:	MainEndLearnMode ()

	Description:
		This routine terminates Learn Mode

	Inputs:

	Outputs:
		MainLearnModeTimer
		MainLearnModeState

	Locals:

*************************************************************/
void MainEndLearnMode ()
{
    // Start the 30 second timer
    MainLearnModeTimer = 0;

    // Enable the Learn Mode State Machine
    MainLearnModeState = LEARN_MODE_OFF;
}



