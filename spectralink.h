/***************************************************************************
	File	:	spectralink.h
	Title	:	spectralink.c header file
	Author	:	Bob Halliday
					bohalliday@aol.com    (781) 863-8245
	Created	:	October, 2006
	Copyright:	(C) 2006 Lifeline Systems, Framingham MA

	Description:
		spectralink Definitions for spectralink.c

	Contents:

	Revision History:

***************************************************************************/
#ifndef HEADER_SPECTRALINK
#define HEADER_SPECTRALINK

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef BODY_SPECTRALINK
#define SCOPE
#else
#define SCOPE	extern 
#endif

/*
 ********************************
 *  Section 1:  Equate Definitions:
 ********************************
 */
		
#define SPECTRALINK_REG_ASCII_LENGTH 			64
#define SPECTRALINK_REG_BINARY_LENGTH			(SPECTRALINK_REG_ASCII_LENGTH/2)
#define SPECTRALINK_OFFSET						4
#define SPECTRALINK_RECORDS						32
#define SPECTRALINK_WALK_MASK					(SPECTRALINK_RECORDS-1)	
#define SPECTRALINK_RECORD_SIZE					32	
#define SPECTRALINK_TRANSMIT_SIZE				50	
#define SPECTRALINK_RECEIVE_SIZE				240	
#define SL_MIN_PACKET_SIZE						10
#define SL_NUMBER_OF_HANDSETS					200
#define SL_HEARTBEAT_TIME						9
#define SL_HANG_UP_TIME							22
#define SL_WATCH_FOR_HEARTBEAT_TIME				39
#define SL_REGISTRATION_BLOCK_SIZE				16
#define SL_REGISTRATION_LIMIT_SIZE				(unsigned int)(64*SL_REGISTRATION_BLOCK_SIZE)
#define SL_HANDSET_INACTIVE						0xFF
#define SL_RETRY_CALL							2
#define	SL_VERSION_OLD							1

// 
#define OAI_GATEWAY_RESTART						0x01
#define OAI_FIELD_OUT_OF_RANGE					0x04
#define OAI_WAIT_FOR_ACK_TIME					300		// 1.5 seconds

#define	OAI_LOGIN_COMMAND						'1'
#define	OAI_HEARTBEAT_COMMAND					'3'
#define	OAI_RING_COMMAND						'4'
#define	OAI_PRIORITY_RING_COMMAND				'5'
#define	OAI_END_CALL_COMMAND					'6'
#define	OAI_SELECT_CONNECTIONS_COMMAND			'7'
#define	OAI_SET_DISPLAY_COMMAND					'8'
#define	OAI_SET_ICON_COMMAND					'9'
#define	OAI_REGISTRATION_REQUEST_COMMAND		'B'
#define	OAI_CHECK_PROTOCOL_VERSION_COMMAND		'C'

#define	OAI_LOGIN_LENGTH						13		// was 14
#define	OAI_HEARTBEAT_LENGTH					2
#define	OAI_PRIORITY_RING_LENGTH				24
#define	OAI_SELECT_CONNECTIONS_LENGTH			9
#define	OAI_SET_DISPLAY_LENGTH					42
#define	OAI_SET_ICON_LENGTH						14
#define	OAI_REGISTRATION_LENGTH					10
#define	OAI_VERSION_LENGTH						2
#define	OAI_END_CALL_LENGTH						7
#define	OAI_NUMBER_OF_COMMANDS					6
#define SL_BUFFER_LIMIT							(255-OAI_SET_DISPLAY_LENGTH)
#define SL_END_OF_FILE							0xFFFF

#define OAI_ACTIVE_ACK							'1'
#define OAI_NO_ACK								'0'
#define OAI_RETAIN_DISPLAY						'1'
#define OAI_CLEAR_DISPLAY						'0'
#define	OAI_APP_PROGRAM							'1'
#define	OAI_RING_TYPE_LOW						'7'				// Pager Ring
#define	OAI_RING_CYCLES_HIGH 					'0'
#define	OAI_RING_CYCLES_LOW						'2'
#define	OAI_RING_BURSTS							'3'
#define	OAI_ALERT_TONE							'1'
#define	OAI_RING_ON_DURATION_HIGH				'0'
#define	OAI_RING_ON_DURATION_LOW				'6'
#define	OAI_RING_OFF_DURATION_HIGH				'0'
#define	OAI_RING_OFF_DURATION_LOW				'6'

#define	OAI_ICON_LEFT_ARROW_HIGH				'0'
#define	OAI_ICON_LEFT_ARROW_LOW					'F'
#define	OAI_ICON_RIGHT_ARROW_HIGH				'1'
#define	OAI_ICON_RIGHT_ARROW_LOW				'0'
#define	OAI_ICON_SOLID_ON						'1'
#define	OAI_ICON_OFF							'0'


// OAI Transmit Packet Order
enum
	{
	SL_LENGTH1,
	SL_LENGTH2,
	SL_OPCODE1,
	SL_OPCODE2,
	SL_PORT_NUMBER0,
	SL_PORT_NUMBER1,
	SL_PORT_NUMBER2,
	SL_PORT_NUMBER3,
	SL_ACK_CODE,
	SL_RETURN_DISPLAY,
	SL_DISPLAY_OFFSET0,
	SL_DISPLAY_OFFSET1,
	SL_DATA
	};



// Spectralink alarm state machine states
enum
	{
	OAI_IDLE,
	OAI_LOGON,
	OAI_GET_VERSION,
	OAI_GET_VERSION_RESPONSE,
	OAI_REGISTER1,
	OAI_REGISTER2,
	OAI_WATCH_FOR_ALARM,
	OAI_PRIORITY_RING,
	OAI_PRIORITY_RING_ACKNOWLEDGE,
	OAI_SET_DISPLAY,
	OAI_SET_DISPLAY_ACKNOWLEDGE,
	OAI_NEXT_PHONE,
	OAI_SPECTRALINK_DISABLED
	};



// Valid OAI Receive Commands
enum
	{
	OAI_HEARTBEAT				=	0x81,
	OAI_ILL_FORMED_MESSAGE,
	OAI_EVENT,
	OAI_WIRELESS_TELEPHONE_EVENT,
	OAI_DECODE_REGISTRATION_RESPONSE,
	OAI_GET_VERSION_NUMBER,
	};


// Spectralink alarm state machine states
enum
	{
	SL_PRINT_IDLE,
	SL_PRINT_ACTIVE,
	SL_PRINT_WAIT
	};



// Hang Up State Machine states
enum
	{
	HANGUP_IDLE,
//	HANGUP_SEND_SET_DISPLAY,
//	HANGUP_WAIT1,
	HANGUP_END_CALL,
	HANGUP_WAIT
	};



// Acknowledgement Responses
enum
	{
	OAI_HANDSET_RESPONDED			= 0x80,
	OAI_NO_HANDSET_RESPONSE,
	OAI_BUSY_IN_PBX_SESSION,
	OAI_BUSY_IN_OAI_SESSION,
	OAI_HANDSET_NOT_ACTIVE,
	OAI_HANDSET_IN_STANDBY,
	OAI_DISPLAY_UPDATE_RECEIVED,
	OAI_SESSION_ENDED,
	OAI_NO_RESPONSE_TO_RING,
	OAI_PBX_STARTED_RINGING,
	OAI_PBX_STOPPED_RINGING
	};


// Key presses on a cellphone
enum
	{
	OAI_1_KEY						= 1,
	OAI_2_KEY,
	OAI_3_KEY,
	OAI_4_KEY,
	OAI_5_KEY,
	OAI_6_KEY,
	OAI_7_KEY,
	OAI_8_KEY,
	OAI_9_KEY,
	OAI_STAR_KEY,							// go backward in list
	OAI_0_KEY,
	OAI_POUND_KEY,							// go forward in list
	OAI_START_KEY,
	OAI_FUNCTION_KEY,
	OAI_END_KEY,
	OAI_LINE_KEY					= 0x12,
	OAI_HOLD_KEY,
	OAI_UP_KEY,
	OAI_SELECT_KEY,
	OAI_DOWN_KEY,
	OAI_MENU_KEY,
	OAI_SOFT_A_KEY,
	OAI_SOFT_B_KEY,
	OAI_SOFT_C_KEY,
	OAI_SOFT_D_KEY
	};


#define OAI_START_OAI_SESSION		0x90




/*******************************
  Section 2:  RAM Variables:
*********************************/

// Private Variables:
#ifdef BODY_SPECTRALINK
		uint8				   SpectralinkInSession;
		uint8				   SL_Up;
		uint8				   SL_HandsetRespondedFlag;
		uint8				   SL_DisplayUpdatedFlag;
		uint8				   SL_RegistrationResponseFlag;


#endif



// Public Variabfles:
SCOPE 	uint8	SpectralinkQueue [SPECTRALINK_RECORDS][SPECTRALINK_RECORD_SIZE];
SCOPE 	uint8	SpectralinkWalkPointer [SL_NUMBER_OF_HANDSETS];
SCOPE 	uint16	SpectralinkHandsets [SL_NUMBER_OF_HANDSETS];
SCOPE 	uint16	SL_OffHook [SL_NUMBER_OF_HANDSETS];
SCOPE 	uint8	SL_Transmit [SPECTRALINK_TRANSMIT_SIZE];
SCOPE 	uint8	SL_Receive  [SPECTRALINK_RECEIVE_SIZE+8];		// v5.17
		   
SCOPE 	uint8	 SL_Head;
SCOPE 	uint8	 SL_Tail;
SCOPE 	uint8	 SL_Display;
SCOPE 	uint8	 SL_AlarmHead;
SCOPE	uint8	 SL_ReceiveIndex;
SCOPE	uint8	 SL_Count;
SCOPE	uint8	 SL_Command;
SCOPE	uint8	 SL_CommandIndex;
SCOPE	uint8	 SL_AlarmState;
SCOPE	uint8	 SL_PrintOutState;
SCOPE	uint8	 SL_PrintIndex;
SCOPE   uint8    SL_GetPortIndex;
SCOPE   uint8    SL_Seconds;
SCOPE   uint8    SL_HeartBeat;
SCOPE	uint16	 SL_PortNumber;

SCOPE 	uint16	 OAI_WaitForAckTimer;
SCOPE 	uint8	 OAI_HeartbeatTimer;
SCOPE 	uint16 	OAI_HangupTimer;

SCOPE 	uint16	SL_RegistrationBase;
SCOPE 	uint8	SL_HandsetsIndex;
SCOPE	uint8 	SL_SpectralinkActive;
SCOPE	uint8 	SL_LeftArrowActive;
SCOPE	uint8 	SL_RightArrowActive;
SCOPE	uint8 	SL_HangUpState;
SCOPE	uint8 	SL_HangUpActive;
SCOPE	uint8 	SL_HangUpIndex;
 


/*******************************
  Section 3:  Function prototypes
*********************************/
void 	SpectralinkInitialize (void);
void 	SpectralinkExecutive (void);
void 	SpectralinkAlarmExecutive (void);
void 	SpectralinkReceiveExecutive (void);
void 	SpectralinkSetup (void);
void 	SpectralinkHeartBeat (void);
void 	SpectralinkHangUp (void);
void 	SpectralinkWatchForHeartBeat (void);
void 	SpectralinkConvertPortNumber (uint16);
void    SpectralinkSaveAlarm (uint8 *);
void 	SpectralinkXmitLogin (void);
void 	SpectralinkXmitRegistration (void);
void 	SpectralinkXmitPriorityRing (void);
void 	SpectralinkXmitSelectConnections (void);
void 	SpectralinkXmitEndCall (void);
void 	SpectralinkXmitHeartbeat (void);
void 	SpectralinkXmitDisplayAlarm (uint8);
void 	SpectralinkXmitIcons (uint8, uint8);
void 	SpectralinkCalculateAndStoreChecksum (uint8);
void 	SpectralinkAddPortNumber (uint8, uint16);
void 	SpectralinkDeleteAllPortNumbers (void);
void 	SpectralinkDeletePortNumber (uint8);
void 	SpectralinkPortPrintOutExecutive (void);
void 	SpectralinkActivatePrintOut (void);

uint8 SpectralinkSendCommandPacket (uint8, uint8);
uint8 SpectralinkDisplayNextMessage (uint8);
uint8 SpectralinkValidateReceiveCommand (void);
uint8 SpectralinkValidateChecksum (uint8);
uint8 SpectralinkCalculateChecksum (uint8 *, uint8);
uint8 SpectralinkGetPortNumber (void);
uint8 SpectralinkGetIndexFromPortNumber (uint16);
uint8 SpectralinkWaitForBuffer (void);
uint16  SpectralinkGetOaiPortNumber (void);
uint16  SpectralinkReadPortNumber (uint8);

uint8 OAI_Heartbeat (uint8 *, uint8 );
uint8 OAI_IllFormedMessage (uint8 *, uint8 );
uint8 OAI_Event (uint8 *, uint8 );
uint8 OAI_WirelessTelephoneEvent (uint8 *, uint8 );
uint8 OAI_DecodeRegistrationResponse (uint8 *, uint8);
uint8 OAI_GetVersionNumber (uint8 *, uint8);

/*******************************
  Section 4:  Macros
*********************************/




/********************************
  Section 5:  ROM Tables
*********************************/
typedef 	uint8		(*FUNCTION_LIST1)(uint8 *, uint8);

#ifdef BODY_SPECTRALINK
//uint8 code  SpectralinkCommands [OAI_NUMBER_OF_COMMANDS] =
const uint8 SpectralinkCommands [] =
	{
	OAI_HEARTBEAT,
	OAI_ILL_FORMED_MESSAGE,
	OAI_EVENT,
	OAI_WIRELESS_TELEPHONE_EVENT,
	OAI_DECODE_REGISTRATION_RESPONSE,
	OAI_GET_VERSION_NUMBER
	};

// Execution routines which correspond to Spectralink commands 			 
const FUNCTION_LIST1 SpectralinkCommandExecute [] =
	{
	(FUNCTION_LIST1) OAI_Heartbeat,						// 
	(FUNCTION_LIST1) OAI_IllFormedMessage,				// 
	(FUNCTION_LIST1) OAI_Event,							// 
	(FUNCTION_LIST1) OAI_WirelessTelephoneEvent, 		//  
	(FUNCTION_LIST1) OAI_DecodeRegistrationResponse,	//  
	(FUNCTION_LIST1) OAI_GetVersionNumber				//  
	};

const uint8 Philips_Logon [ ] =
	{
//	"Philips_V1.0"
	"Spectralink"
	};


#endif




#undef SCOPE
#endif

/* ======== END OF FILE ======== */
