/********************************************************
	File	:	factory.h
	Title	:	factory.h
	Author	:	Bob Halliday
					bohalliday@aol.com  

	Description:
		Header file holding the definitions for factory.c
		
************************************************************/
#ifndef HEADER_FACTORY
#define HEADER_FACTORY

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_FACTORY
#define SCOPE
#else
#define SCOPE	extern
#endif

/*******************************
*   Section 1:  Equate Definitions: 
*********************************/
#define	FACTORY_SELECT_FIQ					0x06D0
#define	FACTORY_SELECT_INTS					0x00D0

#define	FACTORY_TIMER0_INT					0x0010
#define	FACTORY_UART0_INT					0x0040
#define	FACTORY_UART1_INT					0x0080
#define	FACTORY_I2C_INT						0x0200
#define	FACTORY_SPI_INT						0x0400
#define	FACTORY_TIMER0_ENABLE_INTS			0x20
#define FACTORY_DRY_CONTACTS_MASK			0x00700000

#define FACTORY_SWITCHES_DEBOUNCE_PERIOD		(uint32)(100/TIME_BASE)


//  Factory Command test list
enum
	{
	CMD_ENABLE_FACTORY_TEST_MODE = 20,
	CMD_WRITE_RS485_1,
	CMD_WRITE_RS485_2,
	CMD_WRITE_PAGING_PORT,
	CMD_READ_RS485_1,
	CMD_READ_RS485_2,
	CMD_READ_PAGING_PORT,			//26
	CMD_SET_TTL1_HIGH,
	CMD_SET_TTL1_LOW,
	CMD_SET_TTL2_HIGH,
	CMD_SET_TTL2_LOW,
	CMD_SET_TTL3_HIGH,
	CMD_SET_TTL3_LOW,
	CMD_SET_TTL1_INPUT,
	CMD_SET_TTL2_INPUT,
	CMD_SET_TTL3_INPUT,				//35
	CMD_RELAY_1_ON,
	CMD_RELAY_1_OFF,
	CMD_RELAY_2_ON,
	CMD_RELAY_2_OFF,
	CMD_READ_DRY_CONTACT_INPUTS
	};

//  Factory Command Response list
enum
	{
	CMD_RSP_GOT_RS485_1 = 61,
	CMD_RSP_GOT_RS485_2,
	CMD_RSP_GOT_PAGER_PORT,
	CMD_RSP_UNUSED_1,
	CMD_RSP_UNUSED_2,
	CMD_RSP_UNUSED_3,
	CMD_RSP_TTL_1_HIGH,			// 67
	CMD_RSP_TTL_1_LOW,
	CMD_RSP_TTL_2_HIGH,
	CMD_RSP_TTL_2_LOW,
	CMD_RSP_TTL_3_HIGH,
	CMD_RSP_TTL_3_LOW,
	CMD_RSP_UNUSED_4,			// 73
	CMD_RSP_UNUSED_5,
	CMD_RSP_UNUSED_6,
	CMD_RSP_NO_INPUTS_ACTIVE,	// 76
	CMD_RSP_INPUTS_ACTIVE_1,
	CMD_RSP_INPUTS_ACTIVE_2,
	CMD_RSP_INPUTS_ACTIVE_12,
	CMD_RSP_INPUTS_ACTIVE_3,
	CMD_RSP_INPUTS_ACTIVE_13,
	CMD_RSP_INPUTS_ACTIVE_23,
	CMD_RSP_INPUTS_ACTIVE_123
	};


// hex versions of responses:
enum
	{
	CMD_HEX_RSP_GOT_RS485_1 = 0x61,
	CMD_HEX_RSP_GOT_RS485_2,
	CMD_HEX_RSP_GOT_PAGER_PORT,
	CMD_HEX_RSP_UNUSED_1,
	CMD_HEX_RSP_UNUSED_2,
	CMD_HEX_RSP_UNUSED_3,
	CMD_HEX_RSP_TTL_1_HIGH,			// 67
	CMD_HEX_RSP_TTL_1_LOW,
	CMD_HEX_RSP_TTL_2_HIGH,
	CMD_HEX_RSP_TTL_2_LOW = 0x70,
	CMD_HEX_RSP_TTL_3_HIGH,
	CMD_HEX_RSP_TTL_3_LOW,
	CMD_HEX_RSP_UNUSED_4,			// 73
	CMD_HEX_RSP_UNUSED_5,
	CMD_HEX_RSP_UNUSED_6,
	CMD_HEX_RSP_NO_INPUTS_ACTIVE,	// 76
	CMD_HEX_RSP_INPUTS_ACTIVE_1,
	CMD_HEX_RSP_INPUTS_ACTIVE_2,
	CMD_HEX_RSP_INPUTS_ACTIVE_12,
	CMD_HEX_RSP_INPUTS_ACTIVE_3 = 0x80,
	CMD_HEX_RSP_INPUTS_ACTIVE_13,
	CMD_HEX_RSP_INPUTS_ACTIVE_23,
	CMD_HEX_RSP_INPUTS_ACTIVE_123
	};



// Switch read State Machine
enum	
	{
	FACTORY_SWITCHES_IDLE			= 0,
	FACTORY_SWITCHES_DEBOUNCE,
	FACTORY_SWITCHES_CHANGE
	};


// TTL state machine
enum	
	{
	TTL_READ_INPUTS			= 0,
	TTL_COMPARE_INPUTS
	};

#define	FACTORY_BUFFER_SIZE				30


/*******************************
	Section 2: RAM Definitions
********************************/

// Local Variables:
#ifdef BODY_FACTORY

	
#endif



// Global Variables
SCOPE	uint8		FactorySwitchState;
SCOPE	uint8		CpacFactoryMode;
SCOPE	uint8		FactoryReceiveString [4][FACTORY_BUFFER_SIZE];
SCOPE	uint8		FactoryReadRs485_1_Flag;
SCOPE	uint8		FactoryReadRs485_2_Flag;
SCOPE	uint8		FactoryReadPagingPortFlag;
SCOPE	uint8		FactoryCount [4];
SCOPE	uint8		FactoryTtlState;
SCOPE	uint8		FactoryIsVector;
SCOPE	uint32		FactoryTtlImage;

 

/*******************************
*  Section 3:  Function prototypes 
*********************************/
void  FactoryInitialize (void);
void  FactoryExecutive (void);
void  FactoryMonitorSwitches (void);
uint8 FactoryTestSubroutine (void);
uint8 FactoryEnterFactoryMode (void);
uint8 FactoryWriteRs485_1 (void);
uint8 FactoryWriteRs485_2 (void);
uint8 FactoryWritePagingPort (void);
uint8 FactoryReadRs485_1 (void);
uint8 FactoryReadRs485_2 (void);
uint8 FactoryReadPagingPort (void);
uint8 FactoryReadAllPorts (void);
uint8 FactoryReadOnePort (uint8);

uint8 FactorySetTtl1High (void);
uint8 FactorySetTtl1Low (void);
uint8 FactorySetTtl2High (void);
uint8 FactorySetTtl2Low (void);
uint8 FactorySetTtl3High (void);
uint8 FactorySetTtl3Low (void);
uint8 FactorySetTtl1Input (void);
uint8 FactorySetTtl2Input (void);
uint8 FactorySetTtl3Input (void);
uint8 FactorySetRelay_1_On (void);
uint8 FactorySetRelay_1_Off (void);
uint8 FactorySetRelay_2_On (void);
uint8 FactorySetRelay_2_Off (void);
uint8 FactoryReadDryContactInputs (void);
void  FactoryMonitorTtlInputs (void);
void  FactoryTtlChangeMessage (uint32);
void  Uart1Transmitter (void);
void  FactoryEnableRoamAlertBaudRate (void);
void  FactorySetVector (void);


/*******************************
*   Section 4:  Macros 
*********************************/
/*******************************
*   Section 5:  ROM Tables 
*********************************/
typedef 	uint8		(*FUNCTION_LIST3) (void);


#ifdef BODY_FACTORY
// This table is interpreted by AdminState 			 
const FUNCTION_LIST3 FactoryCommandExecute [] =
	{
	(FUNCTION_LIST3) FactoryEnterFactoryMode,				// 20				
	(FUNCTION_LIST3) FactoryWriteRs485_1,				
	(FUNCTION_LIST3) FactoryWriteRs485_2,				
	(FUNCTION_LIST3) FactoryWritePagingPort,				
	(FUNCTION_LIST3) FactoryReadRs485_1,				
	(FUNCTION_LIST3) FactoryReadRs485_2,				
	(FUNCTION_LIST3) FactoryReadPagingPort,				
	(FUNCTION_LIST3) FactorySetTtl1High,				
	(FUNCTION_LIST3) FactorySetTtl1Low,				
	(FUNCTION_LIST3) FactorySetTtl2High,				
	(FUNCTION_LIST3) FactorySetTtl2Low,						// 30				
	(FUNCTION_LIST3) FactorySetTtl3High,				
	(FUNCTION_LIST3) FactorySetTtl3Low,				
	(FUNCTION_LIST3) FactorySetTtl1Input,				
	(FUNCTION_LIST3) FactorySetTtl2Input,				
	(FUNCTION_LIST3) FactorySetTtl3Input,				
	(FUNCTION_LIST3) FactorySetRelay_1_On,				
	(FUNCTION_LIST3) FactorySetRelay_1_Off,				
	(FUNCTION_LIST3) FactorySetRelay_2_On,				
	(FUNCTION_LIST3) FactorySetRelay_2_Off,				
	(FUNCTION_LIST3) FactoryReadDryContactInputs			// 40				
	};


const uint8 TestMessage[] = " Test Message\n\r";
const uint8 fcInvalidCommand[] = "002";

const uint8 FactoryResponse[] = 
	{
	0,
	CMD_HEX_RSP_GOT_RS485_2,
	CMD_HEX_RSP_GOT_PAGER_PORT,
	CMD_HEX_RSP_GOT_RS485_1
	};


extern const uint8 ctOK[];

#else


#endif


#undef SCOPE


#endif

/* ======== END OF FILE ======= */
