/***********************************************************
	Module Name:	isr.c

	Author:			Bob Halliday, August, 2005
					bohalliday@aol.com

	Description:
	The Interrupt Service Routines for the
	2104 are run from this module	
					
	Subroutines:	IsrInterruptHandler()
					IsrInitialize ()	

	Revision History:


*************************************************************/

#define  BODY_ISR

#include <stdio.h>
#include <string.h>
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"

#include "isr.h"
#include "uart.h"



/***********************************************************
	Subroutine:	 IsrInitialize()

	Description:
		Initializations for Interrupts are performed here
		There are 7 interrupts, all running off the fast (FIQ) interrupt:
			1. Uart 0 Receive Char
			2. Uart 0 Transmit Char
			3. Uart 2 Receive Char
			4. Uart 2 Transmit Char
			5. Uart 3 Receive Char
			6. Uart 3 Transmit Char
			7. Timer 0 12.5ms interrupt
		
	Inputs:	
		
	Outputs:
		
	Locals:
		
*************************************************************/
void IsrInitialize ()
{
	// Put all interrupts under the VIC FIQ vectors
	VICIntSelect = ISR_SELECT_FIQ;
	VICIntEnable = ISR_SELECT_INTS;
}





extern void AdminMessage( char * );
//__irq void IRQ_Handler (void)  
//{
//    AdminMessage( (char *) "Goo" );
//	VICVectAddr = 0;
//}


//----------------------------------------------------------

/***********************************************************
	Subroutine:	 IsrInterruptHandlerHigh()

	Description:
		The FIQ Interrupt occurs here
		There are 7 possible interrupts sources
		
	Inputs:	
		
	Outputs:
		
	Locals:
		
*************************************************************/

//----------------------------------------------------------------------------
// FIQ High priority interrupt routine
//void FIQ_Handler (void) __attribute__ ((__irq));

__irq void FIQ_Handler (void)  
{
//----------------------------------------------------------

	// Which interrupt has occurred?
	 if ( VICFIQStatus & ISR_UART0_INT )
		{
		Uart0Interrupt ( );
		}

//----------------------------------------------------------
		
	 if ( VICFIQStatus & ISR_UART1_INT )
		{
		// This is a UART 1 interrupt
		Uart1Interrupt ();
		}
//----------------------------------------------------------
		
	 if ( VICFIQStatus & ISR_UART2_INT )
		{
		// This is a UART 2 interrupt
		Uart2Interrupt ( );
		}

//----------------------------------------------------------
		
	 if ( VICFIQStatus & ISR_UART3_INT )
		{
		// This is a UART 3 interrupt
		Uart3Interrupt ( );
		}
				
//----------------------------------------------------------
		
	 if ( VICFIQStatus & ISR_TIMER0_INT )
		{
		// This is a Timer 0 interrupt
		Timers_msInterrupt ();
		}

	// debug
//	else
//		{
//		// Respond with "goo"
//    	AdminMessage( (char *) "goo" );
//		}


	// Clear out the interrupts
	VICVectAddr = 0;
}




