/************************************************************************/
/*                                                                      */
/* 				 Super Bus Defines					*/
/*				4/11/00 Added ZeroEEPROM  @041100			*/
/*                                                                      */
/* #define SB_MAX_POINTS		4001			 Max points + 33 @020501 &b	*/
/* ALLOC_TYPE unsigned long SBGetDeviceIDx( unsigned char xdata *pMessage );	//&b 011405 */
/* #define SB_MAX_POINTS		4033 	 Max points + 33 @011805 &b	*/
/* ALLOC_TYPE unsigned long SBGetDeviceIDy( unsigned char xdata *pMessage );	//&b 011905 */
/* ALLOC_TYPE unsigned long SBGetDeviceIDz( unsigned char xdata *pMessage );	//&b 030205 */
/************************************************************************/
#ifdef ALLOCATE_SBUS
	#define ALLOC_TYPE
#else
	#define ALLOC_TYPE extern
#endif

/************************************************************************/
/*                                                                      */
/*		Defines																				*/
/*                                                                      */
/************************************************************************/

/* Duplicate message filter defines */
#define RECENT_MESSAGE_BUFFER_SIZE 	11			/* Message buffer size	*/
#define RECENT_MESSAGE_DELAY		30			/* 10,Message delay 10 sec	*/

#define	SB_SNIFF_TRANSMIT			'T'
#define	SB_SNIFF_RECEIVE			'R'
#define ADMIN_SNIFFER_MAX			40
#define SB_SUPERBUS_TIMEOUT			6

#define SB_MAX_OLD_DEVICES			32			 /* Max SBus devices 	 */

#define SB_MAX_NEW_DEVICES			250		 	/* Max SBus devices 	 */

/* Super Bus defines */
//#define SB_DEVICE_RETRYS			60 			/* 1 min=60 sec,Device retrys@020602*/
//#define SB_DEVICE_RETRYS			15 			/* 1 min=60 sec, v5.19 Change to 15 secs */

#define SB_DEVICE_RETRYS			180 	//rv 7-23-2015  cart timout i think		/* 1 min=60 sec, v5.19 Change to 15 secs */
#define SB_DEVICE_MAX_RETRYS		0xFFFF 			
#define SB_DEVICE_RETRY_DELAY 		((SB_DEVICE_RETRYS * 4) * 5)	// v3.87 - fix timer value - 5 minutes
#define SB_DEVICE_INACTIVE 			0			/* Device Inactive code */
#define SB_RECEIVE_TIME				1			// BH - 9/21/05

#define SB_DATA_PACKET				0x93
#define SB_NO_DATA_PACKET			0x80
#define SB_ACK						0x94
#define SB_RSSI_PACKET				0x91
#define SB_JUST_RESET				0x8F
#define SB_SLAVE_ACK				0x82
#define SB_CONFIG_ACK				0x80
#define SB_CONFIG_NAK				0x8E

// v5.00 - CPAC support
#define SB_CPAC_REQUEST_OPTIONS		0x31
#define SB_CPAC_SET_OPTIONS			0x32
#define SB_CPAC_LOCK				0x33
#define SB_CPAC_UNLOCK				0x34
#define SB_CPAC_CLEAR_ALERT			0x35
#define SB_CPAC_ENTER_TEST_MODE		0x36
#define SB_CPAC_EXIT_TEST_MODE		0x37
#define SB_CPAC_WRITE_SERIAL_NUMBER 0x38
#define SB_CPAC_READ_SERIAL_NUMBER	0x39
#define SB_CPAC_SMARTCARE_DOWN		0x40 	// When Smartcare link is Down
#define SB_CPAC_SMARTCARE_UP		0x41    // when Smartcare Link is UP/Back UP.
#define SB_SET_CLOCK				0x42
#define SB_CPAC_LOW_BATTERY_TAG		0x43
#define SB_LOG_MESSAGE				0x44
#define SB_SET_FACILITY_CODE 		0x45


#define SB_CPAC_WRITE_SIZE			8
#define SB_CPAC_WRITE_MSG_SIZE		13

// v5.14 - Network Diagnostics support
#define SB_NETWORK_HOST_RADIO_STATUS		0x3E
#define SB_NETWORK_REMOTE_RADIO_STATUS		0x3F
#define SB_NETWORK_HOST_RADIO_SIZE			37
#define SB_NETWORK_REMOTE_RADIO_SIZE		15
#define SB_NETWORK_HYBRID_PACKET_COUNT		14


// Roam Alert Status Condition bits:
#define SB_STAFF_IN_AREA_STATUS				0x04
#define SB_RA_DEVICE_CONNECTED				0x08
#define SB_DOOR_PROP_ALARM					0x10
#define SB_MAGLOCK_STATUS					0x20
#define SB_DOOR_STATUS						0x40
#define SB_PAUSE_INPUT						0x80


// CPAC alarm types within the old superbus packet format
#define SB_CPAC_RESET_FLAG			0x08
#define SB_OLD_LOITER_FLAG			0x10
#define SB_OLD_PAUSE_FLAG			0x20
#define SB_OLD_DOOR_PROP_FLAG		0x40
#define SB_OLD_DOOR_LOCK_FLAG		0x80

#define SB_CPAC_RFID_MARKER			0xEEEE


/* Point Types 		*/
#define SB_WALLMOUNT_TOUCHPAD 		00 			/* Walmount Touchpad 	*/
#define SB_SMOKE_DETECTOR			02 			/* Smoke Detector 	*/
#define SB_PENDANT					03 			/* Pendant			*/
#define SB_PASSIVE_INFARED_RECEIVER	04 			/* Passive Infared Recv */
#define SB_CPAC_TYPE				06 			// v5.00 -- this message is a Wander Alarm
//Use 7 and 8 for the 7000 PHB and AAHB.
#define SB_7000_PHB 				07
#define SB_7000_AAHB 				0x08
#define SB_GLASS_BREAK_SENSOR 		0x09		/* Micro glass breakage sensor */
#define SB_DOOR_WINDOW_SENSOR 		10			/* Micro door/window 	*/
#define SB_SINGLE_BUTTON_PANIC		11 			/* Single Button Panic	*/
#define SB_7000_MHB							12
#define SB_HANDHELD_TOUCHPAD		15 			/* Handheld Touchpad 	*/

/* Point defines */
#define SB_MAX_POINTS				20001			/* Max points + 33 @020501 &b	*/
#define SB_SUPERVISORY_TIME			1440			/* 12,24 hours in minutes@061901*/

												/* Point Message Types						*/
#define SB_CPAC                  	0x01        // CPAC Wander or Loiter Alert
#define SB_RESTORE                  0x02        /* Restore in msg       */
#define SB_SUPERVISORY              0x04        /* Supervisory in msg   */
#define SB_TAMPER                   0x08        /* Tamper in msg        */
#define SB_NEW_TAMPER               0x10        /* New Tamper in msg    */
#define SB_ALARM					0x40		/* Alarm in msg			*/
#define SB_BATTERY_LOW              0x80        /* Battery Low in msg   */
													/* Point Type 'F' Keypad Defines 	  */
#define SB_KEYPAD_BYPASS			0x1B			/* ASCII ESC				*/

// 437 * 64 = 27,968 bytes
// 4001 * 7 = 28,007 bytes
// 28,007 - 27,968 = 39 uncleared bytes, or 6 points worth
#define SB_NUM_PAGES				437
#define SB_END_OF_PAGES				512

#define SB_BLANK_RFID				0x0CFFFF03

#define DEFAULT_SUPERVISORY_TIME	1440			//v5.00 - increased from 12 to 24 hours
#define SIGNATURE_BYTE				0x5A
#define SB_0LD_POLL_TIME			2
//#define SB_0LD_POLL_TIME			20  //for testing 

// 01/07/08 - lowered from 20ms to 15ms
// 02/29/08 - v5.02 --- restored to 20ms because of bug in NJ beta site
//#define SB_2000_POLL_TIME			3
#define SB_2000_POLL_TIME			4   //5msec counts				
//#define SB_2000_POLL_TIME			25   //for testing 				


#define SB_CPAC_POLL_TIME			4
#define SB_DIAG_POLL_TIME			5
#define PIR_EXPIRED					0xFF
#define PIR_NOMINAL_DELAY			20
#define DATABASE_OUT_OF_ORDER		'X'
#define POINT_NUMBER_OUT_OF_RANGE	'Y'
#define POINTS_DUPLICATE_NUMBERS	'Z'

#define TK_HEADER_SIZE				8
#define TK_MIN_DATA_SIZE			16
#define RS485_RECEIVE_TIMEOUT		15
//#define RS485_RECEIVE_TIMEOUT		1000
#define AMBIENT_MAX					80
#define CC_0C_MODE					0x0C
#define MODE_0C_MODE_PACKET_SIZE	8
#define SBUS_RECEIVER_PRESENT		0x01

// bh - v4.06 - 02/15/07
#define NUM_RECEIVERS_TRACKED		8
#define NUM_TXIDS					25
/*
typedef struct
{
	unsigned char		Samples [NUM_RECEIVERS_TRACKED];
	unsigned char		Receiver [NUM_RECEIVERS_TRACKED];									 
	uint16		Average [NUM_RECEIVERS_TRACKED];
} RSSI_DATA;

typedef struct
{
	uint16		Point;
	RSSI_DATA			RssiData;
}  RSSI_TXID;
*/
		



// DATA packet indeces
enum	
	{
	SB_DEVICE_ADDRESS,
	SB_COUNT,
	SB_FUNCTION_CODE,
	SB_RSSI_1,
	SB_RSSI_2
	};

// poll state machine values
enum	
	{
	POLL_STATE_IDLE,
	POLL_STATE_STARTED,
	POLL_STATE_CONTINUING1,
	POLL_STATE_CONTINUING2,
	POLL_STATE_COMPLETE
	};

enum	
	{
	SB_IDLE			= 0,
	SB_ACTIVE,
	SB_SUPERCHECK,
	SB_PRINT_WAIT
	};
enum	
	{
	SB_PIR_IDLE		= 0,
	SB_PIR_ACTIVE
	};

// Superbus 2000 indeces into a Receive Packet
enum	
	{
	TK_ADDRESS			= 0,
	TK_COUNT,
	TK_RESPONSE_CODE,
	TK_CAPABILITY_CODE,
	TK_STATUS_BYTE,
	TK_NOISE_FLOOR_RIGHT,
	TK_NOISE_FLOOR_LEFT,
	TK_PACKET_COUNT,
	TK_RSSI,
	TK_LEARN_MODE
	};



// Old Superbus indeces into a Receive Packet
enum	
	{
	OLD_ADDRESS			= 0,
	OLD_COUNT,
	OLD_RESPONSE_CODE,
	OLD_RFID_1,
	OLD_RFID_2,
	OLD_RFID_3,
	OLD_RFID_4,
	OLD_F123,
	OLD_F45,
	OLD_RSSI,						 //==9
	NEW_TK_STATUS_BYTE,				//   Added to v5.14
	NEW_TK_NOISE_FLOOR_RIGHT,
	NEW_TK_NOISE_FLOOR_LEFT
	};

#define RS485_MSG_MASK_TXID0    	0xC0
#define RS485_MSG_MASK_TXID3    	0x03

#define SB_POLL_REPEATED	    	0x02


//__align(8)


// bh - v4.06 - averaging structure for received rssi values
//ALLOC_TYPE	RSSI_TXID		RssiAverage [NUM_TXIDS];





/************************************************************************/
/*                                                                      */
/*		Function Prototypes																*/
/*                                                                      */
/************************************************************************/

ALLOC_TYPE void SBInit(void);
ALLOC_TYPE void SBCheckInput(void);
ALLOC_TYPE void SBPoll(void);
ALLOC_TYPE void SBDecrementSupervisorDelay(void);
ALLOC_TYPE void SBDecrementHourDelay(void);		
ALLOC_TYPE void SBDebugOutput( unsigned char   *pMessage,
										 unsigned char ucByteCount );
ALLOC_TYPE unsigned long SBGetDeviceID( unsigned char   *pMessage );

ALLOC_TYPE unsigned long SBGetDeviceIDy( unsigned char   *pMessage );		//&b 011905

ALLOC_TYPE unsigned long SBGetDeviceIDx( unsigned char   *pMessage );		//&b 011405

ALLOC_TYPE unsigned long SBGetDeviceIDz( unsigned char   *pMessage );		//&b 030205

ALLOC_TYPE unsigned char SBGetDeviceType( unsigned char   *pMessage );
ALLOC_TYPE unsigned char SBGetMessageInfo( unsigned char   *pMessage, uint16 unPointNumber);
ALLOC_TYPE unsigned char SBPendantGetInfo( unsigned char   *pMessage );
ALLOC_TYPE unsigned char SBSmokeDetectorGetInfo( unsigned char   *pMessage );
ALLOC_TYPE unsigned char SBSingleButtonPanicGetInfo( unsigned char   *pMessage );
ALLOC_TYPE unsigned char SBHandHeldTouchPadGetInfo( unsigned char   *pMessage );
ALLOC_TYPE unsigned char SBWallMountTouchPadGetInfo( unsigned char   *pMessage );
ALLOC_TYPE unsigned char SBDoorWindowSensor( unsigned char   *, uint16 );
ALLOC_TYPE unsigned char SBPassiveInfaredReceiver( unsigned char   *, uint16 );
ALLOC_TYPE unsigned char SBGlassBreakGetInfo( unsigned char   *pMessage ); //@102201

ALLOC_TYPE unsigned char SBKeyPadCheck( unsigned char   *pMessage );

ALLOC_TYPE unsigned char SBSetPointNumber( uint16 unPointNumber,
										 unsigned char   *pMessage );

ALLOC_TYPE uint16	SBGetPointNumber( unsigned char   *pMessage );

ALLOC_TYPE void SBResetSupervisorTimer (uint16 unPointNumber);
ALLOC_TYPE void SBResetPoints(void);

ALLOC_TYPE void SBDeactivatePoint( uint16 unPointNumber );
ALLOC_TYPE unsigned char  * SBGetPointData( uint16, uint8 );		// bh - 5.13
ALLOC_TYPE unsigned char  * SBGetPointStatus(uint16 unPointNumber);
ALLOC_TYPE unsigned char  * SBGetPointControl(uint16 unPointNumber);
ALLOC_TYPE unsigned char  * SBGetPointState(uint16 unPointNumber);
ALLOC_TYPE unsigned char SBSetPointData( unsigned char   *pMessage );
ALLOC_TYPE unsigned char SBSetRoomData( uint16 unPointNumber,unsigned char   *pMessage ); //@011405 &b

ALLOC_TYPE unsigned char SBSetNumberOfDevices( unsigned char ucNumberOfDevices );
ALLOC_TYPE unsigned char SBGetNumberOfDevices(void );
ALLOC_TYPE unsigned char SBSetDeviceData( uint16, uint16 );
ALLOC_TYPE unsigned char SBGetDeviceData( uint16 unDeviceNumber );
ALLOC_TYPE unsigned char SBResetDevicesData(void );
ALLOC_TYPE unsigned char SBSetSupervisoryTime( uint16 unSupervisoryTime );
ALLOC_TYPE uint16 SBGetSupervisoryTime(void );
ALLOC_TYPE unsigned char SBSetDisplayTime( uint16 unDisplayTime ); 
ALLOC_TYPE uint16 SBGetDisplaytTime(void ); 
ALLOC_TYPE unsigned char SBSetAckDelayTimer( uint16 unAckDelayTimer ); 
ALLOC_TYPE void SBManagePirTimeStateMachine (void);
ALLOC_TYPE void SBManageSupervisoryTimeStateMachine (void);
ALLOC_TYPE unsigned char   aucData[10];     
ALLOC_TYPE unsigned char   aucTimeStamp[32]; 
ALLOC_TYPE void SBusSendPoll (void);
ALLOC_TYPE void SBWatchPollState (void);

ALLOC_TYPE void capture_rssi (unsigned char);
ALLOC_TYPE void get_floor_rssi (unsigned char);
ALLOC_TYPE unsigned char retrurn_floor_rssi_right (void);
ALLOC_TYPE unsigned char retrurn_floor_rssi_left (void);

ALLOC_TYPE void SB_ResetReceiver (void);
ALLOC_TYPE void SB_InitializeProtocolPackets (void);
ALLOC_TYPE void SB_DecodeReceivedRs485Packet (void);
ALLOC_TYPE void SB_DecodeSuperbus2000Packet (void);
ALLOC_TYPE void SB_ReconfigureProtocolPacket (unsigned char);

ALLOC_TYPE void CpuSaveRssiIntoAverage (unsigned char *, uint16 );
ALLOC_TYPE unsigned char CpuCalculateAverage (uint16 );

void SB_SetChecksum (uint8 *);
void SB_SendAck (void);
void SB_CheckForSpecialResponses (void);
unsigned char SBGetLoiterFlag( unsigned char  * );
unsigned char SBGetPauseFlag ( unsigned char  * );
uint16 SBGetCpacRfid( unsigned char  * );
extern unsigned char cReceiverType[255];  //this will hold the receiver type: CAWM=01, DualReceiver=02, GE=03, 00=unknown, ff=not responding
extern unsigned int iReceiverTimeouts[255];
void SendLogMessageToHost (char *sMessage);

#define SUPER_TABLE_SIZE			(SB_MAX_POINTS+32)/32			
#define SUPER_TABLE_WAIT			5			

/************************************************************************/
/*                                                                      */
/*		Structures																			*/
/*                                                                      */
/************************************************************************/

__align(8)


/************************************************************************/
/*                                                                      */
/*		Global Data 													*/
/*                                                                      */
/************************************************************************/
//ALLOC_TYPE struct SBPointSupervisoryStruct   asSBPointSupervisorTable[ SB_MAX_POINTS ];
ALLOC_TYPE uint32   SupervisorTable [SUPER_TABLE_SIZE];
ALLOC_TYPE uint16   auiSBDeviceRetryCounter [ SB_MAX_NEW_DEVICES ];
ALLOC_TYPE uint8    auiSBDeviceAlarmCounter [ SB_MAX_NEW_DEVICES ]; 

// v4.03:  Type 4 - PIR devices filtering counter
ALLOC_TYPE uint16  PirCounter;
ALLOC_TYPE uint16  PirIndex;
ALLOC_TYPE uint32  PirTable [SUPER_TABLE_SIZE];
										 
ALLOC_TYPE uint16   intIndex;
ALLOC_TYPE unsigned char  LedTimeOut;
ALLOC_TYPE unsigned char  ProtocolFlag;

ALLOC_TYPE unsigned char  ucData;
ALLOC_TYPE unsigned char  ucReceiverNumberResponding;	
ALLOC_TYPE uint16		  unSignalStrength;	
ALLOC_TYPE unsigned char  ucByteCount;
ALLOC_TYPE unsigned char  ucRequiredBytes;
ALLOC_TYPE unsigned char  ucMessageInfo;
ALLOC_TYPE uint16  unPointNumberz;
ALLOC_TYPE uint16  unPointNumber;
ALLOC_TYPE uint8   DeviceType;

ALLOC_TYPE unsigned char acTemp [160];

// Variables used in the caching of network status variables
typedef struct
{
	uint8	rssi;									 
	uint16	accum_rssi;									 
	uint8	left_antenna;									 
	uint8	right_antenna;								
	uint8	status;								
} SBUS_DATA;

// Define a structure for Network Receiver Status - NRS
ALLOC_TYPE		SBUS_DATA		NRS [SB_MAX_NEW_DEVICES];

// bh - v4.05 - keep an accumulation of 16 ambient rssi readings for each of 249 receivers
//ALLOC_TYPE unsigned char ambient_rssi [SB_MAX_NEW_DEVICES];
//ALLOC_TYPE uint16  accum_rssi [SB_MAX_NEW_DEVICES];

ALLOC_TYPE uint16  HighestCarPoint;
ALLOC_TYPE uint16  NewCarePoint;
ALLOC_TYPE uint16  ErrorCarePoint;
ALLOC_TYPE uint16  HighestCarePoint;
ALLOC_TYPE unsigned long HighestRfid;
ALLOC_TYPE unsigned long NewRfid;
ALLOC_TYPE uint16  DuplicateIndex;

ALLOC_TYPE uint16  CarePoint1;  
ALLOC_TYPE uint16  CarePoint2;  

ALLOC_TYPE unsigned char TkNumberOfPackets;
ALLOC_TYPE unsigned char TkCapabilityCode;
ALLOC_TYPE unsigned char TkPacketSize;
ALLOC_TYPE unsigned char TkRssi;
ALLOC_TYPE unsigned char TkBaseRssi;

ALLOC_TYPE unsigned char SB_AckFlag;
ALLOC_TYPE unsigned char SB_AckCounter;
ALLOC_TYPE uint8 		 SB_PollTime;
ALLOC_TYPE uint8		 RepeatReceiverNumber;

ALLOC_TYPE long		 lCheckSumErrors;		//RV 11-17-2104
ALLOC_TYPE long		 lPacketsSent;		//RV 11-17-2104
ALLOC_TYPE long		 lBadResponses;	  //RV 11-17-2104
ALLOC_TYPE long		 lGoodResponses;	 //RV 11-17-2104
ALLOC_TYPE long		 lAlertsProcessed;
ALLOC_TYPE long		 lRFPacketsReceived;
ALLOC_TYPE long		 lSupersProcessed;
ALLOC_TYPE long		 iCartTimeouts;
ALLOC_TYPE long		 iSmartCareTimeouts;

ALLOC_TYPE long		 iCpacMessagesSent;
ALLOC_TYPE long		 iCpacMessagesReceived;
ALLOC_TYPE long		 iFlaseAlarms;

#undef ALLOCATE_SBUS
#undef ALLOC_TYPE
/************************************************************************/
