/***************************************************************************
	File	:	uart.h
	Title	:	uart module header file
	Author	:	Bob Halliday
					bohalliday@aol.com
	Created	:	August, 2005

	Description:
		uart Definitions for 2104 uart.c

	Contents:

	Revision History:

***************************************************************************/
#ifndef HEADER_UART
#define HEADER_UART


/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#include "main.h"

#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_MAIN
#define SCOPE
#else
#define SCOPE	extern
#endif




/*******************************
*   Section 1:  Equate Definitions:
*********************************/
#define UART_WAIT_PERIOD						(uint32)(1005/TIME_BASE)
#define UART_RECEIVE_TIMEOUT					(uint32)(15/TIME_BASE)
#define UART_PERIODIC_DELAY						(uint32)(2020/TIME_BASE)
#define UART_RECEIVER_TIMEOUT					(uint32)(13100/TIME_BASE)
#define UART_SOFT_RESET_TIMEOUT					(uint32)(30/TIME_BASE)
#define UART_XMIT_WAIT							(uint32)(15/TIME_BASE)
#define UART_PRIMARY_PORT_SETTING				0x0F
#define UART_RECEIVER_INTERRUPT_ENABLE			0x02
#define UART_TRANSMITTER_INTERRUPT_ENABLE		0x01
#define UART_ACK								0x1F

#define UART_FIFO_SIZE							256
#define UART_FIFO_SIZE_MASK						(UART_FIFO_SIZE-1)

#define UART_RECEIVE_INT						0x01
#define UART_TRANSMIT_INT						0x02
#define UART_ENABLE_TRANSMITTER					0x80
#define UART_DISABLE_TRANSMITTER				0x00
#define UART_ENABLE_FIFOS						0x07
#define UART_CONTROL_0							0x2E
#define UART_CONTROL_2							0x03
#define UART_CONTROL_3							0x1B
#define UART_EVEN_PARITY						0x1B
#define UART_ODD_PARITY							0x0B
#define UART_ENABLE_DLAB						0x80

// Status bits in UxLSR
#define UART_STATUS_RCV							0x01
#define UART_FRAMING_ERROR					0x08
#define UART_STATUS_THRE						0x20
#define UART_TRANSMITTER_EMPTY					0x20
#define UART_STATUS_ERROR						0x0E
#define UART_STATUS_TEMT						0x40
#define UART_REALLY_TRANSMITTER_EMPTY			(UART_STATUS_TEMT |	UART_TRANSMITTER_EMPTY)

// Status bits in UxIIR
#define UART_INT_STATUS_INT						0x01
#define UART_INT_STATUS_THRE					0x02
#define UART_INT_STATUS_RCV						0x04

// the default baud setting nets 19.2K on UART0 and 3, 9.6K on UART1
//#define UART_DEFAULT_BAUD						234		
//#define UART_9600_BAUD							234
//#define UART_19200_BAUD							234
//#define UART_115200_BAUD						39		// 115,200 * 16 * 39 = 71,884,800 --- close enough

// 09/26/07 - reduce pclock from 72 to 48 Mhz
#define UART_DEFAULT_BAUD						156		
#define UART_9600_BAUD							156
#define UART_19200_BAUD							156
// 09/26/07 - reduce pclock from 48 to 24 Mhz
//#define UART_DEFAULT_BAUD						78		
//#define UART_9600_BAUD							78
//#define UART_19200_BAUD							78
#define UART_115200_BAUD						26		// 115,200 * 16 * 26 = 47,923,200 --- close enough
//#define UART_115200_BAUD						13		// 115,200 * 16 * 13 = 24,000,000



// 09/26/07 - reduce pclock from 72 to 48 Mhz
#define UART_57600_BAUD							52		// 115,200 * 16 * 26 = 47,923,200 --- close enough
#define UART_125000_BAUD						24		// 125,000 * 16 * 24 = 48,000,000 




#define UART_RECEIVE_CHAR_AVAIL					0x01

#define ASCII_BLANK								0x20


// Structure to define uart control operations
typedef struct
{
	uint8	ReceiveIn;							// 
	uint8	ReceiveOut;							// 
	uint8	TransmitIn;							// 
	uint8	TransmitOut;						// 
	uint8	TransmitActive; 
	uint8	ReceiveFifo [UART_FIFO_SIZE];		// 
	uint8 TransmitFifo [UART_FIFO_SIZE];		//
	uint8	MessageCount;						//when CR received inc this    
} UART_CONTROL;

	
// indeces into the transmit or receive array:
enum
	{
	UART_0,
	UART_1,
	UART_2,
	UART_3
	};

// indeces into UartArrayReceive:
enum
	{
	UART_COMMAND,
	UART_SLOT_ID,
	UART_COMMAND_OPERAND,
	UART_AVAIL1,
	UART_START_OF_DATA
	};

// uart0-1 transmitter state machine
enum
	{
	UART_TRANSMIT_DISABLED,
	UART_TRANSMIT_IDLE,
	UART_TRANSMIT_IN_PROGRESS,
	UART_DELAY_AFTER_TRANSMIT
	};


// Peer Control operand choices
enum
	{
	UART_PEER_DISABLE,
	UART_PEER_ENABLE,
	UART_PEER_GOTO_SOFTWARE_RESET
	};


#define COM_0_QUEUE_SIZE			1024
#define COM_0_QUEUE_MASK			(COM_0_QUEUE_SIZE-1)

#define COM_1_QUEUE_SIZE			800
//#define COM_1_QUEUE_MASK			(COM_1_QUEUE_SIZE-1)
extern int iCom1TxCharWaiting;

/*******************************
*   Section 2:  RAM Variables:
*********************************/

// Private Variables:
#ifdef BODY_UART


#endif



__align(8)


// Public Variables:

SCOPE		int16		Com0_xmit_in;
SCOPE		int16		Com0_xmit_out;
SCOPE		uint8		Com0_Queue [COM_0_QUEUE_SIZE];

SCOPE		int16		Com1_xmit_in;
SCOPE		int16		Com1_xmit_out;
SCOPE		uint8		Com1_Queue [COM_0_QUEUE_SIZE+1];

SCOPE		uint8		FlashWriteActive;

SCOPE	UART_CONTROL	Uart [4];	   
extern char cLastCharSentUart1;

/*******************************
*   Section 3:  Function prototypes
*********************************/

void   UartInitialize (void);
void   UartExecutive (void);
void   UartReceiveExecutive (uint8);
void   UartTransmitExecutive (void);
void   UartExecuteReceiveCommand (uint8, uint8, uint8);
void   UartEnableTransmitterInterrupt (uint8 );
void   UartDisableTransmitterInterrupt (uint8 );
void   UartTransmitMessage (uint8 *, uint8, uint8);
void   UartTransmitPacket  (uint8 *, uint8, uint8, uint8);
void   UartPrintByte (uint8, uint8);
void   UartSetCommandPending (uint8, uint8);
void   UartRestartReceiveIndex (uint8 );
void    ProcessUart1Messages (void);
void 	SendDebugStats (void);
void	SendCawmDebugStats(void);
void 	SendReceiverStats (void);
void 	SendReceiverStatsList (void);
void 	SendDatabaseStats (void);
void 	SendPollingStats (void);


void   Uart0Interrupt (void);
void   Uart1Interrupt (void);
void   Uart2Interrupt (void);
void   Uart3Interrupt (void);
void   Uart0ReceiveCharacter (void);
void   Uart0Transmitter (void);
void   Uart2Transmitter (void);
void   Uart3Transmitter (void);



/*******************************
*   Section 4:  Macros
*********************************/






/********************************
*   Section 5:  ROM Tables
*********************************/

#ifdef BODY_UART

const uint32 UartInterruptRegister [ ] =
	{
	UART0_BASE_ADDR + 0x04,
	UART1_BASE_ADDR + 0x04,
	UART2_BASE_ADDR + 0x04,
	UART3_BASE_ADDR + 0x04,
	};

const uint8 UartDebugDisabled [ ] =
	{
	"\n\rDebug Mode Disabled"
	};


const uint8 UartCrMessage [ ] =
	{
	"\r\n"
	};







#else

extern const uint8 UartCrMessage [ ];

#endif




#undef SCOPE
#endif

/* ======== END OF FILE ======== */
