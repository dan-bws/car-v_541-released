/***********************************************************
	Module Name:	factory.c  

	Author:			Bob Halliday, September, 2007   
					bohalliday@aol.com 
					781-863-8245

	Description:
	This file holds the factory tester logic for CPAC and Vector
					  
	Subroutines:	FactoryInterruptHandler()
					FactoryInitialize ()	

	Revision History:


*************************************************************/

#define  BODY_FACTORY

#include <stdio.h>
#include <string.h>
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"  

#include "admin.h"
#include "database.h"
#include "factory.h"
#include "inits.h"
#include "sio.h"
#include "uart.h"
#include "utils.h"
#include "wiegand.h"
#include <string.h>
#include <ctype.h>


/***********************************************************
	Subroutine:	 FactoryInitialize()

	Description:
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
void FactoryInitialize ()
{

}




/***********************************************************
	Subroutine:	FactoryExecutive ()

	Description:
		This is the top level Executive controlling routine for the 
		factory diagnostics logic.
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
void FactoryExecutive ()
{
	// Check to see if we are in Factory Mode
	if (!CpacFactoryMode)
		return;

	// We are in Factory Mode
	FactoryReadAllPorts ();

	// Monitor the TTL input lines, if the unit is not a Vector unit.
	if (!FactoryIsVector)
		FactoryMonitorTtlInputs ();

	// Service foreground UART tranmsmitters
	Uart3Transmitter ();				// Superbus 2000 Comm channel
	// Roam Alert RS-485 channel
	Uart1Transmitter ();				
}



/***********************************************************
	Subroutine:	FactoryEnterFactoryMode ()

	Description:
		This routine enters the unit into Factory Mode
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
uint8 FactoryEnterFactoryMode ()
{
	CpacFactoryMode = TRUE;

	// Make sure both relay outputs are cleared:
	FIO0CLR =   INITS_MAGLOCK_CONTROL | INITS_BUZZER_CONTROL;

	// Lock down the baud rate on UART 1
	FactoryEnableRoamAlertBaudRate ();

	// enable receive interrupts on all Uart1 for vector
	U1IER = UART_RECEIVE_INT;

	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}




/***********************************************************
	Subroutine:	 FactoryWriteRs485_1 ()

	Description:
		This routine transmits a fixed string out the rs485_1 port.
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
uint8 FactoryWriteRs485_1 ()
{
	SioSetMessage(UART_3, (uint8 *)TestMessage, strlen((const char *)TestMessage)) ;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}


/***********************************************************
	Subroutine:	 FactoryWriteRs485_2 ()

	Description:
		This routine transmits a fixed string out the rs485_2 port.
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
uint8 FactoryWriteRs485_2 ()
{
	SioSetMessage(UART_1, (uint8 *)TestMessage, strlen((const char *)TestMessage)) ;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/***********************************************************
	Subroutine:	 FactoryWritePagingPort ()

	Description:
		This routine transmits a fixed string out the paging port.
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
uint8 FactoryWritePagingPort ()
{
	SioSetMessage(UART_2, (uint8 *)TestMessage, strlen((const char *)TestMessage)) ;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/***********************************************************
	Subroutine:	 FactoryReadRs485_1 ()

	Description:
		This routine initiates a read of a fixed string from the rs-485_1 port.
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
uint8 FactoryReadRs485_1 ()
{
	FactoryReadRs485_1_Flag = TRUE;
  	FactoryCount [UART_3] = 0;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
  	return TRUE;
}
/***********************************************************
	Subroutine:	 FactoryReadRs485_2 ()

	Description:
		This routine initiates a read of a fixed string from the rs-485_2 port.
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
uint8 FactoryReadRs485_2 ()
{
	FactoryReadRs485_2_Flag = TRUE;
  	FactoryCount [UART_1] = 0;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/***********************************************************
	Subroutine:	 FactoryReadPagingPort ()

	Description:
		This routine initiates a read of a fixed string from the paging port.
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
uint8 FactoryReadPagingPort ()
{
	FactoryReadPagingPortFlag = TRUE;
  	FactoryCount [UART_2] = 0;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}


/***********************************************************
	Subroutine:	 FactoryReadAllPorts ()

	Description:
		This routine watches the 3 serial ports for a test string input.
		        
	Inputs:	
		
	Outputs: 
		
	Locals:
		
*************************************************************/
uint8 FactoryReadAllPorts ()
{
/**********
 	if (FactoryReadRs485_1_Flag)
		{
		// Look for an input byte on UART 3
		if (FactoryReadOnePort (UART_3))
			{
			// The string has been fully collected.
			FactoryReadRs485_1_Flag = 0;
			}
		}
 	if (FactoryReadRs485_2_Flag)
		{
		// Look for a receive byte on UART 1
		if (FactoryReadOnePort (UART_1))
			{
			// The string has been fully collected.
			FactoryReadRs485_2_Flag = 0;
			}
		}
 	if (FactoryReadPagingPortFlag)
		{
		// Look for a receive byte on UART 2
		if (FactoryReadOnePort (UART_2))
			{
			// The string has been fully collected.
			FactoryReadPagingPortFlag = 0;
			}
		}
********************/


    FactoryReadOnePort (UART_1);
    FactoryReadOnePort (UART_2);
    FactoryReadOnePort (UART_3);
	return TRUE;
}

/***********************************************************
	Subroutine:	 FactoryReadOnePort ()

	Description:
		This routine reads a serial port, looking for a test string input.
		        
	Inputs:	
		
	Outputs: 
		
	Returns:
		TRUE - a full string has been received
		FALSE- only a partial string has been received so far
		
*************************************************************/
uint8 FactoryReadOnePort (uint8 port)
{
	uint8 ucByte;
	uint8 index;
	uint8 outstring [12];

	// Check for a new byte received
   	if (SioByteCount (port))
   		{
        /* Get next Receive Byte                   */
		ucByte = SioGetByte (port);
		index = FactoryCount [port];
		// These early compares will enable synchronization with the incoming data stream
		if (ucByte == TestMessage [index] )
			{
			// So far, the characters match
	        FactoryReceiveString [port][index] = ucByte;
			FactoryCount[port]++;
			}
		else
			{
			// No Match:  restart the string acquisition process
			FactoryCount[port] = 0;
			return FALSE;
			}
      	// Test for command terminator cr
      	if( ucByte == '\r' )
      		{                       /* Insert '0' Terminator         */
        	FactoryReceiveString [port][FactoryCount[port]] = 0;

			// Compare the Receive String with the sent string:
			if (strncmp ((const char *) TestMessage, 
					(const char *) FactoryReceiveString [port], 
					strlen ((const char *) TestMessage)))
				{
				// The string comparison was a failure

				}
			else
				{
				// The string comparison was a success
     			outstring [0] = BinHexAscii (FactoryResponse [port] >> 4);		
     			outstring [1] = BinHexAscii (FactoryResponse [port]);
     			outstring [2] = 0;
    			AdminMessage( (char *) outstring );
				}
       		FactoryCount[port] = 0; 		
			return TRUE;
			}

      	else if (FactoryCount[port] >= FACTORY_BUFFER_SIZE)
			{
        	FactoryCount[port] = 0; 		
      		}
   		}
	return FALSE;
}

/*****************************************/
uint8 FactorySetTtl1High (void)
{
	// set the direction register
	FIO0DIR |= WIEGAND_DATA_ONE_MASK;						
	FIO0SET = WIEGAND_DATA_ONE_MASK;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetTtl1Low (void)
{
	// set the direction register
	FIO0DIR |= WIEGAND_DATA_ONE_MASK;						
	FIO0CLR = WIEGAND_DATA_ONE_MASK;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetTtl2High (void)
{
	// set the direction register
	FIO0DIR |=  WIEGAND_DATA_ZERO_MASK;						
	FIO0SET = WIEGAND_DATA_ZERO_MASK;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetTtl2Low (void)
{
	// set the direction register
	FIO0DIR |= WIEGAND_DATA_ZERO_MASK;						
	FIO0CLR = WIEGAND_DATA_ZERO_MASK;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetTtl3High (void)
{
	// set the direction register
	FIO0DIR |=  WIEGAND_LED_CONTROL_MASK;						
	FIO0SET = WIEGAND_LED_CONTROL_MASK;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetTtl3Low (void)
{
	// set the direction register
	FIO0DIR |=  WIEGAND_LED_CONTROL_MASK;						
	FIO0CLR = WIEGAND_LED_CONTROL_MASK;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetTtl1Input (void)
{
	// set the direction register
	FIO0DIR &= (~WIEGAND_DATA_ONE_MASK);						
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetTtl2Input (void)
{
	// set the direction register
	FIO0DIR &= (~WIEGAND_DATA_ZERO_MASK);						
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetTtl3Input (void)
{
	// set the direction register
	FIO0DIR &= (~WIEGAND_LED_CONTROL_MASK);						
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetRelay_1_On (void)
{
	FIO0SET = INITS_MAGLOCK_CONTROL;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetRelay_1_Off (void)
{
	FIO0CLR = INITS_MAGLOCK_CONTROL;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetRelay_2_On (void)
{
	FIO0SET = INITS_BUZZER_CONTROL;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}
/*****************************************/
uint8 FactorySetRelay_2_Off (void)
{
	FIO0CLR = INITS_BUZZER_CONTROL;
	// Respond with "001" (okay)
    AdminMessage( (char *) ctOK );
	return TRUE;
}

/*****************************************/
uint8 FactoryReadDryContactInputs (void)
{
	uint32 mask;
	uint8 outstring [12];

	mask = ((FIO1PIN & FACTORY_DRY_CONTACTS_MASK) >> 20) + CMD_HEX_RSP_NO_INPUTS_ACTIVE;
	// convert numbers above 79 from hex to decimal:
	if (mask > CMD_HEX_RSP_INPUTS_ACTIVE_12)
		mask += 6;
	// output the state of the Dry Contact inputs
	outstring [0] = BinHexAscii ((uint8) mask >> 4);		
	outstring [1] = BinHexAscii ((uint8) mask);
	outstring [2] = 0;
	AdminMessage( (char *) outstring );
	return TRUE;
}


/*****************************************/
void FactoryMonitorTtlInputs (void)
{
	uint32 ReadImage;

	// Skip this routine if any TTL lines are configured as outputs
//	if ((FIO0DIR & WIEGAND_TTL_OUTPUTS) == WIEGAND_TTL_OUTPUTS)
	if (FIO0DIR & WIEGAND_TTL_OUTPUTS)
		{
		// All TTL signals are configured as outputs
		return;
		}

	// Which State are we in?
	switch (FactoryTtlState)
		{
		// Read the initial image
		case TTL_READ_INPUTS:
			FactoryTtlImage = (FIO0PIN & WIEGAND_TTL_OUTPUTS);
			FactoryTtlState++; 
			break;
		// Poll the next device once every 20ms
		case TTL_COMPARE_INPUTS:
			ReadImage = (FIO0PIN & WIEGAND_TTL_OUTPUTS);
			if (ReadImage != FactoryTtlImage)
				{
				// The TTL inputs have changed
				FactoryTtlChangeMessage (ReadImage);
				FactoryTtlState--; 
				}
			break;
		}
}


/*********************************************
	Function:		FactoryTtlChangeMessage ()

	Print:
	67	-	TTL1 input just went high
	68	-	TTL1 input just went low
	69	-	TTL2 input just went high
	70	-	TTL2 input just went low
	71	-	TTL3 input just went high
	72	-	TTL3 input just went low

*****************************************/
void FactoryTtlChangeMessage (uint32 Image)
{
	uint8  PrintValue;
	uint8  outstring [12];

	// I guess we'll just brute-force these 6 paths:
	if (!(FactoryTtlImage & WIEGAND_DATA_ONE_MASK)
	 && (Image & WIEGAND_DATA_ONE_MASK))
		{
		//TTL1 input just went high
		PrintValue = CMD_HEX_RSP_TTL_1_HIGH;
		}

	else if ((FactoryTtlImage & WIEGAND_DATA_ONE_MASK)
	 && !(Image & WIEGAND_DATA_ONE_MASK))
		{
		//TTL1 input just went low
		PrintValue = CMD_HEX_RSP_TTL_1_LOW;
		}

	else if (!(FactoryTtlImage & WIEGAND_DATA_ZERO_MASK)
	 && (Image & WIEGAND_DATA_ZERO_MASK))
		{
		//TTL2 input just went high
		PrintValue = CMD_HEX_RSP_TTL_2_HIGH;
		}

	else if ((FactoryTtlImage & WIEGAND_DATA_ZERO_MASK)
	 && !(Image & WIEGAND_DATA_ZERO_MASK))
		{
		//TTL2 input just went low
		PrintValue = CMD_HEX_RSP_TTL_2_LOW;
		}

	else if (!(FactoryTtlImage & WIEGAND_LED_CONTROL_MASK)
	 && (Image & WIEGAND_LED_CONTROL_MASK))
		{
		//TTL3 input just went high
		PrintValue = CMD_HEX_RSP_TTL_3_HIGH;
		}

	else 
//	if ((FactoryTtlImage & WIEGAND_LED_CONTROL_MASK)
//	 && !(Image & WIEGAND_LED_CONTROL_MASK))
		{
		//TTL3 input just went low
		PrintValue = CMD_HEX_RSP_TTL_3_LOW;
		}

	// output the state of the TTL input change
	outstring [0] = BinHexAscii ((uint8) PrintValue >> 4);		
	outstring [1] = BinHexAscii ((uint8) PrintValue);
	outstring [2] = 0;
	AdminMessage( (char *) outstring );

}





/***********************************************************
	Subroutine:	FactoryEnableRoamAlertBaudRate ()

	Description:
		This routine will set the UART 1 channel to the Roam Alert Baud Rate
		This routine is only used in Tester Mode
     
	Inputs:		

	Outputs:

	Locals:	
			
*************************************************************/
void FactoryEnableRoamAlertBaudRate ()
{
	// Set Roam Alert UART -- 8 data bits, 1 stop bit, No Parity
	U1LCR = UART_CONTROL_2 | UART_ENABLE_DLAB;
	// This is Roam Alert - set baud rate = 57,600
	U1DLL = UART_57600_BAUD;
	U1DLM = 0;
	// Clear DLAB control bit 
	U1LCR &= ~UART_ENABLE_DLAB;
}


/*********************************************
	Subroutine:		FactorySetVector

	Description:
		The Vector flag is set to identify the unit under test as a vector device.
		This code is only called from vector.
     
	Inputs:		

	Outputs:

	Locals:	
			
*************************************************************/
void FactorySetVector ()
{
	FactoryIsVector = TRUE;
}



