/***************************************************************************
	File	:	timers.h
	Title	:	Timer Management Header File
	Author	:	Bob Halliday
					bohalliday@aol.com    (781) 863-8245
	Created	:	June, 2005
	Copyright:	(C) 2005 Exergen Corporation, Watertown, MA

	Description:
		Download Definitions for TAT-2000 timers.c source file

	Contents:

	Revision History:


***************************************************************************/
#ifndef HEADER_DOWNLOAD
#define HEADER_DOWNLOAD

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_DOWNLOAD
#define SCOPE
#else
#define SCOPE	extern
#endif

/*******************************
/*  Section 1:  Equate Definitions:
*********************************/

// The Time Base is defined as 5ms for the Typhoon Project
#define DOWNLOAD_COLON							':'
#define ASCII_A									'A'
#define ASCII_CONVERT							7
#define DOWNLOAD_HEADER_SIZE					9
#define DOWNLOAD_MASK_HIGH        				0x0F
#define DOWNLOAD_MASK_LOW        				0xF0
#define DOWNLOAD_PAGE_MASK						0xFFC0
#define DOWNLOAD_BLOCK_SIZE						64

#ifdef F8722
#define DOWNLOAD_PAGE_SIZE						64
#else
#define DOWNLOAD_PAGE_SIZE						8
#endif

#define DOWNLOAD_MEMORY_MASK					0x3F
#define DOWNLOAD_START_OF_FLASH_IMAGE			0x5000
#define DOWNLOAD_CONFIG_ADDRESS					0x4000
#define DOWNLOAD_END_OF_FLASH_IMAGE				(uint8 rom *) 0x6FFF
#define DOWNLOAD_START_OF_CONFIG_IMAGE			(uint8 rom *) 0x7000
#define DOWNLOAD_START_OF_EEPROM_IMAGE			(uint8 rom *) 0x7100
#define DOWNLOAD_END_OF_ID_IMAGE				(uint8 rom *) 0x7008
#define DOWNLOAD_END_OF_CONFIG_IMAGE			(uint8 rom *) 0x7010
#define DOWNLOAD_END_OF_EEPROM_IMAGE			(uint8 rom *) 0x7300
#define DOWNLOAD_CONFIG_OFFSET					0x1000
#define DOWNLOAD_ENABLE_FLASH					0x84
#define DOWNLOAD_ENABLE_ERASE					0x94
#define DOWNLOAD_ENABLE_WRITE_EEPROM			0x04
#define DOWNLOAD_ENABLE_READ_EEPROM				0x00
#define DOWNLOAD_NUMBER_OF_RETRIES				3

#define DOWNLOAD_END_OF_874CONFIG_IMAGE			(uint8 rom *) 0x7010
#define DOWNLOAD_END_OF_914CONFIG_IMAGE			(uint8 rom *) 0x7040



// Download DUT State Machine:
enum
	{
	DL_CHIP_ERASE,								 
	DL_ERASE_WAIT,								 
	DL_PROGRAM1,								 
	DL_WAIT1,								 
	DL_VERIFY1,
	DL_VERIFY2,
	DL_EEPROM_VERIFY
	};



/*******************************
/*  Section 2:  RAM Variables:
*********************************/

// Private Variables:
#ifdef BODY_DOWNLOAD



#endif


// Public Variables:
#ifdef BODY_DOWNLOAD
#pragma udata download_data=0x540
#endif
		
SCOPE	    uint8		DownloadImage [128];
SCOPE	    uint8		DownloadRetries;
SCOPE	    uint8		DownloadState;
SCOPE	    uint8 rom *	DownloadConfigBoundary;
							  

/*******************************
/*  Section 3:  Function prototypes
*********************************/

void  DownloadInitialize (void);
uint8 DownloadProcessIntelHex ( uint8 * );
uint8 DownloadWriteBinaryToFlash (uint8 *, uint16, uint8);
uint8 DownloadPack (uint8, uint8);
uint8 DownloadEraseBlock ( uint8 rom * );
uint8 DownloadWriteBlock ( uint8 rom *, uint8 * );
uint8 DownloadVerifyBlock ( uint8 rom *, uint8 *, uint8 );





/*******************************
/*  Section 4:  Macros
*********************************/



/********************************
/*  Section 5:  ROM Tables
*********************************/

#ifdef BODY_DOWNLOAD


#endif



#undef SCOPE
#endif

/* ======== END OF FILE ======== */
