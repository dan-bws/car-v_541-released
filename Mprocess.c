/***********************************************************
	Module Name:	mprocess.c

	Author:			Bob Halliday, June, 2007
					bohalliday@aol.com

	Description:
	This file is the message processing application for the Central Alarm Receiver

	Subroutines:	MessageProcessInit()
					Process1 ()
					ProcessMessage ()
					CS4000 ()
					MprocessValidateDatabase ()
					MprocessDumpSavedAlarms ()
					MprocessMonitorSavedAlarms ()
					RecentDuplicate ()
					DecrementRecentDelay ()
					WatchForHostErsPresence ()

	Revision History:


*************************************************************/



#include "system.h"
#include "lpc23xx.h"
#include "main.h"
#include "psi2000.h"
#include "admin.h"
#include "tec.h"
#include "sio.h"
#include "sbus.h"
#include "rtc.h"
#include "database.h"
#include "factory.h"
#include "utils.h"
#include "timers.h"
#include "lapage.h"
#include "uart.h"
#include "carts.h"
#include "cpac.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#define ALLOCATE_MPROCESS
#include "mprocess.h"

#ifdef SPECTRALINK
#include "spectralink.h"
#endif



const uint8  szPCDownMessage[] = "\nPC Down... Paging from CAR!";
const uint8  szRestoreMessage[] = " Acknowledge";
const uint8  szAlarmMessage[] = " Alarm" ;
const uint8  szTamperMessage[] = " Tamper Alarm";
const uint8  szLowBatteryMessage[] = " Low Battery";
const uint8  szDeviceCheckInMessage1[] = " Not Responding.";
const uint8  szDeviceCheckInMessage2[] = " Back Responding.";
const uint8  WANDER[] = " WANDER ";

unsigned char  ucDevice;
unsigned char  ucTypex;
unsigned char  ucDevicex;

//#define MAX_STRING_LENGTH 120
//char sString[MAX_STRING_LENGTH];
static uint8  szPageMessage3[100];
extern unsigned char  aucSBBuffer[];
extern unsigned long  BufferCopy;

uint8  *pszPageMessage;
void CS4000( uint16 unPointNumberz,	char ucType1, unsigned char ucSource, uint16 ucSBReceiverNumber, char *sRFID);

unsigned char  ucLocationFlag;
unsigned char  ucSAckDelayTimer;
unsigned char  SavedAlarmsCount;
unsigned char  SavedUnlearnedPoint[32];
unsigned char  SavedAlarms [MAX_SAVED_ALARMS][35];

/*
 * The Flag indicating the SmartCare Interface is down or up.
 *  1 = Down and 0 = UP
 */

unsigned int SmartCareInterfaceStatus = 1 ; // up BY default
#define DOWN   1
#define UP     0


/*************************************************************************/
void MessageProcessInit()
{

    // bh - 10/28/05 - init WatchHostTimer
    HostTimerLength = MAX_HOST_TIMER;

}



/*		standalone messages  @042001  */
const uint8 DASH[] = "-";
const uint8 ROOM[] = "ROOM ";
const uint8 BED[] = "BED ";
const uint8 BATH[] = "BATH ";
const uint8 COMMONROOM[] = "COMMON ROOM ";
const uint8 PENDANT[] = "PHB ";			// v407 - changed "Pendant" to "PHB"
const uint8 SMOKE[] = "SMOKE ";
const uint8 DOOR[] = "DOOR ";
const uint8 SCREEN[] = "SCREEN ";
const uint8 MOTION[] = "MOTION ";
const uint8 CHECKIN[] = "CHECK IN ";
const uint8 DELAYEGRESS[] = "DELAY EGRESS ";
const uint8 MEDICAL[] = "MEDICAL ";
const uint8 WATCH[] = "WATCH ";
const uint8 INTRUSION[] = "INTRUSION ";
const uint8 APARTMENT[] = "APARTMENT ";
const uint8 SUITE[] = "SUITE ";
const uint8 GLASS[] = "GLASS BRK ";
const uint8 FALL_DETECTED[] = "FALL "; //RAV



/*************************************************************************/
void Process1 (unsigned char Type, unsigned char cAlarm)
{
    szPageMessage3[0] = 0;

    switch (Type)
    {
    case Ha :
    {
        StringCat( szPageMessage3, (uint8 *) BATH, sizeof (BATH));
        break;
    }
    case Hb :
    {
        StringCat( szPageMessage3, (uint8 *) BED, sizeof (BED));
        break;
    }
    case Hc :
    {
        StringCat( szPageMessage3, (uint8 *) COMMONROOM, sizeof (COMMONROOM));
        break;
    }
    case Hd :
    {
        StringCat( szPageMessage3, (uint8 *) DOOR, sizeof (DOOR));
        break;
    }
    case He :
    {
        StringCat( szPageMessage3, (uint8 *) SCREEN, sizeof (SCREEN));
        break;
    }
    case Hg :
    {
        StringCat( szPageMessage3, (uint8 *) DELAYEGRESS, sizeof (DELAYEGRESS));
        break;
    }
    case Hh :
    {
        StringCat( szPageMessage3, (uint8 *) WATCH, sizeof (WATCH));
        break;
    }
    case Hi :
    {
        StringCat( szPageMessage3, (uint8 *) CHECKIN, sizeof (CHECKIN));
        break;
    }
    case Hk :
    {
        StringCat( szPageMessage3, (uint8 *) GLASS, sizeof (GLASS));
        break;
    }
    case Hl :
    {
        StringCat( szPageMessage3, (uint8 *) MEDICAL, sizeof (MEDICAL));
        break;
    }
    case Hm :
    {
        StringCat( szPageMessage3, (uint8 *) MOTION, sizeof (MOTION));
        break;
    }
    case Hn :
    {
        StringCat( szPageMessage3, (uint8 *) APARTMENT, sizeof (APARTMENT));
        break;
    }
    case Hp :
    {
        if ((cAlarm!=POINT_FALL_DETECTED) && (cAlarm!=POINT_AAHB_7000) && (cAlarm!=POINT_MHB_7000))//RAV 4-16-12
            StringCat( szPageMessage3, (uint8 *) PENDANT, sizeof (PENDANT));
        else StringCat( szPageMessage3, (uint8 *) FALL_DETECTED, sizeof (FALL_DETECTED));
        break;
    }
    case Hr :
    {
        StringCat( szPageMessage3, (uint8 *) ROOM, sizeof (ROOM));
        break;
    }
    case Hs :
    {
        StringCat( szPageMessage3, (uint8 *) SMOKE, sizeof (SMOKE));
        break;
    }
    case Ht :
    {
        StringCat( szPageMessage3, (uint8 *) SUITE, sizeof (SUITE));
        break;
    }
    case Hu :
    {
        StringCat( szPageMessage3, (uint8 *) INTRUSION, sizeof (INTRUSION));
        break;
    }
    case Hw :
    {
        StringCat( szPageMessage3, (uint8 *) WANDER, sizeof (WANDER));
        break;
    }
    case Hz :
    {
        StringCat( szPageMessage3, (uint8 *) DASH, 1);
        break;
    }
    default :
    {
        szPageMessage3[0]= 0;
        break;
    }
    }
}



//RV 6-24-2106  Convert a TXID into a GEID. The databse holds TXIDs but all buttons are marked with GEID for RFID code.
unsigned long ConvertTxidToGEID (unsigned char TXID[])
{
//TXID = 10 charcter string
    unsigned long int ID;
    unsigned char c;
    unsigned char cBytes[4];
    unsigned char x, y, bit;
    unsigned int Exp;
    char ss[10];

    sprintf (ss,"%02X%02X%02X%02X",TXID[0],TXID[1],TXID[2],TXID[3]);
    ID = 0;

//'get the first character of the first byte
    c = (ss[0]);
    if ((c=='4') || (c=='5') || (c=='6')|| (c=='7')) ID = 1;
    if ((c=='8') || (c=='9') || (c=='A')|| (c=='B')) ID = 2;
    if ((c=='C') || (c=='D') || (c=='E')|| (c=='F')) ID = 3;

//get the first character of the fourth byte
    c = (ss[7]);
    if ((c=='1') || (c=='5') || (c=='9')|| (c=='D')) ID = ID + pow (2,18);
    if ((c=='2') || (c=='6') || (c=='A')|| (c=='E')) ID = ID + pow (2,19);
    if ((c=='3') || (c=='7') || (c=='B')|| (c=='F')) ID = ID + pow(2,18) + pow(2,19);

//The two middle bytes are used in whole.
    if (ss[3]<'A') cBytes[0] = ss[3]-'0';
    else cBytes[0] = ss[3]-'A' + 10;
    if (ss[2]<'A') cBytes[1] = ss[2]-'0';
    else cBytes[1] = ss[2]-'A' + 10;
    if (ss[5]<'A') cBytes[2] = ss[5]-'0';
    else cBytes[2] = ss[5]-'A' + 10;
    if (ss[4]<'A') cBytes[3] = ss[4]-'0';
    else cBytes[3] = ss[4]-'A' + 10;

    for (y=0; y<=3; y++)
    {
        for (x=0; x<=3; x++)
        {
            bit = (unsigned char)pow(2,x);
            if (bit & cBytes[y])
            {
                Exp = (y * 4) + x + 2;
                ID = ID + (unsigned long)pow(2,Exp);
            }
        }

    }
    return ID;
}




extern char sRFID[];

/*************************************************************************/
unsigned char ProcessMessage( uint16 unPointNumber,
                              unsigned char ucType1,
                              unsigned char ucSource,
                              unsigned char Dupe_Check
                            )
{
    // bh - v4.03 - limited to 32 characters in backup Spectralink support
    uint8  szDeviceCheckInMessage[ 12 ] = " ";
    uint8  szBlankMessage [2] = " ";
    uint8  szTransCheckInMessage[ 50 ] = " Trans ";
    uint8  szReceiverCheckInMessage[ 50 ] = "Receiver ";
    uint8  szDeviceNumber[ 12 ];
    unsigned char  *pPointData;

    // bh - v4.04 - If point is not registered, return immediately
    PetWatchdog ();
    if ((!unPointNumber) && (DeviceType != SB_CPAC_TYPE))
    {
        //SendDebugMessageX ("Reporting", TRUE, TRUE);
        if(cReportingTimer==0) //To prevent multiple reports of the same problem  RV
        {
//			if (FlashRamData.cDebugMessageMode!=0)
            iUnregisteredSignalsReceivered++;
            {
                switch (ucType1)
                {
                case POINT_ALARM:
                    sprintf (sString, "Message: Unregistered alarm from a %s, ", GetDeviceType(DeviceType));
                    break;
                case POINT_FALL_DETECTED:
                    sprintf (sString, "Message: Unregistered fall detected from a %s, ", GetDeviceType(DeviceType));
                    break;
                case POINT_LOW_BATTERY:
                    sprintf (sString, "Message: Unregistered low battery from a %s, ", GetDeviceType(DeviceType));
                    break;
                case POINT_CPAC:
                    sprintf (sString, "Message: Unregistered CAWM signal from a %s, ", GetDeviceType(DeviceType));
                    break;
                case POINT_SUPERVISORY:
                    sprintf (sString, "Message: Unregistered supervision signal from a %s, ", GetDeviceType(DeviceType));
                    break;
                case POINT_PHB_7000:
                    sprintf (sString, "Message: Unregistered 7000PHB signal from a %s, ", GetDeviceType(DeviceType));
                    break;
                case POINT_AAHB_7000:
                    sprintf (sString, "Message: Unregistered 7000AAHB signal from a %s, ", GetDeviceType(DeviceType));
                    break;
                case POINT_MHB_7000:
                    sprintf (sString, "Message: Unregistered 7000MHB signal from a %s, ", GetDeviceType(DeviceType));
                    break;
                default:
                    sprintf (sString, "Message: Unregistered signal from a %s, ", GetDeviceType(DeviceType));
                    break;
                }

                if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
                {
                    SendDebugMessageX (sString, TRUE, FALSE);
                    sprintf (sString,"at rec #%03u, ", (int)aucSBBuffer[0]);
                    SendDebugMessageX (sString, FALSE, FALSE);

                    sprintf (sRFID, "%02X%02X%02X", aucSBBuffer [12]&0x0F, aucSBBuffer[11],aucSBBuffer[10]);
                    sprintf (sString,"RFID=%s",sRFID);
                    SendDebugMessageX (sString, FALSE, TRUE);
                }
            }
            cReportingTimer=4;
        }
        return 0;
    }

    /* Test if duplicate of a recent message */
    // bh - 3.85 - 01/05/06 -- Some calling modules don't care about Recent Duplicate info
    if (Dupe_Check)
    {
        if( RecentDuplicate ( unPointNumber, ucType1, ucSBReceiverNumber ) )
        {
            return 0;        /* if so return do not process */
        }
    }

    /* If not a Device Message Test to reject unknowns */
    if ((unPointNumber)	|| (DeviceType == SB_CPAC_TYPE))
    {
			
        /* Test if host pc present */
        if (!InFactoryMode)
        {
            pPointData = SBGetPointData( unPointNumber, TRUE );
            sprintf (sRFID,"%06lX", ConvertTxidToGEID (pPointData));
 					
						//Send Alarm or signal to SmartCare
            CS4000( unPointNumberz, ucType1, ucSource, unPointNumber, sRFID);
        }
        if( gucstandalone_mode == 0 )   				//PC and no host timeout?
        {
            // host pc is present
            return 0;
        }
        else            /* No host pc stand alone mode */
        {
            // host pc is not present
            if( ucSource ==  DEVICE_MESSAGE )
            {
                if( ucType1 ==   DEVICE_NOT_RESPONDING )
                {
                    nFlag = 1;
                    UIntToAscii( szDeviceNumber, unPointNumber );
                    StringCat( szReceiverCheckInMessage, szDeviceNumber, 40 );
                    StringCat( szReceiverCheckInMessage, (uint8 *) szDeviceCheckInMessage1, 40 );
                    StringCat( szPageMessage3, szReceiverCheckInMessage, 40 );
                }
                if( ucType1 ==   DEVICE_BACK_RESPONDING )
                {
                    nFlag = 1;
                    if ( ucReceiverNumberResponding == 0)
                    {
                        UIntToAscii( szDeviceNumber, unPointNumber  );
                    }
                    else
                    {
                        UIntToAscii( szDeviceNumber, ucReceiverNumberResponding  );
                    }

                    StringCat( szReceiverCheckInMessage, szDeviceNumber, 40 );
                    StringCat( szReceiverCheckInMessage, (uint8 *) szDeviceCheckInMessage2, 40 );
                    StringCat( szPageMessage3, szReceiverCheckInMessage, 40 );
                }
                if (ucType1 == DEVICE_TAMPER)
                {
                    UIntToAscii( szDeviceNumber, unPointNumber );
                    szDeviceNumber[0] = szDeviceNumber[1];
                    szDeviceNumber[1] = szDeviceNumber[2];
                    szDeviceNumber[2] = szDeviceNumber[3];
                    szDeviceNumber[3] = szDeviceNumber[4];
                    StringCat( szDeviceCheckInMessage, szDeviceNumber, 4 );
                    StringCat( szPageMessage3, szDeviceCheckInMessage, 40 );
                    StringCat( szReceiverCheckInMessage, szDeviceNumber, 40 );
                }
            }
            else
            {
                if (ucType1 == POINT_CPAC)
                {
                    // This is a CPAC Wander Alarm
                    if (!CpacBackupPaging (szPageMessage3))
                        return 0;
                }
                else
                {
                    // This is a non-CPAC alarm
                    pszPageMessage = SBGetPointData( unPointNumber, TRUE );
                    // Make sure the passed character is lower case
                    //RAV   The following does the backup paging
                    Process1 ((uint8) pszPageMessage[7] | 0x20, ucType1);
//		  			StringCat( szPageMessage3+1, (pszPageMessage+8), 7 );
                    StringCat( szPageMessage3,   (pszPageMessage+8), 8 );
                }

                if ((ucType1 == POINT_ALARM ) || (ucType1 == POINT_FALL_DETECTED)
                        || (ucType1 == POINT_PHB_7000 ) || (ucType1 == POINT_AAHB_7000) || (ucType1 == POINT_MHB_7000))
                {
                    // v4.08: Don't allow "Alarm" message to start before the 16th character
#ifdef SPECTRALINK
                    //PetWatchdog ();
                    if (SL_SpectralinkActive)
                    {
                        while ( strlen ((const char *)szPageMessage3) < 16 )
                        {
                            StringCat( szPageMessage3, szBlankMessage, 1 );
                        }

                    }
#endif

                    nFlag = 1;
                    StringCat( szPageMessage3, (uint8 *) szAlarmMessage, 20 );
                    UIntToAscii( szDeviceNumber, unPointNumber );
                    szDeviceNumber[0] = szDeviceNumber[1];
                    szDeviceNumber[1] = szDeviceNumber[2];
                    szDeviceNumber[2] = szDeviceNumber[3];
                    szDeviceNumber[3] = szDeviceNumber[4];
                    StringCat( szDeviceCheckInMessage, szDeviceNumber, 4 );
                    StringCat( szPageMessage3, szDeviceCheckInMessage, 40 );
                }
                /* Add Acknowlege to message if required */
                if (ucType1 == POINT_ACKNOWLEGE )
                {
                    nFlag = 1;
                    StringCat( szPageMessage3, (uint8 *) szRestoreMessage, 20 );
                    UIntToAscii( szDeviceNumber, unPointNumber );
                    szDeviceNumber[0] = szDeviceNumber[1];
                    szDeviceNumber[1] = szDeviceNumber[2];
                    szDeviceNumber[2] = szDeviceNumber[3];
                    szDeviceNumber[3] = szDeviceNumber[4];
                    StringCat( szDeviceCheckInMessage, szDeviceNumber, 4 );
                    StringCat( szPageMessage3, szDeviceCheckInMessage, 40 );
                }

                /* Add Tamper to message if required */
                if ( ucType1 == POINT_TAMPER )
                {
                    StringCat( szPageMessage3, (uint8 *) szTamperMessage, 20 );
                }

                /* Add Low Battery to message if required */
                if ( ucType1 == POINT_LOW_BATTERY )
                {
                    StringCat( szPageMessage3, (uint8 *) szLowBatteryMessage, 20 );
                }
                /* Add Supervisory to message if required */

                if( ucType1 == POINT_SUPERVISORY )
                {
                    UIntToAscii( szDeviceNumber, unPointNumber );
                    StringCat( szTransCheckInMessage, szDeviceNumber, 20 );
                    StringCat( szTransCheckInMessage, (uint8 *) szDeviceCheckInMessage1, 20 );
                    StringCat( szPageMessage3, szTransCheckInMessage, 40 );
                }
            }

            // v4.03: Don't allow "PC Down" message to start before the 32nd character
            //PetWatchdog ();
            while ( strlen ((const char *)szPageMessage3) < 32 )
            {
                StringCat( szPageMessage3, szBlankMessage, 1 );
            }

            StringCat( szPageMessage3, (uint8 *) szPCDownMessage, strlen ((const char *)szPCDownMessage) );
            cr_flag = 0;
            if (!InFactoryMode)
                AdminMessage( (char *) szPageMessage3 );

            if ( InFactoryMode || (( nFlag ) && ( !(szPageMessage3[0] == '\x2D' ))) )
            {
#ifdef SPECTRALINK
                if (!SL_SpectralinkActive)
#endif
                    LAPage ( szPageMessage3 );			// v5.13 patch

#ifdef SPECTRALINK
                // Save alarm message for Spectralink backup

                if (SL_SpectralinkActive)
                    SpectralinkSaveAlarm (szPageMessage3);
#endif
            }

            //RV Debug 11-20-2104
            if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            {
                sprintf (sString, "Pager: Sending - ");
                SendDebugMessageX (sString, TRUE, FALSE);
                if (szPageMessage3[32]==0x0A)szPageMessage3[32]=' ';
                if (szPageMessage3[33]==0x0A)szPageMessage3[33]=' ';
                SendDebugMessageX ((char *)szPageMessage3, FALSE, TRUE);
            }


        }
    }
    // Clear out the buffer after this process is finished.
    memset ((void *) szPageMessage3, (int) 0, sizeof (szPageMessage3));
    return 0;
}

/***********************************************************
	CPAC routine:
				Mprocess_ReportCpacStatusMessage ()

	Description:
		A CPAC status message has been received.
		This routine will convert the binary values to ASCII and ship them to SmartCare

	Inputs:

	Outputs:

	Locals:

**************************************************************/
void Mprocess_ReportCpacStatusMessage (uint8 * buffer, uint8 filler, uint8 count, uint8 id, char *sType)
{
    uint8 TransmitBuffer [90];
    uint8 i,j;

//--------------------------------------------------
// Also make sure to save the CPAC status message for the CAR's own usage
    if (filler == '0')
        memcpy ((char *)CpacStatus, (char *) &buffer [TK_STATUS_BYTE], CPAC_STATUS_SIZE);
//--------------------------------------------------

    // The first 5 characters in the message are zeroes or 'z's
    memset ((void *) TransmitBuffer, (int) filler, 5);

    // Now convert the rest of the binary packet to ASCII
    for ( i = 5, j = TK_STATUS_BYTE;  j < (count + TK_STATUS_BYTE);  j++ )
    {
        TransmitBuffer [i++] = BinHexAscii (buffer [j] >> 4);
        TransmitBuffer [i++] = BinHexAscii (buffer [j]);
    }

    // Back up to put in the receiver number:
    i -= 2;
    TransmitBuffer [i++] = BinHexAscii (ucReceiverNumberResponding >> 4);
    TransmitBuffer [i++] = BinHexAscii (ucReceiverNumberResponding);

    // set ID at the end of the string
    TransmitBuffer [i++] = id;
    TransmitBuffer [i++] = '\r';
    TransmitBuffer [i++] = '\n';
    TransmitBuffer [i]   = 0;
    j = i;

    if ( !gucAdminTrapFlag )
    {
        // We are not in "2" Command Mode - simply send the CPAC Status to the Host
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
        {
            sprintf (sString, "CPAC alarm data to SC: ");
            //SendDebugMessageX (sString, TRUE, FALSE);
            for (i=0; i<j-1; i++)
            {
                sString[i] = TransmitBuffer [i];
            }
            sString[i+1]=0;
            //SendDebugMessageX (sString, FALSE, FALSE);
        }
        SioSetMessage( UART_0, TransmitBuffer, strlen ((const char *) TransmitBuffer ) );
    }

    sString[0]=0;

    {
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
        {
            sprintf (sString,"CAWM to SC: sending [%s] from #%i - ", sType, ucReceiverNumberResponding);
            SendDebugMessageX (sString,TRUE, FALSE);
            strncpy (sString,(char *)TransmitBuffer,80);
            sString[80]=0;
            for (i=0; i<strlen(sString); i++)
            {
                if ((sString[i]==0x0D) || (sString[i]==0x0A))
                    sString[i]=' ';
            }
            SendDebugMessageX (sString, FALSE, TRUE);
        }
    }
    iCpacMessagesReceived++;
}




/*************************************************************************/
void CS4000( uint16 unPointNumberz, char ucType1, unsigned char ucSource, uint16 ucSBReceiverNumberx, char *sRFID )
{
    uint8   szMsgBuf1[35] = "0000000000000";
    uint8   szMsgBuf2[12];
    uint8   szMsgBuf3[12];
    uint16  ReceiverNumber;
    char sTemp[25];
    char bSendNothingElse, bSkip;
    unsigned char c, i;

    szMsgBuf2[0]=0;
    bSendNothingElse=FALSE;
    bSkip=FALSE; //no to skip supervise messages. too many

    if ( ucReceiverNumberResponding == 0)			//is this a now-respoding receiver?
    {
        UIntToAscii( szMsgBuf3, ucSBReceiverNumberx );		//n,cont @110301
        ReceiverNumber = ucSBReceiverNumberx;
    }
    else
    {
        UIntToAscii( szMsgBuf3, ucReceiverNumberResponding );	//y, put the now-respoding receiver# in buffer @080505
        ReceiverNumber = ucReceiverNumberResponding;
    }

    // v5.00 --- Check for a CPAC alarm:
    if (unPointNumberz == SB_CPAC_RFID_MARKER)
    {
        // This is a CPAC Alarm - organize a 6-digit Hex-Ascii string
        memcpy (&szMsgBuf1[6], CpacRfidString, 6);
    }
    else
    {
        // This is a GE Alarm - output 5 decimal digits
        UIntToAscii( szMsgBuf2, unPointNumberz );		//n, put non-supervisory number into buffer
        szMsgBuf1[ 05 ] = szMsgBuf2[ 0 ];
        szMsgBuf1[ 06 ] = szMsgBuf2[ 1 ];
        szMsgBuf1[ 07 ] = szMsgBuf2[ 2 ];
        szMsgBuf1[ 10 ] = szMsgBuf2[ 3 ];
        szMsgBuf1[ 11 ] = szMsgBuf2[ 4 ];
    }

    szMsgBuf1[ 13 ] = '\r';
    szMsgBuf1[ 14 ] = '\n';

    if( ucSource == DEVICE_MESSAGE )
    {
        /* Reposition device number */
        // bh - 01/18/06 - send three digits to support 100 or more active carts
        szMsgBuf1[ 05 ] = szMsgBuf3[ 2 ];
        szMsgBuf1[ 06 ] = szMsgBuf3[ 3 ];	//Rec# msd
        szMsgBuf1[ 07 ] = szMsgBuf3[ 4 ];	//Rec# lsd
    }

    sTemp[0]=0;
    switch( ucType1 )
    {
    case DEVICE_NOT_RESPONDING :
    {
        szMsgBuf1[ 10 ] = 'E';
        szMsgBuf1[ 11 ] = '0';
        szMsgBuf1[ 12 ] = 'A';
        sprintf (sString, "To SmartCare: Sending Receiver #%i Not Responding", ReceiverNumber);
        bSendNothingElse=TRUE;
        if (ReceiverNumber<255)cReceiverType[ReceiverNumber]=0xFE;
        break;
    }
    case DEVICE_BACK_RESPONDING :
    {
        szMsgBuf1[ 10 ] = 'E';
        szMsgBuf1[ 11 ] = '0';
        szMsgBuf1[ 12 ] = 'J';
        sprintf (sString, "To SmartCare: Sending Receiver #%i is back Responding", ReceiverNumber);
        bSendNothingElse=TRUE;
        if (ReceiverNumber<255)cReceiverType[ReceiverNumber]=0x00;
        break;
    }
    case POINT_AAHB_7000:
    case POINT_FALL_DETECTED :
    case POINT_MHB_7000:
    {
        //RAV 4/21/2012 This is where we set the alarm type to be sent to SmartCare
        if (FlashRamData.cSmartCareDoesFallDetector)
            szMsgBuf1[ 12 ] = 'F';
        else szMsgBuf1[ 12 ] = 'A';
        strcpy (sTemp, "Fall");
        lAlertsProcessed++;
        break;
    }
    case POINT_PHB_7000:
    case POINT_ALARM :
    {
        //RAV 4/21/2012 This is where we set the alarm type to be sent to SmartCare
        szMsgBuf1[ 12 ] = 'A';
        strcpy (sTemp, "Alarm");
        lAlertsProcessed++;
        break;
    }
    case  POINT_ACKNOWLEGE :
    {
        szMsgBuf1[ 12 ] = 'K';
        strcpy (sTemp, "Ack");
        break;
    }
    case POINT_TAMPER :
    {
        szMsgBuf1[ 12 ] = 'T';
        strcpy (sTemp, "Tamper");
        lAlertsProcessed++;
        break;
    }
    case POINT_LOW_BATTERY  :
    {
        szMsgBuf1[ 12 ] = 'L';
        strcpy (sTemp, "Low Batt");
        lAlertsProcessed++;
        break;
    }
    // This is a CPAC Alert
    case POINT_CPAC  :
    {
        // This is a CPAC Wander Alert
        szMsgBuf1[ 12 ] = CpacGetAlertValue ();
        strcpy (sTemp, "CAWM Message");
        break;
    }
    case POINT_SUPERVISORY:
    {
        bSkip=TRUE;	//dont print these out. They hapen every 70 min for every device
        szMsgBuf1[ 12 ] = 'S';
        //strcpy (sTemp, "Supervision");
        break;
    }
    // bh - v4.04 - Database errors X, Y and Z.
    case DATABASE_OUT_OF_ORDER  :
    case POINT_NUMBER_OUT_OF_RANGE  :
    case POINTS_DUPLICATE_NUMBERS  :
    {
        strcpy (sTemp, "Database Error");
        szMsgBuf1[ 12 ] = ucType1;
        break;
    }
    default :
    {
        szMsgBuf1[ 12 ] = '0';     /* Unknown alarm  for now */
        strcpy (sTemp, "Unknown Alarm");
        break;
    }
    }

    c = 13;

//  	if ((FlashRamData.cDebugMessageMode!=0) && (sTemp[0]!=0))

    if (!bSkip)
    {
        if (bSendNothingElse)
        {
            if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            {
                PetWatchdog();
                SendDebugMessage (sString);
                while (Uart [UART_1].TransmitActive)
                {
                    Uart1Transmitter ();
                }
            }
        }
        else
        {
            if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
            {
                /*
                	read the database for this point
                	Points record size is 15 bytes.
                			aaaa  = point number, 4 bytes
                			bbbbb = RFID, 5 bytes
                			ccccccc = Location, 7 bytes
                */

                sprintf (sString, "To SmartCare: Sending %s Point #%i", sTemp, unPointNumberz);
                SendDebugMessageX (sString, TRUE, FALSE);
                sprintf (sString, " from rec #%i, RFID=%s", ReceiverNumber, sRFID);
                SendDebugMessageX (sString, FALSE, TRUE);
            }
        }
    }


    //RAV Now that we have capturted this fall and set the alarm code correctly,
    //change the alarm to to a regualr Point Alarm so we can process it as a normal PHB
    if (( ucType1 == POINT_FALL_DETECTED ) || (ucType1 == POINT_PHB_7000 ) || (ucType1 == POINT_AAHB_7000) || (ucType1 == POINT_MHB_7000))
    {
        ucType1 = POINT_ALARM;
        ucDevicex= SB_PENDANT;
    }

    if ((( ucType1 == POINT_ALARM ) && (ucDevicex == SB_PENDANT) && FlashRamData.ucLocationFlag)
            ||  (ucType1 == POINT_CPAC))
    {
        // bh - 01/18/06 - send three digits if the CART number is > 99
        if ( ReceiverNumber > 99 )
        {
            szMsgBuf1[ c++ ] = szMsgBuf3 [2];
        }
        else
        {
            szMsgBuf1[ c++ ] = ' ';
        }

        szMsgBuf1[ c++ ] = szMsgBuf3[ 3 ];
        szMsgBuf1[ c++ ] = szMsgBuf3[ 4 ];
        szMsgBuf1[ c++ ] = ',';
        szMsgBuf1[ c++ ] = '0'; 									// signal strength msd
        if  (ucType1 == POINT_CPAC)
        {
            // CPAC messages have no associated rssi signal
            szMsgBuf1[ c++ ] = '0';
            szMsgBuf1[ c++ ] = '0';
        }
        else
        {
            szMsgBuf1[ c++ ] = BinHexAscii(unSignalStrength >> 4);	// signal strength msd
            szMsgBuf1[ c++ ] = BinHexAscii(unSignalStrength);			// signal strength lsd
        }
    }

    szMsgBuf1[ c++ ] = '\r';
    szMsgBuf1[ c++ ] = '\n';
    // bh - 11/08/05 -- Add a  NULL terminator at the end of the message
    szMsgBuf1[ c   ] =  0;

    if( gucAdminTrapFlag )
    {
        // We are in "2" Command Mode - buffer the message until "2" Command Mode is terminated
        if (SavedAlarmsCount < MAX_SAVED_ALARMS)
        {
            // v4.12 - reject any alarms that had just been previously learned
            BufferCopy = SBGetDeviceIDx ( &aucSBBuffer [3] );
            if ( BufferCopy != unregistered_copy )
            {
                strcpy ((char *) SavedAlarms [SavedAlarmsCount++], (const char *) szMsgBuf1);
            }
        }
    }


    else
    {
        // We are not in "2" Command Mode - simply send the alarm to the Host
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
        {
            sprintf (sString, "Alarm data to SC: ");
            SendDebugMessageX (sString, TRUE, FALSE);
            for (i=0; i<c-1; i++)
            {
                sString[i] = szMsgBuf1 [i];
            }
            sString[i]=0;
            SendDebugMessageX (sString, FALSE, FALSE);
        }
        SioSetMessage( UART_0, szMsgBuf1, strlen ((const char *) szMsgBuf1 ) );
    }
}





/*************************************************************************
	     MprocessValidateDatabase ()

		v4.04
		After the Points Database has been completely downloaded, this routine
		will inspect the database and issue one of 3 possible Supervisory Errors:
		 4444 - records in the database are out of order
		 5555 - A CarePoint Point entry is out of range
		 6666 - Duplicate CarePoint points.


*************************************************************************/
void MprocessValidateDatabase ()
{
    // First check for Database out of order
    if (DatabaseOutOfOrderFlag)
    {
        // Database was out of order!
        DatabaseOutOfOrderFlag = 0;
        unPointNumberz = ErrorCarePoint;
        ProcessMessage( SB_MAX_POINTS, DATABASE_OUT_OF_ORDER, SBUS_MESSAGE, NULL );
    }
    HighestRfid = 0;

    // Now check for SmartCare Point Out of Range:
    if (HighestCarePoint >= SB_MAX_POINTS)
    {
        // CarePoint Point is Out of Range
        unPointNumberz = HighestCarePoint;
        ProcessMessage( SB_MAX_POINTS, POINT_NUMBER_OUT_OF_RANGE, SBUS_MESSAGE, NULL );
    }

    // Restart the Highest Care Point for the next points download
    HighestCarePoint = 0;

    // Now activate the Duplicate Point Check
    ActivateDuplicatePointsCheckFlag = 1;
    DuplicateIndex = 1;

}





/*************************************************************************
	     MprocessDumpSavedAlarms ()

		When Command 2 is finished, check the SavedAlarms Queue to see if any alarms
		have been buffered.  If they indeed were buffered, print them out now.


*************************************************************************/
void MprocessDumpSavedAlarms ()
{
    unsigned char i;

    if (SavedAlarmsCount)
    {
        for ( i=0;  i < SavedAlarmsCount;  i++ )
        {
            SioSetMessage( UART_0, SavedAlarms [i], strlen ((const char *) SavedAlarms [i] ) );
        }
    }

    // restart the alarms counter
    SavedAlarmsCount = 0;
}



/*************************************************************************
	     MprocessMonitorSavedAlarms ()

		When Command 2 or 12 is finished, wait 2 seconds.
		Then check the SavedAlarms Queue to see if any alarms
		have been buffered.  If they indeed were buffered, print them out now.


*************************************************************************/
void MprocessMonitorSavedAlarms ()
{

    switch( SavedAlarmsState )
    {
    // Nothing is happening
    case SAVED_ALARMS_IDLE :
        break;

    // Command 2 or 12 just ended
    case SAVED_ALARMS_START_TIMER :
        SavedAlarmsTimer = 0;
        SavedAlarmsState++;
        break;

    // 1 second has passed since command 2 or 12 ended
    case CHECK_SAVED_ALARMS:
        // bh - v4.02
        if (SavedAlarmsTimer >= (ONE_SECOND*3))
        {
            // dump any saved alarms
            MprocessDumpSavedAlarms ();
            // turn off Command 2 or 12 Mode
            gucAdminTrapFlag = 0;
            Command_12_active = 0;
            SavedAlarmsState = SAVED_ALARMS_IDLE;
            // v5.00 -- Turn off the Flash imaging of the Points Database
            Flash_TerminateFlashImaging ();
            // v4.04 --- Validate the newly downloaded Points Database
            MprocessValidateDatabase ();
        }
        break;

    default:
        SavedAlarmsState = SAVED_ALARMS_IDLE;
    }
}




/*************************************************************************
	RecentDuplicate ()


	returns:
		TRUE - throw this alarm away.  It's a duplicate
		FALSE - report this alarm.  It's not a duplicate
*************************************************************************/
unsigned char RecentDuplicate( uint16 unPointNumber, unsigned char ucType1, unsigned char ReceiverNum )
{
    unsigned char c;
    ucSBReceiverNumber = aucSBBuffer[SB_DEVICE_ADDRESS];
    ucTypex = ucType1;
    unSignalStrength = aucSBBuffer[OLD_RSSI];
    // bh - v4.04 - signal strength above 160 is reduced to a nominal value
    if ( unSignalStrength >= RSSI_OUT_OF_RANGE  ) unSignalStrength = RSSI_NOMINAL;

    ucDevice = ((aucSBBuffer[6] >> 2 ) & 0x0F );
    ucDevicex = ucDevice;

    for( c = 0; c < MAX_RECENT; c ++ )
    {
        if( asRecentMessages[ c ].ucDelay > 0 )
        {
            // Test Id and Device Type
            if( unPointNumber == asRecentMessages[ c ].unPointNumber )
            {
                if( ucTypex == asRecentMessages[ c ].ucType )
                {
                    asRecentMessages[ c ].ucPacketCounter++;
                    if( ((ucTypex == POINT_ALARM) || (ucTypex == POINT_FALL_DETECTED) || (ucType1 == POINT_PHB_7000 ) || (ucType1 == POINT_AAHB_7000))
                            && ((ucDevice == SB_PENDANT) || (ucDevice == SB_7000_PHB) || (ucDevice == SB_7000_AAHB) || (ucDevice == SB_7000_MHB))) //RAV 5-29-12
                    {
#ifdef RSSI_FLOOR
                        // bh - v4.07 - save all rssi readings for averaging
                        if (RssiAveragingFlag)
                            ll							CpuSaveRssiIntoAverage (aucSBBuffer, unPointNumber);
#endif

                        //RV 6-13-2016 BUG Trap
                        //For the new 7000PHB and 7000AHB: There is a bug in the receiver where the RSSI is reported as 95 by the Wireless Link and
                        //over 240 (usualy 254 or 255) by the 7000R receiver. This is a bug that need to be fixed. These readings
                        //mess up the locating. For now we will set this RSSI to zero becuase it must have been a weak signal
                        if (((ucDevice == SB_7000_PHB) || (ucDevice == SB_7000_AAHB) || (ucDevice == SB_7000_MHB)) && (unSignalStrength > MAX_RSSI_FOR_7000R))
                        {
                            //Fix this RSSI reading
                            if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
                            {
                                sprintf (sString, "Locating error. Bad RSSI on Receiver=%u RSSI=%u", ucSBReceiverNumber, unSignalStrength);
                                SendDebugMessageX (sString, TRUE, TRUE);
                            }
                            unSignalStrength = 0; //This removes this signal from the locating calculation
                        }

                        if( unSignalStrength > asRecentMessages[ c ].unSignalStrength )
                        {
                            // Reset Timer
                            asRecentMessages[ c ].ucSBReceiverNumber = ucSBReceiverNumber;
                            asRecentMessages[ c ].unSignalStrength = unSignalStrength;
                        }
                    }
                    return 1;  //throw away a dupe
                }
            }
        }
    }

    //RV 6-13-2016  Part of BUG patch above. We get here on the first signal from an alert.
    if (((ucDevice == SB_7000_PHB) || (ucDevice == SB_7000_AAHB) || (ucDevice == SB_7000_MHB)) && (unSignalStrength > MAX_RSSI_FOR_7000R))
    {
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
        {
            sprintf (sString, "Locating error. Bad RSSI on Receiver=%u RSSI=%u", ucSBReceiverNumber, unSignalStrength);
            SendDebugMessageX (sString, TRUE, TRUE);
        }
        unSignalStrength = 0;
    }

    // Not found insert in list
    for( c = 0; c < MAX_RECENT; c ++ )
    {
        if( asRecentMessages[ c ].ucDelay == 0 )
        {
            if (unPointNumber)
            {
//SendDebugMessageX ("Saving recent", TRUE, FALSE);
//sprintf (sString,"  Index=%u ", c);
//SendDebugMessageX (sString, FALSE, FALSE);
                asRecentMessages[ c ].unPointNumber = unPointNumber;
                asRecentMessages[ c ].unPointNumberz = unPointNumberz;
                asRecentMessages[ c ].ucType = ucTypex;
                asRecentMessages[ c ].ucDevice = ucDevice;
                asRecentMessages[ c ].ucSBReceiverNumber = ucSBReceiverNumber;
                asRecentMessages[ c ].unSignalStrength = unSignalStrength;
                asRecentMessages[ c ].ucPacketCounter=1;

//sprintf (sString," P=%u  Pz=%u Index=%u", unPointNumber, unPointNumberz, c);
//SendDebugMessageX (sString, FALSE, FALSE);

//sprintf (sString," Rec=%u  Stren=%u", ucSBReceiverNumber, unSignalStrength);
//SendDebugMessageX (sString, FALSE, TRUE);

                // bh - v4.04 - Start out with a fixed filter time
                if ((ucTypex == POINT_FALL_DETECTED) || (ucTypex == POINT_AAHB_7000) || (ucTypex == POINT_MHB_7000))
                    asRecentMessages[ c ].ucDelay = 50;	 //rav 5-29-12 10 sec for PHB. AAHB sends out packets for up to 36 seconds!
                else asRecentMessages[ c ].ucDelay = 10;
                if( ((ucTypex == POINT_ALARM) || (ucTypex == POINT_FALL_DETECTED) || (ucType1 == POINT_PHB_7000 ) || (ucType1 == POINT_AAHB_7000)|| (ucType1 == POINT_MHB_7000))
                        && ((ucDevice == SB_PENDANT) || (ucDevice == SB_7000_PHB) || (ucDevice == SB_7000_AAHB) || (ucDevice == SB_7000_MHB))) //RAV 5-29-12
                {
                    if ( FlashRamData.ucLocationFlag )
                    {
                        // bh - v4.04 - Locating Time is now a variable retained in EEprom
                        asRecentMessages[ c ].ucDelay = LocatingTime;
                        return 1;
                    }
                }
                return 0;		//
            }
        }
    }
    return 0;
}



/*************************************************************************

	DecrementRecentDelay () -

	This routine is called once a second

***************************************************************************/
void DecrementRecentDelay ( )
{
    unsigned char c;
    unsigned char bBadData;

    /**********************************************************/

    // bh - 10/14/05 -- postpone this function if an EEprom write operation is in progress
    if (EECheckForWriteOperation ())
    {
        // EEprom Write Operation in progress
        return;
    }

    /**********************************************************/
    SetLedBankPort1 ((unsigned char)(lPacketsSent));
    SetLedBankPort2 ((unsigned char)(lRFPacketsReceived));
    if (OneSecondTimer < ONE_SECOND)
    {
        return;
    }

    // One second timeout:  restart the one second timer
    OneSecondTimer = 0;

    //Timers for debugging messages
    lSecondsCounter++;
//	SetLedBankPort1 (lSecondsCounter);
    iDebugMessageTimer++;
    if (iStatusTimer>0) iStatusTimer--;
    if (iSmartCareIsDownTimer>0) iSmartCareIsDownTimer--;
    if (iPauseDebugTimer>0) iPauseDebugTimer--;

    if (iDebugMessageTimer>=299)
    {
        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
        {
//				SendDebugStats();
//				SendReceiverStats();
//				SendCawmDebugStats();
//				SendPollingStats();
//				SendDebugMessageX ("", FALSE, TRUE);
        }
        iDebugMessageTimer=0;
    }

    if (cReportingTimer>0) cReportingTimer--;

    // bh --- 10/28/05 ---- Monitor Host ERS presence
    WatchForHostErsPresence ();
    // Maintain a heart beat
    HeartBeat ();
    // Maintain Supervisor Timer
    SBDecrementSupervisorDelay ();
    // Do Host check in after the first second
    HostCheckIn ();
    // v5.08 - Update the Learn Mode timeout timer
    MainWatchLearnMode ();

    // v5.00 - decrement the superbus monitor timer
    if (SuperbusMonitorTimer)
        --SuperbusMonitorTimer;


#ifdef SPECTRALINK
    // Update Spectralink seconds
    SL_Seconds++;
    SL_HeartBeat++;
#endif

    // bh --- 01/13/06 -- v3.87 -- put the receiver monitoring timer countdown over here
    for( c = 1; c < SB_MAX_NEW_DEVICES; c ++ )
    {
        if  (auiSBDeviceRetryCounter [ c ])
        {
            // v5.00 - only decrement active carts
            CartsConvertToPointerMask (c);
            if (CartsPollingList [ BytePtr ] & BitMask)
                auiSBDeviceRetryCounter [ c ]--;
        }
    }


    // Operate the Locating Delay logic here
    for( c = 0; c < MAX_RECENT; c ++ )
    {
        if( asRecentMessages[ c ].ucDelay > 0 )
        {
            asRecentMessages[ c ].ucDelay--;
            if( asRecentMessages[ c ].ucDelay == 0 )
            {
                if (((asRecentMessages[ c ].ucType == POINT_ALARM ) || ( asRecentMessages[ c ].ucType == POINT_FALL_DETECTED )
                        || (asRecentMessages[ c ].ucType == POINT_PHB_7000 ) || ( asRecentMessages[ c ].ucType == POINT_AAHB_7000) || ( asRecentMessages[ c ].ucType == POINT_MHB_7000 ))
                        && ((asRecentMessages[ c ].ucDevice== SB_PENDANT) || (asRecentMessages[ c ].ucDevice == SB_7000_PHB) || (asRecentMessages[ c ].ucDevice == SB_7000_AAHB)  || (asRecentMessages[ c ].ucDevice == SB_7000_MHB))
                        && (FlashRamData.ucLocationFlag))
                {
                    unPointNumber = asRecentMessages[ c ].unPointNumber;
                    unPointNumberz = asRecentMessages[ c ].unPointNumberz;
                    ucReceiverNumberResponding = asRecentMessages[ c ].ucSBReceiverNumber;
                    unSignalStrength = asRecentMessages[ c ].unSignalStrength;
                    //sprintf (sString, "Sending Locating data: Index=%u  P=%u  Pz=%u ", c, unPointNumber, unPointNumberz);
                    //SendDebugMessageX (sString, TRUE, FALSE);
                    //sprintf (sString," Rec=%u  Str=%u  Count=%u", ucReceiverNumberResponding, unSignalStrength, asRecentMessages[ c ].ucPacketCounter);
                    //SendDebugMessageX (sString, FALSE, TRUE);
                    // bh - v3.84
                    ucDevicex = SB_PENDANT;

#ifdef RSSI_FLOOR
                    // bh - v4.06 - get average rssi values and take the highest
                    if (RssiAveragingFlag)
                        unSignalStrength = CpuCalculateAverage (unPointNumber);
#endif
                    //RV 5-29-12   asRecentMessages[ c ].ucType
                    // bh - v3.85 - 01/05/06 --- unify code for alarm messages
                    //Check for bad data  10-12-2105
                    bBadData = FALSE;

                    if (unPointNumber > SB_MAX_POINTS) bBadData=TRUE;
//						if (unSignalStrength<=40) unSignalStrength=128;
                    //Only do low RSSI check on a single signal if limit is set correctly.
                    if ((FlashRamData.cRssiLevelforFalsePacket>70) && (FlashRamData.cRssiLevelforFalsePacket<128))
                    {
                        //unSignalStrength=70; /For testing
                        if ((asRecentMessages[ c ].ucPacketCounter <= 1) && (unSignalStrength < FlashRamData.cRssiLevelforFalsePacket))
                            bBadData=TRUE;
                    }
                    ///Dont do flase alarm block on the new 7000 PHB and AAHB
                    if ((asRecentMessages[ c ].ucDevice == SB_7000_PHB) || (asRecentMessages[ c ].ucDevice == SB_7000_AAHB) || (asRecentMessages[ c ].ucDevice == SB_7000_MHB))
                        bBadData = FALSE;

                    if (!bBadData)
                        ProcessMessage( unPointNumber, asRecentMessages[ c ].ucType, SBUS_MESSAGE, FALSE );
                    else
                    {
                        iFlaseAlarms++;
                        if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
                        {
                            sprintf (sString, "False alarm blocked due to bad data: Point=%u Count=%u", unPointNumber, asRecentMessages[ c ].ucPacketCounter);
                            SendDebugMessageX (sString, TRUE, TRUE);
                        }
                    }
                    asRecentMessages[ c ].ucPacketCounter=0;
                }
            }
        }
    }
    szPageMessage3 [0] = 0;
}





/*****************************************************************
	Subroutine:		WatchForHostErsPresence ()

	Description:
	Once a second, send an ENQ to the host.
	Once every three seconds, look at the ReceiveHostCharCount.
	If it is non-zero, the host is present.
	If it is zero, the host is not present.

	Receives:

	Returns:
		gucstandalone_mode

*****************************************************************/
void WatchForHostErsPresence ()
{
    const uint8 szPOL5[] = "\x05";

    if (gucAdminMode)
    {
        // no paging while Logged On
        return;
    }

    // v5.04 - 03/11/08 - Check to see if we are in Factory Mode
    if (CpacFactoryMode)
        return;

    if (!DisableEnq)
    {
        // xmit an ENQ to the host.
        SioSetMessage( UART_0, (uint8 *) szPOL5, strlen ((const char *) szPOL5 ));
    }

    if ( ++WatchHostTimer >= HostTimerLength )
    {
        // time out --- time to see if host is present or not
        WatchHostTimer = 0;
        HostTimerLength = MIN_HOST_TIMER;

        gucstandalone_mode = FALSE;
        if (ReceiveHostCharCount == 0)
        {
            // No traffic from the ERS Host - we are in standalone mode
            gucstandalone_mode = TRUE;
            if (iSmartCareIsDownTimer<=0)
            {
                if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
                    SendDebugMessage ("Message: SmartCare is down!");
                iSmartCareIsDownTimer=600;  //print every 10 minutes if still true
            }

//			PagerHeartBeatTimer = 0;
            // Send the message to the CWAMS saying the SmartCare interface is down
            SmartCareInterfaceStatus = 1;
            InformDownStreamQwams( DOWN );
        }
        else if( SmartCareInterfaceStatus == 1)
        {
            if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
                SendDebugMessage ("Message: SmartCare is up and running!");
            iSmartCareIsDownTimer=600;
            SmartCareInterfaceStatus = 0;
            InformDownStreamQwams( UP);
        }
        // restart counter
        ReceiveHostCharCount = 0;
    }
}


