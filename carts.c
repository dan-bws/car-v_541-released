/***********************************************************
	Module Name:	carts.c

	Author:			Bob Halliday, January, 2006
					bohalliday@aol.com    (781) 863-8245

	Description:
	This file is responsible for monitoring the state of 256 carts

	Subroutines:	CartsGetRegistrationData ()
					CartsGetRegistrationData ()
					CartsGetRegistrationData ()

	Revision History:

*************************************************************/
#define BODY_CARTS

#include "system.h"
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"
#include "admin.h"
#include "rtc.h"
#include "database.h"
#include "carts.h"
#include "sio.h"
#include "mprocess.h"
#include "psi2000.h"
#include "utils.h"
#include "tec.h"
#include "sbus.h"
#include "lapage.h"

#include <string.h>
#include <stdio.h>
#include "uart.h"
/***********************************************************
	Subroutine:	CartsInitialize()

	Description:
		This routine will set up the Carts Registration Array


	Inputs:

	Outputs:

	Locals:

*************************************************************/
void CartsInitialize ()
{
    // Set Carts values
    CartsBitMask [CART_SLOW] = CARTS_STARTING_BIT_MASK;
    CartsBitMask [CART_FAST] = CARTS_STARTING_BIT_MASK;
    CartsNumberOfSlowCarts   = SB_MAX_NEW_DEVICES-1;

    // CART 0 is never polled.  Start with CART 1 on the Fast Polling list.
    // set up polling masks
    BitMask = CARTS_STARTING_BIT_MASK;
    CartsMask = CARTS_STARTING_BIT_MASK;
    // Start out by polling for Superbus 2000 CARTs
    ProtocolFlag = 1;
}


/***********************************************************
	Subroutine:	CartsExecutive()

	Description:
		This routine is the nerve center for controlling the sequential
		polling of all carts in the system.
		At Power Up:
		 1) Poll all 250 CARTs using the New Superbus 2000 Protocol
		 2) Poll all 250 CARTs using the old superbus protocol
		 3) Use the standard FAST and SLOW polling lists to determine
			the next cart to be polled

	Inputs:
		CartsState

	Outputs:
		CartsState
		CartLastPolled

	Locals:

*************************************************************/
unsigned char CartsExecutive ()
{
    uint32 i;
    uint8 * ptr;

    // Which State are we in
    switch (CartsState)
    {
    // At power up, first poll 250 CARTS using the new Superbus 2000 protocol
    case CARTS_POLL_250_SUPERBUS_2000:
        CartsMask <<= 1;
        if (!CartsMask)
        {
            // Mask Pointer has overflowed.  Update the Byte pointer and reset the mask pointer
            CartsPtr++;
            CartsMask = 1;
        }
        CartLastPolled = CartsConvertToNumber (CartsPtr, CartsMask);
        if ((CartsPtr == CARTS_LAST_BYTE)
                && (CartsMask == CARTS_LAST_MASK))
        {
            CartsPtr = 0;
            CartsMask = CARTS_STARTING_BIT_MASK;
            // switch protocols
            ProtocolFlag ^= 0x01;
            // if protocol flag goes to zero, stay in this state
            CartsState = ProtocolFlag;
            if (CartsState)
            {
                // bh - v5.16 - watch for a bad Registration List from EEprom
                ptr = (uint8 *) &FlashRamData.CartsRegistrationList [2];
                if ((ptr[0]&ptr[1]&ptr[2]&ptr[3]&ptr[4]) == 0xFF)
                    AdminMessage( (char *) "\r\nBAD DATA\r\n" );
                else
                {
                    // at the end of learn mode, add the Registration List into the Polling List
                    for ( i=0;  i < CARTS_REG_BINARY_LENGTH;  i++ )
                    {
                        CartsPollingList [i] |= FlashRamData.CartsRegistrationList [i];
                    }
                }
                // Update the Fast and Slow Counters
                CartsUpdateCartsCount ();
            }
        }
        // bh - 01/21/07 - v4.04 - make this the last poll when in factory mode
        if (InFactoryMode)
        {
            Factory2 = TRUE;
            ProtocolFlag = 0;		// v4.11 - force old protocol for tester compatibility
            return 1;				// v4.11 - tester is looking for device 0x01
        }
        return CartLastPolled;


    case CARTS_POLL_USE_FAST_AND_SLOW_POLLING_LISTS:
        CartsPollNextCart ();
        if (CartLastPolled==0xFF)
        {
            sprintf (sString, "Ooops Polling  is bad!");
            SendDebugMessageX (sString, TRUE, FALSE);

        }
        return CartLastPolled;
    }
    return 0;
}



/***********************************************************
	Subroutine:	CartsPollNextCart (  )

	Description:
		This subroutine will find the next active CART and poll it.

	Inputs:
		BytePtr
		BitMask

	Outputs:
		BytePtr
		BitMask
		CartLastPolled

	Locals:

*************************************************************/
unsigned char CartsPollNextCart ()
{
    unsigned char x=0;
    PetWatchdog ();
    if ((CartsFastListTraversed && CartsNumberOfSlowCarts && !RunDiagnosticPollFlag && !InFactoryMode)
            || ((!CartsNumberOfFastCarts && !RunDiagnosticPollFlag)))
    {
        // Poll the next CART on the Slow Polling List
        CartsFastListTraversed--;
        // bh - v4.04 - second time through, take the same Poll ID as the previous value

        if (CartsFastListTraversed & 0x01)
        {
            do
            {
                CartsUpdatePointers (CART_SLOW);
                if (++x > 252)
                    break;
            }
            while (CartsPollingList [BytePtr] & BitMask);
        }

        // The protocol flag must alternate between Old and New protocols
        ProtocolFlag = CartsFastListTraversed & 0x01;
//			ProtocolFlag = !ProtocolFlag;  //RV 1-8-2016   line above did not toggle the protocol flag
    }


    // Find the next active CART and poll it.
    else if (CartsNumberOfFastCarts || RunDiagnosticPollFlag || InFactoryMode)
    {
        // There is at least one CART on the fast polling list.
        do
        {
            CartsUpdatePointers (CART_FAST);
        }
        while (!(CartsPollingList [BytePtr] & BitMask));

        // bh - v4.04 - set protocol flag = nonzero: SB2000, or zero: old superbus
        ProtocolFlag = CartsProtocolList [BytePtr] & BitMask;
    }

    CartLastPolled = CartsConvertToNumber (BytePtr, BitMask);
    if (ProtocolFlag)
    {
        // Update the reprogram flag for the next device to be polled
        CartsReprogramFlag = CartsReprogramList [BytePtr] & BitMask;
    }
    return (CartLastPolled);
}


/***********************************************************
	Subroutine:	CartsUpdatePointers (  )

	Description:
		This subroutine will find the next active CART and poll it.

	Inputs:
		index
		BytePtr
		BitMask

	Outputs:
		BytePtr
		BitMask
		CartsFastListTraversed

	Locals:

*************************************************************/
void CartsUpdatePointers (unsigned char index)
{
    // Get the Byte Pointer and Bit Mask
    BytePtr = CartsBytePtr [index];
    BitMask = CartsBitMask [index];

    if ((BytePtr >= CARTS_LAST_BYTE) && (BitMask == CARTS_LAST_MASK))
    {
        // This is the end of the polling list.  Restart to the front of the list.
        // bh - v4.04 - must issue 2 polls back to back from the slow polling list.
        if ( index == CART_FAST )
            CartsFastListTraversed = 2;
        BytePtr = 0;
        BitMask = CARTS_STARTING_BIT_MASK;
        if (RunDiagnosticPollFlag)
        {
            // We are in Diagnostic Polling Mode - terminate the mode now.
            CartsTerminateDiagnosticPoll ();
        }
    }

    // Shift the mask pointer up by one position.
    BitMask <<= 1;
    if (!BitMask)
    {
        // Mask Pointer has overflowed.  Update the Byte pointer and reset the mask pointer
        BytePtr++;
        BitMask = CARTS_STARTING_BIT_MASK;
        // Fast look ahead - any cart activated in the next 8 carts?
        PetWatchdog ();
        while (!CartsPollingList [BytePtr] && index)
        {
            // None of the next 8 carts are registered
            BytePtr++;
            // Check for the end of the array
            if (BytePtr >= CARTS_LAST_BYTE)
            {
                // Restart to the front of the registration array
                BytePtr = 0;
                CartsFastListTraversed = 2;
                if (RunDiagnosticPollFlag)
                {
                    // We are in Diagnostic Polling Mode - terminate the mode now.
                    CartsTerminateDiagnosticPoll ();
                }
            }
        }
    }

    // Save the new values
    CartsBytePtr [index] = BytePtr;
    CartsBitMask [index] = BitMask;
}




/***********************************************************
	Subroutine:	CartsConvertToNumber (  )

	Description:
		This subroutine will convert BytePtr and BitMask to a value between 1-255

	Inputs:
		byteptr
		bitmask

	Outputs:
		value

	Locals:

*************************************************************/
unsigned char CartsConvertToNumber (unsigned char byteptr, unsigned char bitmask)
{
    uint8 value;
    uint32 mask;

    mask = (uint32) (bitmask & 0x000000FF);

    // multiply byte pointer by 8
    value = byteptr << 3;

    // now add in the bit mask
    for ( mask >>= 1;  mask;  mask >>= 1 )
    {
        value++;
    }

    return value;
}


/***********************************************************
	Subroutine:	CartsConvertToPointerMask (  )

	Description:
		This subroutine will convert a value between 1-255 to BytePtr and BitMask

	Inputs:
		value - between 1 - 255

	Outputs:
		BytePtr - 0 - 31
		BitMask - 1,2,4,8,0x10, 0x20,0x40, or 0x80

	Locals:

*************************************************************/
void CartsConvertToPointerMask (unsigned char value)
{
    unsigned char temp;

    // divide value by 8 to get BytePtr
    BytePtr = value >> 3;

    // take the lower 3 bits of value for the MOD value
    temp = value & 0x07;

    // now shift the bitmask for the final result
    BitMask = 0x01;
    BitMask <<= temp;
}

/***********************************************************
	Subroutine:	CartsCheckActive (  )

	Description:
		This subroutine will convert a value between 1-255 to BytePtr and BitMask

	Inputs:
		value - between 1 - 249

	Outputs:
		non-zero	- Cart is active
		zero		- Cart is inactive

	Locals:
		byteptr - 0 - 31
		bitmask - 1,2,4,8,0x10, 0x20,0x40, or 0x80

*************************************************************/
uint8 CartsCheckActive (uint8 value)
{
    uint8 temp, byteptr, bitmask;

    // divide value by 8 to get BytePtr
    byteptr = value >> 3;

    // take the lower 3 bits of value for the MOD value
    temp = value & 0x07;

    // now shift the bitmask for the final result
    bitmask = 0x01;
    bitmask <<= temp;

    // return the state of this receiver
    return ( CartsPollingList [byteptr] & bitmask );
}









/***********************************************************
	Subroutine:	CartsSetLastAnswered (  )

	Description:
		This subroutine will set the value of the last CART that answered its poll

	Inputs:

	Outputs:

	Locals:

*************************************************************/
void CartsSetLastAnswered (unsigned char last_answer)
{
    CartLastAnswered = last_answer;
}

/***********************************************************
	Subroutine:	CartsSetReprogramFlag (  )

	Description:
		The responding Superbus 2000 receiver said that it just came out of
		reset.  Therefore, on the next poll of this device, we must issue
		a Configuration Command.  This routine sets the flag, which will
		be recognized next time around the polling loop and will trigger the
		transmission of a Configuration command.

	Inputs:

	Outputs:

	Locals:

*************************************************************/
void CartsSetReprogramFlag ()
{
    // Set the Reprogram Flag for this device
    CartsReprogramList [BytePtr] |= BitMask;
}


/***********************************************************
	Subroutine:	CartsClearReprogramFlag (  )

	Description:
		The responding Superbus 2000 receiver responded with an ACK.
		This means it has been properly reprogrammed.
		Therefore, its reprogramming flag can be cleared.

	Inputs:

	Outputs:

	Locals:

*************************************************************/
void CartsClearReprogramFlag ()
{
    // Set the Reprogram Flag for this device
    CartsReprogramList [BytePtr] &= ~BitMask;
}


/***********************************************************
	Subroutine:	CartsRemoveFromFastPollingList (  )

	Description:
		This subroutine will move a CART to the slow polling list if it did not respond
		Rules for a CART that does not respond:
		1) If a CART is on the Fast Polling list, move it to the Missing List, and issue the
		   error message
		2) If a CART is on the Slow Polling list and the Missing List, issue the error message
		3) If a CART is on the Slow Polling list, but not the Missing List,
			but is Registered, issue the error message.

	Inputs:
		value - between 1 - 255

	Outputs:
		BytePtr
		BitMask

	Locals:

*************************************************************/
void CartsRemoveFromFastPollingList (void)
{
    // Check to see if there was a response from the CART last polled
    if (CartLastPolled != CartLastAnswered)
    {
        // There was no response
        // Convert Poll address to BytePtr, BitMask
        CartsConvertToPointerMask (CartLastPolled);
        iReceiverTimeouts[CartLastPolled]++;

        if (RunDiagnosticPollFlag)
        {
            // In Diagnostic Polling Mode, Set the Cart Missing
            CartsPollingList [BytePtr] &= ~BitMask;
            // v5.13 - override missing condition if receiver is reporting long-term
            CartsPollingList [BytePtr] |= (CartsCopyRegistrationList [BytePtr] & BitMask);
            CartsPollingList [BytePtr] &= ~(CartsMissingList [BytePtr]);
        }

        // There was no response
        else if( auiSBDeviceRetryCounter[ CartLastPolled ] == 0 )
        {
            // update the Alarm Counter
            auiSBDeviceAlarmCounter[ CartLastPolled ]++;
            // restart the Retry Counter
            auiSBDeviceRetryCounter[ CartLastPolled ] = SB_DEVICE_RETRY_DELAY;

            // v5.17 - clear bit 0 in the status register to say that the receiver is no longer responding
            NRS[CartLastPolled].status &= ~SBUS_RECEIVER_PRESENT;
            cReceiverType[CartLastPolled] = 0xFF;

            // Carts that are already on the slow poll list, but are not missing and are not registered should be instantly dismissed
            if (!(CartsPollingList [BytePtr] & BitMask))
            {
                // Cart is already on the slow polling list
                if ((!(CartsMissingList [BytePtr] & BitMask))
                        && (!(FlashRamData.CartsRegistrationList [BytePtr] & BitMask)))
                {
                    // Cart is not missing and not registered
                    auiSBDeviceRetryCounter[ CartLastPolled ] = SB_DEVICE_MAX_RETRYS;
                }
                return;
            }

            // If the CART was previously already missing, then no need to bump counters
            if (!(CartsMissingList [BytePtr] & BitMask))
            {
                // CARTs that are registered should never be removed from the fast polling list,
                //  even if they are not responding
                if (!(FlashRamData.CartsRegistrationList [BytePtr] & BitMask))
                {
                    // Clear the bit in the fast polling list, causing the CART to be moved to the Slow polling list
                    CartsPollingList [BytePtr] &= ~BitMask;

                    // Decrement the Fast counter
                    if (CartsNumberOfFastCarts)
                        CartsNumberOfFastCarts--;

                    // Increment the Number of Slow Carts
                    if (CartsNumberOfSlowCarts < (SB_MAX_NEW_DEVICES-1))
                        CartsNumberOfSlowCarts++;
                }
                else //RV 12/13/2104  I think this is where we end up if a CART that is registered stops responding?
                {
                    //iCartTimeouts++;
                }

                // Set the Cart Missing
                CartsMissingList [BytePtr] |= BitMask;
            }

            // Send a CART offline messsage to the HOST:
            if (FlashRamData.CartsRegistrationList [BytePtr] & BitMask)
            {
                // v4.04 - don't print the error in the middle of a points download
                if (!gucAdminTrapFlag)
                {
                    ProcessMessage ( CartLastPolled, DEVICE_NOT_RESPONDING, DEVICE_MESSAGE, FALSE );
                }
            }
            // v4.04 - restore lost 1-hour delay for registered CARTs
            if ( auiSBDeviceAlarmCounter[ CartLastPolled ] >= 3 )
            {
                auiSBDeviceRetryCounter[ CartLastPolled ] =
                    ( SB_DEVICE_RETRY_DELAY * 12 ); 	// 3.87 - one hour
            }
        }
    }
    else
    {
        // Clear Cart last answered
        CartLastAnswered = 0;
    }
}


/***********************************************************
	Subroutine:	CartsAddToFastPollingList (  )

	Description:
		This subroutine will add a CART to the FAST polling list because it responded

	Inputs:
		value - between 1 - 255

	Outputs:
		BytePtr
		BitMask
		CartsPollingList []

	Locals:

*************************************************************/
void CartsAddToFastPollingList (unsigned char value)
{
    // Restart the Alarm Counter
    auiSBDeviceAlarmCounter[ value ] = 0;
    // restart the Retry Counter
    auiSBDeviceRetryCounter[ value ] = SB_DEVICE_RETRYS;
    // Convert Poll address to BitPtr, BitMask
    CartsConvertToPointerMask (value);
    // v5.00 - restart the monitor timer
    SuperbusMonitorTimer = SB_SUPERBUS_TIMEOUT;


    // Check to see if Cart was previously missing
    if (CartsMissingList [BytePtr] & BitMask)
    {
        // Cart was previously missing --- clear it from the Missing List
        CartsMissingList [BytePtr] &= ~BitMask;
        // Send a CART online messsage to the HOST:
        if (FlashRamData.CartsRegistrationList [BytePtr] & BitMask)
        {
            // v4.04 - don't print the error in the middle of a points download
            if (!gucAdminTrapFlag)
                ProcessMessage ( CartLastPolled, DEVICE_BACK_RESPONDING, DEVICE_MESSAGE, FALSE );
        }
    }

    // Set the bit in the fast polling list
    if (!(CartsPollingList [BytePtr] & BitMask))
    {
        // Cart was on the Slow polling list - move it over to the Fast List
        CartsPollingList [BytePtr] |= BitMask;

        // Increment the Fast Counter
        if (CartsNumberOfFastCarts < (SB_MAX_NEW_DEVICES-1))
            CartsNumberOfFastCarts++;

        // Decrement the Number of Slow Carts
        if (CartsNumberOfSlowCarts)
            CartsNumberOfSlowCarts--;

        // bh - v4.04 -- Update the protocol setting for this cart
        if (ProtocolFlag)
            CartsProtocolList [BytePtr] |= BitMask;
        else
            CartsProtocolList [BytePtr] &= ~BitMask;
    }
}






/***********************************************************
	Subroutine:	CartsSetDeviceData (  )

	Description:
		This subroutine will add a CART to or subtract a CART from the Registration List

	Inputs:
		device_number - between 1 - 255
		device_data  - zero or non-zero

	Outputs:
		FlashRamData.CartsRegistrationList []

	Locals:

*************************************************************/
void CartsSetDeviceData (unsigned char device_number, unsigned char device_data)
{
    // get the BytePtr and BitMask values
    CartsConvertToPointerMask (device_number);
    if( device_data == SB_DEVICE_INACTIVE )
    {
        /* Test if device currently active	*/
        // the request is to disable the CART
        FlashRamData.CartsRegistrationList [BytePtr] &= ~BitMask;
    }
    else								/* Activating device 					*/
    {
        // the request is to enable the CART
        FlashRamData.CartsRegistrationList [BytePtr] |= BitMask;
    }
    // set flag to write the Registration List to nonvolatile memory
    FlashModified = TRUE;
}

/***********************************************************
	Subroutine:	CartsGetDeviceData (  )

	Description:
		This subroutine will return a CART status as either = 1 (registered) or 0 (unregistered)

	Inputs:
		device_number - between 1 - 255

	Outputs:
		returns = 1 - registered
				  0 - unregistered

	Locals:

*************************************************************/
unsigned char CartsGetDeviceData (unsigned char device_number)
{
    // get the BytePtr and BitMask values
    CartsConvertToPointerMask (device_number);
    if (FlashRamData.CartsRegistrationList [BytePtr] & BitMask)
    {
        /* Test if device currently active	*/
        // the cart is registered
        return TRUE;
    }
    else								/* Activating device 					*/
    {
        // the cart is not registerd
        return FALSE;
    }
}




/***********************************************************
	Subroutine:	CartsWriteRegistration (  )

	Description:
		This subroutine is called once a minute.
		It will check to see if it is time to write the Registration List to EEprom.

	Inputs:
		FlashModified

	Outputs:
		CartsRegistrationList []

	Locals:

*************************************************************/
/****************
void CartsWriteRegistration ()
{
	if (FlashModified)
		{
		// time to write the Registration List to Eeprom
		FlashModified = 0;
		}
}
**********************/




/***********************************************************
	Subroutine:	CartsClearRegistration (  )

	Description:
		This subroutine will clear the registration array to all zeroes

	Inputs:

	Outputs:
		FlashRamData.CartsRegistrationList []

	Locals:
		i

*************************************************************/
void CartsClearRegistration ()
{
    unsigned char i;
    // Get the registration list from nonvolatile memory
    for ( i = 0;  i < REGISTRATION_SIZE; i++ )
    {
        FlashRamData.CartsRegistrationList [i] = 0;
    }
}





/***********************************************************
	Subroutine:	CartsSetRegistrationData()

	Description:
		64 bytes of Carts Registration data will be transmitted to the host
		This routine will convert the 32 Binary bytes to 64 Ascii bytes

	Inputs:
		pMessage

	Outputs:
		Registration []

	Locals:

*************************************************************/
void CartsSetRegistrationData (unsigned char j)
{
    unsigned char i;

    for ( i=0;  i < CARTS_REG_BINARY_LENGTH;  i++ )
    {
        // Convert the next Registration Byte to 2 Ascii Bytes
        CartsTemp[ j++ ] = BinHexAscii( CartsPollingList [i] >> 4 );
        CartsTemp[ j++ ] = BinHexAscii( CartsPollingList [i] );
    }

    // Append a NULL terminator to the end of the string
    CartsTemp[ j ] = 0;
    AdminMessage( (char *) CartsTemp );
}



/***********************************************************
	Subroutine:	CartsInitiateDiagnosticPoll()

	Description:
		This routine will start Diagnostic Polling Mode,
		which involves the one-time polling of each of 249 possible CARTs.

	Inputs:
		FlashRamData.CartsRegistrationList []

	Outputs:
		CartsRegistrationList []
		FlashRamData.CartsRegistrationList []

	Locals:
		i
*************************************************************/
void CartsInitiateDiagnosticPoll ()
{
    unsigned char i;

    // Enable Diagnostic Poll Mode
    RunDiagnosticPollFlag = TRUE;

    // Save the current registration list
    memcpy (CartsCopyRegistrationList, CartsPollingList, REGISTRATION_SIZE);

    // Enable the polling of all 249 CARTs
    for ( i = 0;  i < REGISTRATION_SIZE; i++ )
    {
        CartsPollingList [i] = 0xFF;
    }

    // put zeroes where there are no CARTs
    CartsPollingList [0] = 0xFE;
    CartsPollingList [REGISTRATION_SIZE-1] = 0x03;

    // Start the Diagnostic Polling with Receiver #1
    CartsBitMask [CART_FAST]  = CARTS_STARTING_BIT_MASK;
    CartsBytePtr [CART_FAST]  = 0x00;
}



/***********************************************************
	Subroutine:	CartsTerminateDiagnosticPoll()

	Description:
		This routine will terminate Diagnostic Polling Mode,
		which involved the one-time polling of each of 249 possible CARTs.

	Inputs:
		CartsCopyRegistrationList []

	Outputs:
		CartsRegistrationList []

	Locals:

*************************************************************/
void CartsTerminateDiagnosticPoll ()
{
    unsigned char i;

    // Disable Diagnostic Poll Mode
    RunDiagnosticPollFlag = FALSE;

    // Output the Diagnostic Poll results to the Host
    strcpy ((char *) CartsTemp, (const char *)"51006,");
    CartsSetRegistrationData (6);

    // Restore the current registration list
    memcpy (CartsPollingList, CartsCopyRegistrationList, REGISTRATION_SIZE);

    // If there are no active receivers, the code will hang on return.
    // Therefore, tuck in one active receiver to stop the code from hanging:
    CartsPollingList [0] |= 0x02;
		
	
		//send the llist of receivers to 
    if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
    {
        SendDebugMessageX ("Diag poll complete - ", TRUE,FALSE);
        for (i=0; i<REGISTRATION_SIZE; i++)
        {
            sprintf (sString,"%c",CartsTemp[i]);
            SendDebugMessageX (sString, FALSE,FALSE);
        }
        SendDebugMessageX ("", FALSE,TRUE);
    }}



/***********************************************************
	Subroutine:	CartsUpdateCartsCount ()

	Description:
		Scan the Polling List to determine how many fast and slow carts are in the system.

	Inputs:
		CartsPollingList [ ]

	Outputs:
		CartsNumberOfFastCarts
		CartsNumberOfSlowCarts

	Locals:

*************************************************************/
void CartsUpdateCartsCount ()
{
    uint8 byteptr, mask;

    // Start out with counters at zero
    CartsNumberOfFastCarts = CartsNumberOfSlowCarts = 0;

    // Count up the number of fast and slow carts in the system.
    for ( byteptr=0;  byteptr < CARTS_REG_BINARY_LENGTH;  byteptr++ )
    {
        for ( mask = CARTS_STARTING_BIT_MASK;  mask;  mask <<= 1 )
        {
            if ( CartsPollingList [byteptr] & mask )
            {
                // This Cart is active - increment the Fast Carts Counter
                CartsNumberOfFastCarts++;
            }
            else
            {
                // This Cart is NOT active - increment the Slow Carts Counter
                // v500 - avoid flipping over to zero again!
                if (CartsNumberOfSlowCarts != 255)
                    CartsNumberOfSlowCarts++;
            }
        }
    }

    // Adjust Slow Counter down because the last six in the list do not exist and are not valid
    if (CartsNumberOfSlowCarts >= 6)
        CartsNumberOfSlowCarts -= 6;
}



/***********************************************************
	Subroutine:	CartsGetRegistrationData()

	Description:
		64 bytes of Carts Registration data has been received from the Host.
		This routine will convert the 64 Ascii bytes to 32 Binary bytes
		and store the result in the Registration [] array.

	Inputs:
		pMessage

	Outputs:
		Registration []

	Locals:

*************************************************************/
void CartsGetRegistrationData (unsigned char  * pMessage)
{
    unsigned char i;
    /*
    	  //Print the CART registration data
        sprintf (sString, "CART data = ");
        SendDebugMessageX (sString, TRUE, FALSE);
        for ( i=0;  i < CARTS_REG_ASCII_LENGTH;  i++ )
        {
            sprintf (sString, "%c", pMessage [i]);
            SendDebugMessageX (sString, FALSE, FALSE);
        }
        SendDebugMessageX ("", FALSE, TRUE);
    */

    // Transfer the received masks to the registration list
    for ( i=0;  i < CARTS_REG_BINARY_LENGTH;  i++ )
    {
        FlashRamData.CartsRegistrationList [i] = HexAsciiBin( &pMessage [i*2] );

        // If we are not in Learn mode, copy the Registration into the Polling List
        if (CartsState)
            CartsPollingList [i] |= FlashRamData.CartsRegistrationList [i];

    }

    // Update the Fast and Slow Counters
    if (CartsState)
    {
        CartsUpdateCartsCount ();
        // Write Number of Devices to EEprom
        SBSetNumberOfDevices ( CartsNumberOfFastCarts );
    }


    // Restart the offline debounce counters
    for ( i = 0; i < SB_MAX_NEW_DEVICES; i++ )
    {
        auiSBDeviceRetryCounter [i] = SB_DEVICE_RETRYS;
    }

// 	if (FlashRamData.cDebugMessageMode>0)//0=off,1=SmartCare,2=Alarms,3=all
//		SendDebugMessageX ("Received CART registration list from SmartCare", TRUE, TRUE);

    // set flag to write the Registration List to nonvolatile memory
    FlashModified = TRUE;
}




/* ======== END OF FILE ======== */

