/***************************************************************************
	File	:	cpac.h
	Title	:	CPAC functions Header File
	Author	:	Bob Halliday
					bohalliday@aol.com  
	Created	:	September, 2007

	Description:
		CPAC Definitions for the 2378 cpac.c source file

	Contents:

	Revision History:


***************************************************************************/
#ifndef HEADER_CPAC
#define HEADER_CPAC

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_CPAC
#define SCOPE
#else
#define SCOPE	extern
#endif



typedef unsigned char		    uint8;  /*  8 bits */
typedef unsigned short 		    uint16; /* 16 bits */
typedef unsigned long int	    uint32; /* 32 bits */

typedef signed char			    int8;   /*  8 bits */
typedef signed short 		    int16;  /* 16 bits */
typedef signed long int		    int32;  /* 32 bits */

typedef volatile uint8			vuint8;  /*  8 bits */
typedef volatile uint16			vuint16; /* 16 bits */
typedef volatile uint32			vuint32; /* 32 bits */

/*******************************
*   Section 1:  Equate Definitions:
*********************************/

//#define ONE_TENTH_OF_A_SECOND					(uint8)(100/TIME_BASE)
#define CPAC_POWER_ACTIVE						(uint8)(250/TIME_BASE)
#define CPAC_LED1_OFF							(uint8)(50/TIME_BASE)
#define CPAC_STATUS_SIZE						9
#define CPAC_SERIAL_NUMBER_SIZE					3
#define CPAC_RA_TAG_NUMBER_SIZE					9
// 
#define CPAC_QUEUE_SIZE							500
#define CPAC_CAPABILITY_CODE					0x01

#define CPAC_TITLE_SIZE							15
#define CPAC_TITLE_NUM_RECORDS					48
#define CPAC_TITLE_DATABASE_SIZE				(CPAC_TITLE_SIZE * CPAC_TITLE_NUM_RECORDS)
#define CPAC_TITLE_RECORD_SIZE					16
#define CPAC_TITLE_RECORD_DELETED				0xFE
#define CPAC_START_WRITE						3

#define CPAC_TITLE_SPACE						(uint8 *) 0x0007F000	


// State Machine states for Door Prop Executive
enum	
	{
	CPAC_DOOR_PROP_INACTIVE,
	CPAC_DOOR_PROP_ACTIVE
	};


// State Machine states for Door Open and MagLock Activated
enum	
	{
	CPAC_DOOR_MAGLOCK_OKAY,
	CPAC_DOOR_MAGLOCK_ERROR
	};

// Indeces into CPAC status array
enum	
	{
	CPAC_STATUS_BYTE,
	CPAC_STATUS_CONFIG_OPTIONS,
	CPAC_STATUS_RELAY_LOCK_TIME,
	CPAC_STATUS_ALARM_TIME,
	CPAC_STATUS_PAUSE_TIME,
	CPAC_STATUS_RECEIVER_SELECTION,
	CPAC_STATUS_DOOR_PROP_TIME,
	CPAC_STATUS_LOITER_TIME,
	CPAC_STATUS_AVAIL
	};


// Database for the CPAC Titles
typedef struct
{
	uint8 Receiver;
	uint8 Name [CPAC_TITLE_SIZE];
} TITLE_DATA;



/*******************************
*   Section 2:  RAM Variables:
*********************************/

// Private Variables:
#ifdef BODY_CPAC




#endif

__align(8)


// Public Variables:
SCOPE	    uint8			CpacOutQueue [CPAC_QUEUE_SIZE];
SCOPE	    uint8 *			CpacMarkTheSpot;
SCOPE	    uint8			CpacMessageSent;
SCOPE	    uint8			CpacData [30];
SCOPE		uint8			CpacWriteTitleDatabase;
SCOPE		uint8			CpacStatus [CPAC_STATUS_SIZE];

SCOPE		TITLE_DATA		CpacTitle [CPAC_TITLE_NUM_RECORDS];		// 3/4 K of memory

SCOPE	    uint8			CpacDoorPropState;
SCOPE	    uint8			CpacDoorLockState;
SCOPE	    uint8			CpacNoTagId;
SCOPE	    uint8			CpacAlarmType;
SCOPE	    uint8			CpacRfidString [10];


/*******************************
*   Section 3:  Function prototypes
*********************************/
uint8 	CpacGetAlertValue (void);
void 	CpacAddMessageToQueue (uint8 * );
uint8 * CpacSearchForEndOfQueue (uint8 *);
uint8 * CpacSearchForMessageInQueue (uint8 *, uint8);
void 	CpacDeleteMessageFromQueue (uint8 * );
void 	CpacTransmitCommand (uint8 * );
void 	CpacSaveNewTitle (uint8 * );
uint8 * CpacReadTitle (uint8);
uint8 * CpacConfigureFlashWrite (void);
void 	CpacReadTitleDatabase (void);
void 	CpacInitializeFlashDatabase (void);
uint8 	CpacBackupPaging (uint8 *);
uint8 	CpacAddTitle (uint8 *);
void 	CpacSetRfidNumber (void);




/*******************************
*   Section 4:  Macros
*********************************/



/********************************
*   Section 5:  ROM Tables
*********************************/

#ifdef BODY_CPAC


#endif



#undef SCOPE
#endif

/* ======== END OF FILE ======== */
