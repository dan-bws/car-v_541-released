/************************************************************************/
/*                                                                      */
/*              Serial Communications Defines                           */
/*                                                                      */
/*																		*/
/************************************************************************/

#ifdef ALLOCATE_SIO
	#define ALLOC_TYPE
	#undef  ALLOCATE_SIO
#else
	#define ALLOC_TYPE extern
#endif

#define SIO_WATCHDOG_COUNT				2000	 

#define TRANSMIT_DONE   1           /* Transmit Buffer Done             */
#define TRANSMIT_BUSY   2           /* Transmit Buffer BUSY             */

/************************************************************************/
/*                                                                      */
/*      SCC2692 UART Defines                                            */
/*                                                                      */
/************************************************************************/


/*  UART Control Defines   P5=38400  110700								  */
#define HIGH_SPEED_MODE				0x02
                                                             
#define TXRDY       (unsigned char)0x04         /* Transmitter Ready            */
#define RXRDY       (unsigned char)0x01         /* Receiver Ready               */

/************************************************************************/
/*                                                                      */
/*              Serial Communications Structures                        */
/*                                                                      */
/************************************************************************/


/************************************************************************/
/*                                                                      */
/*      Global Data                                                     */
/*                                                                      */
/************************************************************************/




ALLOC_TYPE unsigned char  ucIndex;


/************************************************************************/
/*                                                                      */
/*      Function Prototypes                                                             */
/*                                                                      */
/************************************************************************/


ALLOC_TYPE unsigned char SioGetByte( unsigned char ucPortNumber );
ALLOC_TYPE uint16 SioByteCount( unsigned char ucPortNumber );
ALLOC_TYPE unsigned char SioSetMessage( uint8, uint8 *, uint16 );
ALLOC_TYPE void SioStartTransmit(uint8, uint16);
ALLOC_TYPE void ServiceInterruptWatchdog (void);
ALLOC_TYPE void SendTimeToCawms( unsigned char *sData);
ALLOC_TYPE void SendFacilityCodeToCawms (void);



/************************************************************************/
/*                                                                      */
/*      Message Handler Structures                                                      */
/*                                                                      */
/************************************************************************/


/************************************************************************/
/*                                                                      */
/*      Global Data                                                                         */
/*                                                                      */
/************************************************************************/


#undef ALLOC_TYPE

/************************************************************************/
