/***********************************************************
	Module Name:	timers.c

	Author:			Bob Halliday, April, 2007
					bohalliday@aol.com

	Description:
	Software Timer creation and maintenance support is provided
	by this module.

	The system clock is 12.0000 Mhz
	The CPU clock is 72.0000 Mhz 		-- 09/26/07 -- now 48 Mhz
	The Timers Peripheral Clock is 18.0000 Mhz -- 09/26/07 -- now 12 Mhz

	Hardware Timers utilizied in this library

	NAME	FUNCTION
	Timer0	Time Base for 12.5ms timer.
			Numerous Software Timers will run off this Time Base.
	Timer1	Timers with a resolution of less than 5ms will be implemented

	Subroutines:	TimersInitialization()
					Timers_MsInterrupt ()
                    TimersRestartTimer ()
                    TimersTimerIsExpired ()

	Revision History:


*************************************************************/
#define BODY_TIMERS

#include <stdio.h>
#include <string.h>
#include "lpc23xx.h"
#include "main.h"
#include "timers.h"
#include "psi2000.h"

#include "inits.h"
#include "uart.h"


#ifdef SPECTRALINK
#include "spectralink.h"
#endif



/********************************************
* The following routines are for Timer0 support:
*********************************************/
/***********************************************************
	Subroutine:	TimersInitialize()

	Description:
    	Initialize Timer0 to interrupt the system once every 5 milliseconds.
		Timer1 will serve to count very short intervals

	Inputs:

	Outputs:
		T0MCR
		T0MR0
		T0PR
		T0TCR
		T1MCR
		T1PR

	Locals:

*************************************************************/
void TimersInitialize (void)
{
    // Timer0 provides the time base to the system.  Start Time Base at 5ms
    // reset timer0 on match and interrupt
    T0MCR = TIMER0_RESET_INT;
    // set match for 5ms
//	T0MR0 = TIMERS_12_5_MS;
    T0MR0 = TIMERS_5_MS;

    // no prescaler
    T0PR = 0;
    // Enable Timer 0
    T0TCR = TRUE;

    // Timer 1 initialization
    T1MCR = TIMER1_STOP_ON_MR0;
    // set match for 500us
//	T1MR0 = 9060;
    // set match for 700us
//	T1MR0 = 12727;			// 10909 = 600us   10727 = 590us
    // no prescaler
    T1PR = 0;
    // Enable Timer 1
//	T1TCR = TRUE;

}




/*******************************************************************************
	Function:	TimersRestartTimer()

	Description:
	This subroutine will reset a selected 5ms Timer to zero.

	Inputs:
		TimerIndex - Index into TimersArray [ ]

	Outputs:
		TimersArray [TimerIndex] = 0

	Locals:
********************************************************************************/
void TimersRestartTimer (uint32 TimerIndex)
{
    // Restart a software timer by setting it to zero
    TimersArray [TimerIndex] = 0;
}




/*******************************************************************************
	Function:	TimersTimerIsExpired()

	Description:
	This subroutine will check to see if a selected 5ms Timer has expired.

	Inputs:
		TimerIndex - Index into TimersArray [ ]
		TimerThreshold - Expiration value of the timer being indexed.

	Outputs:
		TRUE  - Timer is expired.
		FALSE - Timer is not expired.

	Locals:
********************************************************************************/
uint8 TimersTimerIsExpired (uint32 TimerIndex, uint32 TimerThreshold)
{
    // Return TRUE if Timer is expired
    return (TimersArray [TimerIndex] >= TimerThreshold);
}


//--------------------------------------------------------------------------


/********************************************
* The following routines are for Timer1 support:
*********************************************/
/***********************************************************
	Subroutine:	TimersInitShortTimer()

	Description:
    	Initialize Timer1 to interrupt after 1-10ms or less!

	Inputs:
		MatchTime

	Outputs:
		T1MR0
		T1TCR

	Locals:
*************************************************************/
void TimersInitShortTimer (uint32 MatchTime)
{
    // set match for Timer1
    T1MR0 = MatchTime;

    // Enable Timer 1
    T1TCR = TRUE;
}

/***********************************************************
	Subroutine:	TimersShortTimerExpired()

	Description:
    	Return TRUE if Timer1 has expired.

	Inputs:
		TimersDelayAfterProgramTime
		TMR1H

	Outputs:
		TRUE - Timer1 has expired
		FALSE- Timer1 is still running

	Locals:
*************************************************************/
uint8 TimersShortTimerExpired ()
{
    return (uint8) (T1TCR == 0);
}



//-----------------------------------------------------

/***********************************************************
	Subroutine:	TimersExecutive()

	Description:
    	This routine manages time across seconds, minutes, hours and days

	Inputs:
		SecondsTimer
		MinutesTimer
		HoursTimer
		DaysTimer

	Outputs:
		SecondsTimer
		MinutesTimer
		HoursTimer
		DaysTimer

	Locals:
*************************************************************/
/*************
void TimersExecutive ()
{
	// Update the one-second timer
	if (TimersTimerIsExpired (ONE_SECOND_TIMER, ONE_FULL_SECOND))
		{
		// One Second has expired
		TimersRestartTimer (ONE_SECOND_TIMER);

		// Update the Seconds Timers
		++Timer.Analog;
		++Timer.ModemReceiveTimeout;

		if (++Timer.Seconds >= ONE_FULL_MINUTE)
			{
			// One Minute has expired
			Timer.Seconds = 0;
			// Update the Minutes Timers
			++Timer.CountMinutes;
			++Timer.TemperatureMinutes;
			++Timer.ModemRetry;
			++Timer.BatteryMinutes;


			// Update the Minutes Timer
			if ( ++Timer.Minutes >= ONE_FULL_HOUR)
				{
				// One Hour has expired
				Timer.Minutes = 0;
				++Timer.CountHours;
				++Timer.AcPowerHours;

				// Update the Hours Timer
				if ( ++Timer.Hours >= ONE_FULL_DAY )
					{
					// One Day has passed
					Timer.Hours = 0;
					// Update the Days Timer
					Timer.CountDays++;
					}
				}
			}
		}
}
*****************************/

;
extern uint16 iLastSuperPointNumber;
extern uint16 iLastSuperPointNumber2;
extern uint16 iLastSuperPointNumberTime;
/***********************************************************
	Subroutine:	Timers_msInterrupt()

	Description:
    	This routine is called from the interrupts once every 5ms
		Timer0 triggers this interrupt.
		All software timers are incremented by one.

	Inputs:
		TimersArray []

	Outputs:
		TimersArray []

	Locals:
*************************************************************/

extern void SendDebugMessageX( char *pszAdminMessage, char bSendTimeStamp, char bSendCR );	

void Timers_msInterrupt ()
{
//	static uint8 toggle;

    // Timer0 has reached a match  - clear the interrupt flag
    T0IR = TIMER0_MR0_IR;


    // bh -- 12/01/05 --- restart the interrupt watchdog
    InterruptWatchdog = 0;
    // bh -- 12/09/05 --- increment the saved alarms timer
    SavedAlarmsTimer++;
    // increment the poll timer
    PollTimer++;
    // increment the one-second timer
    OneSecondTimer++;
    CounterOf5MsecTicks++;
		//This is the flase UTX alarm filter
		iLastSuperPointNumberTime++;
		if (iLastSuperPointNumberTime > 600)   //100 = .5 sec
		{
			iLastSuperPointNumber = 0;
			iLastSuperPointNumber2 = 0;
			iLastSuperPointNumberTime = 0;
                    //SendDebugMessageX ("Timer reset", TRUE, TRUE);
		}
    if (iMiscTimer<65534) iMiscTimer++;
    // increment the State of Health Timer
    StateOfHealthTimer++;
    PagerHeartBeatTimer++;
    // increment the LED Timer
    LedTimer++;
    // increment the Supervisor Timer
    SupervisorTimer++;
    // increment the Cap Code Timer
    CapCodeTimer++;
    // bh - v4.04 - increment the rs-485 resychronization timer
    Rs485ResyncTimer++;
    // bh - v5.00 - wait 25ms between each Supervisory Alarm
    SB_SupervisorWaitTimer++;
    // bh - v5.00 - wait 30ms after a FLASH erase
    FlashEraseTimer++;

    //This timer is for anything. I think this is a 5msec interrupt?
    cGeneric_5msec_Timer--;

#ifdef SPECTRALINK
    // bh -- v4.06 -- update the Spectralink Timers
    OAI_WaitForAckTimer++;
    OAI_HeartbeatTimer++;
#endif
}




//Set the green LEDs on port 1
/* Port 1:
   Bit	Direction	Function
   31	Output		Test Output
   30	Output		Test Output
   29	Output		Test Output
   28	Output		Test Output
   27	Output		Test Output
   26	Output		Test Output
   25	Output		Test Output
   24	Output		Test Output
   23	Input
   */
void SetLedBankPort1(unsigned char cLeds)
{
    unsigned int i;

    i =	FIO1SET;
    i = i & 0x00FFFFFF;
    i = i |	 (cLeds	<<24);
    FIO1CLR = 0xFF000000;
    FIO1SET = i;
}




//Set the red LEDs on port 2
/* Port 2:
   Bit	Direction	Function
   8	Input
   7	Output		Test Output
   6	Output		Test Output
   5	Output		Test Output
   4	Output		Test Output
   3	Output		Test Output
   2	Output		Test Output
   1	Output		Test Output
   0	Output		Test Output
*/
void SetLedBankPort2(unsigned char cLeds)
{
    unsigned int i;

    i =	FIO2SET;
    i = i & 0xFFFFFF00;
    i = i |	 cLeds;
    FIO2CLR = 0xFF;
    FIO2SET = i;
}


uint32	IntSave;
void PetWatchdog ()
{
    // Disable Interrupts
    IntSave = VICIntEnable;
    VICIntEnClr = 0xFFFFFFFF;
    VICIntSelect = 0;

    WDFEED = INITS_WD_1;
    WDFEED = INITS_WD_2;

    // re-enable interrupts
    VICIntSelect = IntSave;
    VICIntEnable = IntSave;
}



/* ======== END OF FILE ======== */
